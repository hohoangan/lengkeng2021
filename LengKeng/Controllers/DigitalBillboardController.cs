﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DigitalBillboardController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;

        public DigitalBillboardController(LengKengDbContext context, IMapper mapper, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _blobService = blobService;
        }

        #region GET- CREATE - UPDATE
        //GET api/digitalbillboard
        /// <summary>
        /// Danh sách Digital
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="locationId"></param>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<IEnumerable<DigitalBillboard>> GetAllDigitalBillboard(int PageNumber, int PageSize,
                                                     int locationId, int companyId, string name)
        {
            try
            {
                var models = _context.DigitalBillboards.Where(b => b.CompanyId == companyId).Where(x => x.LocationId == locationId);
                if (!string.IsNullOrEmpty(name))
                    models = models.Where(x => x.Name.Contains(name));

                var response = PagedList<DigitalBillboard>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/digitalbillboard/{id}
        /// <summary>
        /// Thông tin chi tiết 1 Digital
        /// </summary>
        /// <param name="Id">Digital Id</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetDigitalBillboard")]
        public ActionResult<DigitalBillboard> GetDigitalBillboard(int Id)
        {
            try
            {
                var model = _context.DigitalBillboards.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(x => x.Locations)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/digitalbillboard
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="digital"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult Create(DigitalBillboard digital)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.DigitalBillboards.Add(digital);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetDigitalBillboard), new { id = digital.Id }, digital);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/digitalbillboard/{id}
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult Update(int Id, DigitalBillboardUpdateDto model)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.DigitalBillboards.Find(Id);
                if (b == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "Digital Billboard"].Value);
                else
                {
                    var v = _context.DigitalBillboards.Where(x => x.Name == model.Name && x.Id != Id).FirstOrDefault();
                    if (v != null)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, model.Name].Value);
                    }
                }

                _mapper.Map(model, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/digitalbillboard/{id}
        /// <summary>
        /// Change Status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("{Id}/changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult ChangeStatus(int Id, int statusId)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.DigitalBillboards.Find(Id);
                if (b == null)
                    return NotFound();

                var status = _context.Statuses.Find(statusId);

                if (status.Name == Constant.ConstantStatus.Inactive)
                {
                    var booking = _context.DigitalBillboardBookings.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .Where(x => DateTime.Now <= x.ToDate).FirstOrDefault();
                    if (booking != null)
                        return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()]);
                }
                b.StatusId = statusId;

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region LOCATION
        //GET api/digitalbillboard/locations
        /// <summary>
        /// LOCATION | Danh sách tỉnh thành có Digital dùng khi booking
        /// </summary>
        /// <returns></returns>
        [HttpGet("locations")]
        public ActionResult GetCityForBooking()
        {
            try
            {
                var digital = _context.DigitalBillboards.Include(x => x.Locations).Select(x => x.Locations).Distinct();

                var models = from loc in _context.Locations.Where(x => x.Level == 1)
                             join bb in digital on loc.LocationId equals (bb.ParentId ?? bb.LocationId)
                             select loc;

                return Ok(models.Distinct());
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/digitalbillboard/locations/district
        /// <summary>
        /// LOCATION | Danh sách quận/huyện thuộc tỉnh
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [HttpGet("locations/district")]
        public ActionResult GetDistrictForBooking(int locationId)
        {
            try
            {
                var billboard = _context.DigitalBillboards.Include(x => x.Locations).Where(x => x.Locations.ParentId == locationId).Select(x => x.Locations).Distinct();

                var models = from loc in _context.Locations.Where(x => x.Level == 2)
                             join bb in billboard on loc.LocationId equals bb.LocationId
                             select loc;

                return Ok(models.Distinct().OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region PICTURES
        //PUT api/digitalbillboard/pictures
        /// <summary>
        /// PICTURES - MOBILE | Thêm hình campaign
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost("pictures")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult AddPictures(List<DigitalBillboardPicture> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.DigitalBillboardPictures.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/digitalbillboard/pictures/{Id}
        /// <summary>
        /// PICTURES - MOBILE | Danh sách hình Digital trong campaign
        /// </summary>
        /// <param name="Id">Digital Id</param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("pictures/{Id}")]
        public ActionResult GetPicture(int Id, int campaignId)
        {
            try
            {
                var query = _context.DigitalBillboardPictures.Where(b => b.DigitalBillboardId == Id && b.CampaignId == campaignId).ToList();

                var list = _context.MediaPictures.Include(x => x.PictureTypes)
                                                  .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Billboard)
                                                  .AsNoTracking()
                                                  .AsEnumerable()
                                                  .Select(item => new
                                                  {
                                                      item.PictureTypes.Id,
                                                      item.PictureTypes.Name,
                                                      list = query.Where(x => x.PictureTypeId == item.PictureTypes.Id).OrderByDescending(b => b.LastUpdated)
                                                  });
                return Ok(list);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// PICTURES - MOBILE | Xóa hình trong campaign
        /// </summary>
        /// <param name="digitalPictureId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpDelete("Pictures/{digitalPictureId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult DelPicture(int digitalPictureId, int campaignId)
        {
            try
            {
                var pic = _context.DigitalBillboardPictures.Where(b => b.Id == digitalPictureId && b.CampaignId == campaignId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.DigitalBillboardPictures.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren clond
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// PICTURES - WEB | Thêm hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        [HttpPost("display")]
        public ActionResult AddDislay(List<DigitalBillboardDisplay> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.DigitalBillboardDisplays.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// PICTURES - WEB | Sửa hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        [HttpPut("display")]
        public ActionResult UpdateDislay(List<DigitalBillboardDisplayUpdateDto> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                list.ForEach(item =>
                {
                    var b = _context.DigitalBillboardDisplays.Find(item.Id);
                    _mapper.Map(item, b);
                    _context.SaveChanges();
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// PICTURES - WEB | xóa hình đại diện
        /// </summary>
        /// <param name="digitalDisplayId"></param>
        /// <returns></returns>
        [HttpDelete("display/{digitalDisplayId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelPictureDisplay(int digitalDisplayId)
        {
            try
            {
                var pic = _context.DigitalBillboardDisplays.Where(b => b.Id == digitalDisplayId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.DigitalBillboardDisplays.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region PRICES
        //GET api/digitalbillboard/price
        /// <summary>
        /// PRICES | Get ALL
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpGet("price")]
        public ActionResult<IEnumerable<DigitalBillboardPrice>> GetAllPrice(int PageNumber, int PageSize)
        {
            try
            {
                var models = _context.DigitalBillboardPrices;
                var response = PagedList<DigitalBillboardPrice>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/digitalbillboard/price/{Id}
        /// <summary>
        /// PRICES | Bảng giá từ ngày đến ngày
        /// </summary>
        /// <param name="Id">Digital Id</param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("price/{Id}")]
        public ActionResult<IEnumerable<DigitalBillboardPrice>> GetPrice(int Id, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = _context.DigitalBillboardPrices.Where(b => b.DigitalBillboardId == Id)
                    .Where(b => fromDate.Date <= b.ToDate.Date)
                    .Where(b => b.FromDate.Date <= toDate.Date)
                    .Include(b => b.DigitalBillboards).OrderBy(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/digitalbillboard/Price
        /// <summary>
        /// PRICES | Thêm giá
        /// </summary>
        /// <param name="digital"></param>
        /// <returns></returns>
        [HttpPost("Price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult AddPrice(DigitalBillboardPrice digital)
        {
            try
            {
                _context.Database.BeginTransaction();
                //check timeoverlap
                var price = _context.DigitalBillboardPrices.Where(x => x.DigitalBillboardId == digital.DigitalBillboardId
                                                            && x.FromDate <= digital.ToDate && digital.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                _context.DigitalBillboardPrices.Add(digital);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/digitalbillboard/Price/AddList
        /// <summary>
        /// PRICES | thêm danh sách giá
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("Price/AddList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult AddListShelterPrice(IEnumerable<DigitalBillboardPrice> lst)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.DigitalBillboardPrices.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/digitalbillboard/Price/{id}
        /// <summary>
        /// PRICES | sửa giá
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("Price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult UpdatePrices(int Id, DigitalBillboardPriceUpdateDto model)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.DigitalBillboardPrices.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //check timeoverlap
                var price = _context.DigitalBillboardPrices.Where(x => x.Id != Id
                                                            && x.DigitalBillboardId == model.DigitalBillboardId
                                                            && x.FromDate <= model.ToDate && model.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);

                _mapper.Map(model, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/digitalbillboard/copyPrice
        /// <summary>
        /// PRICES | copy giá (chưa hoàn chỉnh)
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="oldYear"></param>
        /// <param name="newYear"></param>
        /// <returns></returns>
        [HttpPost("copyPrice")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult CopyPrice(int? companyId, int oldYear, int newYear)
        {
            try
            {
                _context.Database.BeginTransaction();
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                var digital = from tx in _context.DigitalBillboardPrices.Where(x => x.FromDate.Year == oldYear) select tx;
                if (companyId != null)
                    digital = digital.Where(x => x.DigitalBillboards.CompanyId == companyId);

                var lst = from price in digital
                          select new DigitalBillboardPrice
                          {
                              Id = 0,
                              Price = price.Price,
                              FromDate = price.FromDate.AddYears(newYear - oldYear),
                              ToDate = price.ToDate.AddYears(newYear - oldYear),
                              UnitId = price.UnitId,
                              DigitalBillboardId = price.DigitalBillboardId,
                              CreatedUserId = userId,
                              UserId = userId
                          };
                if (lst == null)
                    return NotFound();
                _context.DigitalBillboardPrices.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/digitalbillboard/price/{Id}
        /// <summary>
        /// PRICES | xóa giá
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequirePartnerGroup)]
        public ActionResult DeletePrice(int Id)
        {
            try
            {
                _context.Database.BeginTransaction();
                var digitalBillboardPrice = _context.DigitalBillboardPrices.Find(Id);
                if (digitalBillboardPrice == null)
                    return NotFound();

                var booking = _context.DigitalBillboardBookings
                                                  .Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                  .Where(x => x.FromDate <= digitalBillboardPrice.ToDate && digitalBillboardPrice.FromDate <= x.ToDate)
                                                  .Where(x => x.DigitalBillboardId == digitalBillboardPrice.DigitalBillboardId)
                                                  .FirstOrDefault();
                if (booking != null)
                    return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()].ToString());
                else
                    _context.DigitalBillboardPrices.Remove(digitalBillboardPrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region DOWNTIME    
        //GET api/digitalbillboard/downtime/{Id}
        /// <summary>
        /// DOWNTIME | Thông tin downtime trong năm
        /// </summary>
        /// <param name="Id">DigitalBillboardId Id</param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("downtime/{Id}")]
        public ActionResult<IEnumerable<DigitalBillboardDownTime>> GetDowntime(int Id, int year)
        {
            try
            {
                var query = _context.DigitalBillboardDownTimes.Where(b => b.DigitalBillboardId == Id)
                    .Where(b => b.FromDate.Year == year)
                    .Include(b => b.DigitalBillboards).OrderByDescending(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/digitalbillboard/downtime/
        /// <summary>
        /// DOWNTIME | Tạo downtime
        /// </summary>
        /// <param name="downTime"></param>
        /// <returns></returns>
        [HttpPost("downtime")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult AddDowntime(List<DigitalBillboardDownTime> downTime)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<DigitalBillboard> lst = new List<DigitalBillboard>();
                for (int i = 0; i < downTime.Count; i++)
                {
                    //check timeoverlap
                    var check = _context.DigitalBillboardDownTimes.Where(x => x.DigitalBillboardId == downTime[i].DigitalBillboardId
                                                                && x.FromDate <= downTime[i].ToDate && downTime[i].FromDate <= x.ToDate
                                                            ).Include(x => x.DigitalBillboards).FirstOrDefault();
                    if (check != null)
                    {
                        lst.Add(check.DigitalBillboards);
                        //throw new NotImplementedException(string.Format("{0} {1}-{2}:{3}",
                        //            check.DigitalBillboards.Name, check.FromDate, check.ToDate, _localizer[Constant.ConstantMessage.TimeOverlap]));
                    }

                }
                if (lst.Count > 0)
                {
                    return NotFound(lst);
                }
                _context.DigitalBillboardDownTimes.AddRange(downTime);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(downTime),
                    UserId = userId
                });
            }
        }

        //PUT api/billboard/downtime/id
        /// <summary>
        /// DOWNTIME | sửa downtime
        /// </summary>
        /// <param name="Id">DigitalBillboardDownTimes Id</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult UpdateDowntime(int Id, DigitalBillboardDownTimeUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.DigitalBillboardDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// DOWNTIME | Xóa downtime
        /// </summary>
        /// <param name="Id">DigitalBillboardDownTimes Id</param>
        /// <returns></returns>
        [HttpDelete("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireDigitalAdminRole)]
        public ActionResult DelDowntime(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.DigitalBillboardDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                _context.Remove(b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion
    }
}
