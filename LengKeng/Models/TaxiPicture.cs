﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiPicture
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        public int CampaignId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
        [Required, Id]
        public int TaxiId { get; set; }
        [JsonIgnore]
        public Taxi Taxis { get; set; }
        [Required, Id]
        public int PictureTypeId { get; set; }
        [JsonIgnore]
        public PictureType PictureTypes { get; set; }
    }
}
