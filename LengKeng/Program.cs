using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace LengKeng
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            //run cronjob in Starup.cs
            //shutdown job when close webapp
            CronJob.ShutdownAllJob();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //webBuilder.UseKestrel(c => c.AddServerHeader = false);//xoa header khi response
                    webBuilder.UseStartup<Startup>();
                });
    }
}
