﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CountOn",
                table: "BillboardCameras",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "RTSPLink",
                table: "BillboardCameras",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountOn",
                table: "BillboardCameras");

            migrationBuilder.DropColumn(
                name: "RTSPLink",
                table: "BillboardCameras");
        }
    }
}
