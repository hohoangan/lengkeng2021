﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusShelterUpdateDto
    {
        [Required]
        public string Name { get; set; }
        public double GPSLocX { get; set; }
        public double GPSLocY { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
    }
}
