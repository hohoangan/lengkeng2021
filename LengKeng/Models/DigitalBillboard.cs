﻿using LengKeng.Ultils;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class DigitalBillboard 
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Orientation { get; set; }
        public string TVC { get; set; }//Type of TVC (sec)	30s, 15s
        public string Spot { get; set; }
        [Required, Id]
        public int BroadcastingTime { get; set; } //ex: 15 => 15h/day
        public string DurationCircle { get; set; }
        public string HighlightFeature { get; set; }
        public string Visibility { get; set; }
        public string TrafficDensity { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]//bỏ qua khi swagger gen api
        public Company Companies { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        public double GPSLocX { get; set; }
        public double GPSLocY { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserDigitalBillboard> UserDigitalBillboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardPicture> DigitalBillboardPictures { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardPrice> DigitalBillboardPrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardDownTime> DigitalBillboardDowTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardBooking> DigitalBillboardBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardDisplay> DigitalBillboardDisplays { get; set; }
    }
}
