﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class UserRole
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int UserId { get; set; }
        [JsonIgnore]
        public User Users { get; set; }
        [Required, Id]
        public int RoleId { get; set; }
        [JsonIgnore]
        public Role Roles { get; set; }
    }
}
