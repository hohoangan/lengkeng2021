﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BillboardDisplay
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int BillboardId { get; set; }
        [JsonIgnore]
        public Billboard Billboards { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string TypeName { get; set; } // main, detail, video
        public int Order { get; set; } // main = 0
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
    }
}
