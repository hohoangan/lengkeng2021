﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class MobileTaxiBookingRead
    {
        public int BookingId { get; set; }
        public double TaxiId { get; set; }
        public string PlateNumber { get; set; }
        public double OTC { get; set; }
        public Company Company { get; set; }
        public Location Location { get; set; }
        public TaxiType CarType { get; set; }
        public TaxiTypeOfArtwork ArtType { get; set; }
        public Status Status { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; }
        public bool IsTakePicture { get; set; }
    }

    public class MobileTaxiBooking
    {
        public IEnumerable<PictureType> PictureTypes { get; set; }
        public IEnumerable<MobileTaxiBookingRead> List { get; set; }
    }
}
