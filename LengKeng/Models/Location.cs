﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Location
    {
        [Key]
        public int LocationId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]// LevelCode: City, District, Ward, Address
        public int Level { get; set; }
        //int? = nullable || System.Nullable<int>
        public int? ParentId { get; set; }
        public int OrderBy { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelter> BusShelters { get; set; }
        [JsonIgnore]
        public virtual ICollection<Billboard> Billboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboard> DigitalBillboards { get; set; }
    }
}
