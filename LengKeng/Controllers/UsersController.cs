﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration.Conventions;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserRepo _reponsitory;
        private readonly IStringLocalizer<Message> _localizer;

        public UsersController(LengKengDbContext context, IMapper mapper, IUserRepo reponsitory, IStringLocalizer<Message> localizer)
        {
            _context = context;
            _mapper = mapper;
            _reponsitory = reponsitory;
            _localizer = localizer;
        }

        #region User management
        //GET api/users/company
        /// <summary>
        /// Danh sách công ty theo role
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        [HttpGet("company")]
        public ActionResult GetListCompany(int? roleId)
        {
            try
            {
                if (roleId != null)
                    return Ok(_context.Companies.Include(x => x.Roles).Where(x => x.RoleId == roleId).OrderBy(x => x.Name).ToList());
                else
                    return Ok(_context.Companies.Include(x => x.Roles).OrderBy(x => x.Name).ToList());
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }

        }

        //GET api/users/company/{companyId}/members
        /// <summary>
        /// Lấy danh sách account
        /// </summary>
        /// <param name="companyId">0: để tìm tất cả công ty</param>
        /// <param name="search">empty hoặc nhập tên/sdt/email cần tìm</param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        [HttpGet("company/{companyId}/members")]
        public ActionResult GetUserOfCompany(int? companyId,int PageNumber, int PageSize, string search = "")
        {
            try
            {
                var models = _context.Users.Include(x => x.Companies).Include(x => x.UserRoles.Roles).AsNoTracking().ToList()
                            .Select(x => { x.Password = null; return x; });
                if (companyId != 0)
                    models = models.Where(x => x.CompanyId == companyId).ToList();
                if (!string.IsNullOrEmpty(search))
                    models = models.Where(x => x.Username.ToLower().Contains(search.ToLower()) 
                                            || x.Email.ToLower().Contains(search.ToLower()) || 
                                               x.Phone.Contains(search)).ToList();

                models = models.OrderBy(x => x.Username);
                var response = PagedList<User>.ToPagedListIEnumerable(models, PageNumber, PageSize);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/users/company/{companyId}/roles
        /// <summary>
        /// Lấy danh sách các loại account của 1 công ty
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        [HttpGet("company/{companyId}/roles")]
        public ActionResult GetRolesOfCompany(int companyId)
        {
            try
            {
                #region Check Company
                var com = _context.Companies.Include(x=>x.Roles).Where(x=>x.CompanyId == companyId).FirstOrDefault();
                if (com == null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundX, "Company"]);
                }
                #endregion
                switch (com.Roles.RoleName)
                {
                    case Constant.ConstantRole.BusCompany:
                        return Ok( _context.Roles.Where(x => x.RoleName == Constant.ConstantRole.BusCompany 
                                                        || x.RoleName == Constant.ConstantRole.BusDriver));
                    case Constant.ConstantRole.TaxiCompany:
                        return Ok(_context.Roles.Where(x => x.RoleName == Constant.ConstantRole.TaxiCompany
                                                       || x.RoleName == Constant.ConstantRole.TaxiDriver));
                    case Constant.ConstantRole.BillboardCompany:
                        return Ok(_context.Roles.Where(x => x.RoleName == Constant.ConstantRole.BillboardCompany
                                                       || x.RoleName == Constant.ConstantRole.BillboardOperator));
                    case Constant.ConstantRole.ShelterCompany:
                        return Ok(_context.Roles.Where(x => x.RoleName == Constant.ConstantRole.ShelterCompany));

                    case Constant.ConstantRole.Customer:
                        return Ok(_context.Roles.Where(x => x.RoleName == Constant.ConstantRole.Customer));

                    case Constant.ConstantRole.InternalCompany:
                        return Ok(_context.Roles.Where(x => x.RoleName == Constant.ConstantRole.SystemAdmin
                                                         || x.RoleName == Constant.ConstantRole.SuperUser
                                                         || x.RoleName == Constant.ConstantRole.Coordinator));

                    default:
                        return Ok(null);
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }

        }

        //POST api/users
        /// <summary>
        /// Create account
        /// </summary>
        /// <param name="userCreateDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        public ActionResult CreateUser(UserCreateDto userCreateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            _context.Database.BeginTransaction();
            try
            {
                #region Check Company
                var com = _context.Companies.Find(userCreateDto.CompanyId);
                if (com == null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundX, "Company"]);
                }
                #endregion

                #region check username
                var us = _context.Users.Where(x=>x.Username == userCreateDto.Username || x.Email == userCreateDto.Email).FirstOrDefault();
                if (us != null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.DuplicateX, "UserName or Email"]);
                }
                #endregion

                #region Check Roles
                var role = _context.Roles.Find(userCreateDto.RoleId);
                if (role == null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundX, "Role"]);
                }
                #endregion

                var rand = MyUltil.RandomString(6);
                var newPassword = MyUltil.encodeM5(rand);
                userCreateDto.Password = newPassword;

                var userModel = _mapper.Map<UserCreateDto, User>(userCreateDto);
                _context.Users.Add(userModel);
                _context.SaveChanges();

                //sendmail
                bool kq = Gmail.SendMail(userModel, rand);
                if (kq == false)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }

                var isRole = _context.UserRoles.Add(new UserRole { UserId = userModel.Id, RoleId = userCreateDto.RoleId });
                _context.SaveChanges();

                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetUserInfo), new { userId = userModel.Id }, userModel);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(userCreateDto),
                    UserId = userId
                });
            }
        }

        //PUT api/users/{id}
        /// <summary>
        /// Update account
        /// </summary>
        /// <param name="Id">Users Id</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        public ActionResult Update(int Id, UserUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Users.Where(b => b.Id == Id).FirstOrDefault();
                if (b == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "User"].Value);
                else
                {
                    var email = _context.Users.Where(x => x.Email == updateDto.Email && x.Id != Id).FirstOrDefault();
                    if(email != null)
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.DuplicateX, "Email"]);
                    }
                }
                //update user
                _mapper.Map(updateDto, b);
                //update role
                var role = _context.UserRoles.Where(x => x.UserId == b.Id).FirstOrDefault();
                role.RoleId = updateDto.RoleId;
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region USERS
        //GET api/users/id
        /// <summary>
        /// USERS | Get User Info
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        [HttpGet("{UserID}", Name = "GetUserInfo")]
        public ActionResult<User> GetUserInfo(int userId)
        {
            try
            {
                var users = _context.Users.Where(s => s.Id == userId)
                            .Include(s => s.UserRoles.Roles)
                            .Include(s => s.Companies)
                            .Single();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        //GET api/userinfo
        /// <summary>
        /// USERS | Get User Info by Token
        /// </summary>
        /// <returns></returns>
        [HttpGet("info")]
        public ActionResult<User> GetUserInfobyToken()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                var users = _context.Users.Where(s => s.Id == userId)
                            .Include(s => s.UserRoles.Roles)
                            .Include(s => s.Companies)
                            .Single();
                users.Password = string.Empty;
                return Ok(users);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// USERS | Get Location
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [HttpGet("getLocation/{locationId}")]
        public ActionResult<IEnumerable<Location>> GetLocation(int locationId)
        {
            try
            {
                return Ok(MyUltil.GetLocation(_context, locationId));
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        //POST api/users/UserBillboard
        /// <summary>
        /// USERS | Add Billboard User
        /// </summary>
        /// <param name="userBillboard"></param>
        /// <returns></returns>
        [HttpPost("UserBillboard")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserBillboard(UserBillboard userBillboard)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserBillboards.Add(userBillboard);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/UserBus
        /// <summary>
        /// USERS | AddUserBus
        /// </summary>
        /// <param name="userBus"></param>
        /// <returns></returns>
        [HttpPost("UserBus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserBus(UserBus userBus)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserBuses.Add(userBus);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/UserDigitalBillboard
        /// <summary>
        /// USERS | AddUserDigitalBillboard
        /// </summary>
        /// <param name="userDigitalBillboard"></param>
        /// <returns></returns>
        [HttpPost("UserDigitalBillboard")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserDigitalBillboard(UserDigitalBillboard userDigitalBillboard)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserDigitalBillboards.Add(userDigitalBillboard);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/UserLocations
        /// <summary>
        /// USERS | AddUserLocations người chịu trách nhiệm cho khu vực
        /// </summary>
        /// <param name="userLocation"></param>
        /// <returns></returns>
        [HttpPost("UserLocations")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserLocations(UserLocation userLocation)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserLocations.Add(userLocation);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/UserShelter
        /// <summary>
        /// USERS | AddUserShelter
        /// </summary>
        /// <param name="userShelter"></param>
        /// <returns></returns>
        [HttpPost("UserShelter")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserShelter(UserShelter userShelter)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserShelters.Add(userShelter);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/UserTaxi
        /// <summary>
        /// USERS | AddUserTaxi
        /// </summary>
        /// <param name="userTaxi"></param>
        /// <returns></returns>
        [HttpPost("UserTaxi")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserTaxi(UserTaxi userTaxi)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserTaxis.Add(userTaxi);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/users/list
        /// <summary>
        /// USERS | Get List Users By Role
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("list/{roleId}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult GetListUsers(int roleId)
        {
            try
            {
                var models = _context.UserRoles.Include(x => x.Users.Companies).AsNoTracking()
                            .Where(x => x.RoleId == roleId).ToList()
                            .Select(x => { x.Users.Password = null; return x.Users; });

                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region ASSIGN USER TO CAMPAIGN
        //GET api/users/usercampaign
        /// <summary>
        /// Coordinator | Get User Campaign
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("usercampaign/{campaignId}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult GetUserCampaign(int campaignId)
        {
            try
            {
                var models = _context.UserCampaigns.Include(x => x.Users).AsNoTracking()
                                                    .Where(x => x.CampaignId == campaignId).ToList()
                                                    .Select(x => { x.Users.Password = null; return x.Users; });
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/users/usercampaign
        /// <summary>
        /// Coordinator | Get User NotIn Campaign
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("usercampaign/{campaignId}/NotIn")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult GetUserNotInCampaign(int campaignId)
        {
            try
            {
                var models = _context.Users.Where(x => x.UserRoles.Roles.RoleName == Constant.ConstantRole.Coordinator)
                                           .Where(x =>
                                                !(_context.UserCampaigns.Where(x => x.CampaignId == campaignId).Select(x => x.UserId)).Contains(x.Id)
                                           )
                                           ;
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //DEL api/users/usercampaign
        /// <summary>
        /// Coordinator | Delete Coordinator
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("usercampaign/{campaignId}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult DeleteUserCampaign(int campaignId, int userId)
        {
            try
            {
                _context.Database.BeginTransaction();
                var model = _context.UserCampaigns.Where(x=>x.CampaignId == campaignId && x.UserId == userId).FirstOrDefault();
                _context.UserCampaigns.Remove(model);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/users/usercampaign
        /// <summary>
        /// Coordinator | Add Coordinator into Campaign
        /// </summary>
        /// <param name="userCampaign"></param>
        /// <returns></returns>
        [HttpPost("usercampaign")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddUserCampaign(UserCampaign userCampaign)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.UserCampaigns.Add(userCampaign);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region GET CAMPAIGN INFO TO MOBILE
        //GET api/users/bookinglist
        //[Authorize]
        //[HttpGet("bookinglist")]
        //public ActionResult<IEnumerable<BookingDetail>> GetBookingList()
        //{
        //    try
        //    {
        //        var identity = HttpContext.User.Identity as ClaimsIdentity;
        //        int userId  = MyUltil.GetCurrentUserId(identity);
        //        return Ok(_reponsitory.Campaigns(userId));
        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}
        #endregion

        #region ROLEs
        /// <summary>
        /// ROLE | GetRole
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("roles/{roleId}")]
        public ActionResult<IEnumerable<Role>> GetRole(int roleId)
        {
            var users = _context.Roles.Where(s => s.Id == roleId).Include(s => s.UserRoles).ThenInclude(s => s.Users).ToList();
            return users;
        }
        #endregion

        #region PASSWORD
        /// <summary>
        /// PASSWORD | UpdatePassword
        /// </summary>
        /// <param name="curPass"></param>
        /// <param name="newPass"></param>
        /// <returns></returns>
        //PUT api/Users/updatePassword
        [HttpPut("updatePassword")]
        public ActionResult UpdatePassword(string curPass, string newPass)
        {
            try
            {
                _context.Database.BeginTransaction();

                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                var users = _context.Users.Where(s => s.Id == userId)
                            .Include(s => s.UserRoles.Roles)
                            .Include(s => s.Companies)
                            .Single();

                var curPassword = MyUltil.encodeM5(curPass);
                var newPassword = MyUltil.encodeM5(newPass);
                if (!users.Password.Equals(curPassword))
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.IncorrectPassword]);
                else
                {
                    users.Password = newPassword;
                    _context.SaveChanges();
                }

                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/Users/resetPassword
        /// <summary>
        /// PASSWORD | ResetPassword
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("resetPassword")]
        [AllowAnonymous]
        public ActionResult ResetPassword(string username, string email)
        {
            try
            {
                _context.Database.BeginTransaction();

                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                var users = _context.Users.Where(s => s.Username == username && s.Email == email)
                            .Include(s => s.UserRoles.Roles)
                            .Include(s => s.Companies)
                            .Single();

                var rand = MyUltil.RandomString(6);
                var newPassword = MyUltil.encodeM5(rand);
                users.Password = newPassword;
                _context.SaveChanges();

                //sendmail
                bool kq = Gmail.SendMail(users, rand);
                if(kq == false)
                {
                    return Ok(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }

                _context.Database.CommitTransaction();
                return Ok(_localizer[Constant.ConstantMessage.ResetPassword].Value);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region REMIND

        //GET api/users/remind
        /// <summary>
        /// REMIND | GetRemind
        /// </summary>
        /// <returns></returns>
        [HttpGet("remind")]
        public ActionResult GetRemind()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                var list = _context.Reminders.Where(x => x.UserId == userId && x.Statuses.Name == Constant.ConstantStatus.New);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/users/remind
        /// <summary>
        /// REMIND | UpdateRemind
        /// </summary>
        /// <param name="remindId"></param>
        /// <returns></returns>
        [HttpPut("remind")]
        public ActionResult UpdateRemind(int remindId)
        {
            try
            {
                _context.Database.BeginTransaction();
                var remind = _context.Reminders.Where(x => x.Id == remindId).FirstOrDefault();
                var status = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Confirmed).Select(x => x.Id).FirstOrDefault();
                remind.StatusId = status;
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}
