﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class UserIdentity
    {
        public string UserName { get; set; }
        public int Id { get; set; }
        public string CompanyId { get; set; }
        public string RoleName { get; set; }
    }
}
