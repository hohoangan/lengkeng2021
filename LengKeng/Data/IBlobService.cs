﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Data
{
    public interface IBlobService
    {
        public Task<BlobInfox> GetBlobAsync(string fileName);
        public Task<IEnumerable<string>> ListBlobAsync();
        public Task UploadFileBlobAsync(string filePath, string fileName);
        public Task UploadContentBlobAsync(string content, string fileName);
        public Task<string> UploadImageBlobAsync(Stream content, string fileName);
        public Task DeleteBlobAsync(string blobName);
    }
}
