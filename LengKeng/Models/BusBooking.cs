﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusBooking
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int InvoiceId { get; set; }
        [JsonIgnore]
        public Invoice Invoices { get; set; }
        [Required, price]
        public double Price { get; set; }
        public double PromotePercent { get; set; } = 0;
        [Required, Id]
        public int Amount { get; set; }
        [Required]//Chi phí lắp đặt
        public double InstallationCost { get; set; }
        public int InstallationTimes { get; set; }
        [Required]
        public double DiscountPercent { get; set; }
        [Required, Id]
        public int BusTypeOfArtworkId { get; set; }
        [JsonIgnore]
        public BusTypeOfArtwork BusTypeOfArtworks { get; set; }
        [Required]
        public double TotalFee { get; set; }
        [Required]
        public int BusId { get; set; }
        [JsonIgnore]
        public Bus Buses { get; set; }
        [Required, Id]
        public int UnitId { get; set; }
        [JsonIgnore]
        public Unit Units { get; set; }
        public double OTC { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
    }
}
