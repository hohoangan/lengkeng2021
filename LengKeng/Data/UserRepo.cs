﻿using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace LengKeng.Data
{
    public class UserRepo : IUserRepo
    {
        private readonly LengKengDbContext _context;

        public UserRepo(LengKengDbContext context)
        {
            _context = context;
        }
        //public IEnumerable<BookingDetail> Campaigns(int userId)
        //{
        //    try
        //    {
        //        /*-------------------------- GET role -----------------------------*/
        //        var userRole = _context.UserRoles.Where(r => r.UserId == userId).Select(s=> new { 
        //                                                                                            RoleId =s.Roles.Id,
        //                                                                                            RoleName = s.Roles.RoleName
        //                                                                                        }).FirstOrDefault();
        //        var roleName = userRole?.RoleName ?? string.Empty;


        //        if (roleName.Equals(Constant.ConstantRole.Coordinator))
        //        {
        //            return GetBookingCoordinator(userId);
        //        }
        //        else if(roleName.Equals(Constant.ConstantRole.TaxiDriver))
        //        {
        //            return GetBookingTaxiDriver(userId);
        //        }
        //        else if (roleName.Equals(Constant.ConstantRole.BusDriver))
        //        {
        //            return GetBookingBusDriver(userId);
        //        }
        //        else if (roleName.Equals(Constant.ConstantRole.BillboardOperator))
        //        {
        //            return GetBookingBillboardOperator(userId);
        //        }

        //        return new List<BookingDetail>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}
        
        #region GET DETAIL BOOKING
        //public List<BookingDetail> GetBookingCoordinator(int userId)
        //{
        //    var bookingDtlList = new List<BookingDetail>();

        //    /*------------------------------ get list campaign --------------------------*/
        //    var listCampaign = from userCampaign in _context.UserCampaigns.Where(x => x.UserId == userId)
        //                       from campaign in _context.Campaigns.Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
        //                                                        .Where(x => x.Id == userCampaign.CampaignId)
        //                        select new
        //                        {
        //                            Id = campaign.Id,
        //                            Campaign = campaign
        //                        };

        //    /*------------------------------ get list taxi --------------------------*/
        //    var listTaxi = from cp in listCampaign
        //                    join booking in _context.TaxiBookings on cp.Id equals booking.Invoices.CampaignId
        //                    select new
        //                    {
        //                        CampaignId = cp.Id,
        //                        Row = new MobileTaxiBookingRead
        //                        {
        //                            TaxiId = booking.TaxiId,
        //                            PlateNumber = booking.Taxis.PlateNumber,
        //                            OTC = booking.Taxis.OTC,
        //                            Location = booking.Taxis.TaxiAreas.Locations,
        //                            CarType = booking.Taxis.TaxiTypes,
        //                            ArtType = booking.TaxiTypeOfArtworks,
        //                            Status = booking.Statuses,
        //                            FromDate = booking.FromDate,
        //                            ToDate = booking.ToDate
        //                        }
        //                    };
        //    /*------------------------------ get list bus --------------------------*/
        //    var listBus = from cp in listCampaign
        //                   join booking in _context.BusBookings on cp.Id equals booking.Invoices.CampaignId
        //                   select new
        //                   {
        //                       CampaignId = cp.Id,
        //                       Row = new MobileBusBookingRead
        //                       {
        //                           BusId = booking.BusId,
        //                           PlateNumber = booking.Buses.PlateNumber,
        //                           OTC = booking.Buses.OTC,
        //                           Location = booking.Buses.BusRoutes.Locations,
        //                           CarType = booking.Buses.BusTypes,
        //                           ArtType = booking.BusTypeOfArtworks,
        //                           Status = booking.Statuses,
        //                           FromDate = booking.FromDate,
        //                           ToDate = booking.ToDate
        //                       }
        //                   };
        //    /*------------------------------ get list billboard --------------------------*/
        //    var listBillboard = from cp in listCampaign
        //                  join bbb in _context.BillboardBookings on cp.Id equals bbb.Invoices.CampaignId
        //                  select new
        //                  {
        //                      CampaignId = cp.Id,
        //                      Row = new MobileBillboardBookingRead
        //                      {
        //                          BillboardId = bbb.BillboardId,
        //                          Name = bbb.Billboards.Name,
        //                          Type = bbb.Billboards.Type,
        //                          OTC = bbb.Billboards.OTC,
        //                          Location = bbb.Billboards.Locations,
        //                          Status = bbb.Statuses,
        //                          FromDate = bbb.FromDate,
        //                          ToDate = bbb.ToDate
        //                      }
        //                  };

        //    /*------------------------------ get list digital --------------------------*/
        //    var listDigital = from cp in listCampaign
        //                        join dbb in _context.DigitalBillboardBookings on cp.Id equals dbb.Invoices.CampaignId
        //                        select new
        //                        {
        //                            CampaignId = cp.Id,
        //                            Row = new MobileDigitalBillboardBookingRead
        //                            {
        //                                DigitalBillboardId = dbb.DigitalBillboardId,
        //                                Name = dbb.DigitalBillboards.Name,
        //                                Type = dbb.DigitalBillboards.Type,
        //                                OTC = dbb.DigitalBillboards.OTC,
        //                                Location = dbb.DigitalBillboards.Locations,
        //                                Status = dbb.Statuses,
        //                                FromDate = dbb.FromDate,
        //                                ToDate = dbb.ToDate
        //                            }
        //                        };
        //    /*------------------------------ get list shelter --------------------------*/
        //    var listShelter = from cp in listCampaign
        //                        join bbb in _context.BusShelterBookings on cp.Id equals bbb.Invoices.CampaignId
        //                        select new
        //                        {
        //                            CampaignId = cp.Id,
        //                            Row = new MobileShelterBookingRead
        //                            {
        //                                ShelterId = bbb.BusShelterId,
        //                                Name = bbb.BusShelters.Name,
        //                                OTC = bbb.BusShelters.OTC,
        //                                Location = bbb.BusShelters.Locations,
        //                                Status = bbb.Statuses,
        //                                FromDate = bbb.FromDate,
        //                                ToDate = bbb.ToDate
        //                            }
        //                        };

        //    if (listCampaign != null)
        //    {
        //        /*------------------------------ group by Campaign --------------------------*/
        //        var campaign = listCampaign.Select(x => x.Campaign).Distinct().ToList();
        //        campaign?.ForEach(item =>
        //        {
        //            var bookingDtl = new BookingDetail
        //            {
        //                CampaignBookings = new CampaignBooking
        //                {
        //                    Id = item.Id,
        //                    Name = item.Name,
        //                    Taxis = listTaxi?.Where(x => x.CampaignId == item.Id).Select(x => x.Row).ToList(),
        //                    Buses = listBus?.Where(x => x.CampaignId == item.Id).Select(x => x.Row).ToList(),
        //                    Billboards = listBillboard?.Where(x => x.CampaignId == item.Id).Select(x => x.Row).ToList(),
        //                    DigitalBillboards = listDigital?.Where(x => x.CampaignId == item.Id).Select(x => x.Row).ToList(),
        //                    BusShelters = listShelter?.Where(x => x.CampaignId == item.Id).Select(x => x.Row).ToList(),
        //                }
        //            };
        //            bookingDtlList.Add(bookingDtl);
        //        });

        //    }
        //    return bookingDtlList;
        //}

        //public List<BookingDetail> GetBookingTaxiDriver(int userId)
        //{
        //    var bookingDtlList = new List<BookingDetail>();
        //    /*------------------------------ get list taxi --------------------------*/
        //    var listReturn = from userTaxi in _context.UserTaxis.Where(x => x.UserId == userId)
        //                     join booking in _context.TaxiBookings
        //                                      .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
        //                                      on userTaxi.TaxiId equals booking.TaxiId
        //                     select new
        //                     {
        //                         Campaign = booking.Invoices.Campaigns,
        //                         Row = new MobileTaxiBookingRead
        //                         {
        //                             TaxiId = booking.TaxiId,
        //                             PlateNumber = booking.Taxis.PlateNumber,
        //                             OTC = booking.Taxis.OTC,
        //                             CarType = booking.Taxis.TaxiTypes,
        //                             ArtType = booking.TaxiTypeOfArtworks,
        //                             Status = booking.Statuses,
        //                             FromDate = booking.FromDate,
        //                             ToDate = booking.ToDate
        //                         }
        //                     };

        //    if (listReturn != null)
        //    {
        //        /*------------------------------ group by Campaign --------------------------*/
        //        var campaign = listReturn.Select(x => x.Campaign).Distinct().ToList();
        //        campaign?.ForEach(item =>
        //        {
        //            var bookingDtl = new BookingDetail
        //            {
        //                CampaignBookings = new CampaignBooking
        //                {
        //                    Id = item.Id,
        //                    Name = item.Name,
        //                    Taxis = listReturn.Where(x => x.Campaign == item).Select(x => x.Row).ToList()
        //                }
        //            };
        //            bookingDtlList.Add(bookingDtl);
        //        });

        //    }
        //    return bookingDtlList;
        //}

        //public List<BookingDetail> GetBookingBusDriver(int userId)
        //{
        //    var bookingDtlList = new List<BookingDetail>();
        //    /*------------------------------ get list taxi --------------------------*/
        //    var listReturn = from userBus in _context.UserBuses.Where(x => x.UserId == userId)
        //                     join booking in _context.BusBookings
        //                                      .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
        //                                      on userBus.BusId equals booking.BusId
        //                     select new
        //                     {
        //                         Campaign = booking.Invoices.Campaigns,
        //                         Row = new MobileBusBookingRead
        //                         {
        //                             BusId = booking.BusId,
        //                             PlateNumber = booking.Buses.PlateNumber,
        //                             OTC = booking.Buses.OTC,
        //                             CarType = booking.Buses.BusTypes,
        //                             ArtType = booking.BusTypeOfArtworks,
        //                             Status = booking.Statuses,
        //                             FromDate = booking.FromDate,
        //                             ToDate = booking.ToDate
        //                         }
        //                     };

        //    if (listReturn != null)
        //    {
        //        /*------------------------------ group by Campaign --------------------------*/
        //        var campaign = listReturn.Select(x => x.Campaign).Distinct().ToList();
        //        campaign?.ForEach(item =>
        //        {
        //            var bookingDtl = new BookingDetail
        //            {
        //                CampaignBookings = new CampaignBooking
        //                {
        //                    Id = item.Id,
        //                    Name = item.Name,
        //                    Buses = listReturn?.Where(x => x.Campaign == item).Select(x => x.Row).ToList(),
        //                }
        //            };

        //            bookingDtlList.Add(bookingDtl);
        //        });

        //    }
        //    return bookingDtlList;
        //}

        //public List<BookingDetail> GetBookingBillboardOperator(int userId)
        //{
        //    var bookingDtlList = new List<BookingDetail>();
        //    /*------------------------------ get list billboard --------------------------*/
        //    var listBBB = from userBillboard in _context.UserBillboards.Where(x => x.UserId == userId)
        //                     join bbb in _context.BillboardBookings.Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
        //                                        on userBillboard.BillboardId equals bbb.BillboardId
        //                     select new
        //                     {
        //                         Campaign = bbb.Invoices.Campaigns,
        //                         BBB = new MobileBillboardBookingRead
        //                         {
        //                             BillboardId = bbb.BillboardId,
        //                             Name = bbb.Billboards.Name,
        //                             Type = bbb.Billboards.Type,
        //                             OTC = bbb.Billboards.OTC,
        //                             Status = bbb.Statuses,
        //                             FromDate = bbb.FromDate,
        //                             ToDate = bbb.ToDate
        //                         }
        //                     };
        //    /*------------------------------ get list digital --------------------------*/
        //    var listDBB = from useDigital in _context.UserDigitalBillboards.Where(x => x.UserId == userId)
        //                  join dbb in _context.DigitalBillboardBookings.Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
        //                                     on useDigital.DigitalBillboardId equals dbb.DigitalBillboardId
        //                  select new
        //                  {
        //                      Campaign = dbb.Invoices.Campaigns,
        //                      DBB = new MobileDigitalBillboardBookingRead
        //                      {
        //                          DigitalBillboardId = dbb.DigitalBillboardId,
        //                          Name = dbb.DigitalBillboards.Name,
        //                          Type = dbb.DigitalBillboards.Type,
        //                          OTC = dbb.DigitalBillboards.OTC,
        //                          Status = dbb.Statuses,
        //                          FromDate = dbb.FromDate,
        //                          ToDate = dbb.ToDate
        //                      }
        //                  };

        //    if (listBBB != null || listDBB != null)
        //    {
        //        listDBB = null;
        //        var campaign = new List<Campaign>();
        //        if (listBBB != null && listDBB != null)
        //            campaign = listBBB.Select(x => x.Campaign).Union(listDBB.Select(x => x.Campaign)).Distinct().OrderBy(x => x.Id).ToList();
        //        else
        //        {
        //            campaign = listBBB != null ? listBBB.Select(x => x.Campaign).Distinct().ToList() : listDBB?.Select(x => x.Campaign).Distinct().ToList();
        //        }
        //        /*------------------------------ group by Campaign --------------------------*/
        //        campaign?.ForEach(item =>
        //        {
        //            var bookingDtl = new BookingDetail
        //            {
        //                CampaignBookings = new CampaignBooking
        //                {
        //                    Id = item.Id,
        //                    Name = item.Name,
        //                    Billboards = listBBB?.Where(x => x.Campaign == item).Select(x => x.BBB).ToList(),
        //                    DigitalBillboards = listDBB?.Where(x => x.Campaign == item).Select(x => x.DBB).ToList()
        //                }
        //            };
        //            bookingDtlList.Add(bookingDtl);
        //        });

        //    }
        //    return bookingDtlList;
        //}
        #endregion
    }
}
