﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BillboardCamera
    {
        [Key]
        public int Id { get; set; }
        public string Link { get; set; }
        public string RTSPLink { get; set; }
        public string Type { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public bool CountOn { get; set; } = false;
        [Required, Id]
        public int BillboardId { get; set; }
        [JsonIgnore]
        public Billboard Billboards { get; set; }
    }
}
