﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusInstallationCost
    {
        [Required]
        public int Id { get; set; }
        [Required, price]
        public double Price { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        [JsonIgnore]
        public BusType BusTypes { get; set; }
        [Required, Id]
        public int BusTypeOfArtworkId { get; set; }
        [JsonIgnore]
        public BusTypeOfArtwork BusTypeOfArtworks { get; set; }
    }
}
