﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BillboardDiscount
    {
        [Required]
        public int Id { get; set; }
        [Required, Id]
        public int BillboardPriceId { get; set; }
        [JsonIgnore]
        public BillboardPrice BillboardPrice { get; set; }
        //[Required, Id]
        //public int LocationId { get; set; }
        //[Required, Id]
        //public int CompanyId { get; set; }
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
    }
}
