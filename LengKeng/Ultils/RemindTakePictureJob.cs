﻿using LengKeng.Controllers;
using LengKeng.Data;
using LengKeng.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace LengKeng.Ultils
{
    public class RemindTakePictureJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            //await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Take photo");
            await TakePhotos();
        }

        public async Task TakePhotos()
        {
            try
            {
                var connectionstring = MyUltil.ConnectionString;

                var optionsBuilder = new DbContextOptionsBuilder<LengKengDbContext>();
                optionsBuilder.UseSqlServer(connectionstring, options => options.EnableRetryOnFailure());
                using (LengKengDbContext _context = new LengKengDbContext(optionsBuilder.Options))
                {
                    try
                    {
                        //_context.Database.BeginTransaction();
                        List<Reminder> reminds = new List<Reminder>();

                        #region /* coordinator */
                        var statusNew = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.New).Select(x => x.Id).FirstOrDefault();
                        var campaigns = from userCampaign in _context.UserCampaigns
                                           join campaign in _context.Campaigns.Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                                            .AsNoTracking()
                                                                            on userCampaign.CampaignId equals campaign.Id
                                           select new Reminder
                                           {
                                               Message = string.Format("Take photos of the campaign: {0}", campaign.Name),
                                               campaignId = campaign.Id,
                                               StatusId = statusNew,
                                               UserId = userCampaign.UserId,
                                               Users = userCampaign.Users
                                           };

                        reminds.AddRange(campaigns);
                        #endregion
                        DateTime dt = DateTime.Now;
                        #region/* taxi driver */
                        var taxis = from userTaxi in _context.UserTaxis
                                    join booking in _context.TaxiBookings.Include(x => x.Invoices.Campaigns)
                                                     .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                     .Where(x => x.FromDate <= dt && dt <= x.ToDate)
                                                     .AsNoTracking()
                                                     on userTaxi.TaxiId equals booking.TaxiId
                                    select new Reminder
                                    {
                                        Message = string.Format("Take photos of the campaign: {0}", booking.Invoices.Campaigns.Name),
                                        campaignId = booking.Invoices.Campaigns.Id,
                                        StatusId = statusNew,
                                        UserId = userTaxi.UserId,
                                        Users = userTaxi.Users
                                    };
                        reminds.AddRange(taxis);
                        #endregion

                        #region/* bus driver */
                        var bus = from userBus in _context.UserBuses
                                  join booking in _context.BusBookings
                                                   .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                   .Where(x => x.FromDate <= dt && dt <= x.ToDate)
                                                   .AsNoTracking()
                                                   on userBus.BusId equals booking.BusId
                                  select new Reminder
                                  {
                                      Message = string.Format("Take photos of the campaign: {0}", booking.Invoices.Campaigns.Name),
                                      campaignId = booking.Invoices.Campaigns.Id,
                                      StatusId = statusNew,
                                      UserId = userBus.UserId,
                                      Users = userBus.Users
                                  };
                        reminds.AddRange(bus);
                        #endregion

                        #region/* Billboard Operator */
                        var listBBB = from userBillboard in _context.UserBillboards
                                      join bbb in _context.BillboardBookings.Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                            .Where(x => x.FromDate <= dt && dt <= x.ToDate)
                                                            .AsNoTracking()
                                                         on userBillboard.BillboardId equals bbb.BillboardId
                                      select new Reminder
                                      {
                                          Message = string.Format("Take photos of the campaign: {0}", bbb.Invoices.Campaigns.Name),
                                          campaignId = bbb.Invoices.Campaigns.Id,
                                          StatusId = statusNew,
                                          UserId = userBillboard.UserId,
                                          Users = userBillboard.Users
                                      };

                        reminds.AddRange(listBBB);

                        var listDBB = from useDigital in _context.UserDigitalBillboards
                                      join dbb in _context.DigitalBillboardBookings.Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                            .Where(x => x.FromDate <= dt && dt <= x.ToDate)
                                                            .AsNoTracking()
                                                         on useDigital.DigitalBillboardId equals dbb.DigitalBillboardId
                                      select new Reminder
                                      {
                                          Message = string.Format("Take photos of the campaign: {0}", dbb.Invoices.Campaigns.Name),
                                          campaignId = dbb.Invoices.Campaigns.Id,
                                          StatusId = statusNew,
                                          UserId = useDigital.UserId,
                                          Users = useDigital.Users
                                      };

                        reminds.AddRange(listDBB);
                        #endregion

                        reminds = reminds.GroupBy(x => new { x.UserId, x.campaignId }, y => y)
                                            .Select(x => x.First())
                                            .ToList();

                        foreach (var item in reminds)
                        {
                            //sendmail
                            bool kq = Gmail.SendMailTakePhoto(item.Users, item.Message);
                            item.Users = null;
                        }
                        _context.Reminders.AddRange(reminds);
                        _context.SaveChanges();
                        
                        //_context.Database.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        //_context.Database.RollbackTransaction();
                    }
                }
                //return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Remind Take Photos | FAILURE " + ex.Message);
            }
        }
    }

    public class RemindSetPriceJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Remind Set Prices");
            await RemindSetPrice();
        }

        public async Task RemindSetPrice()
        {
            try
            {
                var connectionstring = MyUltil.ConnectionString;

                var optionsBuilder = new DbContextOptionsBuilder<LengKengDbContext>();
                optionsBuilder.UseSqlServer(connectionstring);
                using (LengKengDbContext _context = new LengKengDbContext(optionsBuilder.Options))
                {
                    try
                    {
                        #region TAXI
                        _context.Database.BeginTransaction();
                        var statusNew = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.New).Select(x => x.Id).FirstOrDefault();
                        
                        var listTaxiArea = from listArea in _context.TaxiAreas.Where(x => !_context.TaxiAreaPrices.Select(x => x.TaxiAreaId).Contains(x.Id))
                                           join user in _context.Users on listArea.CompanyId equals user.CompanyId
                                           join role in _context.UserRoles.Where(x => x.Roles.RoleName == Constant.ConstantRole.TaxiCompany)
                                           on user.Id equals role.UserId
                                           select new { listArea, role.Users };

                        foreach (var item in listTaxiArea)
                        {
                            var remind = new Reminder
                            {
                                Id = 0,
                                Message = string.Format("Please set price for taxi area: {0}", item.listArea.Name),
                                UserId = item.Users.Id,
                                StatusId = statusNew
                            };
                            _context.Reminders.Add(remind);
                            _context.SaveChanges();
                            //Sending Email
                            await Gmail.SendMailSetPrice(item.Users, item.listArea.Name);
                        }
                        
                        #endregion

                        #region BUS
                        var listBusRoute = from listRoute in _context.BusRoutes.Where(x => !_context.BusRoutePrices.Select(x => x.BusRouteId).Contains(x.Id))
                                           join bus in _context.Buses on listRoute.Id equals bus.BusRouteId
                                           join user in _context.Users on bus.CompanyId equals user.CompanyId
                                           join role in _context.UserRoles.Where(x => x.Roles.RoleName == Constant.ConstantRole.BusCompany)
                                           on user.Id equals role.UserId
                                           select new { listRoute, role.Users };

                        listBusRoute = listBusRoute.Distinct();
                        foreach (var item in listBusRoute)
                        {
                            var remind = new Reminder
                            {
                                Id = 0,
                                Message = string.Format("Please set price for bus route {0}: {1}", item.listRoute.Code, item.listRoute.Name),
                                UserId = item.Users.Id,
                                StatusId = statusNew
                            };
                            _context.Reminders.Add(remind);
                            _context.SaveChanges();
                            //Sending Email
                            await Gmail.SendMailSetPrice(item.Users, string.Format("{0} : {1}", item.listRoute.Code, item.listRoute.Name ) , true);
                        }
                        #endregion
                        _context.Database.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        _context.Database.RollbackTransaction();
                        await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Remind Set Prices | FAILURE " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Remind Set Prices | FAILURE " + ex.Message);
            }
        }
    }

    public class CheckCameraWorkJobJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Take photo");
            await CheckCameraWork();
        }

        public async Task CheckCameraWork()
        {
            try
            {
                var connectionstring = MyUltil.ConnectionString;

                var optionsBuilder = new DbContextOptionsBuilder<LengKengDbContext>();
                optionsBuilder.UseSqlServer(connectionstring, options => options.EnableRetryOnFailure());
                using (LengKengDbContext _context = new LengKengDbContext(optionsBuilder.Options))
                {
                    try
                    {
                        string message = "";
                        List<Reminder> reminds = new List<Reminder>();

                        DateTime dt = DateTime.Now;
                        var bbcamera = from bbcam in _context.BillboardCameras.Where(x => Constant.CameraType.Count.Equals(x.Type) && x.CountOn == true)
                                       join bbcount in _context.BillboardCountings on
                                                    new { bbcam.BillboardId, bbcam.Id } equals
                                                    new { bbcount.BillboardId, Id = bbcount.BillboardCameraId }into bbJ
                                       from count in bbJ.DefaultIfEmpty()
                                       select new { BillboardId = bbcam.BillboardId,
                                           BillboardCameraId = bbcam.Id,
                                           Countings = count
                                       };

                        var list = bbcamera.ToList().GroupBy(x => new { x.BillboardCameraId, x.BillboardId }).Select(x => x.Last()).ToList();

                        
                        foreach (var item in list)
                        {
                            if (item.Countings == null)
                            {
                                message += string.Format("<b>BillboardId:{0} | BillboardCameraId:{1} | Teamview:{2}/{3}</b><br/>",
                                item.BillboardId, item.BillboardCameraId, item?.Countings?.TeamViewId ?? "", item?.Countings?.TeamViewPasswords ?? "");
                            }
                            else
                            {
                                var min = (dt - item.Countings.LastUpdated).TotalMinutes;
                                if (min > 65) // nếu hon 65p ko có dữ liệu thì gửi mail
                                {
                                    message += string.Format("<b>BillboardId:{0} | BillboardCameraId:{1} | Teamview:{2}/{3}</b><br/>",
                                    item.BillboardId, item.BillboardCameraId, item?.Countings?.TeamViewId ?? "", item?.Countings?.TeamViewPasswords ?? "");
                                }
                            }
                            //sendmail
                        }
                        if(!string.IsNullOrEmpty(message))
                            Gmail.SendMailCameraNotWork("hohoangan2014@gmail.com", message);

                    }
                    catch (Exception ex)
                    {
                        await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Check Camera Work | FAILURE " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync(DateTime.Now.ToString() + " Check Camera Work | FAILURE " + ex.Message);
            }
        }
    }
}
