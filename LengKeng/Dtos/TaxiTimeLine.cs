﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class TaxiTimeLine
    {
        public IEnumerable<TaxiBooking> bookings { get; set; }
        public IEnumerable<TaxiDownTime> downTimes { get; set; }
    }
}
