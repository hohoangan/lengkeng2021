﻿using System;
using LengKeng.Ultils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class TaxiDiscountUpdateDto
    {
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
    }
}
