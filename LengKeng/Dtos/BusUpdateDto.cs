﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusUpdateDto
    {
        [Required]
        public string PlateNumber { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int BusRouteId { get; set; }
    }
}
