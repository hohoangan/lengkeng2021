﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using Quartz.Logging;
using LengKeng.Ultils;
using Quartz.Impl.Matchers;

namespace LengKeng.Ultils
{
    public static class CronJob
    {
        /*
         * get info job
         * https://stackoverflow.com/questions/12489450/get-all-jobs-in-quartz-net-2-0
         */
        public static StdSchedulerFactory factory = new StdSchedulerFactory();
        public static IScheduler scheduler;

        public static async void CreateTakePictureJob(int timeRepeat)
        {
            try
            {
                if (scheduler != null)
                {
                    await scheduler.DeleteJob(new JobKey("takePictureJob", "group1"));
                }
                #region CREATE CRON JOB
                LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
                //chi 1 lan duy nhat trong job dau tien
                scheduler = await factory.GetScheduler();

                // and start it off
                if(!scheduler.IsShutdown)
                    await scheduler.Start();
                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<RemindTakePictureJob>()
                    .WithIdentity("takePictureJob", "group1")
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1")
                    .StartNow()
                    .WithSimpleSchedule(x => 
                    x
                        .WithIntervalInHours(24 * timeRepeat)
                        //.WithIntervalInSeconds(timeRepeat)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);
                #endregion
            }
            catch { }
        }

        public static async void CreateRemindSetPriceJob(int timeRepeat)
        {
            try
            {
                if (scheduler != null)
                {
                    await scheduler.DeleteJob(new JobKey("remindSetPriceJob", "group2"));
                }
                #region CREATE CRON JOB
                LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
                if(scheduler==null)
                    scheduler = await factory.GetScheduler();
                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<RemindSetPriceJob>()
                    .WithIdentity("remindSetPriceJob", "group2")
                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger2", "group2")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInHours(24 * timeRepeat)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);
 
                #endregion
            }
            catch (Exception){
            }
        }

        public static async void CreateCheckCameraWorkJob(int timeRepeat)
        {
            try
            {
                if (scheduler != null)
                {
                    await scheduler.DeleteJob(new JobKey("checkCameraWorkJob", "group3"));
                }
                #region CREATE CRON JOB
                LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
                if (scheduler == null)
                    scheduler = await factory.GetScheduler();

                // and start it off
                if (!scheduler.IsShutdown)
                    await scheduler.Start();
                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<CheckCameraWorkJobJob>()
                    .WithIdentity("checkCameraWorkJob", "group3")
                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger3", "group3")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(timeRepeat)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);

                #endregion
            }
            catch (Exception)
            {
            }
        }

        public static async void ShutdownAllJob()
        {
            try
            {
                if (scheduler != null)
                {
                    // and last shut down the scheduler when you are ready to close your program
                    await scheduler.Shutdown();
                };
            }
            catch { }
        }
    }
}
