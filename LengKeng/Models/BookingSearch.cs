﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BookingSearch
    {
        #region Dung chung
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Amount { get; set; } //total month or hour
        public int LocationId { get; set; }
        #endregion

        #region/*--- dung cho Digital ----*/
        public int HourPerDay { get; set; }
        #endregion

        #region/*--- dung cho bus/taxi ----*/
        public int CarAmount { get; set; }
        public int CompanyId { get; set; } 
        public int BusRouteId { get; set; } 
        public int ArtTypeId { get; set; } 
        public int CarTypeId { get; set; } 
        public int CarAmountFrom { get; set; } 
        public int CarAmountTo { get; set; } 
        public double PriceFrom { get; set; } 
        public double PriceTo { get; set; } 
        #endregion
    }
}
