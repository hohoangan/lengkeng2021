﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiArea
    {

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        [Required, Id]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiAreaPrice> TaxiAreaPrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<Taxi> Taxis { get; set; }
    }
}
