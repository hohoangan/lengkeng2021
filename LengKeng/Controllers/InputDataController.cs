﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper.Configuration.Conventions;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RestSharp;
using RestSharp.Serialization.Json;

namespace LengKeng.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = Constant.ConstantRole.SuperUser)]
    public class InputDataController : ControllerBase
    {
        private readonly LengKengDbContext _context;

        public InputDataController(LengKengDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Them bus cho he thong
        /// </summary>
        /// <param name="bus"></param>
        /// <param name="totalCar"></param>
        /// <returns></returns>
        //POST api/InputData/InitBusProduct
        [HttpPost("InitBusProduct")]
        public ActionResult InitBusProduct(Bus bus, int totalCar)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Bus> buses = new List<Bus>();
                //lay so xe taxi moi
                var tx = _context.Buses.Where(x => x.PlateNumber.Contains("54B1XXXX")).OrderByDescending(x => x.PlateNumber).Select(x => x.PlateNumber).FirstOrDefault() ?? "54B1XXXX-0";
                //int[] arrRouteId = listRoutes.Trim().Split("\n").Select(x => int.Parse(x)).ToArray();

                int num = int.Parse(tx.Split('-')[1]);
                string bienso = tx.Split('-')[0];
                //get BusRoutes chưa co xe bus
                for (int i = 1; i <= totalCar; i++)
                {
                    bus.Id = 0;
                    num++;
                    string no = "54B1XXXX" + '-' + num.ToString().PadLeft(5, '0');
                    var newBus = new Bus
                    {
                        Id = 0,
                        PlateNumber = no,
                        BusTypeId = bus.BusTypeId,
                        DimensionH = bus.DimensionH,
                        DimensionW = bus.DimensionW,
                        BusRouteId = bus.BusRouteId,
                        StatusId = bus.StatusId,
                        CompanyId = bus.CompanyId
                    };
                    buses.Add(newBus);
                }
                _context.Buses.AddRange(buses);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = System.Text.Json.JsonSerializer.Serialize(bus),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Them taxi cho he thong
        /// </summary>
        /// <param name="taxi"></param>
        /// <param name="totalCar"></param>
        /// <returns></returns>
        //POST api/InputData/InitTaxiProduct
        [HttpPost("InitTaxiProduct")]
        public ActionResult InitTaxiProduct(Taxi taxi, int totalCar)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Taxi> taxis = new List<Taxi>();
                //lay so xe taxi moi
                var tx = _context.Taxis.Where(x => x.PlateNumber.Contains("54T1XXXX")).OrderByDescending(x => x.PlateNumber).Select(x => x.PlateNumber).FirstOrDefault() ?? "54T1XXXX-0";
                var num = int.Parse(tx.Split('-')[1]);
                //get BusRoutes chưa co xe bus
                for (int i = 1; i <= totalCar; i++)
                {
                    taxi.Id = 0;
                    var number = num + i;
                    string no = "54T1XXXX" + '-' + number.ToString().PadLeft(5, '0');
                    var newTaxi = new Taxi
                    {
                        Id = 0,
                        PlateNumber = no,
                        TaxiTypeId = taxi.TaxiTypeId,
                        DimensionH = taxi.DimensionH,
                        DimensionW = taxi.DimensionW,
                        TaxiAreaId = taxi.TaxiAreaId,
                        StatusId = taxi.StatusId,
                        CompanyId = taxi.CompanyId
                    };
                    taxis.Add(newTaxi);
                }
                _context.Taxis.AddRange(taxis);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = System.Text.Json.JsonSerializer.Serialize(taxi),
                    UserId = userId
                });
            }
        }

        /*
        #region SAU NAY BO
        //https://vi.wikipedia.org/wiki/Danh_s%C3%A1ch_c%C3%A1c_%C4%91%C6%A1n_v%E1%BB%8B_h%C3%A0nh_ch%C3%ADnh_t%E1%BA%A1i_Vi%E1%BB%87t_Nam
        [HttpPost("Location")]
        //[AllowAnonymous]
        public ActionResult InsertLocation(int level, int parentId, string names, string wardsOrCity, string split)
        {
            try
            {
                _context.Database.BeginTransaction();
                string[] listName = names.Trim().Split(split);
                foreach (var item in listName)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        _context.Locations.Add(new Models.Location
                        {
                            Name = string.Format("{0} {1}", wardsOrCity, item.Trim()),
                            ParentId = parentId,
                            Level = level,
                            OrderBy = 1
                        });
                    }
                }

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok(listName);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex.Message);
            }
        }

        //[HttpPost("BusDiscount")]
        //public ActionResult InsertBusDiscount(string loc, string percentList, bool isNotIN = false)
        //{
        //    try
        //    {
        //        _context.Database.BeginTransaction();
        //        var location = loc.Split(',');
        //        var percent = percentList.Split(',');
        //        var months = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        //        var perMonths = new { percent, months };

        //        var company = from com in _context.Companies select com;
        //        if (!isNotIN)
        //            company = company.Where(x => x.RoleId == 9 && location.Contains(x.LocationId.ToString()));
        //        else
        //            company = company.Where(x => x.RoleId == 9 && !location.Contains(x.LocationId.ToString()));

        //        foreach (var item in company)
        //        {
        //            int count = 1;
        //            foreach (var per in percent)
        //            {
        //                var dis = new BusDiscount
        //                {
        //                    Id = 0,
        //                    //LocationId = item.LocationId,
        //                    //CompanyId = item.CompanyId,
        //                    Month = count++,
        //                    Percent = double.Parse(per)
        //                };
        //                _context.BusDiscounts.Add(dis);
        //                _context.SaveChanges();
        //            }
        //        }
        //        _context.Database.CommitTransaction();
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        _context.Database.RollbackTransaction();
        //        return BadRequest(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //[HttpPost("BusRustePrice")]
        //public ActionResult InsertBusRoutePrice(string locList, string ArtWorkList, double price, bool isNotIN = false)
        //{
        //    try
        //    {
        //        _context.Database.BeginTransaction();
        //        var locations = locList.Split(',');
        //        var artIds = ArtWorkList.Split(',');
        //        var artworks = _context.BusTypeOfArtworks.Where(x => artIds.Contains(x.Id.ToString()));


        //        var routes = from route in _context.BusRoutes select route;
        //        if (!isNotIN)
        //            routes = routes.Where(x => locations.Contains(x.LocationId.ToString()));
        //        else
        //            routes = routes.Where(x => !locations.Contains(x.LocationId.ToString()));

        //        var list = from route in routes
        //                   from art in artworks
        //                   from type in _context.BusTypes
        //                   select new BusRoutePrice
        //                   {
        //                       Id = 0,
        //                       Price = price,
        //                       BusTypeOfArtworkId = art.Id,
        //                       BusTypeId = type.Id,
        //                       FromDate = new DateTime(2020, 1, 1),
        //                       ToDate = new DateTime(2020, 12, 31),
        //                       BusRouteId = route.Id,
        //                       CreatedUserId = 3,
        //                       UserId = 3,
        //                       UnitId = 1,
        //                       MinCarBooking = 1
        //                   };

        //        _context.BusRoutePrices.AddRange(list);
        //        _context.SaveChanges();

        //        _context.Database.CommitTransaction();
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        _context.Database.RollbackTransaction();
        //        return BadRequest(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //POST api/InputData/InitBusProductWithRoutes
        [HttpPost("InitBusProductWithRoutes")]
        public ActionResult InitBusProductWithRoutes(Bus bus, int totalCar, string listRoutes = null)
        {
            try
            {
                _context.Database.BeginTransaction();
                List<Bus> buses = new List<Bus>();
                //lay so xe taxi moi
                var tx = _context.Buses.Where(x => x.PlateNumber.Contains("54B1XXXX")).OrderByDescending(x => x.PlateNumber).Select(x => x.PlateNumber).FirstOrDefault() ?? "54B1XXXX-0";
                int[] arrRouteId = listRoutes.Trim().Split("\n").Select(x => int.Parse(x)).ToArray();

                int num = int.Parse(tx.Split('-')[1]);
                string bienso = tx.Split('-')[0];
                //get BusRoutes chưa co xe bus
                for (int j = 0; j < arrRouteId.Count(); j++)
                {
                    for (int i = 1; i <= totalCar; i++)
                    {
                        bus.Id = 0;
                        num++;
                        string no = "54B1XXXX" + '-' + num.ToString().PadLeft(5, '0');
                        var newBus = new Bus
                        {
                            Id = 0,
                            PlateNumber = no,
                            BusTypeId = bus.BusTypeId,
                            //OTC = bus.OTC,
                            BusRouteId = arrRouteId[j],
                            StatusId = bus.StatusId,
                            CompanyId = bus.CompanyId
                        };
                        buses.Add(newBus);
                    }
                }
                _context.Buses.AddRange(buses);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Chi them data tu file test
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sheet"></param>
        /// <param name="nameCty"></param>
        /// <param name="address"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="typeCol"></param>
        /// <param name="codeCol"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="loc"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost("Company")]
        public ActionResult InsertCompany(string fileName, string sheet, string companyNameCol,
                                                string codeCol, int from, int to, string location)
        {
            try
            {
                _context.Database.BeginTransaction();
                List<TaxiArea> areas = new List<TaxiArea>();
                List<BusRouteCompany> busRouteCompanies = new List<BusRouteCompany>();
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        if (xSheet.Cells[companyNameCol + i.ToString()].Text.Trim() != string.Empty)
                        {
                            string locname = xSheet.Cells[location + i.ToString()].Text.Trim();
                            string companyName = xSheet.Cells[companyNameCol + i.ToString()].Text.Trim();
                            string code = xSheet.Cells[codeCol + i.ToString()].Text.Trim();
                            //int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "60 - 80c" ? 2 : 1;//Taxi: "4 chỗ" ? 4 : 5;
                            //int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "4 chỗ" ? 4 : 5;
                            int locid = _context.Locations.Where(x => x.Name == locname).Select(x => x.LocationId).FirstOrDefault();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            //int taxiAreaId = _context.TaxiAreas.Where(x => x.CompanyId == companyId && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            int busRouteId = _context.BusRoutes.Where(x => x.Code == code && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            #region Add taxiarea
                            //var area = new TaxiArea
                            //{
                            //    Id = 0,
                            //    CompanyId = companyId,
                            //    LocationId = locid,
                            //    Name = string.Format("{0} - {1}", companyName, locname)
                            //};
                            //areas.Add(area);
                            #endregion
                            #region Add Busroute Company
                            //ktra xem da add trong bang BusRouteCompany chua
                            var checkBusRouteComp = _context.BusRouteCompanies.Where(x => x.CompanyId == companyId && x.BusRouteId == busRouteId).FirstOrDefault();
                            if (checkBusRouteComp != null)
                            {
                                var busRouteCom = new BusRouteCompany
                                {
                                    CompanyId = companyId,
                                    BusRouteId = busRouteId,
                                    Id = 0,
                                };
                                busRouteCompanies.Add(busRouteCom);
                            }
                            #endregion
                        }
                        else
                        {
                            int row = i;
                        }
                    }
                    var err = busRouteCompanies.Where(x => x.BusRouteId == 0 || x.CompanyId == 0).ToList();
                    if (err.Count != 0)
                    {
                        return BadRequest();
                    }
                    busRouteCompanies = busRouteCompanies.Where(x => x.BusRouteId != 0).GroupBy(x => new { x.CompanyId, x.BusRouteId }).Select(x => x.First()).Distinct().ToList();
                    _context.BusRouteCompanies.AddRange(busRouteCompanies);

                    //areas = areas.GroupBy(x => x.Name).Select(x => x.First()).ToList();
                    //_context.TaxiAreas.AddRange(areas);
                    //_context.Companies.AddRange(coms);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        #region init data API: Area, BusRoutes
        //POST api/InputData/InitTaxiAreaPrice
        [HttpPost("InitTaxiAreaPrice")]
        public ActionResult InitTaxiAreaPrice(TaxiAreaPrice price, string listPrice, string listArea)
        {
            try
            {
                _context.Database.BeginTransaction();
                double[] arrPrice = listPrice.Trim().Replace(",", "").Split("\t").Select(x => double.Parse(x)).ToArray();
                int[] arrAreaId = listArea.Trim().Split("\n").Select(x => int.Parse(x)).ToArray();
                price.Price = arrPrice[0];
                //price.Price = arrPrice[0] * 1000;

                for (int j = 0; j < arrAreaId.Count(); j++)
                {
                    var pr = new TaxiAreaPrice
                    {
                        Id = 0,
                        CreatedUserId = price.CreatedUserId,
                        FromDate = price.FromDate,
                        ToDate = price.ToDate,
                        LastUpdated = price.LastUpdated,
                        MinCarBooking = price.MinCarBooking,
                        TaxiAreaId = arrAreaId[j],
                        Price = price.Price,
                        TaxiTypeId = price.TaxiTypeId,
                        TaxiTypeOfArtworkId = price.TaxiTypeOfArtworkId,
                        UnitId = price.UnitId,
                        UserId = price.UserId
                    };
                    _context.TaxiAreaPrices.Add(pr);
                    _context.SaveChanges();
                    List<TaxiDiscount> discounts = new List<TaxiDiscount>();
                    for (int i = 0; i < 12; i++)
                    {
                        double per = 0;
                        if (i > 0)
                        {
                            double giacuathang = arrPrice[i];
                            per = Math.Round(100 * (1 - (giacuathang / (arrPrice[0] * (i + 1)))), 3);
                        }

                        var dis = new TaxiDiscount
                        {
                            Id = 0,
                            Month = i + 1,
                            Percent = per,
                            TaxiAreaPriceId = pr.Id,
                        };
                        discounts.Add(dis);
                    }
                    _context.TaxiDiscounts.AddRange(discounts);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/InputData/InitBusRoutePrice
        [HttpPost("InitBusRoutePrice")]
        public ActionResult InitBusRoutePrice(BusRoutePrice price, string listPrice, string listRoute, double price1m)
        {
            try
            {
                _context.Database.BeginTransaction();
                double[] arrPrice = listPrice.Trim().Replace(",", "").Split("\t").Select(x => double.Parse(x)).ToArray();
                int[] arrRouteId = listRoute.Trim().Split("\n").Select(x => int.Parse(x)).ToArray();
                price.Price = price1m;

                for (int j = 0; j < arrRouteId.Count(); j++)
                {
                    var pr = new BusRoutePrice
                    {
                        Id = 0,
                        CreatedUserId = price.CreatedUserId,
                        FromDate = price.FromDate,
                        ToDate = price.ToDate,
                        LastUpdated = price.LastUpdated,
                        MinCarBooking = price.MinCarBooking,
                        BusRouteId = arrRouteId[j],
                        Price = price.Price,
                        BusTypeId = price.BusTypeId,
                        BusTypeOfArtworkId = price.BusTypeOfArtworkId,
                        UnitId = price.UnitId,
                        UserId = price.UserId,
                        CompanyId = price.CompanyId

                    };
                    _context.BusRoutePrices.Add(pr);
                    _context.SaveChanges();
                    List<BusDiscount> discounts = new List<BusDiscount>();
                    for (int i = 0; i < 12; i++)
                    {
                        double per = 0;
                        if (i > 0)
                        {
                            double giacuathang = arrPrice[i];
                            per = Math.Round(100 * (1 - (giacuathang / (arrPrice[0] * (i + 1)))), 3);
                        }

                        var dis = new BusDiscount
                        {
                            Id = 0,
                            Month = i + 1,
                            Percent = per,
                            BusRoutePriceId = pr.Id,
                        };
                        discounts.Add(dis);
                    }
                    _context.BusDiscounts.AddRange(discounts);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/InputData/InitBusRoutePriceOther
        [HttpPost("InitBusRoutePriceOther")]
        public ActionResult InitBusRoutePriceOther(BusRoutePrice price, string listPrice, string listRoute, double price1m)
        {
            try
            {
                _context.Database.BeginTransaction();
                double[] arrPrice = listPrice.Trim().Replace(",", "").Split("\t").Select(x => double.Parse(x)).ToArray();

                //danh sach nhung route chua co gia
                var list = (from com in _context.BusRouteCompanies.Where(x => !(_context.BusRoutePrices.Select(x => x.BusRouteId)).Contains(x.BusRouteId))
                            join bus in _context.Buses on com.BusRouteId equals bus.BusRouteId
                            select new
                            {
                                com.CompanyId,
                                com.BusRouteId,
                                bus.BusTypeId
                            }).Distinct().ToArray();

                for (int k = 0; k < list.Count(); k++)
                {
                    price.Price = price1m;

                    var pr = new BusRoutePrice
                    {
                        Id = 0,
                        CreatedUserId = price.CreatedUserId,
                        FromDate = price.FromDate,
                        ToDate = price.ToDate,
                        LastUpdated = price.LastUpdated,
                        MinCarBooking = price.MinCarBooking,
                        BusRouteId = list[k].BusRouteId,//route
                        Price = price.Price,
                        BusTypeId = list[k].BusTypeId,//type
                        BusTypeOfArtworkId = price.BusTypeOfArtworkId,
                        UnitId = price.UnitId,
                        UserId = price.UserId,
                        CompanyId = list[k].CompanyId //company

                    };
                    _context.BusRoutePrices.Add(pr);
                    _context.SaveChanges();
                    List<BusDiscount> discounts = new List<BusDiscount>();
                    for (int i = 0; i < 12; i++)
                    {
                        double per = 0;
                        if (i > 0)
                        {
                            double giacuathang = arrPrice[i];
                            per = Math.Round(100 * (1 - (giacuathang / (arrPrice[0] * (i + 1)))), 1);
                        }

                        var dis = new BusDiscount
                        {
                            Id = 0,
                            Month = i + 1,
                            Percent = per,
                            BusRoutePriceId = pr.Id,
                        };
                        discounts.Add(dis);
                    }
                    _context.BusDiscounts.AddRange(discounts);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/InputData/DiscountPercent
        [HttpGet("DiscountPercent")]
        [AllowAnonymous]
        public ActionResult GetDiscountPercent(string listPrice)
        {
            try
            {
                List<string> list = new List<string>();
                string[] listconvert = listPrice.Trim().Split("\t");
                double[] arrPrice = listconvert.Select(x => double.Parse(x.Replace(".", "").Replace(",", ""))).ToArray();
                //double[] arrPrice = listPrice.Trim().Replace(",", "").Replace(".", "").Split("\t").Select(x => double.Parse(x)).ToArray();
                for (int i = 0; i < 12; i++)
                {
                    double per = 0;
                    if (i > 0)
                    {
                        double giacuathang = arrPrice[i];
                        per = Math.Round(100 * (1 - (giacuathang / (arrPrice[0] * (i + 1)))), 3);
                    }
                    list.Add(string.Format("{0} tháng: {1} %", i + 1, per.ToString().Replace(",", ".")));
                }
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region Init data from file
        [HttpPost("BusRoute")]
        public ActionResult InsertBusRoute(string fileName, string sheet, string Code, string busroutename, string name, string chieudi, string chieuve, int from, int to, string locCol)
        {
            int row = 0;
            try
            {
                _context.Database.BeginTransaction();
                List<BusRoute> routes = new List<BusRoute>();
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        string companyName = xSheet.Cells[name + i.ToString()].Text.Trim();
                        string locName = xSheet.Cells[locCol + i.ToString()].Text.Trim();
                        string code = xSheet.Cells[Code + i.ToString()].Text.Trim();
                        string _busroutename = xSheet.Cells[busroutename + i.ToString()].Text.Trim();
                        int locid = _context.Locations.Where(x => x.Name == locName).Select(x => x.LocationId).FirstOrDefault();
                        var busRoute = _context.BusRoutes.Where(x => x.Code == code && x.LocationId == locid).FirstOrDefault();

                        //ktra neu co busroute roi thi update name cua tuyen bus
                        //neu chua co bustoute thi them moi
                        if (busRoute != null)
                        {
                            busRoute.Name = _busroutename;
                            _context.SaveChanges();
                        }
                        else if (xSheet.Cells[Code + i.ToString()].Text.Trim() != string.Empty)
                        {
                            var route = new BusRoute
                            {
                                Id = 0,
                                Code = xSheet.Cells[Code + i.ToString()].Text.Trim(),
                                Name = xSheet.Cells[name + i.ToString()].Text.Trim(),
                                Description = string.Format("Chiều đi:{0}\n Chiều về:{1}",
                                        xSheet.Cells[chieudi + i.ToString()].Text.Trim(),
                                        xSheet.Cells[chieuve + i.ToString()].Text.Trim()),
                                LocationId = locid
                            };
                            routes.Add(route);
                        }
                        else
                        {
                            row = i;
                        }
                    }
                    var err = routes.Where(x => x.LocationId == 0);
                    routes = routes.GroupBy(x => new { x.Code, x.Name, x.Locations }).Select(x => x.First()).Distinct().ToList();
                    _context.BusRoutes.AddRange(routes);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(row + " - " + ex?.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPost("InsertTaxi4Company")]//Them so luong taxi
        //sau khi da insert cty - area cua taxi-form
        //insert tat cac cac xe taxi nam trong danh sach cung cap theo so luong Khanh dua
        public ActionResult InsertAllTaxiOfCompany(string fileName, string sheet, string locCol, string nameCty,
                                                    string amountCol, string OTCCol,
                                                    string typeCol, int from, int to)
        {
            int row = 0;
            try
            {
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        row = i;
                        string num = xSheet.Cells[amountCol + i.ToString()].Text.Trim();
                        int amount = int.Parse(num.Equals(string.Empty) ? "0" : num);
                        if (xSheet.Cells[nameCty + i.ToString()].Text.Trim() != string.Empty && amount > 0)
                        {
                            string locname = xSheet.Cells[locCol + i.ToString()].Text.Trim();
                            string companyName = xSheet.Cells[nameCty + i.ToString()].Text.Trim();
                            int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "4 chỗ" ? 4 : 5;
                            int locid = _context.Locations.Where(x => x.Name == locname).Select(x => x.LocationId).FirstOrDefault();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            int taxiAreaId = _context.TaxiAreas.Where(x => x.CompanyId == companyId && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            int OTC = int.Parse(xSheet.Cells[OTCCol + i.ToString()].Text.Trim().Replace(".", "").Replace(",", ""));
                            #region Add taxi
                            var taxi = new Taxi
                            {
                                Id = 0,
                                TaxiTypeId = typeId,
                                TaxiAreaId = taxiAreaId,
                                StatusId = 8,
                                CompanyId = companyId
                            };
                            InitTaxiProduct(taxi, amount);
                            #endregion
                        }
                        else
                        {
                            row = i;
                        }
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(row + " " + ex?.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPost("InsertTaxiPrice4Company")]//them gia taxi
        public ActionResult InsertTaxiPrice4Company(string fileName, string sheet, string locCol, string nameCty,
                                                    string minBooking, int typeArtwork,
                                                    string typeCol, int from, int to)
        {
            int row = 0;
            try
            {
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        row = i;
                        string price = xSheet.Cells["AU" + i.ToString()].Text.Trim();
                        double price1 = double.Parse(price.Equals(string.Empty) || price.Equals("-") ? "0" : price);
                        if (xSheet.Cells[nameCty + i.ToString()].Text.Trim() != string.Empty && price1 > 0)
                        {
                            string locname = xSheet.Cells[locCol + i.ToString()].Text.Trim();
                            string companyName = xSheet.Cells[nameCty + i.ToString()].Text.Trim();
                            int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "4 chỗ" ? 4 : 5;
                            int locid = _context.Locations.Where(x => x.Name == locname).Select(x => x.LocationId).FirstOrDefault();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            int taxiAreaId = _context.TaxiAreas.Where(x => x.CompanyId == companyId && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            int minBook = int.Parse(xSheet.Cells[minBooking + i.ToString()].Text.Trim().Replace(".", "").Replace(",", ""));
                            #region Add taxi price
                            var taxi = new TaxiAreaPrice
                            {
                                Id = 0,
                                TaxiTypeId = typeId,
                                TaxiAreaId = taxiAreaId,
                                CreatedUserId = 3,
                                FromDate = new DateTime(2021, 1, 1),
                                ToDate = new DateTime(2021, 12, 31),
                                LastUpdated = DateTime.Now,
                                MinCarBooking = minBook,
                                Price = 0,
                                TaxiTypeOfArtworkId = typeArtwork,
                                UnitId = 1,
                                UserId = 3
                            };

                            //list price
                            string price2 = xSheet.Cells["AV" + i.ToString()].Text.Trim();
                            string price3 = xSheet.Cells["AW" + i.ToString()].Text.Trim();
                            string price4 = xSheet.Cells["AX" + i.ToString()].Text.Trim();
                            string price5 = xSheet.Cells["AY" + i.ToString()].Text.Trim();
                            string price6 = xSheet.Cells["AZ" + i.ToString()].Text.Trim();
                            string price7 = xSheet.Cells["BA" + i.ToString()].Text.Trim();
                            string price8 = xSheet.Cells["BB" + i.ToString()].Text.Trim();
                            string price9 = xSheet.Cells["BC" + i.ToString()].Text.Trim();
                            string price10 = xSheet.Cells["BD" + i.ToString()].Text.Trim();
                            string price11 = xSheet.Cells["BE" + i.ToString()].Text.Trim();
                            string price12 = xSheet.Cells["BF" + i.ToString()].Text.Trim();
                            string listPrice = price + "\t";
                            listPrice += price2 + "\t";
                            listPrice += price3 + "\t";
                            listPrice += price4 + "\t";
                            listPrice += price5 + "\t";
                            listPrice += price6 + "\t";
                            listPrice += price7 + "\t";
                            listPrice += price8 + "\t";
                            listPrice += price9 + "\t";
                            listPrice += price10 + "\t";
                            listPrice += price11 + "\t";
                            listPrice += price12 + "\t";

                            if (taxiAreaId == 0)
                            {
                                var area = new TaxiArea
                                {
                                    Id = 0,
                                    Name = companyName + " - " + locname,
                                    CompanyId = companyId,
                                    LocationId = locid
                                };
                                _context.TaxiAreas.Add(area);
                                _context.SaveChanges();
                                taxiAreaId = area.Id;
                                taxi.TaxiAreaId = taxiAreaId;
                                InitTaxiAreaPrice(taxi, listPrice, taxiAreaId.ToString());
                                row = i;
                            }
                            else
                            {
                                row = i;
                                InitTaxiAreaPrice(taxi, listPrice, taxiAreaId.ToString());
                            }
                            #endregion
                        }
                        else
                        {
                            row = i;
                        }
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(row + " " + ex?.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPost("InsertBus4Company")]//Them xe bus
        //sau khi da insert cty - bus route cua bus-form
        //insert tat cac cac xe bus nam trong danh sach cung cap theo so luong Khanh dua
        public ActionResult InsertBus4Company(string fileName, string sheet, string locCol, string nameCty,
                                                    string amountCol, string OTCCol, string codeCol,
                                                    string typeCol, int from, int to)
        {
            int row = 0;
            try
            {
                List<Company> coms = new List<Company>();
                List<TaxiArea> areas = new List<TaxiArea>();
                List<Taxi> taxis = new List<Taxi>();
                List<Bus> buses = new List<Bus>();
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        row = i;
                        string num = xSheet.Cells[amountCol + i.ToString()].Text.Trim();
                        int amount = int.Parse(num.Equals(string.Empty) ? "0" : num);
                        if (xSheet.Cells[nameCty + i.ToString()].Text.Trim() != string.Empty && amount > 0)
                        {
                            string locname = xSheet.Cells[locCol + i.ToString()].Text.Trim();
                            string companyName = xSheet.Cells[nameCty + i.ToString()].Text.Trim();
                            string code = xSheet.Cells[codeCol + i.ToString()].Text.Trim();
                            int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "60 - 80c" ? 2 : 1;
                            int locid = _context.Locations.Where(x => x.Name == locname).Select(x => x.LocationId).FirstOrDefault();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            int OTC = int.Parse(xSheet.Cells[OTCCol + i.ToString()].Text.Trim().Replace(".", "").Replace(",", ""));
                            int busRouteId = _context.BusRoutes.Where(x => x.Code == code && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            #region Add Bus
                            var bus = new Bus
                            {
                                Id = 0,
                                BusTypeId = typeId,
                                BusRouteId = busRouteId,
                                StatusId = 8,
                                CompanyId = companyId
                            };
                            if (busRouteId == 0)
                            {
                                row = i;
                            }
                            else InitBusProductWithRoutes(bus, amount, busRouteId.ToString());
                            #endregion
                        }
                        else
                        {
                            row = i;
                        }
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(row + " " + ex?.InnerException?.Message ?? ex.Message);
            }
        }
        [HttpPost("InsertBusPrice4Company")]//Them gia cho bus
        public ActionResult InsertBusPrice4Company(string fileName, string sheet, string locCol, string nameCty,
                                                    string minBooking, int typeArtwork, string codeCol,
                                                    string typeCol, int from, int to)
        {
            int row = 0;
            try
            {
                List<Company> coms = new List<Company>();
                List<TaxiArea> areas = new List<TaxiArea>();
                List<Taxi> taxis = new List<Taxi>();
                List<Bus> buses = new List<Bus>();
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        row = i;
                        string price = xSheet.Cells["BQ" + i.ToString()].Text.Trim();
                        double price1 = double.Parse(price.Equals(string.Empty) || price.Equals("-") ? "0" : price);

                        if (xSheet.Cells[nameCty + i.ToString()].Text.Trim() != string.Empty && price1 > 0)
                        {
                            string locname = xSheet.Cells[locCol + i.ToString()].Text.Trim();
                            string companyName = xSheet.Cells[nameCty + i.ToString()].Text.Trim();
                            string code = xSheet.Cells[codeCol + i.ToString()].Text.Trim();
                            int typeId = xSheet.Cells[typeCol + i.ToString()].Text.Trim() == "60 - 80c" ? 2 : 1;
                            int locid = _context.Locations.Where(x => x.Name == locname).Select(x => x.LocationId).FirstOrDefault();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            //int minBook = int.Parse(xSheet.Cells[minBooking + i.ToString()].Text.Trim().Replace(".", "").Replace(",", ""));
                            int busRouteId = _context.BusRoutes.Where(x => x.Code == code && x.LocationId == locid).Select(x => x.Id).FirstOrDefault();
                            #region Add Bus Price
                            var busprice = new BusRoutePrice
                            {
                                Id = 0,
                                BusTypeId = typeId,
                                BusRouteId = busRouteId,
                                CreatedUserId = 3,
                                FromDate = new DateTime(2021, 1, 1),
                                ToDate = new DateTime(2021, 12, 31),
                                LastUpdated = DateTime.Now,
                                MinCarBooking = 1,
                                Price = 0,
                                BusTypeOfArtworkId = typeArtwork,
                                UnitId = 1,
                                UserId = 3,
                                CompanyId = companyId
                            };
                            //list price
                            string price2 = xSheet.Cells["BR" + i.ToString()].Text.Trim();
                            string price3 = xSheet.Cells["BS" + i.ToString()].Text.Trim();
                            string price4 = xSheet.Cells["BT" + i.ToString()].Text.Trim();
                            string price5 = xSheet.Cells["BU" + i.ToString()].Text.Trim();
                            string price6 = xSheet.Cells["BV" + i.ToString()].Text.Trim();
                            string price7 = xSheet.Cells["BW" + i.ToString()].Text.Trim();
                            string price8 = xSheet.Cells["BX" + i.ToString()].Text.Trim();
                            string price9 = xSheet.Cells["BY" + i.ToString()].Text.Trim();
                            string price10 = xSheet.Cells["BZ" + i.ToString()].Text.Trim();
                            string price11 = xSheet.Cells["CA" + i.ToString()].Text.Trim();
                            string price12 = xSheet.Cells["CB" + i.ToString()].Text.Trim();
                            string listPrice = price + "\t";
                            listPrice += price2 + "\t";
                            listPrice += price3 + "\t";
                            listPrice += price4 + "\t";
                            listPrice += price5 + "\t";
                            listPrice += price6 + "\t";
                            listPrice += price7 + "\t";
                            listPrice += price8 + "\t";
                            listPrice += price9 + "\t";
                            listPrice += price10 + "\t";
                            listPrice += price11 + "\t";
                            listPrice += price12 + "\t";

                            if (busRouteId == 0)
                            {
                                row = i;
                            }
                            else
                            {
                                row = i;
                                InitBusRoutePrice(busprice, listPrice, busRouteId.ToString(), price1);
                            }
                            #endregion
                        }
                        else
                        {
                            row = i;
                        }
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(row + " " + ex?.InnerException?.Message ?? ex.Message);
            }
        }

        [HttpPost("InsertBB")]
        public ActionResult InsertBB(string fileName, string sheet, int from, int to)
        {
            int row = 0;
            try
            {
                _context.Database.BeginTransaction();
                List<Billboard> BBlist = new List<Billboard>();
                using (var package = new ExcelPackage(new FileInfo(fileName)))
                {
                    var xSheet = package.Workbook.Worksheets[sheet];
                    for (int i = from; i <= to; i++)
                    {
                        if (xSheet.Cells["B" + i.ToString()].Text.Trim() != string.Empty)
                        {
                            int locationId = int.Parse(xSheet.Cells["B" + i.ToString()].Text.Trim());
                            string companyName = xSheet.Cells["C" + i.ToString()].Text.Trim();
                            string name = xSheet.Cells["D" + i.ToString()].Text.Trim();
                            int companyId = _context.Companies.Where(x => x.Name == companyName).Select(x => x.CompanyId).FirstOrDefault();
                            string type = xSheet.Cells["E" + i.ToString()].Text.Trim();
                            string orientation = xSheet.Cells["F" + i.ToString()].Text.Trim();
                            string feature = xSheet.Cells["G" + i.ToString()].Text.Trim();
                            string DemensionH = xSheet.Cells["H" + i.ToString()].Text.Trim();
                            string DemensionW = xSheet.Cells["I" + i.ToString()].Text.Trim();
                            string[] xy = xSheet.Cells["J" + i.ToString()].Text.Trim().Split(",");
                            double OTC = double.Parse(xSheet.Cells["K" + i.ToString()].Text.Trim());

                            #region Add BB to list
                            var bb = new Billboard
                            {
                                Id = 0,
                                LocationId = locationId,
                                CompanyId = companyId,
                                Name = name,
                                Type = type,
                                Orientation = orientation,
                                HighlightFeature = feature,
                                DimensionH = double.Parse(DemensionH),
                                DimensionW = double.Parse(DemensionW),
                                GPSLocX = double.Parse(xy[0].Replace(".", ",")),
                                GPSLocY = double.Parse(xy[1].Replace(".", ",")),
                                OTC = OTC,
                                OTCDate = DateTime.Now.Date,
                                StatusId = 8
                            };
                            BBlist.Add(bb);
                            row = i;
                            #endregion
                        }
                    }
                    var duplicate = _context.Billboards.Where(x => BBlist.Select(x => x.Name).Contains(x.Name)).ToList();
                    var cop = BBlist.Where(x =>x.CompanyId == 0).ToList();
                    if (duplicate.Count == 0 && cop.Count == 0)
                    {
                        _context.Billboards.AddRange(BBlist);
                        _context.SaveChanges();
                    }
                    else { return NotFound("Trùng Tên"); }
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(string.Format("{0} - {1}", row, ex?.InnerException?.Message ?? ex.Message));
            }
        }
        #endregion

        #endregion
        */
        //#region SHOPEE
        //IRestResponse GetCookie()
        //{
        //    try
        //    {
        //        var client = new RestClient("https://shopee.vn/api/v0/buyer/login/");
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.POST);
        //        IRestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //string ConcatCookie(IRestResponse cookie, string csrftoken)
        //{
        //    try
        //    {
        //        //string cok = "_gcl_au=1.1.1945103089.1608988731; csrftoken=9mEUWE1UUGQ6D3PMTmFVi3IHQEFWQqw2; welcomePkgShown=true;";
        //        string cok = string.Format("_gcl_au=1.1.{0}.{1}; csrftoken={2}; welcomePkgShown=true;", MyUltil.RandomNum(10), MyUltil.RandomNum(10), csrftoken);
        //        foreach (var item in cookie.Cookies)
        //        {
        //            cok += item.Name + "=" + item.Value + ";";
        //        }
        //        return cok;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //IRestResponse Login(string username, string password, string csrftoken, string cookie)
        //{
        //    try
        //    {
        //        var client = new RestClient("https://shopee.vn/api/v2/authentication/login");
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.POST);
        //        request.AddHeader("authority", "shopee.vn");
        //        client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
        //        request.AddHeader("x-api-source", "pc");
        //        request.AddHeader("accept", "application/json");
        //        request.AddHeader("x-shopee-language", "vi");
        //        request.AddHeader("x-requested-with", "XMLHttpRequest");
        //        request.AddHeader("content-type", "application/json");
        //        request.AddHeader("x-csrftoken", csrftoken);
        //        request.AddHeader("origin", "https://shopee.vn");
        //        request.AddHeader("sec-fetch-site", "same-origin");
        //        request.AddHeader("sec-fetch-mode", "cors");
        //        request.AddHeader("sec-fetch-dest", "empty");
        //        request.AddHeader("referer", "https://shopee.vn/buyer/login?from=https%3A%2F%2Fshopee.vn%2Fuser%2Fpurchase%2F&next=https%3A%2F%2Fshopee.vn%2Fuser%2Fpurchase%2F");
        //        request.AddHeader("accept-language", "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");
        //        request.AddHeader("cookie", cookie);
        //        request.AddJsonBody(new
        //        {
        //            username,
        //            password,
        //            support_whats_app = true
        //        }, "application/json");
        //        //request.AddParameter("application/json", "{\r\n    \"username\": \"{username}\",\r\n    \"password\": \"{password}\",\r\n    \"support_whats_app\": true\r\n}".Replace("{username}", username).Replace("{password}", password), ParameterType.RequestBody);
        //        IRestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //IRestResponse Follow(string cookie, int shopid)
        //{
        //    try
        //    {
        //        var client = new RestClient("https://shopee.vn/api/v4/shop/follow");
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.POST);
        //        request.AddHeader("authority", "shopee.vn");
        //        request.AddHeader("x-api-source", "pc");
        //        request.AddHeader("accept", "application/json");
        //        request.AddHeader("content-type", "application/json");
        //        request.AddHeader("Cookie", cookie);
        //        request.AddJsonBody(new
        //        {
        //            shopid = shopid
        //        }, "application/json");
        //        //request.AddParameter("application/json", "{\"shopid\":28627169}", ParameterType.RequestBody);
        //        IRestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //int GetShopId(string shopname)
        //{
        //    try
        //    {
        //        var client = new RestClient(string.Format("https://shopee.vn/api/v4/shop/get_shop_detail?username={0}", shopname));
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.GET);
        //        IRestResponse response = client.Execute(request);

        //        int json = (int)JObject.Parse(response.Content)["data"]["shopid"];
        //        return json;
        //    }
        //    catch (Exception)
        //    {
        //        return 0;
        //    }
        //}

        //[HttpPost("AutoSub/{shopname}")]
        //[AllowAnonymous]
        //public ActionResult loginshp(string shopname, List<Account> accounts)
        //{
        //    try
        //    {
        //        //get shopId
        //        int ShopId = GetShopId(shopname);
        //        if (ShopId == 0)
        //            return NotFound("Không tìm được ID shop");

        //        Dictionary<string, string> result = new Dictionary<string, string>();

        //        accounts.ForEach(item =>
        //        {
        //            //csrftoken
        //            string csrftoken = MyUltil.RandomString(32);// "sBC0z8SxK9R5wuIiCZz5kiMNaKjm7pDi";
        //            //get cookie truoc khi dang nhap
        //            IRestResponse getCookie = GetCookie();

        //            //REC_T_ID cần để xác thực LOGIN
        //            string REC_T_ID = getCookie.Cookies.Where(x => x.Name == "REC_T_ID").Select(x => x.Name + "=" + x.Value + ";").FirstOrDefault();
        //            string cookie = ConcatCookie(getCookie, csrftoken);

        //            //dang nhap bang account vao he thong: sau khi đăng nhập REC_T_ID bị xóa
        //            IRestResponse login = Login(item.username, item.password, csrftoken, cookie);

        //            //follow 
        //            string cookieAfterLogin = ConcatCookie(login, csrftoken);
        //            IRestResponse follow = Follow(REC_T_ID + cookieAfterLogin, ShopId);

        //            //check kết quả
        //            string kq = JObject.Parse(follow.Content)["data"]["follow_successful"].ToString();
        //            result.Add(item.username, kq);

        //        });
        //        return Ok(result);

        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //}
        ////like
        //IRestResponse GetAllProduct(int shopid)
        //{
        //    try
        //    {
        //        var client = new RestClient(string.Format("https://shopee.vn/api/v2/search_items/?by=pop&limit=99&match_id={0}&newest=0&order=desc&page_type=shop&version=2", shopid));
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.GET);
        //        request.AddHeader("authority", "shopee.vn");
        //        request.AddHeader("x-shopee-language", "vi");
        //        request.AddHeader("x-requested-with", "XMLHttpRequest");
        //        client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
        //        request.AddHeader("x-api-source", "pc");
        //        request.AddHeader("accept", "*/*");
        //        request.AddHeader("sec-fetch-site", "same-origin");
        //        request.AddHeader("sec-fetch-mode", "cors");
        //        request.AddHeader("sec-fetch-dest", "empty");
        //        request.AddHeader("accept-language", "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");
        //        IRestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //IRestResponse LikeProduct(int shopid, string productid, string cookie, string csrftoken)
        //{
        //    try
        //    {
        //        var client = new RestClient(string.Format("https://shopee.vn/api/v0/buyer/like/shop/{0}/item/{1}/", shopid, productid));
        //        client.Timeout = -1;
        //        var request = new RestRequest(Method.POST);
        //        request.AddHeader("authority", "shopee.vn");
        //        client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
        //        request.AddHeader("x-api-source", "pc");
        //        request.AddHeader("accept", "application/json");
        //        request.AddHeader("x-shopee-language", "vi");
        //        request.AddHeader("x-requested-with", "XMLHttpRequest");
        //        request.AddHeader("content-type", "application/json");
        //        request.AddHeader("x-csrftoken", csrftoken);
        //        request.AddHeader("origin", "https://shopee.vn");
        //        request.AddHeader("sec-fetch-site", "same-origin");
        //        request.AddHeader("sec-fetch-mode", "cors");
        //        request.AddHeader("sec-fetch-dest", "empty");
        //        request.AddHeader("referer", string.Format("https://shopee.vn/shop/{0}/search", shopid));
        //        request.AddHeader("accept-language", "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");
        //        request.AddHeader("cookie", cookie);
        //        //request.AddHeader("cookie", "_gcl_au=1.1.1689127151.1606459571; REC_T_ID=3c117542-307c-11eb-a951-ccbbfe5def3d; SPC_IA=-1; SPC_F=3Zc57hBoA1J2hmk3QArr9SE1VZC8DxdR; REC_T_ID=3c20ef24-307c-11eb-b380-20283e97f834; _hjid=07779bb2-6ff2-40fc-a1f2-2fafb0cb9e03; G_ENABLED_IDPS=google; SPC_CLIENTID=M1pjNTdoQm9BMUoytxfpxofpyjgephpa; SC_DFP=DumD5OR48kvMJoe0JPD1195hB0JhTzF1; _gcl_aw=GCL.1608715825.CjwKCAiA8ov_BRAoEiwAOZogwbDEqJBlTe_bmFcfT1WVDe3bm0_vCP6JzE07HVBlRkn17lAadfmXbBoCuSEQAvD_BwE; _gac_UA-61914164-6=1.1608715826.CjwKCAiA8ov_BRAoEiwAOZogwbDEqJBlTe_bmFcfT1WVDe3bm0_vCP6JzE07HVBlRkn17lAadfmXbBoCuSEQAvD_BwE; _gid=GA1.2.274186061.1609223542; csrftoken=N0EzXbOGqLdgV2RX9CCXvU8z1Mu80o9Y; _hjAbsoluteSessionInProgress=0; G_AUTHUSER_H=1; SPC_SC_TK=304bd2be382c592c2cdf988d75917258; SPC_STK=\"b8VTGRp/FvZAIcXdRgpARpI59vuEXnfpZyapgMONZKTQoAcSN+wyXB/9TNZX2yXjJwFCcNMiSUvw2q7Beu6BjpoENW8a3cGaIeRhbjhD5ur61/C7eYgHDtE2kPef3ZQbiJmIC3EioiXnp6QHsqDB9njYMQmlIdD6y4jOJLIjAA96TrXYL9awR6mkanEC/E/t\"; SPC_SC_UD=361480825; SPC_CT_cb1033e2=\"1609235779.8Bwiazlw7SgXYQjo5xQSVjcMxqd9lTUk6Xr3PRfKrtY=\"; SPC_CT_d9bfc718=\"1609235780.neaUv+HaK5HDhMxNj5d67nn2SRqLWepmJZnQ438YaMk=\"; SPC_SI=mall.M2Nik91Czhdseu6Azo3KDaQeI7eKx3sL; AMP_TOKEN=%24NOT_FOUND; SPC_U=292696784; SPC_EC=PPJKTXhG0ruBu31wFdk0iafoXsQLThFzCWVnummAxqHGOzQXD3fCZTvJvap3mvKDX58BBh/hbbFI0jMfOWJ0W90j2JY/+6h1Srkxs7ARbSPVJvFoaK2kqwq3oNqR2pmiOEf7SWIgEzaAj+WRwf08E+EaN1tYV7VJd6V5TxoI2bXFp3bKT2oKQtqPTGccCSSN; SPC_CT_0da6b674=\"1609236619.LQt2bDXU87fLz6yZJ8d68ux6Iz8C4PTeytnxXTT3T20=\"; SPC_CT_81e8b2bd=\"1609236619.7joZ+BXc92dBpW2Kr8nrG8vm2RB6EjVv4MBwT0uvdBI=\"; _ga=GA1.1.1736722229.1606459573; SPC_CT_acc5435f=\"1609236820.ZB3aL1Cd6i0Rag1cZIPY0sGEmBOkxt/QSMC3LhKG0nQ=\"; SPC_CT_c618775e=\"1609236820.cbaNTKTRfwWlqavFCwCqIzF7O7eC/WizsIC+ajUb4pQ=\"; SPC_CT_e61d21ec=\"1609236820.HtedV15WuW8Zysh9yX7r8+MSTxMWhsCM0GjDP6SdIsw=\"; SPC_CT_2df139c6=\"1609236820.+987fZLZbpk8n/G72zYsraOruYXkU9UcHY78gp8i/4o=\"; SPC_CT_c4151085=\"1609236841.v+6wwG5vi6xdlw9S/UpastJSKXHyy8C7Rn8FqYTjqps=\"; SPC_CT_74c58e0c=\"1609236993.RXS1WezP8fSznaHoM9stj5sVXb/KDolswR2S/NRuGUQ=\"; _ga_M32T05RVZT=GS1.1.1609232558.22.1.1609237145.0; SPC_R_T_ID=\"YhINVgExnkyxDA+bK/1vvUxsRLDCG3Z/FxkNSf9ybF1YgaR3k8AmfNHBvla5T6D9Frymnqw4iTae/duEMocEanissO8pKZG2wKSbdlD/bZY=\"; SPC_T_IV=\"eZMMgG32n961tA1d7pSeUQ==\"; SPC_R_T_IV=\"eZMMgG32n961tA1d7pSeUQ==\"; SPC_T_ID=\"YhINVgExnkyxDA+bK/1vvUxsRLDCG3Z/FxkNSf9ybF1YgaR3k8AmfNHBvla5T6D9Frymnqw4iTae/duEMocEanissO8pKZG2wKSbdlD/bZY=\"; SPC_SI=mall.M2Nik91Czhdseu6Azo3KDaQeI7eKx3sL; SPC_R_T_ID=\"XgV0TjarmyoFB9C4xeR59Y1g/9KBNA/o+iwaYBArqQQyac18YkjddHnfw66UbDS0WLERAZc5Pccpm1V99IP1pVqweQU6EhWaL7RO9t4Xxoc=\"; SPC_T_IV=\"qvMZEy4Qj58fryJEvf2ozA==\"; SPC_R_T_IV=\"qvMZEy4Qj58fryJEvf2ozA==\"; SPC_T_ID=\"XgV0TjarmyoFB9C4xeR59Y1g/9KBNA/o+iwaYBArqQQyac18YkjddHnfw66UbDS0WLERAZc5Pccpm1V99IP1pVqweQU6EhWaL7RO9t4Xxoc=\"");
        //        request.AddParameter("application/json", "{}", ParameterType.RequestBody);
        //        IRestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //[HttpPost("AutoLike/{shopname}")]
        //[AllowAnonymous]
        //public ActionResult likeSP(string shopname, List<Account> accounts)
        //{
        //    try
        //    {
        //        //get shopId
        //        int ShopId = GetShopId(shopname);
        //        if (ShopId == 0)
        //            return NotFound("Không tìm được ID shop");

        //        //get all products
        //        IRestResponse Iproducts = GetAllProduct(ShopId);
        //        var products = JObject.Parse(Iproducts.Content)["items"].ToList();
        //        if (products == null || products?.Count == 0)
        //            return NotFound("Shop không có sản phẩm");


        //        Dictionary<string, string> result = new Dictionary<string, string>();
        //        accounts.ForEach(item =>
        //        {
        //            //csrftoken
        //            string csrftoken = MyUltil.RandomString(32);// "sBC0z8SxK9R5wuIiCZz5kiMNaKjm7pDi";
        //            //get cookie truoc khi dang nhap
        //            IRestResponse getCookie = GetCookie();

        //            //REC_T_ID cần để xác thực LOGIN
        //            string REC_T_ID = getCookie.Cookies.Where(x => x.Name == "REC_T_ID").Select(x => x.Name + "=" + x.Value + ";").FirstOrDefault();
        //            string cookie = ConcatCookie(getCookie, csrftoken);

        //            //dang nhap bang account vao he thong: sau khi đăng nhập REC_T_ID bị xóa
        //            IRestResponse login = Login(item.username, item.password, csrftoken, cookie);

        //            //LIKE
        //            string cookieAfterLogin = REC_T_ID + ConcatCookie(login, csrftoken);
        //            products?.ForEach(sp =>
        //            {
        //                string itemid = sp["itemid"].ToString();
        //                //LIKE
        //                IRestResponse likePro = LikeProduct(ShopId, itemid, cookieAfterLogin, csrftoken);
        //                result.Add(item.username + " -:-> " + itemid, likePro.StatusDescription);
        //            });
        //        });
        //        return Ok(result);

        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //}

        //[HttpPost("AutoLike/{shopname}/{productId}")]
        //[AllowAnonymous]
        //public ActionResult LikeProductId(string shopname, List<Account> accounts, string productId)
        //{
        //    try
        //    {
        //        //get shopId
        //        int ShopId = GetShopId(shopname);
        //        if (ShopId == 0)
        //            return NotFound("Không tìm được ID shop");

        //        //get all products
        //        //IRestResponse Iproducts = GetAllProduct(ShopId);
        //        //var products = JObject.Parse(Iproducts.Content)["items"].ToList();
        //        //if (products == null || products?.Count == 0)
        //        //    return NotFound("Shop không có sản phẩm");


        //        Dictionary<string, string> result = new Dictionary<string, string>();
        //        accounts.ForEach(item =>
        //        {
        //            //csrftoken
        //            string csrftoken = MyUltil.RandomString(32);// "sBC0z8SxK9R5wuIiCZz5kiMNaKjm7pDi";
        //            //get cookie truoc khi dang nhap
        //            IRestResponse getCookie = GetCookie();

        //            //REC_T_ID cần để xác thực LOGIN
        //            string REC_T_ID = getCookie.Cookies.Where(x => x.Name == "REC_T_ID").Select(x => x.Name + "=" + x.Value + ";").FirstOrDefault();
        //            string cookie = ConcatCookie(getCookie, csrftoken);

        //            //dang nhap bang account vao he thong: sau khi đăng nhập REC_T_ID bị xóa
        //            IRestResponse login = Login(item.username, item.password, csrftoken, cookie);

        //            //LIKE
        //            string cookieAfterLogin = REC_T_ID + ConcatCookie(login, csrftoken);

        //            string itemid = productId;
        //            //LIKE
        //            IRestResponse likePro = LikeProduct(ShopId, itemid, cookieAfterLogin, csrftoken);
        //            result.Add(item.username + " -:-> " + itemid, likePro.StatusDescription);
        //        });
        //        return Ok(result);

        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //}
        //#endregion
    }
}
