﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusRouteCompany
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [Required, Id]
        public int BusRouteId { get; set; }
        [JsonIgnore]
        public BusRoute BusRoutes { get; set; }
    }
}
