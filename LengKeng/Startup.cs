﻿using LengKeng.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using AutoMapper;
using System;
using Newtonsoft.Json.Serialization;
using LengKeng.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Azure;
using Azure.Storage.Queues;
using Azure.Core.Extensions;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Features;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using LengKeng.Ultils;
using AspNetCoreRateLimit;
using System.Net.WebSockets;

namespace LengKeng
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region //add Dbcontext
            string con = Configuration.GetConnectionString("CommanderConnection");
            Constant.Environment.Permission = Configuration["Environment"];
            if (Constant.Environment.Production.Equals(Constant.Environment.Permission))
                con = Configuration.GetConnectionString("CommanderConnectionPRODUCT");
            MyUltil.ConnectionString = con;
            services.AddDbContext<LengKengDbContext>(opt => opt.UseSqlServer(con));
            #endregion

            #region //add token
            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyMethod().AllowAnyHeader().AllowCredentials().Build());
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                    };
                });
            #endregion

            services.AddControllers().AddNewtonsoftJson(s => {
                s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                s.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;//loi Self referencing loop detected for property
            });

            #region//add Azure Blob Storage 
            string connectBlod = Configuration["AzureBlobStorageConnectionString"];
            if (Constant.Environment.Production.Equals(Constant.Environment.Permission))
                connectBlod = Configuration["AzureBlobStorageConnectionStringPRODUCT"];
            services.AddSingleton(x => new BlobServiceClient(connectBlod));
            services.AddSingleton<IBlobService, BlobService>();
            //services.AddAzureClients(builder =>
            //{
            //    builder.AddBlobServiceClient(Configuration["ConnectionStrings:AzureBlobStorageConnectionString:blob"], preferMsi: true);
            //    builder.AddQueueServiceClient(Configuration["ConnectionStrings:AzureBlobStorageConnectionString:queue"], preferMsi: true);
            //});
            #endregion

            #region//Mapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion

            #region //add interface for a controller
            //services.AddScoped<ICommanderRepo, MockCommanderRepo>();
            services.AddScoped<ICommanderRepo, SqlCommanderRepo>();
            services.AddScoped<ICampaignRepo, CampaignRepo>();
            services.AddScoped<IUserRepo, UserRepo>();
            #endregion

            #region Upload large files .
            //chinh trong webconfig
            //<requestLimits maxAllowedContentLength="1073741824" />
            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = int.MaxValue;
            });
            services.Configure<Microsoft.AspNetCore.Server.Kestrel.Core.KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = int.MaxValue; // if don't set default value is: 30 MB
            });
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = long.MaxValue; // if don't set default value is: 128 MB
                x.MultipartHeadersLengthLimit = int.MaxValue;
                x.MemoryBufferThreshold = Int32.MaxValue;
                x.ValueCountLimit = 10;
            });
            #endregion

            #region //Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "LengKeng API", Version = "v1" });
            });
            //them description cho API
            services.AddControllers();
            services.AddSwaggerGen(options =>
            {
                options.IncludeXmlComments(Constant.XmlCommentsFilePath);
            });
            #endregion

            #region Language
            services.AddLocalization(opt => opt.ResourcesPath = "");
            #endregion
            // sua loi null company trong digitalbillboard
            //services.AddMvc(option => option.EnableEndpointRouting = false)
            //    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            //    .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            #region Add Policy
            services.AddAuthorization(options =>
            {
                //take pic
                options.AddPolicy(Constant.ConstantPolicy.RequireMobileRole,
                     policy => policy.RequireRole(Constant.ConstantRole.Coordinator, Constant.ConstantRole.TaxiDriver,
                                                    Constant.ConstantRole.BusDriver, Constant.ConstantRole.BillboardOperator));

                //managed channel
                options.AddPolicy(Constant.ConstantPolicy.RequireBillboardAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.BillboardCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireDigitalAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.BillboardCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireBusAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.BusCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireTaxiAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.TaxiCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireShelterAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.ShelterCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireCustomerAdminRole,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.Customer));

                options.AddPolicy(Constant.ConstantPolicy.RequirePartnerGroup,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.BillboardCompany
                                                    , Constant.ConstantRole.BusCompany, Constant.ConstantRole.ShelterCompany
                                                    , Constant.ConstantRole.TaxiCompany));
                //view caimpaign web
                options.AddPolicy(Constant.ConstantPolicy.RequireAdminCustomerBusCompany,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.Customer, Constant.ConstantRole.BusCompany ));

                options.AddPolicy(Constant.ConstantPolicy.RequireAdminCustomerTaxiCompany,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.Customer, Constant.ConstantRole.TaxiCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireAdminCustomerShelterCompany,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.Customer, Constant.ConstantRole.ShelterCompany));

                options.AddPolicy(Constant.ConstantPolicy.RequireAdminCustomerBillboardCompany,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.Customer, Constant.ConstantRole.BillboardCompany));
                //view detail campaign: admin, bus, taxi, billboard, shetler and customer
                options.AddPolicy(Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser
                                                        , Constant.ConstantRole.Customer
                                                        , Constant.ConstantRole.BusCompany
                                                        , Constant.ConstantRole.TaxiCompany
                                                        , Constant.ConstantRole.ShelterCompany
                                                        , Constant.ConstantRole.BillboardCompany));

                //view admin - supper
                options.AddPolicy(Constant.ConstantPolicy.RequireAdminSupperUser,
                     policy => policy.RequireRole(Constant.ConstantRole.SuperUser, Constant.ConstantRole.SystemAdmin));

            });
            #endregion

            #region Chong DDos
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddOptions();
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimit"));
            services.AddSingleton<IIpPolicyStore,
            MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore,
            MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IRateLimitConfiguration,
            RateLimitConfiguration>();
            services.AddHttpContextAccessor();
            #endregion

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            #region Swagger
            if (Constant.Environment.Develop.Equals(Constant.Environment.Permission))
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "LengKeng API Version 1");
                    c.DocExpansion(DocExpansion.None);
                });
            }
            #endregion

            #region Localization
            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("vi-VN")
            };

            var requestLocalizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("vi-VN"),
                //format numbers, dates, etc
                SupportedCultures = supportedCultures,
                //UI strings that we have localized
                SupportedUICultures = supportedCultures
            };
            app.UseRequestLocalization(requestLocalizationOptions);
            #endregion


            #region WebSocket
            //https://www.youtube.com/watch?v=ycVgXe6v1VQ&ab_channel=LesJackson
            //app.UseWebSockets();
            //app.Use(async (context, next) =>
            //{
            //    if (context.WebSockets.IsWebSocketRequest)
            //    {
            //        WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
            //        Console.WriteLine("WebSocket Connected");
            //    }
            //    else
            //    {
            //        await next();
            //    }
            //});
            #endregion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            #region Chong DDos
            app.UseIpRateLimiting();
            #endregion
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();//token secure
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //run cron job
            //CronJob.CreateTakePictureJob(15);
            //CronJob.CreateRemindSetPriceJob(15);
            CronJob.CreateCheckCameraWorkJob(59);//59p check 1 lần
        }
    }
    internal static class StartupExtensions
    {
        public static IAzureClientBuilder<BlobServiceClient, BlobClientOptions> AddBlobServiceClient(this AzureClientFactoryBuilder builder, string serviceUriOrConnectionString, bool preferMsi)
        {
            if (preferMsi && Uri.TryCreate(serviceUriOrConnectionString, UriKind.Absolute, out Uri serviceUri))
            {
                return builder.AddBlobServiceClient(serviceUri);
            }
            else
            {
                return builder.AddBlobServiceClient(serviceUriOrConnectionString);
            }
        }
        public static IAzureClientBuilder<QueueServiceClient, QueueClientOptions> AddQueueServiceClient(this AzureClientFactoryBuilder builder, string serviceUriOrConnectionString, bool preferMsi)
        {
            if (preferMsi && Uri.TryCreate(serviceUriOrConnectionString, UriKind.Absolute, out Uri serviceUri))
            {
                return builder.AddQueueServiceClient(serviceUri);
            }
            else
            {
                return builder.AddQueueServiceClient(serviceUriOrConnectionString);
            }
        }
    }
}
