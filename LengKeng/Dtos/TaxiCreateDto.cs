﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class TaxiCreateDto
    {
        [Required]
        public Taxi Taxi { get; set; }
        [Required]
        public List<string> PlateNumbers { get; set; }
    }
}
