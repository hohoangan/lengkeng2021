﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusShelterBooking
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int InvoiceId { get; set; }
        [JsonIgnore]
        public Invoice Invoices { get; set; }
        [Required, price]
        public double Price { get; set; }
        [Required, Id]
        public int Amount { get; set; }
        [Required, Id]//Chi phí lắp đặt
        public double InstallationCost { get; set; }
        public int InstallationTimes { get; set; }
        [Required]
        public double DiscountPercent { get; set; }
        [Required]
        public double TotalFee { get; set; }
        [Required, Id]
        public int BusShelterId { get; set; }
        [JsonIgnore]
        public BusShelter BusShelters { get; set; }
        [Required, Id]
        public int UnitId { get; set; }
        [JsonIgnore]
        public Unit Units { get; set; }
        public double OTC { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
    }
}
