﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class DigitalTimeLine
    {
        public int BroadcastingTime { get; set; } //ex: 15 => 15h/day
        public IEnumerable<DigitalHourPerDay> bookings { get; set; }
        public IEnumerable<DigitalBillboardDownTime> downTimes { get; set; }
    }
    public class DigitalHourPerDay
    {
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Date { get; set; } = DateTime.Now;
        public double TotalHours { get; set; }
        public bool IsFull { get; set; }
    }
}
