﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusRoutePriceUpdateDto
    {
        [Required, price]
        public double Price { get; set; }
        [Required]
        public double PromotePercent { get; set; }
        [Required, Id]
        public int MinCarBooking { get; set; }//10 bus/month
        [Required, Id]
        public int BusTypeOfArtworkId { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
        [Required, Id]
        public int UnitId { get; set; }
        [Required, Id]
        public int BusRouteId { get; set; }
        [Required, Id]
        public int UserId { get; set; }
    }
}
