﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descriptione",
                table: "Campaigns");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Campaigns",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "BusRouteCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(nullable: false),
                    BusRouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusRouteCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusRouteCompanies_BusRoutes_BusRouteId",
                        column: x => x.BusRouteId,
                        principalTable: "BusRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusRouteCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusRouteCompanies_BusRouteId",
                table: "BusRouteCompanies",
                column: "BusRouteId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRouteCompanies_CompanyId",
                table: "BusRouteCompanies",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BusRouteCompanies");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Campaigns");

            migrationBuilder.AddColumn<string>(
                name: "Descriptione",
                table: "Campaigns",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
