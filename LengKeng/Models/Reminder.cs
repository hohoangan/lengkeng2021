﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Reminder
    {
        public int Id { get; set; }
        public int campaignId { get; set; }
        public int UserId { get; set; }
        public User Users { get; set; }
        public string Message { get; set; }
        public int StatusId { get; set; }
        public Status Statuses { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdate { get; set; } = DateTime.Now;
    }
}
