﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class TaxiAreaUpdateDto
    {
        [Required]
        public string Name { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
    }
}
