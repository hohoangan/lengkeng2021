﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusTimeLine
    {
        public IEnumerable<BusBooking> bookings { get; set; }
        public IEnumerable<BusDownTime> downTimes { get; set; }
    }
}
