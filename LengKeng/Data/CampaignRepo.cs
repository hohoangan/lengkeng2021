﻿using Azure;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace LengKeng.Data
{
    public class CampaignRepo : ICampaignRepo
    {
        private readonly LengKengDbContext _context;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;
        public CampaignRepo(LengKengDbContext context, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _localizer = localizer;
            _blobService = blobService;
        }
        public Booking CreateCampaign(Booking booking)
        {
            string mess = "Success";
            try
            {
                _context.Database.BeginTransaction();
                //1. Create Campaign
                var campaign = booking.Campaigns;
                var checkCampaign = _context.Campaigns.Where(x => x.Name == campaign.Name).FirstOrDefault();
                if (checkCampaign != null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.DuplicateName]);
                }
                var statusReserved = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Reserved).Select(x => x.Id).FirstOrDefault();
                campaign.StatusId = statusReserved;
                _context.Campaigns.Add(campaign);
                _context.SaveChanges();

                var statusNew = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.New).Select(x => x.Id).FirstOrDefault();
                var reminds = _context.UserRoles.Where(x => x.Roles.RoleName == Constant.ConstantRole.SuperUser && x.Users.Statuses.Name == Constant.ConstantStatus.Active)
                                       .Select(x => new Reminder
                                       {
                                           Message = _localizer[Constant.ConstantMessage.RemindNewCampaign, campaign.Name].Value,
                                           campaignId = campaign.Id,
                                           StatusId = statusNew,
                                           UserId = x.UserId
                                       });

                _context.Reminders.AddRange(reminds);
                _context.SaveChanges();
                //save invoice pdf
                var invoicepdf = booking.CampaignFile;
                invoicepdf.CampaignId = campaign.Id;
                _context.CampaignFiles.Add(invoicepdf);
                _context.SaveChanges();
                //2. Create Invoice
                var invoice = booking.Invoices;
                invoice.CampaignId = campaign.Id;
                invoice.LastUpdate = DateTime.Now;
                _context.Invoices.Add(invoice);
                _context.SaveChanges();

                #region//3. Add shelter booking
                var shelterList = booking.BusShelterBookings;
                shelterList?.ForEach(item =>
                {
                    #region Check Price
                    var price = (from pr in _context.BusShelterPrices.Where(x => (x.FromDate <= item.FromDate && item.FromDate <= x.ToDate))
                                                                     .Where(x => x.BusShelterId == item.BusShelterId)
                                                                     .ToList()
                                                                     .Where(x => CheckShelterPriceNextyNext(x, item.FromDate, item.ToDate) == true)
                                 select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                    var installCost = _context.BusShelterInstallationCosts.Where(x => x.Units.Name == Constant.ConstantUnit.m2).Select(x => x.Price).FirstOrDefault();

                    //var shelter = _context.BusShelters.Where(x => x.Id == item.BusShelterId).FirstOrDefault();
                    //var loc = _context.Locations.Where(x => x.LocationId == shelter.LocationId).FirstOrDefault();

                    if (price == null)
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundPrice]);
                    }
                    double dis = _context.BusShelterDiscounts.Where(x => x.Month == item.Amount)
                                                         .Where(x => x.BusShelterPriceId == price.Id)
                                                         .Select(x => x.Percent)
                                                         .FirstOrDefault();

                    if (price?.Price != item.Price || installCost != item.InstallationCost || dis != item.DiscountPercent)
                    {
                        item.Price = price?.Price ?? 0;
                        item.InstallationCost = installCost;
                        item.DiscountPercent = dis;
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.PriceChanged]);
                    }
                    #endregion

                    if (CheckBooking(item))
                    {
                        var bb = _context.BusShelters.Find(item.BusShelterId);
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Shelter", bb.Name]);
                    }
                    item.InvoiceId = invoice.Id;
                    //item.Invoices = null;
                    _context.BusShelterBookings.Add(item);
                    _context.SaveChanges();
                });
                #endregion

                #region//4. Add billboard booking
                var billboardList = booking.BillboardBookings;
                billboardList?.ForEach(item =>
                {
                    #region Check Price
                    var price = (from pr in _context.BillboardPrices.Where(x => (x.FromDate.Date <= item.FromDate.Date && item.FromDate.Date <= x.ToDate.Date))
                                                                     .Where(x => x.BillboardId == item.BillboardId)
                                                                     .ToList()
                                                                     .Where(x => CheckBillboardPriceNextyNext(x, item.FromDate, item.ToDate) == true)
                                 select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                    var installCost = _context.BillboardInstallationCosts.Where(x => x.Units.Name == Constant.ConstantUnit.m2).Select(x => x.Price).FirstOrDefault();

                    //var billboard = _context.Billboards.Where(x => x.Id == item.BillboardId).FirstOrDefault();
                    //var loc = _context.Locations.Where(x => x.LocationId == billboard.LocationId).FirstOrDefault();
                    if (price == null)
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundPrice]);
                    }
                    double dis = _context.BillboardDiscounts.Where(x => x.Month == item.Amount)
                                                         .Where(x => x.BillboardPriceId == price.Id)
                                                         //.Where(x => x.CompanyId == billboard.CompanyId)
                                                         //.Where(x => x.LocationId == loc.LocationId || x.LocationId == loc.ParentId)
                                                         .Select(x => x.Percent)
                                                         .FirstOrDefault();

                    var bb = _context.Billboards.Where(x => x.Id == item.BillboardId).Include(x=>x.Companies).AsNoTracking().FirstOrDefault();

                    if (price?.Price != item.Price || installCost != item.InstallationCost
                    || dis != item.DiscountPercent || price.PromotePercent != item.PromotePercent)
                    {
                        item.Price = price?.Price ?? 0;
                        item.InstallationCost = installCost;
                        item.DiscountPercent = dis;
                        item.Billboards = bb;
                        item.PromotePercent = price?.PromotePercent ?? 0;
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.PriceChanged]);
                    }
                    #endregion

                    var rs = CheckBooking(item);
                    if (rs)
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Billboard", bb.Name]);
                    }
                    item.InvoiceId = invoice.Id;
                    //item.Invoices = null;
                    _context.BillboardBookings.Add(item);
                    _context.SaveChanges();
                });
                #endregion

                #region//5. Add digital booking
                var digital = booking.DigitalBillboardBookings;
                digital?.ForEach(item =>
                {
                    var bb = _context.DigitalBillboards.Find(item.DigitalBillboardId);
                    if (CheckBooking(item))
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Digital", bb.Name]);
                    }

                    #region Check Price
                    var price = (from pr in _context.DigitalBillboardPrices.Where(x => (x.FromDate.Date <= item.FromDate.Date && item.FromDate.Date <= x.ToDate.Date))
                                                                            .Where(x => x.DigitalBillboardId == item.DigitalBillboardId)
                                                                            .ToList()
                                                                            .Where(x => CheckDigitalPriceNextyNext(x, item.FromDate, item.ToDate) == true)
                                 select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                    if (price == null)
                    {
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundPrice]);
                    }
                    if (price?.Price != item.Price)
                    {
                        item.Price = price?.Price ?? 0;
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.PriceChanged]);
                    }
                    #endregion

                    /*
                     * ktra tong so gio chay hang ngay cua digital
                     * neu >=16 bao loi
                     */
                    DateTime dt = item.FromDate;
                    while (dt <= item.ToDate)
                    {
                        if (CheckBookingDigitalFullTime(item, dt))
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotEnoughTime, bb.Name, dt.Date.ToString("yyyy-MM-dd")]);
                        }
                        dt = dt.AddDays(1);
                    }

                    item.InvoiceId = invoice.Id;
                    //item.Invoices = null;
                    _context.DigitalBillboardBookings.Add(item);
                    _context.SaveChanges();
                });
                #endregion

                #region //6. Add bus booking 
                int statusUnconfimedId = _context.Statuses.Where(t => t.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();
                var busList = booking.BusBookingInfoList;
                busList?.ForEach(item =>
                {
                    var bustype = _context.BusTypes.Find(item.BusTypeId);
                    //Lay danh sash taxi Available
                    var busAvailable = from bus in _context.Buses.Where(t => t.BusRouteId == item.BusRouteId
                                                                               && t.BusTypeId == item.BusTypeId
                                                                                && t.CompanyId == item.CompanyId
                                                                               && t.BusRoutes.LocationId == item.LocationId)
                                                                   //Not in Booking
                                                                   .Where(t => !(from booking in _context.BusBookings
                                                                                                  .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                                                                  .Where(b => b.FromDate <= item.BusBookings.ToDate
                                                                                                  && item.BusBookings.FromDate <= b.ToDate)
                                                                                 select booking.BusId)
                                                                               .Contains(t.Id))
                                                                   //Not in Downtime
                                                                   .Where(t => !(from downtime in _context.BusDownTimes
                                                                                                  .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                                                  .Where(b => b.FromDate <= item.BusBookings.ToDate
                                                                                                  && item.BusBookings.FromDate <= b.ToDate)
                                                                                 select downtime.BusId)
                                                                               .Contains(t.Id))
                                       join busRoute in _context.BusRoutes.Where(a => a.LocationId == item.LocationId) on bus.BusRouteId equals busRoute.Id
                                       select new
                                       {
                                           BusId = bus.Id,
                                           PlateNumber = bus.PlateNumber,
                                           BusRouteId = busRoute.Id,
                                           BusRouteName = busRoute.Name,
                                           OTC = bus.BusRoutes.OTC
                                       };
                    int count = busAvailable.Count();
                    if (count < item.CarAmount)
                    {
                        item.NumberOfVehicles = count;
                        string info = string.Format("{0} | {1} | {2}", bustype.Name, item.BusRouteName, item.CompanyName);
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotEnoughBus, info, item.CarAmount, count]);
                    }
                    else //insert vao taxibooking cho đến khỉ đủ taxi
                    {
                        #region check Price
                        var prices = (from pr in _context.BusRoutePrices.Where(x => (x.FromDate.Date <= item.BusBookings.FromDate.Date && item.BusBookings.FromDate.Date <= x.ToDate.Date))
                                                         .Where(x => x.BusRouteId == item.BusRouteId
                                                                        && x.BusTypeId == item.BusTypeId
                                                                        && x.BusTypeOfArtworkId == item.BusBookings.BusTypeOfArtworkId
                                                                        && x.MinCarBooking == item.MinCarBooking
                                                                        && x.CompanyId == item.CompanyId)
                                                         .ToList()
                                                         .Where(x => CheckBusPriceIsSequential(x, item.BusBookings.FromDate, item.BusBookings.ToDate) == true)
                                      select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                        var cost = _context.BusInstallationCosts.Where(x => x.BusTypeId == item.BusTypeId && x.BusTypeOfArtworkId == item.BusBookings.BusTypeOfArtworkId).FirstOrDefault();

                        if (prices == null || cost == null)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundPrice]);
                        }
                        else if (item.CarAmount < prices.MinCarBooking)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.CarBookingNotEnough]);
                        }
                        double dis = _context.BusDiscounts.Where(x => x.BusRoutePriceId == prices.Id && x.Month == item.BusBookings.Amount).Select(x => x.Percent).FirstOrDefault();

                        if (prices?.Price != item.BusBookings.Price || cost.Price != item.BusBookings.InstallationCost
                        || dis != item.BusBookings.DiscountPercent || prices.PromotePercent != item.BusBookings.PromotePercent)
                        {
                            item.BusBookings.Price = prices?.Price ?? 0;
                            item.BusBookings.InstallationCost = cost.Price;
                            item.BusBookings.DiscountPercent = dis;
                            item.BusBookings.PromotePercent = prices?.PromotePercent ?? 0; ;
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.PriceChanged]);
                        }
                        #endregion

                        int n = 0;
                        BusBooking bb = item.BusBookings;
                        bb.InvoiceId = invoice.Id;
                        bb.StatusId = statusUnconfimedId;
                        foreach (var tx in busAvailable.ToList())
                        {
                            if (n >= item.CarAmount)
                                break;
                            /*
                             * check bus lại 1 lần nữa
                             * nếu đã có booking hoặc downtime thì bỏ qua
                             * add đến khi đủ số lượng xe cần
                             * nếu ko đủ báo lỗi
                             */
                            bb.Id = 0;
                            bb.BusId = tx.BusId;
                            bb.OTC = tx.OTC;
                            //bb.Invoices = null;
                            if (CheckBooking(bb))
                                continue;
                            _context.BusBookings.Add(bb);
                            _context.SaveChanges();
                            n++;
                        }
                        if (n != item.CarAmount)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotEnoughBus, string.Format("{0}-{1}",item.BusRouteName, bustype.Name), item.CarAmount, n]);
                        }
                    }
                });
                #endregion

                #region //7. Add taxi booking
                var taxiList = booking.TaxiBookingInfoList;
                taxiList?.ForEach(item =>
                {
                    var txtype = _context.TaxiTypes.Find(item.TaxiTypeId);
                    //Lay danh sash taxi Available
                    var taxiArea = _context.TaxiAreas.Where(x => x.LocationId == item.LocationId && x.CompanyId == item.CompanyId).FirstOrDefault();
                    var taxiAvailable = from taxi in _context.Taxis.Where(t => t.CompanyId == item.CompanyId
                                                                            && t.TaxiTypeId == item.TaxiTypeId
                                                                            && t.TaxiAreaId == taxiArea.Id)
                                                                   //Not in Booking
                                                                   .Where(t => !(from booking in _context.TaxiBookings
                                                                                                  .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                                                                  .Where(b => b.FromDate <= item.TaxiBookings.ToDate
                                                                                                  && item.TaxiBookings.FromDate <= b.ToDate)
                                                                                 select booking.TaxiId)
                                                                               .Contains(t.Id))
                                                                   //Not in Downtime
                                                                   .Where(t => !(from downtime in _context.TaxiDownTimes
                                                                                                  .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                                                  .Where(b => b.FromDate <= item.TaxiBookings.ToDate
                                                                                                  && item.TaxiBookings.FromDate <= b.ToDate)
                                                                                 select downtime.TaxiId)
                                                                               .Contains(t.Id))
                                        join area in _context.TaxiAreas.Where(a => a.LocationId == item.LocationId) on taxi.TaxiAreaId equals area.Id
                                        //into TaxiAreas from m in TaxiAreas.DefaultIfEmpty()
                                        select new
                                        {
                                            TaxiId = taxi.Id,
                                            PlateNumber = taxi.PlateNumber,
                                            TaxiAreaId = area.Id,
                                            TaxiAreaName = area.Name,
                                            OTC = taxi.TaxiAreas.OTC
                                        };
                    int count = taxiAvailable.Count();
                    if (count < item.CarAmount)
                    {
                        item.NumberOfVehicles = count;
                        string info = string.Format("{0} | {1}", txtype.Name, item.TaxiAreaName);
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotEnoughTaxi, info, item.CarAmount, count]);
                    }
                    else //insert vao taxibooking cho đến khỉ đủ taxi
                    {
                        #region Check Price
                        var prices = (from pr in _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= item.TaxiBookings.FromDate.Date && item.TaxiBookings.FromDate.Date <= x.ToDate.Date))
                                                                        .Where(x => x.TaxiAreaId == item.TaxiAreaId
                                                                                    && x.TaxiTypeId == item.TaxiTypeId
                                                                                    && x.TaxiTypeOfArtworkId == item.TaxiBookings.TaxiTypeOfArtworkId
                                                                                    && x.MinCarBooking == item.MinCarBooking)
                                                                        .ToList()
                                                                        .Where(x => CheckTaxiPriceIsSequential(x, item.TaxiBookings.FromDate, item.TaxiBookings.ToDate) == true)
                                      select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                        var cost = _context.TaxiInstallationCosts.Where(x => x.TaxiTypeId == item.TaxiTypeId && x.TaxiTypeOfArtworkId == item.TaxiBookings.TaxiTypeOfArtworkId).FirstOrDefault();

                        if (prices == null || cost == null)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundPrice]);
                        }
                        else if (item.CarAmount < prices.MinCarBooking)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.CarBookingNotEnough]);
                        }

                        double dis = _context.TaxiDiscounts.Where(x => x.TaxiAreaPriceId == prices.Id && x.Month == item.TaxiBookings.Amount).Select(x => x.Percent).FirstOrDefault();

                        if (prices?.Price != item.TaxiBookings.Price || cost.Price != item.TaxiBookings.InstallationCost
                        || dis != item.TaxiBookings.DiscountPercent || prices.PromotePercent != item.TaxiBookings.PromotePercent)
                        {
                            item.TaxiBookings.Price = prices?.Price ?? 0;
                            item.TaxiBookings.InstallationCost = cost?.Price ?? 0;
                            item.TaxiBookings.DiscountPercent = dis;
                            item.TaxiBookings.PromotePercent = prices?.PromotePercent ?? 0;
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.PriceChanged]);
                        }
                        #endregion

                        int n = 0;
                        TaxiBooking tb = item.TaxiBookings;
                        tb.InvoiceId = invoice.Id;
                        tb.StatusId = statusUnconfimedId;
                        foreach (var tx in taxiAvailable.ToList())
                        {
                            if (n >= item.CarAmount)
                                break;
                            /*
                             * check taxi lại 1 lần nữa
                             * nếu đã có booking hoặc downtime thì bỏ qua
                             * add đến khi đủ số lượng xe cần
                             * nếu ko đủ báo lỗi
                             */
                            tb.Id = 0;
                            tb.TaxiId = tx.TaxiId;
                            tb.OTC = tx.OTC;
                            //tb.Invoices = null;
                            if (CheckBooking(tb))
                                continue;
                            _context.TaxiBookings.Add(tb);
                            _context.SaveChanges();
                            n++;
                        }
                        if (n != item.CarAmount)
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotEnoughTaxi, string.Format("taxis {0}-{1}", item.TaxiAreaName, txtype.Name), item.CarAmount, n]);
                        }
                    }
                });
                #endregion

                _context.Database.CommitTransaction();
                booking.Campaigns = campaign;
                return booking;
            }
            catch (Exception ex)
            {
                //del file invoice pdf
                _blobService.DeleteBlobAsync(Path.GetFileName(booking.CampaignFile.Url));
                _context.Database.RollbackTransaction();
                //save tracking
                mess = ex?.InnerException?.Message ?? ex.Message;
                if ((mess).Equals(_localizer[Constant.ConstantMessage.PriceChanged]))
                {
                    booking.Campaigns.Id = 0;
                    return booking;
                }
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = booking.Campaigns.Id,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(booking),
                    UserId = booking.Campaigns.UserId
                });
            }
        }
        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        #region CHECK PRICE/AMOUNT IN CART BEFORE BOOKING
        public List<CheckBookingCart> CheckCartBeforeBooking(Booking booking)
        {
            List<CheckBookingCart> lst = new List<CheckBookingCart>();
            try
            {
                //bus
                CheckBookingCart busck = new CheckBookingCart
                {
                    Type = "bus",
                    CheckBookings = new List<CheckBooking>()
                };
                var busList = booking.BusBookingInfoList;
                busList?.ForEach(item =>
                {
                    CheckBooking bus = CheckBusBeforeBooking(item);
                    busck.CheckBookings.Add(bus);
                });
                lst.Add(busck);

                //taxi
                CheckBookingCart txck = new CheckBookingCart
                {
                    Type = "taxi",
                    CheckBookings = new List<CheckBooking>()
                };
                var taxiList = booking.TaxiBookingInfoList;
                taxiList?.ForEach(item =>
                {
                    CheckBooking tx = CheckTaxiBeforeBooking(item);
                    txck.CheckBookings.Add(tx);
                });
                lst.Add(txck);

                //BB
                CheckBookingCart bbck = new CheckBookingCart
                {
                    Type = "billboard",
                    CheckBookings = new List<CheckBooking>()
                };
                var billboardList = booking.BillboardBookings;
                billboardList?.ForEach(item => {
                    CheckBooking bb = CheckBillboardBeforeBooking(item);
                    bbck.CheckBookings.Add(bb);
                });
                lst.Add(bbck);
                return lst;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public CheckBooking CheckTaxiBeforeBooking(TaxiBookingInfo item)
        {
            CheckBooking checkBooking = new CheckBooking
            {
                StatusCode = StatusCodes.Status200OK
            };
            try
            {
                var txtype = _context.TaxiTypes.Find(item.TaxiTypeId);
                //Lay danh sash taxi Available
                var taxiArea = _context.TaxiAreas.Where(x => x.LocationId == item.LocationId && x.CompanyId == item.CompanyId).FirstOrDefault();
                var taxiAvailable = from taxi in _context.Taxis.Where(t => t.CompanyId == item.CompanyId
                                                                        && t.TaxiTypeId == item.TaxiTypeId
                                                                        && t.TaxiAreaId == taxiArea.Id)
                                                               //Not in Booking
                                                               .Where(t => !(from booking in _context.TaxiBookings
                                                                                              .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                                                              .Where(b => b.FromDate <= item.TaxiBookings.ToDate
                                                                                              && item.TaxiBookings.FromDate <= b.ToDate)
                                                                             select booking.TaxiId)
                                                                           .Contains(t.Id))
                                                               //Not in Downtime
                                                               .Where(t => !(from downtime in _context.TaxiDownTimes
                                                                                              .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                                              .Where(b => b.FromDate <= item.TaxiBookings.ToDate
                                                                                              && item.TaxiBookings.FromDate <= b.ToDate)
                                                                             select downtime.TaxiId)
                                                                           .Contains(t.Id))
                                    join area in _context.TaxiAreas.Where(a => a.LocationId == item.LocationId) on taxi.TaxiAreaId equals area.Id
                                    //into TaxiAreas from m in TaxiAreas.DefaultIfEmpty()
                                    select new
                                    {
                                        TaxiId = taxi.Id,
                                        PlateNumber = taxi.PlateNumber,
                                        TaxiAreaId = area.Id,
                                        TaxiAreaName = area.Name,
                                        OTC = taxi.TaxiAreas.OTC
                                    };
                int count = taxiAvailable.Count();
                if (count < item.CarAmount)
                {
                    item.NumberOfVehicles = count;
                    checkBooking.Item = item;
                    checkBooking.StatusCode = StatusCodes.Status400BadRequest;
                    checkBooking.KindOf400 = Constant.ConstantMessage.NotEnoughCar;
                    return checkBooking;
                }
                else //insert vao taxibooking cho đến khỉ đủ taxi
                {
                    #region Check Price
                    var prices = (from pr in _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= item.TaxiBookings.FromDate.Date && item.TaxiBookings.FromDate.Date <= x.ToDate.Date))
                                                                    .Where(x => x.TaxiAreaId == item.TaxiAreaId
                                                                                && x.TaxiTypeId == item.TaxiTypeId
                                                                                && x.TaxiTypeOfArtworkId == item.TaxiBookings.TaxiTypeOfArtworkId
                                                                                && x.MinCarBooking == item.MinCarBooking)
                                                                    .ToList()
                                                                    .Where(x => CheckTaxiPriceIsSequential(x, item.TaxiBookings.FromDate, item.TaxiBookings.ToDate) == true)
                                  select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                    var cost = _context.TaxiInstallationCosts.Where(x => x.TaxiTypeId == item.TaxiTypeId && x.TaxiTypeOfArtworkId == item.TaxiBookings.TaxiTypeOfArtworkId).FirstOrDefault();

                    if (prices == null || cost == null)
                    {
                        checkBooking.StatusCode = StatusCodes.Status404NotFound;
                        checkBooking.Message404 = _localizer[Constant.ConstantMessage.NotFoundPrice].Value;
                        return checkBooking;
                    }
                    else if (item.CarAmount < prices.MinCarBooking)
                    {
                        checkBooking.StatusCode = StatusCodes.Status404NotFound;
                        checkBooking.Message404 = _localizer[Constant.ConstantMessage.CarBookingNotEnough].Value;
                        return checkBooking;
                    }

                    double dis = _context.TaxiDiscounts.Where(x => x.TaxiAreaPriceId == prices.Id && x.Month == item.TaxiBookings.Amount).Select(x => x.Percent).FirstOrDefault();

                    if (prices?.Price != item.TaxiBookings.Price || cost.Price != item.TaxiBookings.InstallationCost
                    || dis != item.TaxiBookings.DiscountPercent || prices.PromotePercent != item.TaxiBookings.PromotePercent)
                    {
                        item.TaxiBookings.Price = prices?.Price ?? 0;
                        item.TaxiBookings.InstallationCost = cost?.Price ?? 0;
                        item.TaxiBookings.DiscountPercent = dis;
                        item.TaxiBookings.PromotePercent = prices?.PromotePercent ?? 0;
                        //Price changed
                        checkBooking.Item = item;
                        checkBooking.StatusCode = StatusCodes.Status400BadRequest;
                        checkBooking.KindOf400 = Constant.ConstantMessage.PriceChanged;
                        return checkBooking;
                    }
                    #endregion
                }
                return checkBooking;
            }
            catch (Exception ex)
            {
                checkBooking.StatusCode = StatusCodes.Status404NotFound;
                checkBooking.Message404 = ex?.InnerException?.Message ?? ex.Message;
                return checkBooking;
            }
        }

        public CheckBooking CheckBusBeforeBooking(BusBookingInfo item)
        {
            CheckBooking checkBooking = new CheckBooking
            {
                StatusCode = StatusCodes.Status200OK
            };
            try
            {
                var bustype = _context.BusTypes.Find(item.BusTypeId);
                //Lay danh sash taxi Available
                var busAvailable = from bus in _context.Buses.Where(t => t.BusRouteId == item.BusRouteId
                                                                           && t.BusTypeId == item.BusTypeId
                                                                            && t.CompanyId == item.CompanyId
                                                                           && t.BusRoutes.LocationId == item.LocationId)
                                                               //Not in Booking
                                                               .Where(t => !(from booking in _context.BusBookings
                                                                                              .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                                                              .Where(b => b.FromDate <= item.BusBookings.ToDate
                                                                                              && item.BusBookings.FromDate <= b.ToDate)
                                                                             select booking.BusId)
                                                                           .Contains(t.Id))
                                                               //Not in Downtime
                                                               .Where(t => !(from downtime in _context.BusDownTimes
                                                                                              .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                                              .Where(b => b.FromDate <= item.BusBookings.ToDate
                                                                                              && item.BusBookings.FromDate <= b.ToDate)
                                                                             select downtime.BusId)
                                                                           .Contains(t.Id))
                                   join busRoute in _context.BusRoutes.Where(a => a.LocationId == item.LocationId) on bus.BusRouteId equals busRoute.Id
                                   select new
                                   {
                                       BusId = bus.Id,
                                       PlateNumber = bus.PlateNumber,
                                       BusRouteId = busRoute.Id,
                                       BusRouteName = busRoute.Name,
                                       OTC = bus.BusRoutes.OTC
                                   };
                int count = busAvailable.Count();
                if (count < item.CarAmount)
                {
                    item.NumberOfVehicles = count;
                    checkBooking.Item = item;
                    checkBooking.StatusCode = StatusCodes.Status400BadRequest;
                    checkBooking.KindOf400 = Constant.ConstantMessage.NotEnoughCar;
                    return checkBooking;
                }
                else //insert vao taxibooking cho đến khỉ đủ taxi
                {
                    #region check Price
                    var prices = (from pr in _context.BusRoutePrices.Where(x => (x.FromDate.Date <= item.BusBookings.FromDate.Date && item.BusBookings.FromDate.Date <= x.ToDate.Date))
                                                     .Where(x => x.BusRouteId == item.BusRouteId
                                                                    && x.BusTypeId == item.BusTypeId
                                                                    && x.BusTypeOfArtworkId == item.BusBookings.BusTypeOfArtworkId
                                                                    && x.MinCarBooking == item.MinCarBooking
                                                                    && x.CompanyId == item.CompanyId)
                                                     .ToList()
                                                     .Where(x => CheckBusPriceIsSequential(x, item.BusBookings.FromDate, item.BusBookings.ToDate) == true)
                                  select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                    var cost = _context.BusInstallationCosts.Where(x => x.BusTypeId == item.BusTypeId && x.BusTypeOfArtworkId == item.BusBookings.BusTypeOfArtworkId).FirstOrDefault();

                    if (prices == null || cost == null)
                    {
                        checkBooking.StatusCode = StatusCodes.Status404NotFound;
                        checkBooking.Message404 = _localizer[Constant.ConstantMessage.NotFoundPrice].Value;
                        return checkBooking;
                    }
                    else if (item.CarAmount < prices.MinCarBooking)
                    {
                        checkBooking.StatusCode = StatusCodes.Status404NotFound;
                        checkBooking.Message404 = _localizer[Constant.ConstantMessage.CarBookingNotEnough].Value;
                        return checkBooking;
                    }

                    double dis = _context.BusDiscounts.Where(x => x.BusRoutePriceId == prices.Id && x.Month == item.BusBookings.Amount).Select(x => x.Percent).FirstOrDefault();

                    if (prices?.Price != item.BusBookings.Price || cost.Price != item.BusBookings.InstallationCost
                    || dis != item.BusBookings.DiscountPercent || prices.PromotePercent != item.BusBookings.PromotePercent)
                    {
                        item.BusBookings.Price = prices?.Price ?? 0;
                        item.BusBookings.InstallationCost = cost.Price;
                        item.BusBookings.DiscountPercent = dis;
                        item.BusBookings.PromotePercent = prices?.PromotePercent ?? 0; ;
                        //Price changed
                        checkBooking.Item = item;
                        checkBooking.StatusCode = StatusCodes.Status400BadRequest;
                        checkBooking.KindOf400 = Constant.ConstantMessage.PriceChanged;
                        return checkBooking;
                    }
                    #endregion
                }
                return checkBooking;
            }
            catch (Exception ex)
            {
                checkBooking.StatusCode = StatusCodes.Status404NotFound;
                checkBooking.Message404 = ex?.InnerException?.Message ?? ex.Message;
                return checkBooking;
            }
        }

        public CheckBooking CheckBillboardBeforeBooking(BillboardBooking item)
        {
            CheckBooking checkBooking = new CheckBooking
            {
                StatusCode = StatusCodes.Status200OK
            };
            try
            {
                #region Check Price
                var price = (from pr in _context.BillboardPrices.Where(x => (x.FromDate.Date <= item.FromDate.Date && item.FromDate.Date <= x.ToDate.Date))
                                                                 .Where(x => x.BillboardId == item.BillboardId)
                                                                 .ToList()
                                                                 .Where(x => CheckBillboardPriceNextyNext(x, item.FromDate, item.ToDate) == true)
                             select pr).OrderByDescending(x => x.Price).FirstOrDefault();

                var installCost = _context.BillboardInstallationCosts.Where(x => x.Units.Name == Constant.ConstantUnit.m2).Select(x => x.Price).FirstOrDefault();

                if (price == null)
                {
                    checkBooking.StatusCode = StatusCodes.Status404NotFound;
                    checkBooking.Message404 = _localizer[Constant.ConstantMessage.NotFoundPrice].Value;
                    return checkBooking;
                }
                double dis = _context.BillboardDiscounts.Where(x => x.Month == item.Amount)
                                                     .Where(x => x.BillboardPriceId == price.Id)
                                                     .Select(x => x.Percent)
                                                     .FirstOrDefault();

                var bb = _context.Billboards.Where(x => x.Id == item.BillboardId).Include(x=>x.Companies).AsNoTracking().FirstOrDefault();

                if (price?.Price != item.Price || installCost != item.InstallationCost
                || dis != item.DiscountPercent || price.PromotePercent != item.PromotePercent)
                {
                    item.Price = price?.Price ?? 0;
                    item.InstallationCost = installCost;
                    item.DiscountPercent = dis;
                    item.Billboards = bb;
                    item.PromotePercent = price?.PromotePercent ?? 0;
                    //Price changed
                    checkBooking.Item = item;
                    checkBooking.StatusCode = StatusCodes.Status400BadRequest;
                    checkBooking.KindOf400 = Constant.ConstantMessage.PriceChanged;
                    return checkBooking;
                }
                #endregion
                var rs = CheckBooking(item);
                if (rs)
                {
                    checkBooking.StatusCode = StatusCodes.Status404NotFound;
                    checkBooking.Message404 = _localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Billboard", bb.Name].Value;
                    return checkBooking;
                }
                return null;
            }
            catch (Exception ex)
            {
                checkBooking.StatusCode = StatusCodes.Status404NotFound;
                checkBooking.Message404 = ex?.InnerException?.Message ?? ex.Message;
                return checkBooking;
            }
        }
        #endregion  

        #region CHECK EXISTS BOOKING AND DOWNTIME
        public bool CheckBooking(BillboardBooking billboardBooking)
        {
            try
            {
                var bbbooking = _context.BillboardBookings.Where(b => b.BillboardId == billboardBooking.BillboardId)
                                                            .Where(bk => bk.FromDate <= billboardBooking.ToDate
                                                                  && billboardBooking.FromDate <= bk.ToDate)
                                                            .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();

                var bbdowntime = _context.BillboardDownTimes.Where(b => b.BillboardId == billboardBooking.BillboardId)
                                                            .Where(bk => bk.FromDate <= billboardBooking.ToDate
                                                                  && billboardBooking.FromDate <= bk.ToDate)
                                                            .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();
                if (bbbooking > 0 || bbdowntime > 0)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool CheckBooking(BusShelterBooking booking)
        {
            try
            {
                var bbbooking = _context.BusShelterBookings.Where(b => b.BusShelterId == booking.BusShelterId)
                                                           .Where(bk => bk.FromDate <= booking.ToDate && booking.FromDate <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();
                var bbdowntime = _context.BusShelterDownTimes.Where(b => b.BusShelterId == booking.BusShelterId)
                                                           .Where(bk => bk.FromDate <= booking.ToDate && booking.FromDate <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();
                if (bbbooking > 0 || bbdowntime > 0)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool CheckBooking(DigitalBillboardBooking booking)
        {
            try
            {
                var bbdowntime = _context.DigitalBillboardDownTimes.Where(b => b.DigitalBillboardId == booking.DigitalBillboardId)
                                                           .Where(bk => bk.FromDate <= booking.ToDate && booking.FromDate <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();
                if (bbdowntime > 0)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool CheckBookingDigitalFullTime(DigitalBillboardBooking booking, DateTime dt)
        {
            try
            {
                var bbbooking = _context.DigitalBillboardBookings.Where(b => b.DigitalBillboardId == booking.DigitalBillboardId)
                                                           .Where(bk => bk.FromDate <= dt && dt <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                           .GroupBy(b => b.DigitalBillboardId)
                                                           .Select(x => new
                                                           {
                                                               DigitalBillboardId = x.Key,
                                                               total = x.Sum(t => t.HourPerDay)
                                                           }).FirstOrDefault();

                var broadcastingTime = _context.DigitalBillboards.Where(b => b.Id == booking.DigitalBillboardId).FirstOrDefault();
                if (bbbooking?.total + booking.HourPerDay > broadcastingTime?.BroadcastingTime)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool CheckBooking(TaxiBooking booking)
        {
            try
            {
                var bbbooking = _context.TaxiBookings.Where(b => b.TaxiId == booking.TaxiId)
                                                           .Where(bk => bk.FromDate <= booking.ToDate && booking.FromDate <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed).Count();
                var bbdowntime = _context.TaxiDownTimes.Where(b => b.TaxiId == booking.TaxiId)
                                                           .Where(bk => bk.FromDate <= booking.ToDate && booking.FromDate <= bk.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active).Count();
                if (bbbooking > 0 || bbdowntime > 0)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool CheckBooking(BusBooking booking)
        {
            try
            {
                var bbbooking = _context.BusBookings.Where(b => b.BusId == booking.BusId)
                                                           .Where(b => b.FromDate <= booking.ToDate && booking.FromDate <= b.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                           .Count();
                var bbdowntime = _context.BusDownTimes.Where(b => b.BusId == booking.BusId)
                                                           .Where(b => b.FromDate <= booking.ToDate && booking.FromDate <= b.ToDate)
                                                           .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                           .Count();
                if (bbbooking > 0 || bbdowntime > 0)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
        #endregion

        #region UPDATE CAMPAIGN
        //public void AddTaxiBooking(int campaignId, string plateNumber)
        //{
        //    try
        //    {
        //        _context.Database.BeginTransaction();
        //        /*
        //         * ktra thong tin taxi nhap vao
        //         * neu co thi check co nam trong booking hay downtime ko
        //         */
        //        var taxi = _context.Taxis.Where(t => t.PlateNumber == plateNumber).FirstOrDefault();
        //        if (taxi == null)
        //            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundTaxi]);
        //        // lay danh sach taxi cua INVOICE
        //        var taxiAvailable = from inv in _context.Invoices.Where(i => i.CampaignId == campaignId)
        //                            join taxibk in _context.TaxiBookings.Where(t => t.Statuses.Name == Constant.ConstantStatus.Unconfirmed) on inv.Id equals taxibk.InvoiceId
        //                            join tx in _context.Taxis.Where(t => t.CompanyId == taxi.CompanyId
        //                                                                            && t.TaxiAreaId == taxi.TaxiAreaId
        //                                                                            && t.TaxiTypeId == taxi.TaxiTypeId) on taxibk.TaxiId equals tx.Id
        //                            select new
        //                            {
        //                                InvoiceId = inv.Id,
        //                                TaxiId = tx.Id,
        //                                PlateNumber = tx.PlateNumber,
        //                                FromDate = taxibk.FromDate,
        //                                ToDate = taxibk.ToDate,
        //                                BookingId = taxibk.Id
        //                            };
        //        /*
        //         * tim danh sach taxi phu hop voi TAXI nhap vao de doi trong danh sach taxiAvailable
        //         * cung company + area + type va status = unconfirmed
        //         * Neu danh sach taxiAvailable > 0
        //             *[1] neu taxi nam trong danh sach cua campain thi chuyen status = Actice
        //             *[2] neu chua co trong danh sach thi thay cho 1 taxi bat ky, chuyen status = Active
        //         * thong bao khong the doi
        //         */
        //        if (taxiAvailable?.Count() > 0)
        //        {
        //            //tim taxi co trong danh sach cua TAXI BOOKING thuoc campaign hay chua?
        //            var findTaxi = taxiAvailable.Where(x => x.TaxiId == taxi.Id).FirstOrDefault();
        //            int statusActive = _context.Statuses.Where(s => s.Name == Constant.ConstantStatus.Active).Single()?.Id ?? 0;
        //            if (findTaxi != null)
        //            {
        //                //[1] 
        //                var updateTaxi = _context.TaxiBookings.Where(x => x.Id == findTaxi.BookingId).FirstOrDefault();
        //                updateTaxi.StatusId = statusActive;
        //                _context.SaveChanges();
        //            }
        //            else
        //            {
        //                //ktra taxi muon doi hien tai co available ko?
        //                if (CheckBooking(new TaxiBooking
        //                {
        //                    TaxiId = taxi.Id,
        //                    FromDate = taxiAvailable.ToArray()[0].FromDate,
        //                    ToDate = taxiAvailable.ToArray()[0].ToDate
        //                }))
        //                {
        //                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Taxi", plateNumber]);
        //                }
        //                //[2]
        //                var firstTaxiAvailable = _context.TaxiBookings.Where(x => x.Id == taxiAvailable.ToArray()[0].BookingId).FirstOrDefault();
        //                firstTaxiAvailable.StatusId = statusActive;
        //                firstTaxiAvailable.TaxiId = taxi.Id;
        //                _context.SaveChanges();
        //            }
        //        }
        //        else
        //        {
        //            throw new NotImplementedException(_localizer[Constant.ConstantMessage.TaxiNotSuitable]);
        //        }

        //        _context.Database.CommitTransaction();
        //    }
        //    catch (Exception ex)
        //    {
        //        _context.Database.RollbackTransaction();
        //        throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //public void AddBusBooking(int campaignId, string plateNumber)
        //{
        //    try
        //    {
        //        _context.Database.BeginTransaction();
        //        /*
        //         * ktra thong tin bus nhap vao
        //         * neu co thi check co nam trong booking hay downtime ko
        //         */
        //        var bus = _context.Buses.Where(t => t.PlateNumber == plateNumber).FirstOrDefault();
        //        if (bus == null)
        //            throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundBus]);
        //        // lay danh sach bus cua INVOICE
        //        var busAvailable = from inv in _context.Invoices.Where(i => i.CampaignId == campaignId)
        //                           join busbk in _context.BusBookings.Where(t => t.Statuses.Name == Constant.ConstantStatus.Unconfirmed) on inv.Id equals busbk.InvoiceId
        //                           join bs in _context.Buses.Where(t => t.CompanyId == bus.CompanyId
        //                                                                           && t.BusRouteId == bus.BusRouteId
        //                                                                           && t.BusTypeId == bus.BusTypeId) on busbk.BusId equals bs.Id
        //                           select new
        //                           {
        //                               InvoiceId = inv.Id,
        //                               BusId = bs.Id,
        //                               PlateNumber = bs.PlateNumber,
        //                               FromDate = busbk.FromDate,
        //                               ToDate = busbk.ToDate,
        //                               BookingId = busbk.Id
        //                           };
        //        /*
        //         * tim danh sach bus phu hop voi xe BUS nhap vao de doi trong danh sach busAvailable
        //         * cung company + route + type va status = unconfirmed
        //         * Neu danh sach busAvailable > 0
        //             *[1] neu bus nam trong danh sach cua campain thi chuyen status = Actice
        //             *[2] neu chua co trong danh sach thi thay cho 1 bus bat ky, chuyen status = Active
        //         * thong bao khong the doi
        //         */
        //        if (busAvailable?.Count() > 0)
        //        {
        //            //tim bus co trong danh sach cua BUS BOOKING thuoc campaign hay chua?
        //            var findBus = busAvailable.Where(x => x.BusId == bus.Id).FirstOrDefault();
        //            int statusActive = _context.Statuses.Where(s => s.Name == Constant.ConstantStatus.Active).Single()?.Id ?? 0;
        //            if (findBus != null)
        //            {
        //                //[1] 
        //                var updateBus = _context.BusBookings.Where(x => x.Id == findBus.BookingId).FirstOrDefault();
        //                updateBus.StatusId = statusActive;
        //                _context.SaveChanges();
        //            }
        //            else
        //            {
        //                //ktra bus muon doi hien tai co available ko?
        //                if (CheckBooking(new BusBooking
        //                {
        //                    BusId = bus.Id,
        //                    FromDate = busAvailable.ToArray()[0].FromDate,
        //                    ToDate = busAvailable.ToArray()[0].ToDate
        //                }))
        //                {
        //                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Bus", plateNumber]);
        //                }
        //                //[2]
        //                var firstBusAvailable = _context.BusBookings.Where(x => x.Id == busAvailable.ToArray()[0].BookingId).FirstOrDefault();
        //                firstBusAvailable.StatusId = statusActive;
        //                firstBusAvailable.BusId = bus.Id;
        //                _context.SaveChanges();
        //            }
        //        }
        //        else
        //        {
        //            throw new NotImplementedException(_localizer[Constant.ConstantMessage.BusNotSuitable]);
        //        }

        //        _context.Database.CommitTransaction();
        //    }
        //    catch (Exception ex)
        //    {
        //        _context.Database.RollbackTransaction();
        //        throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        public TaxiBooking MatchTaxi(int bookingId, string plateNumber, int userId)
        {
            string mess = string.Empty;
            try
            {
                _context.Database.BeginTransaction();
                /*
                 * [1] ktra thong tin taxi nhap vao
                 * [2] neu co thi check co nam trong booking hay downtime ko
                 */
                var taxi = _context.Taxis.Where(t => t.PlateNumber == plateNumber)
                                       .FirstOrDefault();

                var booking = _context.TaxiBookings.Where(x => x.Id == bookingId).FirstOrDefault();
                var taxiOld = _context.Taxis.Where(t => t.Id == booking.TaxiId).FirstOrDefault();
                //neu xe moi ko co, ma so hien tai la so ao, thi đổi biển số
                if (taxi == null && taxiOld.PlateNumber.Contains("XXXX"))
                {
                    taxiOld.PlateNumber = plateNumber;
                    _context.SaveChanges();
                }
                else if (taxi == null)
                {
                    //khong tim thay thong tin xe
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundTaxi]);
                }
                /*
                 * tim taxi co trong danh sach cua TAXI BOOKING thuoc campaign hay chua?
                 * nếu có trong booking có 2 trường hợp
                 * - tìm trong chung invoice trước nếu có thì thay thế
                 * - ko có thì mở rộng ra các invoice khác
                 * nếu trùng thi change status
                 * cuối cùng thì tìm xe ngoài booking
                 * */
                int statusActive = _context.Statuses.Where(s => s.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var bookingCheckSameInvoiceId = _context.TaxiBookings
                                                        .Where(x => x.InvoiceId == booking.InvoiceId)
                                                        .Where(x => x.TaxiId == taxi.Id)
                                                        .Where(x => x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                        .Where(t => t.Taxis.CompanyId == taxiOld.CompanyId
                                                                    && t.Taxis.TaxiAreaId == taxiOld.TaxiAreaId
                                                                    && t.Taxis.TaxiTypeId == taxiOld.TaxiTypeId)
                                                        .FirstOrDefault();
                var bookingCheck = _context.TaxiBookings
                                                        //.Where(x => x.InvoiceId == booking.InvoiceId)
                                                        .Where(x => x.TaxiId == taxi.Id)
                                                        .Where(x => x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                        .Where(t => t.Taxis.CompanyId == taxiOld.CompanyId
                                                                    && t.Taxis.TaxiAreaId == taxiOld.TaxiAreaId
                                                                    && t.Taxis.TaxiTypeId == taxiOld.TaxiTypeId)
                                                        .FirstOrDefault();
                if (bookingCheckSameInvoiceId != null)
                {
                    /*
                     * tìm trong chung 1 booking invoice
                     * doi Id 2 then do
                     */
                    booking.StatusId = statusActive;
                    booking.TaxiId = taxi.Id;
                    bookingCheckSameInvoiceId.TaxiId = taxiOld.Id;
                    _context.SaveChanges();
                }
                else if (bookingCheck != null)
                {
                    /*
                     * neu tim ra chinh no hoac then khac phu hop trong list booking
                     * doi Id 2 then do
                     */
                    booking.StatusId = statusActive;
                    booking.TaxiId = taxi.Id;
                    bookingCheck.TaxiId = taxiOld.Id;
                    _context.SaveChanges();
                }
                else
                {
                    //[1]
                    var taxiNew = _context.Taxis.Where(t => t.PlateNumber == plateNumber)
                                             .Where(t => t.CompanyId == taxiOld.CompanyId
                                                    && t.TaxiAreaId == taxiOld.TaxiAreaId
                                                    && t.TaxiTypeId == taxiOld.TaxiTypeId)
                                            .FirstOrDefault();
                    if (taxiNew == null)
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.TaxiNotSuitable]);

                    //[2] ktra taxi muon doi hien tai co available ko?
                    else
                    {
                        if (CheckBooking(new TaxiBooking
                        {
                            TaxiId = taxiNew.Id,
                            FromDate = booking.FromDate,
                            ToDate = booking.ToDate
                        }))
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Taxi", plateNumber]);
                        }
                        else
                        {
                            booking.StatusId = statusActive;
                            booking.TaxiId = taxiNew.Id;
                            _context.SaveChanges();
                        }
                    }
                }
                _context.Database.CommitTransaction();
                return booking;
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                //save tracking
                mess = ex?.InnerException?.Message ?? ex.Message;
                throw new NotImplementedException(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = bookingId,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("bookingId: {0}, plateNumber: {1}", bookingId, plateNumber),
                    UserId = userId
                });
            }
        }

        public BusBooking MatchBus(int bookingId, string plateNumber, int userId)
        {
            string mess = string.Empty;
            try
            {
                _context.Database.BeginTransaction();
                /*
                 * [1] ktra thong tin bus nhap vao
                 * [2] neu co thi check co nam trong booking hay downtime ko
                 */
                var bus = _context.Buses.Where(t => t.PlateNumber == plateNumber)
                                       .FirstOrDefault();

                var booking = _context.BusBookings.Where(x => x.Id == bookingId).FirstOrDefault();
                var busOld = _context.Buses.Where(t => t.Id == booking.BusId).FirstOrDefault();
                //neu xe moi ko co, ma so hien tai la so ao, thi đổi biển số
                if (bus == null && busOld.PlateNumber.Contains("XXXX"))
                {
                    busOld.PlateNumber = plateNumber;
                    _context.SaveChanges();
                }
                else if (bus == null)
                {
                    //khong tim thay thong tin xe
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.NotFoundBus]);
                }
                /*
                 * tim bus co trong danh sach cua BUS BOOKING thuoc campaign hay chua?
                 * nếu trùng thi change status
                 * */
                int statusActive = _context.Statuses.Where(s => s.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var bookingCheckSameInvoiceId = _context.BusBookings
                                                        .Where(x => x.InvoiceId == booking.InvoiceId)
                                                        .Where(x => x.BusId == bus.Id)
                                                        .Where(x => x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                        .Where(t => t.Buses.CompanyId == busOld.CompanyId
                                                                    && t.Buses.BusRouteId == busOld.BusRouteId
                                                                    && t.Buses.BusTypeId == busOld.BusTypeId)
                                                        .FirstOrDefault();
                var bookingCheck = _context.BusBookings
                                                        //.Where(x => x.InvoiceId == booking.InvoiceId)
                                                        .Where(x => x.BusId == bus.Id)
                                                        .Where(x => x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                        .Where(t => t.Buses.CompanyId == busOld.CompanyId
                                                                    && t.Buses.BusRouteId == busOld.BusRouteId
                                                                    && t.Buses.BusTypeId == busOld.BusTypeId)
                                                        .FirstOrDefault();
                if (bookingCheckSameInvoiceId != null)
                {
                    /*
                     * neu tim ra chinh no hoac then khac phu hop trong list booking
                     * doi Id 2 then do
                     */
                    booking.StatusId = statusActive;
                    booking.BusId = bus.Id;
                    bookingCheckSameInvoiceId.BusId = busOld.Id;
                    _context.SaveChanges();
                }
                else if (bookingCheck != null)
                {
                    /*
                     * neu tim ra chinh no hoac then khac phu hop trong list booking
                     * doi Id 2 then do
                     */
                    booking.StatusId = statusActive;
                    booking.BusId = bus.Id;
                    bookingCheck.BusId = busOld.Id;
                    _context.SaveChanges();
                }
                else
                {
                    //[1]
                    var busNew = _context.Buses.Where(t => t.PlateNumber == plateNumber)
                                               .Where(t => t.CompanyId == busOld.CompanyId
                                                        && t.BusRouteId == busOld.BusRouteId
                                                        && t.BusTypeId == busOld.BusTypeId)
                                                .FirstOrDefault();
                    if (busNew == null)
                        throw new NotImplementedException(_localizer[Constant.ConstantMessage.TaxiNotSuitable]);

                    //[2] ktra taxi muon doi hien tai co available ko?
                    else
                    {
                        if (CheckBooking(new BusBooking
                        {
                            BusId = busNew.Id,
                            FromDate = booking.FromDate,
                            ToDate = booking.ToDate
                        }))
                        {
                            throw new NotImplementedException(_localizer[Constant.ConstantMessage.ItWasReservedByOtherPeople, "Bus", plateNumber]);
                        }
                        else
                        {
                            booking.StatusId = statusActive;
                            booking.BusId = busNew.Id;
                            _context.SaveChanges();
                        }
                    }
                }
                _context.Database.CommitTransaction();
                return booking;
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                throw new NotImplementedException(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = bookingId,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("bookingId: {0}, plateNumber: {1}", bookingId, plateNumber),
                    UserId = userId
                });
            }
        }

        public void UpdateStatus(int campaignId, int statusId, int userId)
        {
            string mess = string.Empty;
            try
            {
                _context.Database.BeginTransaction();
                var status = _context.Statuses.Where(x => x.Id == statusId).Select(x => x.Name).FirstOrDefault();
                var campaign = _context.Campaigns.Where(x => x.Id == campaignId).FirstOrDefault();
                if (campaign == null)
                    throw new NotImplementedException("Not Found");

                /* update CAMPAIGN */
                campaign.StatusId = statusId;
                campaign.LastUpdate = DateTime.Now;
                campaign.UserId = userId;
                _context.SaveChanges();

                if (Constant.ConstantStatus.Canceled.Equals(status) || Constant.ConstantStatus.Completed.Equals(status))
                {
                    /*
                     * caimpaign: cancel - channel: inactive
                     */
                    var statusINACTIVE = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Inactive).Select(x => x.Id).FirstOrDefault();

                    var shelter = _context.BusShelterBookings.Where(x => x.Invoices.CampaignId == campaignId).ToList();
                    shelter?.ForEach(item =>
                    {
                        item.StatusId = statusINACTIVE;
                        _context.SaveChanges();
                    });

                    var billboard = _context.BillboardBookings.Where(x => x.Invoices.CampaignId == campaignId).ToList();
                    billboard?.ForEach(item =>
                    {
                        item.StatusId = statusINACTIVE;
                        _context.SaveChanges();
                    });

                    var digital = _context.DigitalBillboardBookings.Where(x => x.Invoices.CampaignId == campaignId).ToList();
                    digital?.ForEach(item =>
                    {
                        item.StatusId = statusINACTIVE;
                        _context.SaveChanges();
                    });

                    var bus = _context.BusBookings.Where(x => x.Invoices.CampaignId == campaignId).ToList();
                    bus?.ForEach(item =>
                    {
                        item.StatusId = statusINACTIVE;
                        _context.SaveChanges();
                    });

                    var taxi = _context.TaxiBookings.Where(x => x.Invoices.CampaignId == campaignId).ToList();
                    taxi?.ForEach(item =>
                    {
                        item.StatusId = statusINACTIVE;
                        _context.SaveChanges();
                    });
                }

                _context.Database.CommitTransaction();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                throw new NotImplementedException(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = campaignId,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("campaignId: {0}, statusId: {1}", campaignId, statusId),
                    UserId = userId
                });
            }
        }
        #endregion

        #region SEARCH INFO FOR BOOKING
        public IEnumerable<BusShelterBookingReadDto> GetShelter(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var installCost = _context.BusShelterInstallationCosts.Where(x => x.Units.Name == Constant.ConstantUnit.m2).Select(x => x.Price).FirstOrDefault();

                var bookingList = _context.BusShelterBookings.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusShelterId);

                var downtimeList = _context.BusShelterDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusShelterId);

                /*
                * neu ko co Parent locId thi no chinh la tp va ko co quan/huyen
                * lay id cua chinh no de search
                */
                var listNotAvail = from shelter in _context.BusShelters.Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                       .Where(x => x.StatusId == statusActive)
                                                                       .Where(x =>
                                                                        (
                                                                            (bookingList).Contains(x.Id)
                                                                            || (downtimeList).Contains(x.Id)
                                                                        ))
                                                                       .Include(x => x.BusShelterDisplays)
                                   select new BusShelterAvailable
                                   {
                                       BusShelter = shelter,
                                       LocId = shelter.Locations.ParentId ?? shelter.LocationId,
                                       IsAvailable = false
                                   };

                var listAvail = from shelter in _context.BusShelters.Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                    .Where(x => x.StatusId == statusActive)
                                                                    .Where(x =>
                                                                    !(
                                                                        (bookingList).Contains(x.Id)
                                                                        || (downtimeList).Contains(x.Id)
                                                                    ))
                                                                    .Include(x => x.BusShelterDisplays)

                                select new BusShelterAvailable
                                {
                                    BusShelter = shelter,
                                    LocId = shelter.Locations.ParentId ?? shelter.LocationId,
                                    IsAvailable = true
                                };

                var listShelter = listAvail.Union(listNotAvail);

                var prices = (from xAvail in listShelter
                              join pr in _context.BusShelterPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date && bookingSearch.FromDate.Date <= x.ToDate.Date))
                                      on xAvail.BusShelter.Id equals pr.BusShelterId
                              select pr).ToList();

                var listPriceChecked = prices.Where(x => CheckShelterPriceNextyNext(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).ToList();

                var list = from shelter in listShelter.ToList()
                           join price in listPriceChecked
                           on shelter.BusShelter.Id equals price.BusShelterId

                           join dis in _context.BusShelterDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                    on price.Id equals dis.BusShelterPriceId into discount
                           from dis in discount.DefaultIfEmpty()

                           orderby shelter.IsAvailable descending
                           select new BusShelterBookingReadDto
                           {
                               IsAvailable = price.MinMonthBooking > bookingSearch.Amount ? false : shelter.IsAvailable,
                               MinMonthBooking = price.MinMonthBooking,
                               Price = price.Price,
                               OTC = shelter.BusShelter.OTC,
                               Amount = bookingSearch.Amount,
                               InstallationCost = installCost,
                               InstallationTimes = 1,
                               DiscountPercent = dis?.Percent ?? 0,
                               BusShelterId = shelter.BusShelter.Id,
                               BusShelters = shelter.BusShelter,
                               UnitId = unit,
                               FromDate = bookingSearch.FromDate,
                               StatusId = statusActive,
                               ToDate = bookingSearch.ToDate,
                               TotalFee = 0 /* (price.Price * bookingSearch.Amount)
                               * (1 - (dis == null ? 0 : dis.Percent / 100))
                               + (installCost) * (shelter.BusShelter.DimensionW * shelter.BusShelter.DimensionH) */
                           };

                if (Constant.Sort.DESC.Equals(sort))
                    return list.OrderByDescending(x => x.Price);
                else
                    return list.OrderBy(x => x.Price);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public IEnumerable<BillboardBookingReadDto> GetBillboard(BookingSearch bookingSearch, UserIdentity user, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();

                var installCost = _context.BillboardInstallationCosts.Where(x => x.Units.Name == Constant.ConstantUnit.m2).Select(x => x.Price).FirstOrDefault();

                var bookingList = _context.BillboardBookings.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate).Select(x => x.BillboardId);
                var downtimeList = _context.BillboardDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate).Select(x => x.BillboardId);

                /*
                * neu ko co Parent locId thi no chinh la tp va ko co quan/huyen
                * lay id cua chinh no de search
                */
                var listNotAvail = from billboard in _context.Billboards.Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                        .Where(x => x.StatusId == statusActive)
                                                                        .Where(x =>
                                                                        (
                                                                            (bookingList).Contains(x.Id)
                                                                            || (downtimeList).Contains(x.Id)
                                                                        ))
                                                                        .Include(x => x.BillboardDisplays)
                                                                        .Include(x => x.BillboardCameras)
                                                                        .Include(x => x.Companies)
                                                                        .Include(x => x.Locations)
                                   select new { billboard, locId = billboard.Locations.ParentId ?? billboard.LocationId, isAvailable = false };

                var listAvail = from billboard in _context.Billboards.Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                    .Where(x => x.StatusId == statusActive)
                                                                    .Where(x =>
                                                                    !(
                                                                        (bookingList).Contains(x.Id)
                                                                        || (downtimeList).Contains(x.Id)
                                                                    ))
                                                                    .Include(x => x.BillboardDisplays)
                                                                    .Include(x => x.BillboardCameras)
                                                                    .Include(x => x.Companies)
                                                                    .Include(x => x.Locations)
                                select new { billboard, locId = billboard.Locations.ParentId ?? billboard.LocationId, isAvailable = true };

                var listBillboard = listNotAvail.Union(listAvail);
                //get price
                var prices = (from xAvail in listBillboard
                              join pr in _context.BillboardPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date && bookingSearch.FromDate.Date <= x.ToDate.Date))
                                      on xAvail.billboard.Id equals pr.BillboardId
                              select pr).ToList();

                var listPriceChecked = prices.Where(x => CheckBillboardPriceNextyNext(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).ToList();
                //lay danh sach CAMERA BB
                //var cameras = from bb in listBillboard.AsNoTracking().ToList()
                //              join cam in _context.BillboardCameras.AsNoTracking() on bb.billboard.Id equals cam.BillboardId
                //              select cam;

                //var listCam = from cam in cameras
                //              group cam by cam.BillboardId into g
                //              select new { BillboardId = g.Key, BillboardCameras = g };
                var lstBB = listBillboard.AsNoTracking().ToList();
                if (user.RoleName != Constant.ConstantRole.SuperUser)
                {
                    lstBB = lstBB.Select(x => { x.billboard.Companies.Name = ""; return x; }).ToList();
                }

                var list = from billboard in lstBB//listBillboard.AsNoTracking().ToList()
                           join price in listPriceChecked
                           on billboard.billboard.Id equals price.BillboardId
                           join dis in _context.BillboardDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                        on price.Id equals dis.BillboardPriceId into discount
                           from dis in discount.DefaultIfEmpty()
                               //join cam in listCam on billboard.billboard.Id equals cam.BillboardId into camT
                               //from cam in camT.DefaultIfEmpty()
                           orderby billboard.isAvailable descending//, billboard.billboard.Name ascending
                           select new BillboardBookingReadDto
                           {
                               IsAvailable = price.MinMonthBooking > bookingSearch.Amount ? false : billboard.isAvailable,
                               //BillboardCameras = MyUltil.EnsureNotEmpty(cam?.BillboardCameras).ToList(),
                               MinMonthBooking = price.MinMonthBooking,
                               Price = price.Price,
                               OTC = billboard.billboard.OTC,
                               Amount = bookingSearch.Amount,
                               InstallationCost = installCost,
                               InstallationTimes = 1,
                               DiscountPercent = dis?.Percent ?? 0,
                               PromotePercent = price.PromotePercent,
                               BillboardId = billboard.billboard.Id,
                               Billboards = billboard.billboard,
                               UnitId = unit,
                               FromDate = bookingSearch.FromDate,
                               StatusId = statusActive,
                               ToDate = bookingSearch.ToDate,
                               TotalFee = 0 /* (price.Price * bookingSearch.Amount)
                                            * (1 - (dis == null ? 0 : dis.Percent / 100))
                                            + (installCost) * (billboard.billboard.DimensionW * billboard.billboard.DimensionH) */
                           };

                if (Constant.Sort.DESC.Equals(sort))
                    return list.OrderByDescending(x => x.Billboards.Name);
                else
                    return list.OrderBy(x => x.Billboards.Name);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public IEnumerable<DigitalBillboardBookingReadDto> GetDigital(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Hour).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();

                var downtimeList = _context.DigitalBillboardDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.DigitalBillboardId);

                /*
                 * lay danh sach: downtime va listavail khong nam trong downtime
                 * set trang thai avail, va union 2 list lai
                 * [00]chay tung ngay, tinh sum gio cua tung Digital
                    *[1] neu sum(HourPerDay) + hour/day > broadcasting time => remove ra khoi listAvail
                    *[2] nhung digital ko thoa dk thi add vo listNotAvail
                * set price
                */
                var listDownTime = from digital in _context.DigitalBillboards.Include(x => x.DigitalBillboardDisplays)
                                                                             .Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                            .Where(x => (downtimeList).Contains(x.Id)).AsNoTracking().ToList()
                                   select new { digital, isAvailable = false };

                var notVail = from digital in _context.DigitalBillboards.Include(x => x.DigitalBillboardDisplays)
                                                                        .Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                        .Where(x => x.StatusId == statusActive)
                                                                        .Where(x => x.BroadcastingTime < bookingSearch.HourPerDay)
                                                                        .Where(x => !(downtimeList).Contains(x.Id)).AsNoTracking().ToList()

                              select new { digital, isAvailable = false };

                var listAvail = from digital in _context.DigitalBillboards.Include(x => x.DigitalBillboardDisplays)
                                                                          .Where(x => x.Locations.ParentId == bookingSearch.LocationId
                                                                                || x.LocationId == bookingSearch.LocationId)
                                                                          .Where(x => x.StatusId == statusActive)
                                                                          .Where(x => x.BroadcastingTime >= bookingSearch.HourPerDay)
                                                                          .Where(x => !(downtimeList).Contains(x.Id)).AsNoTracking().ToList()
                                select new { digital, isAvailable = true };

                var list = listAvail;
                var listNotAvail = listDownTime.Union(notVail);

                DateTime dt = bookingSearch.FromDate;
                //[00]
                while (dt <= bookingSearch.ToDate)
                {
                    var bookingList = _context.DigitalBillboardBookings
                                                            .Where(bk => bk.FromDate <= dt && dt <= bk.ToDate)
                                                            .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .GroupBy(b => b.DigitalBillboardId)
                                                            .Select(x => new
                                                            {
                                                                DigitalBillboardId = x.Key,
                                                                total = x.Sum(t => t.HourPerDay)
                                                            });
                    if (bookingList.Count() > 0)
                    {
                        var listRemove = from digital in list
                                         join bk in bookingList on digital.digital.Id equals bk.DigitalBillboardId
                                         where bk.total + bookingSearch.HourPerDay > digital.digital.BroadcastingTime
                                         select digital;

                        if (listRemove.Count() > 0)
                        {
                            //[1] list avail
                            list = list.Where(x => !listRemove.Contains(x)).ToList();
                            //[2] list notavail
                            listNotAvail = listNotAvail.Union(listRemove);
                        }

                    }
                    dt = dt.AddDays(1);
                }

                listNotAvail = from lstNotAvail in listNotAvail
                               select new { digital = lstNotAvail.digital, isAvailable = false };
                list = list.Union(listNotAvail);

                //get price
                var prices = (from xAvail in list
                              join pr in _context.DigitalBillboardPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date && bookingSearch.FromDate.Date <= x.ToDate.Date))
                                        on new { DigitalBillboardId = xAvail.digital.Id }
                                        equals new { pr.DigitalBillboardId }
                              select pr).ToList();

                var listPriceChecked = prices.Where(x => CheckDigitalPriceNextyNext(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).ToList();

                /*create booking: set price */
                var listReturn = from digital in list
                                 join price in listPriceChecked
                                 on digital.digital.Id equals price.DigitalBillboardId
                                 orderby digital.isAvailable descending, digital.digital.Name ascending
                                 select new DigitalBillboardBookingReadDto
                                 {
                                     IsAvailable = digital.isAvailable,
                                     Price = price.Price,
                                     OTC = digital.digital.OTC,
                                     Amount = bookingSearch.Amount,
                                     HourPerDay = bookingSearch.HourPerDay,
                                     DigitalBillboardId = digital.digital.Id,
                                     DigitalBillboards = digital.digital,
                                     UnitId = unit,
                                     FromDate = bookingSearch.FromDate,
                                     StatusId = statusActive,
                                     ToDate = bookingSearch.ToDate,
                                     TotalFee = price.Price * bookingSearch.Amount
                                 };

                if (Constant.Sort.DESC.Equals(sort))
                    return listReturn.OrderByDescending(x => x.Price);
                else
                    return listReturn.OrderBy(x => x.Price);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        /// <summary>
        /// Seach taxi theo khu vuc
        /// </summary>
        /// <param name="bookingSearch"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public IEnumerable<TaxiBookingInfo> GetTaxi(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.TaxiBookings.Where(b => b.StatusId == statusActive
                                                                || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);

                var downtimeList = _context.TaxiDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);
                /*
                 * lay danh sach taxi available
                 * groupby 
                 * join voi Price: 1taxi-n price tuy thuoc vao TaxiTypes + ArtType
                 * join voi installation cost 
                 */
                var taxiArea = _context.TaxiAreas.Where(x => x.LocationId == bookingSearch.LocationId && x.CompanyId == bookingSearch.CompanyId).FirstOrDefault();
                var listTaxi = _context.Taxis
                                            .Where(x => x.TaxiAreaId == taxiArea.Id)
                                            .Where(x => x.CompanyId == bookingSearch.CompanyId)
                                            .Where(x => x.StatusId == statusActive)
                                            .Where(x =>
                                            !(
                                                (bookingList).Contains(x.Id)
                                                || (downtimeList).Contains(x.Id)
                                            ))
                                            .Include(x => x.TaxiAreas)
                                            .Include(x => x.Companies)
                                            .Include(x => x.TaxiTypes)
                                            .ToList()
                                            .GroupBy(x => new { x.TaxiAreaId, x.CompanyId, x.TaxiTypeId, TaxiTypeName = x.TaxiTypes.Name, x.TaxiAreas.LocationId })
                                            .Select(x => new
                                            {
                                                x.Key.TaxiAreaId,
                                                x.Key.CompanyId,
                                                x.Key.TaxiTypeId,
                                                x.Key.TaxiTypeName,
                                                x.Key.LocationId,
                                                //OTC = x.Sum(x => x.OTC), 
                                                Count = x.Count()
                                            });
                //show image dai dien cho taxi
                var displays = from dis in _context.TaxiDisplays.Where(x => x.CompanyId == null).AsEnumerable()
                               group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                               select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, TaxiDisplays = g };

                var displaysForCompany = from dis in _context.TaxiDisplays.Where(x => x.CompanyId != null).AsEnumerable()
                                         group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                                         select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, TaxiDisplays = g };

                var prices = (from busAvail in listTaxi
                              join pr in _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date && bookingSearch.FromDate.Date <= x.ToDate.Date))
                                        on new { busAvail.TaxiAreaId, busAvail.TaxiTypeId }
                                        equals new { pr.TaxiAreaId, pr.TaxiTypeId }
                              select pr).ToList();

                var listPriceChecked = prices.Where(x => CheckTaxiPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).ToList();

                var list = from taxiAvail in listTaxi
                           join price in listPriceChecked
                                      on new { TaxiAreaId = taxiAvail.TaxiAreaId, TaxiTypeId = taxiAvail.TaxiTypeId }
                                      equals new { price.TaxiAreaId, price.TaxiTypeId }
                                      //where taxiAvail.Count >= price.MinCarBooking
                           join artwork in _context.TaxiTypeOfArtworks on price.TaxiTypeOfArtworkId equals artwork.Id

                           join display in displaysForCompany on new { ArtTypeId = artwork.Id, CarTypeId = taxiAvail.TaxiTypeId, CompanyId = taxiAvail.CompanyId }
                                     equals new { ArtTypeId = display.ArtTypeId, CarTypeId = display.CarTypeId, CompanyId = display.CompanyId.GetValueOrDefault() } into displayT
                           from display in displayT.DefaultIfEmpty()

                           join displayNull in displays on new { ArtTypeId = artwork.Id, CarTypeId = taxiAvail.TaxiTypeId }
                                     equals new { ArtTypeId = displayNull.ArtTypeId, CarTypeId = displayNull.CarTypeId } into displayNullT
                           from displayNull in displayNullT.DefaultIfEmpty()

                           join cost in _context.TaxiInstallationCosts
                                       on new { price.TaxiTypeId, price.TaxiTypeOfArtworkId }
                                       equals new { cost.TaxiTypeId, cost.TaxiTypeOfArtworkId } into insCost
                           from cost in insCost.DefaultIfEmpty()

                           join dis in _context.TaxiDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                    on price.Id equals dis.TaxiAreaPriceId into discount
                           from dis in discount.DefaultIfEmpty()
                           select new TaxiBookingInfo
                           {
                               IsAvailable = taxiAvail.Count >= price.MinCarBooking ? true : false,
                               OTC = taxiArea.OTC,
                               OTCDate = taxiArea.OTCDate,
                               MinCarBooking = price.MinCarBooking,
                               CompanyId = taxiAvail.CompanyId,
                               LocationId = taxiAvail.LocationId,
                               TaxiAreaId = taxiArea.Id,
                               TaxiAreaName = taxiArea.Name,
                               TaxiTypeId = taxiAvail.TaxiTypeId,
                               TaxiTypeName = taxiAvail.TaxiTypeName,
                               TaxiTypeOfArtworkName = artwork.Name,
                               CarAmount = taxiAvail.Count,
                               TaxiDisplays = MyUltil.EnsureNotEmpty(display == null ? displayNull?.TaxiDisplays.OrderBy(x => x.Order) : display?.TaxiDisplays.OrderBy(x => x.Order)),
                               TaxiBookings = new TaxiBooking
                               {
                                   Price = price.Price,
                                   Amount = bookingSearch.Amount,
                                   InstallationCost = cost?.Price ?? 0,
                                   InstallationTimes = 1,
                                   DiscountPercent = dis?.Percent ?? 0,
                                   PromotePercent = price.PromotePercent,
                                   TaxiTypeOfArtworkId = price.TaxiTypeOfArtworkId,
                                   UnitId = unit,
                                   StatusId = statusUnconfirmed,
                                   FromDate = bookingSearch.FromDate,
                                   ToDate = bookingSearch.ToDate,
                                   TotalFee = 0//(price.Price * bookingSearch.Amount) * (1 - (dis?.Percent ?? 0) / 100) + (cost?.Price ?? 0)
                               }
                           };

                if (Constant.Sort.DESC.Equals(sort))
                    return list.OrderByDescending(x => x.TaxiTypeName).ThenBy(x => x.TaxiTypeOfArtworkName);
                else
                    return list.OrderBy(x => x.TaxiTypeName).ThenBy(x => x.TaxiTypeOfArtworkName);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public IEnumerable<TaxiBookingInfo> GetTaxiAvailAllRoute(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.TaxiBookings.Where(b => b.StatusId == statusActive
                                                                || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);

                var downtimeList = _context.TaxiDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);
                /*
                 * lay danh sach taxi available
                 * groupby 
                 * join voi Price: 1taxi-n price tuy thuoc vao TaxiTypes + ArtType
                 * join voi installation cost 
                 */
                var listTaxi = (from taxi in _context.Taxis
                                            .Where(x => x.StatusId == statusActive)
                                            .Where(x =>
                                            !(
                                                (bookingList).Contains(x.Id)
                                                || (downtimeList).Contains(x.Id)
                                            )).AsEnumerable()
                                join area in _context.TaxiAreas.Where(x => x.LocationId == bookingSearch.LocationId).ToList()
                                on new { taxi.TaxiAreaId, taxi.CompanyId } equals new { TaxiAreaId = area.Id, area.CompanyId }
                                select taxi).AsEnumerable();
                var listArea = listTaxi.GroupBy(x => new { x.TaxiAreaId, x.TaxiTypeId }).Select(x => new { x.Key.TaxiTypeId, x.Key.TaxiAreaId });
                //1 gia
                var prices = (from are in listArea
                              join pr in _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                            && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                            && bookingSearch.ToDate.Date <= x.ToDate.Date)
                                        on new { are.TaxiAreaId, are.TaxiTypeId }
                                        equals new { pr.TaxiAreaId, pr.TaxiTypeId }
                              select pr).AsEnumerable();
                //nhieu đoạn gía
                var pricesManyPeriod = (from are in listArea
                                        join pr in _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                      && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                      && bookingSearch.ToDate.Date > x.ToDate.Date)
                                                  on new { are.TaxiAreaId, are.TaxiTypeId }
                                                  equals new { pr.TaxiAreaId, pr.TaxiTypeId }
                                        select pr).AsEnumerable();

                var listPriceChecked = pricesManyPeriod.Where(x => CheckTaxiPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true)
                                       .GroupBy(x => new { x.TaxiAreaId, x.TaxiTypeId }).Select(x => x.First()).AsEnumerable();

                prices = prices.Union(listPriceChecked).GroupBy(x => new { x.TaxiAreaId, x.TaxiTypeId })
                    .Select(x => new TaxiAreaPrice { TaxiAreaId = x.Key.TaxiAreaId, TaxiTypeId = x.Key.TaxiTypeId }); ;

                var listModel = (from taxi in listTaxi.ToList()
                                 join area in _context.TaxiAreas.Where(x => x.LocationId == bookingSearch.LocationId).ToList()
                                        on new { taxi.TaxiAreaId, taxi.CompanyId } equals new { TaxiAreaId = area.Id, area.CompanyId }
                                 join type in _context.TaxiTypes on taxi.TaxiTypeId equals type.Id
                                 join price in prices
                                          on new { TaxiAreaId = taxi.TaxiAreaId, TaxiTypeId = taxi.TaxiTypeId }
                                          equals new { price.TaxiAreaId, price.TaxiTypeId }
                                 group new { taxi, area, type } by new { area, type } into g
                                 select new TaxiBookingInfo
                                 {
                                     CompanyId = g.Key.area.CompanyId,
                                     LocationId = bookingSearch.LocationId,
                                     TaxiAreaId = g.Key.area.Id,
                                     TaxiAreaName = g.Key.area.Name,
                                     TaxiTypeId = g.Key.type.Id,
                                     TaxiTypeName = g.Key.type.Name,
                                     CarAmount = g.Count()
                                 }).ToList();

                if (Constant.Sort.DESC.Equals(sort))
                    return listModel.OrderByDescending(x => x.TaxiAreaName);
                else
                    return listModel.OrderBy(x => x.TaxiAreaName);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        /// <summary>
        /// seach all cua taxi kem theo gia
        /// </summary>
        /// <param name="bookingSearch"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public List<TaxiBookingInfoSearchAll> GetAllTaxiAreaWithPrice(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.TaxiBookings.Where(b => b.StatusId == statusActive
                                                                || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);

                var downtimeList = _context.TaxiDownTimes.Where(b => b.StatusId == statusActive)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.TaxiId);
                /*
                 * lay danh sach taxi available
                 * groupby 
                 * join voi Price: 1taxi-n price tuy thuoc vao TaxiTypes + ArtType
                 * join voi installation cost 
                 */
                #region Advanced Search
                var lstTaxi = _context.Taxis.Where(x => 1 == 1);
                var taxiAreaPrice = _context.TaxiAreaPrices.Where(x => x.TaxiAreas.LocationId == bookingSearch.LocationId);
                var art = _context.TaxiTypeOfArtworks.Where(x => 1 == 1).AsNoTracking();

                if (bookingSearch.CarTypeId != 0)
                    lstTaxi = lstTaxi.Where(x => x.TaxiTypeId == bookingSearch.CarTypeId);
                if (bookingSearch.CompanyId != 0)
                {
                    lstTaxi = lstTaxi.Where(x => x.CompanyId == bookingSearch.CompanyId);
                    taxiAreaPrice = taxiAreaPrice.Where(x => x.TaxiAreas.CompanyId == bookingSearch.CompanyId);
                }
                if (bookingSearch.ArtTypeId != 0)
                {
                    taxiAreaPrice = taxiAreaPrice.Where(x => x.TaxiTypeOfArtworkId == bookingSearch.ArtTypeId);
                    art = art.Where(x => x.Id == bookingSearch.ArtTypeId);
                }
                #endregion

                var listTaxi = (from taxi in lstTaxi //_context.Taxis
                                            .Where(x => x.StatusId == statusActive)
                                            .Where(x =>
                                            !(
                                                (bookingList).Contains(x.Id)
                                                || (downtimeList).Contains(x.Id)
                                            )).AsEnumerable()
                                join area in _context.TaxiAreas.Where(x => x.LocationId == bookingSearch.LocationId).ToList()
                                on new { taxi.TaxiAreaId, taxi.CompanyId } equals new { TaxiAreaId = area.Id, area.CompanyId }
                                select taxi).AsEnumerable();
                var listArea = listTaxi.GroupBy(x => new { x.TaxiAreaId, x.TaxiTypeId })
                                    .Select(x => new
                                    {
                                        x.Key.TaxiTypeId,
                                        x.Key.TaxiAreaId,
                                        Count = x.Count()
                                    });
                //show image dai dien cho taxi
                var displays = from dis in _context.TaxiDisplays.Where(x => x.CompanyId == null).AsEnumerable()
                               group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                               select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, TaxiDisplays = g };

                var displaysForCompany = from dis in _context.TaxiDisplays.Where(x => x.CompanyId != null).AsEnumerable()
                                         group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                                         select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, TaxiDisplays = g };
                //1 gia
                var prices = (from txAvail in listArea
                                  //_context.TaxiAreaPrices
                              join pr in taxiAreaPrice.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                    && bookingSearch.FromDate.Date <= x.ToDate.Date))
                                        on new { txAvail.TaxiAreaId, txAvail.TaxiTypeId }
                                        equals new { pr.TaxiAreaId, pr.TaxiTypeId }
                              select pr).ToList();

                var listPriceChecked = prices.Where(x => CheckTaxiPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).ToList();

                var listModel = (from taxi in listArea.ToList()
                                 join area in _context.TaxiAreas
                                        on new { taxi.TaxiAreaId } equals new { TaxiAreaId = area.Id }
                                 join type in _context.TaxiTypes on taxi.TaxiTypeId equals type.Id
                                 join price in listPriceChecked
                                          on new { TaxiAreaId = taxi.TaxiAreaId, TaxiTypeId = taxi.TaxiTypeId }
                                          equals new { price.TaxiAreaId, price.TaxiTypeId }

                                 join artwork in art on price.TaxiTypeOfArtworkId equals artwork.Id
                                 join display in displaysForCompany on new { ArtTypeId = artwork.Id, CarTypeId = taxi.TaxiTypeId, CompanyId = area.CompanyId }
                                     equals new { ArtTypeId = display.ArtTypeId, CarTypeId = display.CarTypeId, CompanyId = display.CompanyId.GetValueOrDefault() } into displayT
                                 from display in displayT.DefaultIfEmpty()

                                 join displayNull in displays on new { ArtTypeId = artwork.Id, CarTypeId = taxi.TaxiTypeId }
                                           equals new { ArtTypeId = displayNull.ArtTypeId, CarTypeId = displayNull.CarTypeId } into displayNullT
                                 from displayNull in displayNullT.DefaultIfEmpty()

                                 join cost in _context.TaxiInstallationCosts
                                      on new { price.TaxiTypeId, price.TaxiTypeOfArtworkId }
                                      equals new { cost.TaxiTypeId, cost.TaxiTypeOfArtworkId } into insCost
                                 from cost in insCost.DefaultIfEmpty()

                                 join dis in _context.TaxiDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                          on price.Id equals dis.TaxiAreaPriceId into discount
                                 from dis in discount.DefaultIfEmpty()
                                 select new TaxiBookingInfo
                                 {
                                     IsAvailable = taxi.Count >= price.MinCarBooking ? true : false,
                                     OTC = area.OTC,
                                     MinCarBooking = price.MinCarBooking,
                                     CompanyId = area.CompanyId,
                                     LocationId = area.LocationId,
                                     TaxiAreaId = area.Id,
                                     TaxiAreaName = area.Name,
                                     TaxiTypeId = taxi.TaxiTypeId,
                                     TaxiTypeName = type.Name,
                                     CarAmount = taxi.Count,
                                     TaxiDisplays = MyUltil.EnsureNotEmpty(display == null ? displayNull?.TaxiDisplays.OrderBy(x => x.Order) : display?.TaxiDisplays.OrderBy(x => x.Order)),
                                     TaxiBookings = new TaxiBooking
                                     {
                                         Price = price.Price,
                                         Amount = bookingSearch.Amount,
                                         InstallationCost = cost?.Price ?? 0,
                                         InstallationTimes = 1,
                                         DiscountPercent = dis?.Percent ?? 0,
                                         PromotePercent = price.PromotePercent,
                                         TaxiTypeOfArtworkId = price.TaxiTypeOfArtworkId,
                                         TaxiTypeOfArtworks = artwork,
                                         UnitId = unit,
                                         StatusId = statusUnconfirmed,
                                         FromDate = bookingSearch.FromDate,
                                         ToDate = bookingSearch.ToDate,
                                         TotalFee = 0
                                     }
                                 });

                #region Advanced Search
                listModel = listModel.Where(x => bookingSearch.CarAmountFrom <= x.CarAmount && x.CarAmount <= bookingSearch.CarAmountTo).ToList();
                //listModel = listModel.Where(x => bookingSearch.PriceFrom <=
                //                                ((x.TaxiBookings.Price * bookingSearch.Amount)
                //                                * (1 - x.TaxiBookings.DiscountPercent / 100))
                //                                * (1 - x.TaxiBookings.PromotePercent / 100)
                //                                && ((x.TaxiBookings.Price * bookingSearch.Amount)
                //                                * (1 - x.TaxiBookings.DiscountPercent / 100))
                //                                * (1 - x.TaxiBookings.PromotePercent / 100) <= bookingSearch.PriceTo)
                //                            .ToList();
                #endregion

                //gom nhóm theo: cty - loại xe
                var result = (listModel.GroupBy(x => new
                {
                    x.IsAvailable,
                    x.OTC,
                    x.MinCarBooking,
                    x.CompanyId,
                    x.LocationId,
                    x.TaxiAreaId,
                    x.TaxiTypeId,
                    x.TaxiAreaName,
                    x.TaxiTypeName,
                    x.CarAmount,
                    x.TaxiDisplays
                })
                                .Select(x => new TaxiBookingInfoSearchAll
                                {
                                    IsAvailable = x.Key.IsAvailable,
                                    OTC = x.Key.OTC,
                                    MinCarBooking = x.Key.MinCarBooking,
                                    CompanyId = x.Key.CompanyId,
                                    LocationId = x.Key.LocationId,
                                    TaxiAreaId = x.Key.TaxiAreaId,
                                    TaxiTypeId = x.Key.TaxiTypeId,
                                    TaxiAreaName = x.Key.TaxiAreaName,
                                    TaxiTypeName = x.Key.TaxiTypeName,
                                    CarAmount = x.Key.CarAmount,
                                    TaxiDisplays = x.Key.TaxiDisplays,
                                    TaxiBookings = x.Select(a => a.TaxiBookings).OrderBy(a => a.TaxiTypeOfArtworkId).FirstOrDefault(),
                                    PriceFrom = x.Min(x => x.TaxiBookings.Price * (1 - x.TaxiBookings.DiscountPercent / 100) * (1 - x.TaxiBookings.PromotePercent / 100)),
                                    PriceTo = x.Max(x => x.TaxiBookings.Price * (1 - x.TaxiBookings.DiscountPercent / 100) * (1 - x.TaxiBookings.PromotePercent / 100)),
                                    PromotePercent = x.Max(x => x.TaxiBookings.PromotePercent)
                                })).ToList();
                if (Constant.Sort.DESC.Equals(sort))
                    return result.OrderByDescending(x => x.TaxiAreaName).ThenBy(x => x.TaxiTypeName).ToList();
                else
                    return result.OrderBy(x => x.TaxiAreaName).ThenBy(x => x.TaxiTypeName).ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public IEnumerable<BusBookingInfo> GetBus(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.BusBookings.Where(b => b.StatusId == statusActive || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);

                var downtimeList = _context.BusDownTimes.Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);
                /*
                 * lay danh sach bus available
                 * groupby 
                 * join voi Price: 1 bus-n price tuy thuoc vao BusTypes + ArtType
                 * join voi installation cost 
                 */
                #region Advanced Search
                var lstBus = _context.Buses.Where(x => 1 == 1);
                var busrouteprice = _context.BusRoutePrices.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId);
                var art = _context.BusTypeOfArtworks.Where(x => 1 == 1).AsNoTracking();

                if (bookingSearch.CarTypeId != 0)
                    lstBus = lstBus.Where(x => x.BusTypeId == bookingSearch.CarTypeId);
                if (bookingSearch.ArtTypeId != 0)
                {
                    busrouteprice = busrouteprice.Where(x => x.BusTypeOfArtworkId == bookingSearch.ArtTypeId);
                    art = art.Where(x => x.Id == bookingSearch.ArtTypeId);
                }
                #endregion

                var listBus = from bus in lstBus.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                        .Where(x => x.BusRouteId == bookingSearch.BusRouteId)
                                                        .Where(x => x.StatusId == statusActive)
                                                                 .Where(x =>
                                                                 !(
                                                                     (bookingList).Contains(x.Id)
                                                                  || (downtimeList).Contains(x.Id)
                                                                 )).AsEnumerable()
                              select bus;
                var listRoute = listBus.GroupBy(x => new { x.CompanyId, x.BusRouteId, x.BusTypeId })
                                        .Select(x => new
                                        {
                                            x.Key.CompanyId,
                                            x.Key.BusRouteId,
                                            x.Key.BusTypeId,
                                            Count = x.Count()
                                        });

                //show image dai dien cho bus
                var displays = from dis in _context.BusDisplays.Where(x => x.CompanyId == null).AsEnumerable()
                               group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                               select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, BusDisplays = g };

                var displaysForCompany = from dis in _context.BusDisplays.Where(x => x.CompanyId != null).AsEnumerable()
                                         group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                                         select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, BusDisplays = g };

                var prices = (from busAvail in listRoute
                              join pr in busrouteprice.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                                .Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                && bookingSearch.ToDate.Date <= x.ToDate.Date)

                                        on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                        equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                              select pr).AsEnumerable();
                //nếu thời gian đặt thuộc nhiều đoạn giá thì ktra tính liên tiếp
                var pricesManyPeriod = (from busAvail in listRoute
                                        join pr in busrouteprice.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                                          .Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                      && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                      && bookingSearch.ToDate.Date > x.ToDate.Date)

                                                  on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                                  equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                                        select pr).AsEnumerable();

                var listPriceChecked = pricesManyPeriod.Where(x => CheckBusPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).AsEnumerable();
                prices = prices.Union(listPriceChecked);

                var list = from busAvail in listRoute
                           join com in _context.Companies.AsNoTracking() on busAvail.CompanyId equals com.CompanyId
                           join type in _context.BusTypes.AsNoTracking() on busAvail.BusTypeId equals type.Id
                           join route in _context.BusRoutes.Where(x => x.Id == bookingSearch.BusRouteId).AsNoTracking()
                                        on busAvail.BusRouteId equals route.Id
                           join price in prices
                                      on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                      equals new { price.BusRouteId, price.BusTypeId, price.CompanyId }
                                      //where busAvail.Count >= price.MinCarBooking
                           join artwork in art on price.BusTypeOfArtworkId equals artwork.Id

                           join display in displaysForCompany on new { ArtTypeId = artwork.Id, CarTypeId = busAvail.BusTypeId, CompanyId = busAvail.CompanyId }
                                     equals new { ArtTypeId = display.ArtTypeId, CarTypeId = display.CarTypeId, CompanyId = display.CompanyId.GetValueOrDefault() } into displayT
                           from display in displayT.DefaultIfEmpty()

                           join displayNull in displays on new { ArtTypeId = artwork.Id, CarTypeId = busAvail.BusTypeId }
                                     equals new { ArtTypeId = displayNull.ArtTypeId, CarTypeId = displayNull.CarTypeId } into displayNullT
                           from displayNull in displayNullT.DefaultIfEmpty()

                           join cost in _context.BusInstallationCosts
                                       on new { price.BusTypeId, price.BusTypeOfArtworkId }
                                       equals new { cost.BusTypeId, cost.BusTypeOfArtworkId } into insCost
                           from cost in insCost.DefaultIfEmpty()

                           join dis in _context.BusDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                    on price.Id equals dis.BusRoutePriceId into discount
                           from dis in discount.DefaultIfEmpty()

                           select new BusBookingInfo
                           {
                               IsAvailable = busAvail.Count >= price.MinCarBooking ? true : false,
                               OTC = route.OTC,
                               OTCDate = route.OTCDate,
                               MinCarBooking = price.MinCarBooking,
                               CompanyId = busAvail.CompanyId,
                               CompanyName = com.Name,
                               LocationId = route.LocationId,
                               BusRouteId = route.Id,
                               BusRouteName = string.Format("Tuyến {0} | {1}", route.Code, route.Name),
                               Description = route.Description,
                               BusTypeId = type.Id,
                               BusTypeName = type.Name,
                               BusTypeOfArtworkName = artwork.Name,
                               CarAmount = busAvail.Count,
                               BusDisplays = MyUltil.EnsureNotEmpty(display == null ? displayNull?.BusDisplays.OrderBy(x => x.Order) : display?.BusDisplays.OrderBy(x => x.Order)),
                               BusBookings = new BusBooking
                               {
                                   Price = price.Price,
                                   Amount = bookingSearch.Amount,
                                   InstallationCost = cost?.Price ?? 0,
                                   InstallationTimes = 1,
                                   DiscountPercent = dis?.Percent ?? 0,
                                   PromotePercent = price.PromotePercent,
                                   BusTypeOfArtworkId = price.BusTypeOfArtworkId,
                                   UnitId = unit,
                                   StatusId = statusUnconfirmed,
                                   FromDate = bookingSearch.FromDate,
                                   ToDate = bookingSearch.ToDate,
                                   TotalFee = 0// (price.Price * bookingSearch.Amount) * (1 - (dis?.Percent ?? 0) / 100) + (cost?.Price ?? 0)
                               }
                           };

                if (Constant.Sort.DESC.Equals(sort))
                    return list.OrderByDescending(x => x.BusTypeName).ThenBy(x => x.BusTypeOfArtworkName);
                else
                    return list.OrderBy(x => x.BusTypeName).ThenBy(x => x.BusTypeOfArtworkName);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public IEnumerable<BusBookingInfo> GetBusAvailAllRoute(BookingSearch bookingSearch, string sort)
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.BusBookings.Where(b => b.StatusId == statusActive
                                                                || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);

                var downtimeList = _context.BusDownTimes.Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);
                /*
                 * lay danh sach bus available
                 * groupby 
                 * join voi Price: 1 bus-n price tuy thuoc vao BusTypes + ArtType
                 * join voi installation cost 
                 */
                var listBus = from bus in _context.Buses.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                       .Where(x => x.StatusId == statusActive)
                                                                 //.Where(x=>x.BusRouteId == 143)
                                                                 .Where(x =>
                                                                 !(
                                                                     (bookingList).Contains(x.Id)
                                                                  || (downtimeList).Contains(x.Id)
                                                                 )).AsEnumerable()
                              select bus;
                var listRoute = listBus.GroupBy(x => new { x.CompanyId, x.BusRouteId, x.BusTypeId }).Select(x => new { x.Key.CompanyId, x.Key.BusRouteId, x.Key.BusTypeId });
                //danh sach gía chỉ có 1 đoạn
                var prices = (from busAvail in listRoute
                              join pr in _context.BusRoutePrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                && bookingSearch.ToDate.Date <= x.ToDate.Date)
                                        on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                        equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                              select pr).AsEnumerable();
                //nếu thời gian đặt thuộc nhiều đoạn giá thì ktra tính liên tiếp
                var pricesManyPeriod = (from busAvail in listRoute
                                        join pr in _context.BusRoutePrices.Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                      && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                      && bookingSearch.ToDate.Date > x.ToDate.Date)
                                                  on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                                  equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                                        select pr).AsEnumerable();

                var listPriceChecked = pricesManyPeriod.Where(x => CheckBusPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).AsEnumerable();

                prices = prices.Union(listPriceChecked).GroupBy(x => new { x.BusRouteId, x.CompanyId, x.BusTypeId })
                    .Select(x => new BusRoutePrice { BusRouteId = x.Key.BusRouteId, CompanyId = x.Key.CompanyId, BusTypeId = x.Key.BusTypeId });

                var listModel = (from bus in listBus.ToList()
                                 join price in prices
                                 on new { bus.BusRouteId, bus.BusTypeId, bus.CompanyId }
                                 equals new { price.BusRouteId, price.BusTypeId, price.CompanyId }
                                 join route in _context.BusRoutes on bus.BusRouteId equals route.Id
                                 group bus by route into g
                                 select new BusBookingInfo
                                 {
                                     LocationId = bookingSearch.LocationId,
                                     BusRouteId = g.Key.Id,
                                     BusRouteName = string.Format("Tuyến {0} | {1}", g.Key.Code, g.Key.Name),
                                     Description = g.Key.Description,
                                     CarAmount = g.Count()
                                 }).ToList();
                if (Constant.Sort.DESC.Equals(sort))
                    return listModel.OrderByDescending(x => x.BusRouteName);
                else
                    return listModel.OrderBy(x => x.BusRouteName);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public List<BusBookingInfoSearchAll> GetAllBusRouteWithPrice(BookingSearch bookingSearch, string sort)//List<BusAllPrice>
        {
            try
            {
                var unit = _context.Units.Where(x => x.Name == Constant.ConstantUnit.Month).Select(x => x.Id).FirstOrDefault();
                var statusActive = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).Select(x => x.Id).FirstOrDefault();
                var statusUnconfirmed = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Unconfirmed).Select(x => x.Id).FirstOrDefault();

                var bookingList = _context.BusBookings.Where(b => b.StatusId == statusActive || b.StatusId == statusUnconfirmed)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);

                var downtimeList = _context.BusDownTimes.Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                     .Where(bk => bk.FromDate <= bookingSearch.ToDate && bookingSearch.FromDate <= bk.ToDate)
                                                                     .Select(x => x.BusId);
                /*
                 * lay danh sach bus available
                 * groupby 
                 * join voi Price: 1 bus-n price tuy thuoc vao BusTypes + ArtType
                 * join voi installation cost 
                 */

                #region Advanced Search
                var lstBus = _context.Buses.Where(x => 1 == 1);
                var busrouteprice = _context.BusRoutePrices.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId);
                var art = _context.BusTypeOfArtworks.Where(x => 1 == 1).AsNoTracking();

                if (bookingSearch.CarTypeId != 0)
                    lstBus = lstBus.Where(x => x.BusTypeId == bookingSearch.CarTypeId);
                //search chi tiết 1 tuyến bus
                if (bookingSearch.BusRouteId != 0)
                {
                    lstBus = lstBus.Where(x => x.BusRouteId == bookingSearch.BusRouteId);
                    busrouteprice = busrouteprice.Where(x => x.BusRouteId == bookingSearch.BusRouteId);
                }
                if (bookingSearch.ArtTypeId != 0)
                {
                    busrouteprice = busrouteprice.Where(x => x.BusTypeOfArtworkId == bookingSearch.ArtTypeId);
                    art = art.Where(x => x.Id == bookingSearch.ArtTypeId);
                }
                #endregion

                var listBus = from bus in lstBus.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                        .Where(x => x.StatusId == statusActive)
                                                                 .Where(x =>
                                                                 !(
                                                                     (bookingList).Contains(x.Id)
                                                                  || (downtimeList).Contains(x.Id)
                                                                 )).AsEnumerable()
                              select bus;
                var listRoute = listBus.GroupBy(x => new { x.CompanyId, x.BusRouteId, x.BusTypeId })
                                        .Select(x => new
                                        {
                                            x.Key.CompanyId,
                                            x.Key.BusRouteId,
                                            x.Key.BusTypeId,
                                            Count = x.Count()
                                        });

                //show image dai dien cho bus
                var displays = from dis in _context.BusDisplays.Where(x => x.CompanyId == null).AsEnumerable()
                               group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                               select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, BusDisplays = g };

                var displaysForCompany = from dis in _context.BusDisplays.Where(x => x.CompanyId != null).AsEnumerable()
                                         group dis by new { dis.ArtTypeId, dis.CarTypeId, dis.CompanyId } into g
                                         select new { g.Key.ArtTypeId, g.Key.CarTypeId, g.Key.CompanyId, BusDisplays = g };

                //_context.BusRoutePrices
                var prices = (from busAvail in listRoute
                              join pr in busrouteprice.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                                .Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                && bookingSearch.ToDate.Date <= x.ToDate.Date)

                                        on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                        equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                              select pr).AsEnumerable();
                //nếu thời gian đặt thuộc nhiều đoạn giá thì ktra tính liên tiếp
                var pricesManyPeriod = (from busAvail in listRoute
                                        join pr in busrouteprice.Where(x => x.BusRoutes.LocationId == bookingSearch.LocationId)
                                                                          .Where(x => (x.FromDate.Date <= bookingSearch.FromDate.Date
                                                                                      && bookingSearch.FromDate.Date <= x.ToDate.Date)
                                                                                      && bookingSearch.ToDate.Date > x.ToDate.Date)

                                                  on new { busAvail.BusRouteId, busAvail.BusTypeId, busAvail.CompanyId }
                                                  equals new { pr.BusRouteId, pr.BusTypeId, pr.CompanyId }
                                        select pr).AsEnumerable();

                var listPriceChecked = pricesManyPeriod.Where(x => CheckBusPriceIsSequential(x, bookingSearch.FromDate, bookingSearch.ToDate) == true).AsEnumerable();
                prices = prices.Union(listPriceChecked);

                //danh sach tat ca cac tuyen theo: company, route, location, type, price
                var listModel = (from bus in listRoute//.ToList()
                                 join com in _context.Companies.AsNoTracking() on bus.CompanyId equals com.CompanyId
                                 join type in _context.BusTypes.AsNoTracking() on bus.BusTypeId equals type.Id
                                 join price in prices
                                             on new { bus.BusRouteId, bus.BusTypeId, bus.CompanyId }
                                             equals new { price.BusRouteId, price.BusTypeId, price.CompanyId }
                                             //join artwork in _context.BusTypeOfArtworks.AsNoTracking() on price.BusTypeOfArtworkId equals artwork.Id
                                 join artwork in art on price.BusTypeOfArtworkId equals artwork.Id

                                 join display in displaysForCompany on new { ArtTypeId = artwork.Id, CarTypeId = bus.BusTypeId, CompanyId = bus.CompanyId }
                                     equals new { ArtTypeId = display.ArtTypeId, CarTypeId = display.CarTypeId, CompanyId = display.CompanyId.GetValueOrDefault() } into displayT
                                 from display in displayT.DefaultIfEmpty()

                                 join displayNull in displays on new { ArtTypeId = artwork.Id, CarTypeId = bus.BusTypeId }
                                           equals new { ArtTypeId = displayNull.ArtTypeId, CarTypeId = displayNull.CarTypeId } into displayNullT
                                 from displayNull in displayNullT.DefaultIfEmpty()

                                 join route in _context.BusRoutes.AsNoTracking() on bus.BusRouteId equals route.Id
                                 join cost in _context.BusInstallationCosts
                                       on new { price.BusTypeId, price.BusTypeOfArtworkId }
                                       equals new { cost.BusTypeId, cost.BusTypeOfArtworkId } into insCost
                                 from cost in insCost.DefaultIfEmpty()

                                 join dis in _context.BusDiscounts.Where(x => x.Month == bookingSearch.Amount)
                                          on price.Id equals dis.BusRoutePriceId into discount
                                 from dis in discount.DefaultIfEmpty()
                                 select new BusBookingInfo
                                 {
                                     IsAvailable = bus.Count >= price.MinCarBooking ? true : false,
                                     OTC = route.OTC,
                                     MinCarBooking = price.MinCarBooking,
                                     CompanyId = com.CompanyId,
                                     CompanyName = com.Name,
                                     LocationId = route.LocationId,
                                     BusRouteId = route.Id,
                                     BusRoutes = route,
                                     BusRouteName = string.Format("Tuyến {0} | {1}", route.Code, route.Name),
                                     Description = route.Description,
                                     SearchKeywordText = route.SearchKeywordText,
                                     BusTypeId = type.Id,
                                     BusTypeName = type.Name,
                                     CarAmount = bus.Count,
                                     BusDisplays = MyUltil.EnsureNotEmpty(display == null ? displayNull?.BusDisplays.OrderBy(x => x.Order) : display?.BusDisplays.OrderBy(x => x.Order)),
                                     BusBookings = new BusBooking
                                     {
                                         Price = price.Price,
                                         Amount = bookingSearch.Amount,
                                         InstallationCost = cost?.Price ?? 0,
                                         InstallationTimes = 1,
                                         DiscountPercent = dis?.Percent ?? 0,
                                         PromotePercent = price.PromotePercent,
                                         BusTypeOfArtworkId = artwork.Id,
                                         BusTypeOfArtworks = artwork,
                                         UnitId = unit,
                                         StatusId = statusUnconfirmed,
                                         FromDate = bookingSearch.FromDate,
                                         ToDate = bookingSearch.ToDate,
                                         TotalFee = 0
                                     }
                                 }).ToList();

                #region Advanced Search
                listModel = listModel.Where(x => bookingSearch.CarAmountFrom <= x.CarAmount && x.CarAmount <= bookingSearch.CarAmountTo).ToList();
                //listModel = listModel.Where(x => bookingSearch.PriceFrom <= 
                //                                ((x.BusBookings.Price * bookingSearch.Amount) 
                //                                * (1 - x.BusBookings.DiscountPercent / 100)) 
                //                                * (1 - x.BusBookings.PromotePercent / 100)
                //                                && ((x.BusBookings.Price * bookingSearch.Amount) 
                //                                * (1 - x.BusBookings.DiscountPercent / 100)) 
                //                                * (1 - x.BusBookings.PromotePercent / 100) <= bookingSearch.PriceTo)
                //                            .ToList();
                #endregion
                //gom nhom lai theo: cty - loai xe
                var lstGroupByBustype = (listModel.GroupBy(x => new
                {
                    x.IsAvailable,
                    x.OTC,
                    x.MinCarBooking,
                    x.CompanyId,
                    x.CompanyName,
                    x.LocationId,
                    x.BusRouteId,
                    x.BusRoutes,
                    x.BusRouteName,
                    x.Description,
                    x.SearchKeywordText,
                    x.BusTypeId,
                    x.BusTypeName,
                    x.CarAmount,
                    x.BusDisplays
                })
                                .Select(x => new BusBookingInfoSearchAll
                                {
                                    IsAvailable = x.Key.IsAvailable,
                                    OTC = x.Key.OTC,
                                    MinCarBooking = x.Key.MinCarBooking,
                                    CompanyId = x.Key.CompanyId,
                                    CompanyName = x.Key.CompanyName,
                                    LocationId = x.Key.LocationId,
                                    BusRouteId = x.Key.BusRouteId,
                                    BusRoutes = x.Key.BusRoutes,
                                    BusRouteName = x.Key.BusRouteName,
                                    Description = x.Key.Description,
                                    SearchKeywordText = x.Key.SearchKeywordText,
                                    BusTypeId = x.Key.BusTypeId,
                                    BusTypeName = x.Key.BusTypeName,
                                    CarAmount = x.Key.CarAmount,
                                    BusDisplays = x.Key.BusDisplays,
                                    BusBookings = x.Select(x => x.BusBookings).OrderBy(x => x.BusTypeOfArtworkId).FirstOrDefault(),//.ToList(),
                                    PriceFrom = x.Min(x => x.BusBookings.Price * (1 - x.BusBookings.DiscountPercent / 100) * (1 - x.BusBookings.PromotePercent / 100)),//giá MIN của nhiều loại dán theo loại xe của 1 cty
                                    PriceTo = x.Max(x => x.BusBookings.Price * (1 - x.BusBookings.DiscountPercent / 100) * (1 - x.BusBookings.PromotePercent / 100)),//giá MAX của nhiều loại dán theo loại xe của 1 cty
                                    PromotePercent = x.Max(x => x.BusBookings.PromotePercent)
                                })).OrderBy(x => x.BusTypeName).ThenBy(x => x.CompanyName).ToList();

                //gom tiep tuc theo tuyen
                //var lstGroupByBusroute = (lstGroupByBustype.GroupBy(x => new
                //{
                //    x.LocationId,
                //    x.BusRouteId,
                //    x.BusRoutes.Code,
                //    x.BusRouteName,
                //    x.Description,
                //    x.SearchKeywordText
                //})
                //    .Select(x => new BusAllPrice
                //    {
                //        LocationId = x.Key.LocationId,
                //        BusRouteId = x.Key.BusRouteId,
                //        BusRouteCode = x.Key.Code,
                //        BusRouteName = x.Key.BusRouteName,
                //        Description = x.Key.Description,
                //        SearchKeywordText = x.Key.SearchKeywordText,
                //        TotalCar = x.Sum(x => x.CarAmount),
                //        PriceFrom = x.Min(x => x.PriceFrom),// giá MIN cua nhiu cty thuoc 1 tuyen
                //        PriceTo = x.Max(x => x.PriceTo),// giá MAX cua nhiu cty thuoc 1 tuyen
                //        PromotePercent = x.Max(x=>x.PromotePercent),
                //        BusBookingInfo = x.Select(x => { x.Description = ""; return x; }).ToList(),
                //    })
                //    ).ToList();

                //if (Constant.Sort.DESC.Equals(sort))
                //    return lstGroupByBusroute.OrderByDescending(x => x.BusRouteName).ToList();
                //else
                //    return lstGroupByBusroute.OrderBy(x => x.BusRouteName).ToList();
                return lstGroupByBustype.OrderBy(x => x.BusRouteName).ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /*---------------------------------- CHECK GIÁ LIÊN TỤC ----------------------------------------*/
        public bool CheckShelterPriceNextyNext(BusShelterPrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.BusShelterPrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date))
                                                    .Where(x => x.BusShelterId == price.BusShelterId)
                                                    .Where(x => x.MinMonthBooking == price.MinMonthBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                bool isSequential = prices.Zip(prices.Skip(1),
                                  (a, b) => b.FromDate.Date == a.ToDate.Date.AddDays(1))
                             .All(x => x);
                return isSequential;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckBillboardPriceNextyNext(BillboardPrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.BillboardPrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date))
                                                    .Where(x => x.BillboardId == price.BillboardId)
                                                    .Where(x => x.MinMonthBooking == price.MinMonthBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                bool isSequential = prices.Zip(prices.Skip(1),
                                  (a, b) => b.FromDate.Date == a.ToDate.Date.AddDays(1))
                             .All(x => x);
                return isSequential;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckDigitalPriceNextyNext(DigitalBillboardPrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.DigitalBillboardPrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date))
                                                    .Where(x => x.DigitalBillboardId == price.DigitalBillboardId)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                bool isSequential = prices.Zip(prices.Skip(1),
                                  (a, b) => b.FromDate.Date == a.ToDate.Date.AddDays(1))
                             .All(x => x);
                return isSequential;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckTaxiPriceNextyNext(TaxiAreaPrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date) //chong cheo
                                                                                                                                   //|| (fromDate.Date <= x.FromDate.Date && x.ToDate.Date <= toDate.Date)
                                                             ) //bao ben ngoai ||====\-------\=====||
                                                    .Where(x => x.TaxiAreaId == price.TaxiAreaId
                                                            && x.TaxiTypeId == price.TaxiTypeId
                                                            && x.TaxiTypeOfArtworkId == price.TaxiTypeOfArtworkId
                                                            )
                                                    .Where(x => x.MinCarBooking == price.MinCarBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                for (int i = 0; i < prices.Count() - 1; i++)
                {
                    DateTime toDt = prices[i].ToDate.AddDays(1).Date;
                    if (!toDt.Equals(prices[i + 1].FromDate.Date))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckTaxiPriceIsSequential(TaxiAreaPrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.TaxiAreaPrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date))
                                                    .Where(x => x.TaxiAreaId == price.TaxiAreaId
                                                            && x.TaxiTypeId == price.TaxiTypeId
                                                            && x.TaxiTypeOfArtworkId == price.TaxiTypeOfArtworkId
                                                            )
                                                    .Where(x => x.MinCarBooking == price.MinCarBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                bool isSequential = prices.Zip(prices.Skip(1),
                                   (a, b) => b.FromDate.Date == a.ToDate.Date.AddDays(1))
                              .All(x => x);
                return isSequential;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckBusPriceNextyNext(BusRoutePrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.BusRoutePrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date) //chong cheo
                                                                                                                                   //|| (fromDate.Date <= x.FromDate.Date && x.ToDate.Date <= toDate)
                                                             ) //bao ben ngoai ||====\-------\=====||
                                                    .Where(x => x.BusRouteId == price.BusRouteId
                                                            && x.BusTypeId == price.BusTypeId
                                                            && x.BusTypeOfArtworkId == price.BusTypeOfArtworkId
                                                            && x.CompanyId == price.CompanyId)
                                                    .Where(x => x.MinCarBooking == price.MinCarBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                for (int i = 0; i < prices.Count() - 1; i++)
                {
                    DateTime toDt = prices[i].ToDate.AddDays(1).Date;
                    if (!toDt.Equals(prices[i + 1].FromDate.Date))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckBusPriceIsSequential(BusRoutePrice price, DateTime fromDate, DateTime toDate)
        {
            try
            {
                /*
                 * lay danh sach gia trong doan
                 * [1] ktra ngay toDate cua booking co nam trong đoạn có giá ko
                 * [2] check time có lien tuc ko
                 */
                var prices = _context.BusRoutePrices.Where(x => (x.FromDate.Date <= toDate.Date && fromDate.Date <= x.ToDate.Date))
                                                    .Where(x => x.BusRouteId == price.BusRouteId
                                                            && x.BusTypeId == price.BusTypeId
                                                            && x.BusTypeOfArtworkId == price.BusTypeOfArtworkId
                                                            && x.CompanyId == price.CompanyId)
                                                    .Where(x => x.MinCarBooking == price.MinCarBooking)
                                                    .OrderBy(x => x.FromDate).ToList();
                //[1]
                int count = prices.Count();
                if (count == 0 || toDate > prices[count - 1].ToDate.Date)
                    return false;
                //[2]
                bool isSequential = prices.Zip(prices.Skip(1),
                                  (a, b) => b.FromDate.Date == a.ToDate.Date.AddDays(1))
                             .All(x => x);
                return isSequential;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region TIME LINE
        public TaxiTimeLine GetTaxiTimeLine(int id, int year)
        {
            try
            {
                var bookingList = _context.TaxiBookings.Where(x => x.TaxiId == id)
                                                             .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active
                                                             || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                             .Where(bk => bk.FromDate.Year == year)
                                                             .Include(x => x.Invoices.Campaigns);

                var downtimeList = _context.TaxiDownTimes.Where(x => x.TaxiId == id)
                                                               .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                               .Where(bk => bk.FromDate.Year == year);
                var list = new TaxiTimeLine
                {
                    bookings = bookingList,
                    downTimes = downtimeList
                };

                return list;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public BusTimeLine GetBusTimeLine(int id, int year)
        {
            try
            {
                var bookingList = _context.BusBookings.Where(x => x.BusId == id)
                                                             .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active
                                                             || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                             .Where(bk => bk.FromDate.Year == year)
                                                             .Include(x => x.Invoices.Campaigns);

                var downtimeList = _context.BusDownTimes.Where(x => x.BusId == id)
                                                               .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                               .Where(bk => bk.FromDate.Year == year);
                var list = new BusTimeLine
                {
                    bookings = bookingList,
                    downTimes = downtimeList
                };

                return list;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public BusShelterTimeLine GetShelterTimeLine(int id, int year)
        {
            try
            {
                var bookingList = _context.BusShelterBookings.Where(x => x.BusShelterId == id)
                                                             .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active
                                                             || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                             .Where(bk => bk.FromDate.Year == year)
                                                             .Include(x => x.Invoices.Campaigns);

                var downtimeList = _context.BusShelterDownTimes.Where(x => x.BusShelterId == id)
                                                               .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                               .Where(bk => bk.FromDate.Year == year);
                var list = new BusShelterTimeLine
                {
                    bookings = bookingList,
                    downTimes = downtimeList
                };

                return list;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public BillboardTimeLine GetBillboardTimeLine(int id, int year)
        {
            try
            {
                var bookingList = _context.BillboardBookings.Where(x => x.BillboardId == id)
                                                            .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active
                                                            || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                            .Where(bk => bk.FromDate.Year == year)
                                                            .Include(x => x.Invoices.Campaigns);

                var downtimeList = _context.BillboardDownTimes.Where(x => x.BillboardId == id)
                                                              .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                              .Where(bk => bk.FromDate.Year == year);
                var list = new BillboardTimeLine
                {
                    bookings = bookingList,
                    downTimes = downtimeList
                };

                return list;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public DigitalTimeLine GetDigitalTimeLine(int id, DateTime fromDate, DateTime toDate, double hourPerDay, DateTime bookFromDate, DateTime bookToDate)
        {
            try
            {
                var downtimeList = _context.DigitalBillboardDownTimes.Where(x => x.DigitalBillboardId == id)
                                                                     .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active)
                                                                     .Where(bk => bk.FromDate <= toDate && fromDate <= bk.ToDate);

                var digital = _context.DigitalBillboards.Where(x => x.Id == id).FirstOrDefault();

                DateTime dt = fromDate;
                List<DigitalHourPerDay> bookings = new List<DigitalHourPerDay>();
                while (dt <= toDate)
                {
                    var bookingList = _context.DigitalBillboardBookings
                                                            .Where(x => x.DigitalBillboardId == id)
                                                            .Where(bk => bk.FromDate <= dt && dt <= bk.ToDate)
                                                            .Where(b => b.Statuses.Name == Constant.ConstantStatus.Active
                                                            || b.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                            .GroupBy(b => b.DigitalBillboardId)
                                                            .Select(x => new
                                                            {
                                                                DigitalBillboardId = x.Key,
                                                                Total = x.Sum(t => t.HourPerDay)
                                                            }).FirstOrDefault();

                    double totalHour = bookingList?.Total ?? 0;
                    double hourAddBook = totalHour;
                    if (bookFromDate <= dt && dt <= bookToDate)
                    {
                        hourAddBook = hourAddBook + hourPerDay;
                    }

                    bookings.Add(new DigitalHourPerDay
                    {
                        Date = dt,
                        TotalHours = totalHour,
                        IsFull = hourAddBook > digital.BroadcastingTime ? true : false
                    });

                    dt = dt.AddDays(1);
                }

                var list = new DigitalTimeLine
                {
                    BroadcastingTime = digital.BroadcastingTime,
                    downTimes = downtimeList,
                    bookings = bookings
                };

                return list;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region GET REPORT FOR PARTNER GROUP
        public IEnumerable<Campaign> GetCaimpaignForPartnerGroup(int userId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var userRole = _context.UserRoles.Where(r => r.UserId == userId).Select(s => new
                {
                    RoleId = s.Roles.Id,
                    RoleName = s.Roles.RoleName,
                    CompanyId = s.Users.CompanyId
                }).FirstOrDefault();
                var roleName = userRole?.RoleName ?? string.Empty;

                var list = from campaign in _context.Campaigns.Include(b => b.Invoices)
                                                                .Include(b => b.Statuses)
                                                                .OrderBy(b => b.StatusId)
                           select campaign;
                /*------------------------------ get list campaign --------------------------*/
                if (roleName.Equals(Constant.ConstantRole.TaxiCompany))
                {
                    return list.Where(x => x.Invoices.TaxiBookings.Any(x => x.Taxis.CompanyId == userRole.CompanyId))
                        .OrderBy(x => x.Invoices.Campaigns.StatusId).ThenBy(x => x.FromDate);
                }
                else if (roleName.Equals(Constant.ConstantRole.BusCompany))
                {
                    return list.Where(x => x.Invoices.BusBookings.Any(x => x.Buses.CompanyId == userRole.CompanyId))
                               .Include(x => x.Invoices.BusBookings).ThenInclude(x => x.Buses)
                               .OrderBy(x => x.Invoices.Campaigns.StatusId).ThenBy(x => x.FromDate);
                }
                else if (roleName.Equals(Constant.ConstantRole.BillboardCompany))
                {
                    return list.Where(x => x.Invoices.BillboardBookings.Any(x => x.Billboards.CompanyId == userRole.CompanyId)
                                        || x.Invoices.DigitalBillboardBookings.Any(x => x.DigitalBillboards.CompanyId == userRole.CompanyId))
                        .OrderBy(x => x.Invoices.Campaigns.StatusId).ThenBy(x => x.FromDate);
                }
                else if (roleName.Equals(Constant.ConstantRole.ShelterCompany))
                {
                    return list.Where(x => x.Invoices.BusShelterBookings.Any(x => x.BusShelters.CompanyId == userRole.CompanyId))
                        .OrderBy(x => x.Invoices.Campaigns.StatusId).ThenBy(x => x.FromDate);
                }

                return new List<Campaign>();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region MOBILE: GET CAMPAIGN INFO TO TAKE PHOTO
        public IEnumerable<Campaign> GetCaimpaignOwner(int userId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;

                if (Constant.ConstantRole.Coordinator.Equals(roleName))
                {
                    var listCampaign = from userCampaign in _context.UserCampaigns.Where(x => x.UserId == userId)
                                       join campaign in _context.Campaigns.Include(x => x.Invoices)
                                                                    .Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                                    .AsNoTracking()
                                                                    on userCampaign.CampaignId equals campaign.Id
                                       select campaign;
                    return listCampaign.Distinct();
                }
                else if (Constant.ConstantRole.TaxiDriver.Equals(roleName))
                {
                    var listCampaign = from userTaxi in _context.UserTaxis.Where(x => x.UserId == userId)
                                       join booking in _context.TaxiBookings
                                                        .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                        .AsNoTracking()
                                                        on userTaxi.TaxiId equals booking.TaxiId
                                       join campaign in _context.Campaigns.Include(x => x.Invoices).Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                        on booking.Invoices.CampaignId equals campaign.Id
                                       select campaign;
                    return listCampaign.Distinct();
                }
                else if (Constant.ConstantRole.BusDriver.Equals(roleName))
                {
                    var listCampaign = from userBus in _context.UserBuses.Where(x => x.UserId == userId)
                                       join booking in _context.BusBookings.Include(x => x.Invoices)
                                                        .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                        .AsNoTracking()
                                                        on userBus.BusId equals booking.BusId
                                       join campaign in _context.Campaigns.Include(x => x.Invoices).Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                        on booking.Invoices.CampaignId equals campaign.Id
                                       select campaign;
                    return listCampaign.Distinct();
                }
                else if (Constant.ConstantRole.BillboardOperator.Equals(roleName))
                {
                    var listBBB = from userBillboard in _context.UserBillboards.Where(x => x.UserId == userId)
                                  join bbb in _context.BillboardBookings.Include(x => x.Invoices)
                                                       .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                     on userBillboard.BillboardId equals bbb.BillboardId
                                  join campaign in _context.Campaigns.Include(x => x.Invoices).Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                   on bbb.Invoices.CampaignId equals campaign.Id
                                  select campaign;

                    var listDBB = from useDigital in _context.UserDigitalBillboards.Where(x => x.UserId == userId)
                                  join dbb in _context.DigitalBillboardBookings.Include(x => x.Invoices)
                                                                                .Where(x => x.Invoices.Campaigns.Statuses.Name == Constant.ConstantStatus.Sold)
                                                                                .AsNoTracking()
                                                                                on useDigital.DigitalBillboardId equals dbb.DigitalBillboardId
                                  join campaign in _context.Campaigns.Include(x => x.Invoices).Where(x => x.Statuses.Name == Constant.ConstantStatus.Sold)
                                                   on dbb.Invoices.CampaignId equals campaign.Id
                                  select campaign;
                    return listBBB.Union(listDBB).Distinct();
                }

                return new List<Campaign>();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public MobileBusBooking GetBusBookingByUser(int campaignId, int locationId, int userId, int? busRouteId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;
                if (!Constant.ConstantRole.BusDriver.Equals(roleName) && !Constant.ConstantRole.Coordinator.Equals(roleName))
                    return null;
                /*------------------------------ get list bus --------------------------*/
                var listBus = from booking in _context.BusBookings.Where(x => x.Invoices.CampaignId == campaignId)
                          .Where(x => x.Buses.BusRoutes.LocationId == locationId)
                          .AsNoTracking()
                              select booking;

                if (busRouteId != null)
                {
                    listBus = listBus.Where(x => x.Buses.BusRouteId == busRouteId);
                }
                if (Constant.ConstantRole.BusDriver.Equals(roleName))
                {
                    listBus = listBus.Where(x => x.Buses.UserBuses.Any(x => x.UserId == userId));
                }

                DateTime dt = DateTime.Now;
                var pictures = _context.BusPictures.Where(x => x.CampaignId == campaignId).ToList()
                               .OrderByDescending(x => x.LastUpdated).GroupBy(x => x.BusId).Select(x => x.First());

                var list = from booking in listBus.OrderByDescending(x => x.Statuses.Name)
                           select new MobileBusBookingRead
                           {
                               BookingId = booking.Id,
                               BusId = booking.BusId,
                               PlateNumber = booking.Statuses.Name == Constant.ConstantStatus.Unconfirmed ? " - - - " : booking.Buses.PlateNumber,
                               OTC = booking.OTC,
                               BusRoute = booking.Buses.BusRoutes,
                               Location = booking.Buses.BusRoutes.Locations,
                               Company = booking.Buses.Companies,
                               CarType = booking.Buses.BusTypes,
                               ArtType = booking.BusTypeOfArtworks,
                               Status = booking.Statuses,
                               FromDate = booking.FromDate,
                               ToDate = booking.ToDate
                           };

                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Bus)
                                                 .Select(x => x.PictureTypes).AsNoTracking();
                var listReturn = from lst in list.ToList()
                                 join pic in pictures
                                     on lst.BusId equals pic.BusId into picJ
                                 from pic in picJ.DefaultIfEmpty()
                                 select new MobileBusBookingRead
                                 {
                                     BookingId = lst.BookingId,
                                     BusId = lst.BusId,
                                     PlateNumber = lst.PlateNumber,
                                     OTC = lst.OTC,
                                     BusRoute = lst.BusRoute,
                                     Location = lst.Location,
                                     Company = lst.Company,
                                     CarType = lst.CarType,
                                     ArtType = lst.ArtType,
                                     Status = lst.Status,
                                     FromDate = lst.FromDate,
                                     ToDate = lst.ToDate,
                                     IsTakePicture = pic != null ? ((dt - pic.LastUpdated).GetValueOrDefault().Days <= 3 ? true : false) : false
                                 };

                return new MobileBusBooking
                {
                    PictureTypes = types,
                    List = listReturn
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public MobileTaxiBooking GetTaxiBookingByUser(int campaignId, int locationId, int userId, int? taxiCompanyId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;
                if (!Constant.ConstantRole.TaxiDriver.Equals(roleName) && !Constant.ConstantRole.Coordinator.Equals(roleName))
                    return null;
                /*------------------------------ get list bus --------------------------*/
                var listBooking = from booking in _context.TaxiBookings.Where(x => x.Invoices.CampaignId == campaignId)
                              .Where(x => x.Taxis.TaxiAreas.LocationId == locationId)
                              .AsNoTracking()
                                  select booking;

                if (taxiCompanyId != null)
                {
                    listBooking = listBooking.Where(x => x.Taxis.CompanyId == taxiCompanyId);
                }
                if (Constant.ConstantRole.TaxiDriver.Equals(roleName))
                {
                    listBooking = listBooking.Where(x => x.Taxis.UserTaxis.Any(x => x.UserId == userId));
                }

                DateTime dt = DateTime.Now;
                var pictures = _context.TaxiPictures.Where(x => x.CampaignId == campaignId).ToList()
                               .OrderByDescending(x => x.LastUpdated).GroupBy(x => x.TaxiId).Select(x => x.First());

                var list = from booking in listBooking.OrderByDescending(x => x.Statuses.Name)
                           select new MobileTaxiBookingRead
                           {
                               BookingId = booking.Id,
                               TaxiId = booking.TaxiId,
                               PlateNumber = booking.Statuses.Name == Constant.ConstantStatus.Unconfirmed ? " - - - " : booking.Taxis.PlateNumber,
                               OTC = booking.OTC,
                               Location = booking.Taxis.TaxiAreas.Locations,
                               Company = booking.Taxis.Companies,
                               CarType = booking.Taxis.TaxiTypes,
                               ArtType = booking.TaxiTypeOfArtworks,
                               Status = booking.Statuses,
                               FromDate = booking.FromDate,
                               ToDate = booking.ToDate
                           };

                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Taxi)
                                                 .Select(x => x.PictureTypes).AsNoTracking();
                var listReturn = from lst in list.ToList()
                                 join pic in pictures
                                     on lst.TaxiId equals pic.TaxiId into picJ
                                 from pic in picJ.DefaultIfEmpty()
                                 select new MobileTaxiBookingRead
                                 {
                                     BookingId = lst.BookingId,
                                     TaxiId = lst.TaxiId,
                                     PlateNumber = lst.PlateNumber,
                                     OTC = lst.OTC,
                                     Location = lst.Location,
                                     Company = lst.Company,
                                     CarType = lst.CarType,
                                     ArtType = lst.ArtType,
                                     Status = lst.Status,
                                     FromDate = lst.FromDate,
                                     ToDate = lst.ToDate,
                                     IsTakePicture = pic != null ? ((dt - pic.LastUpdated).GetValueOrDefault().Days <= 3 ? true : false) : false
                                 };

                return new MobileTaxiBooking
                {
                    PictureTypes = types,
                    List = listReturn
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public MobileShelterBooking GetShelterBookingByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;
                if (!Constant.ConstantRole.Coordinator.Equals(roleName))
                    return null;
                /*------------------------------ get list bus --------------------------*/
                var listBooking = from booking in _context.BusShelterBookings.Where(x => x.Invoices.CampaignId == campaignId)
                              .Where(x => x.BusShelters.Locations.ParentId == locationId)
                              .AsNoTracking()
                                  select booking;

                DateTime dt = DateTime.Now;
                var pictures = _context.BusShelterPictures.Where(x => x.CampaignId == campaignId).ToList()
                               .OrderByDescending(x => x.LastUpdated).GroupBy(x => x.BusShelterId).Select(x => x.First());

                var list = from booking in listBooking.OrderBy(x => x.BusShelters.Name)
                           join bb in _context.BusShelters.AsNoTracking() on booking.BusShelterId equals bb.Id
                           join loc in _context.Locations.AsNoTracking() on bb.LocationId equals loc.LocationId
                           join stt in _context.Statuses.AsNoTracking() on booking.StatusId equals stt.Id
                           select new MobileShelterBookingRead
                           {
                               BookingId = booking.Id,
                               ShelterId = booking.BusShelterId,
                               Name = bb.Name,
                               OTC = bb.OTC,
                               Location = loc,
                               Status = stt,
                               FromDate = booking.FromDate,
                               ToDate = booking.ToDate
                           };

                var listReturn = from lst in list.ToList()
                                 join pic in pictures
                                     on lst.ShelterId equals pic.BusShelterId into picJ
                                 from pic in picJ.DefaultIfEmpty()
                                 select new MobileShelterBookingRead
                                 {
                                     BookingId = lst.BookingId,
                                     ShelterId = lst.ShelterId,
                                     Name = lst.Name,
                                     OTC = lst.OTC,
                                     Location = lst.Location,
                                     Status = lst.Status,
                                     FromDate = lst.FromDate,
                                     ToDate = lst.ToDate,
                                     IsTakePicture = pic != null ? ((dt - pic.LastUpdated).GetValueOrDefault().Days <= 3 ? true : false) : false
                                 };

                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.BusShelter)
                                                 .Select(x => x.PictureTypes).AsNoTracking();
                return new MobileShelterBooking
                {
                    PictureTypes = types,
                    List = listReturn
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public IEnumerable<MobileBillboardBookingRead> GetBillboardBookingByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;
                if (!Constant.ConstantRole.BillboardOperator.Equals(roleName) && !Constant.ConstantRole.Coordinator.Equals(roleName))
                    return null;
                /*------------------------------ get list bus --------------------------*/
                var listBooking = from booking in _context.BillboardBookings.Where(x => x.Invoices.CampaignId == campaignId)
                                  .Where(x => x.Billboards.Locations.ParentId == locationId)
                                  .AsNoTracking()
                                  select booking;

                if (Constant.ConstantRole.BillboardOperator.Equals(roleName))
                {
                    listBooking = listBooking.Where(x => x.Billboards.UserBillboards.Any(x => x.UserId == userId));
                }

                DateTime dt = DateTime.Now;
                var pictures = _context.BillboardPictures.Where(x => x.CampaignId == campaignId).ToList()
                               .OrderByDescending(x => x.LastUpdated).GroupBy(x => x.BillboardId).Select(x => x.First());

                var list = from booking in listBooking.OrderBy(x => x.Billboards.Name).ToList()
                           join bb in _context.Billboards.AsNoTracking() on booking.BillboardId equals bb.Id
                           join loc in _context.Locations.AsNoTracking() on bb.LocationId equals loc.LocationId
                           join stt in _context.Statuses.AsNoTracking() on booking.StatusId equals stt.Id
                           select new MobileBillboardBookingRead
                           {
                               BookingId = booking.Id,
                               Id = booking.BillboardId,
                               Name = bb.Name,
                               Type = bb.Type,
                               OTC = bb.OTC,
                               Location = loc,
                               Status = stt,
                               FromDate = booking.FromDate,
                               ToDate = booking.ToDate,
                               ChannelName = Constant.ConstantMediaChannel.Billboard
                           };

                var listReturn = from lst in list.ToList()
                                 join pic in pictures
                                     on lst.Id equals pic.BillboardId into picJ
                                 from pic in picJ.DefaultIfEmpty()
                                 select new MobileBillboardBookingRead
                                 {
                                     BookingId = lst.BookingId,
                                     Id = lst.Id,
                                     Name = lst.Name,
                                     Type = lst.Type,
                                     OTC = lst.OTC,
                                     Location = lst.Location,
                                     Status = lst.Status,
                                     FromDate = lst.FromDate,
                                     ToDate = lst.ToDate,
                                     ChannelName = Constant.ConstantMediaChannel.Billboard,
                                     IsTakePicture = pic != null ? ((dt - pic.LastUpdated).GetValueOrDefault().Days <= 3 ? true : false) : false
                                 };

                return listReturn;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public IEnumerable<MobileBillboardBookingRead> GetDigitalBookingByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var roleName = MyUltil.GetUser(_context, userId)?.Roles.RoleName;
                if (!Constant.ConstantRole.BillboardOperator.Equals(roleName) && !Constant.ConstantRole.Coordinator.Equals(roleName))
                    return null;
                /*------------------------------ get list bus --------------------------*/
                var listBooking = from booking in _context.DigitalBillboardBookings.Where(x => x.Invoices.CampaignId == campaignId)
                              .Where(x => x.DigitalBillboards.Locations.ParentId == locationId)
                              .AsNoTracking()
                                  select booking;

                if (Constant.ConstantRole.BillboardOperator.Equals(roleName))
                {
                    listBooking = listBooking.Where(x => x.DigitalBillboards.UserDigitalBillboards.Any(x => x.UserId == userId));
                }

                DateTime dt = DateTime.Now;
                var pictures = _context.DigitalBillboardPictures.Where(x => x.CampaignId == campaignId).ToList()
                               .OrderByDescending(x => x.LastUpdated).GroupBy(x => x.DigitalBillboardId).Select(x => x.First());

                var list = from booking in listBooking.OrderBy(x => x.DigitalBillboards.Name)
                           join bb in _context.DigitalBillboards.AsNoTracking() on booking.DigitalBillboardId equals bb.Id
                           join loc in _context.Locations.AsNoTracking() on bb.LocationId equals loc.LocationId
                           join stt in _context.Statuses.AsNoTracking() on booking.StatusId equals stt.Id
                           select new MobileBillboardBookingRead
                           {
                               BookingId = booking.Id,
                               Id = booking.DigitalBillboardId,
                               Name = bb.Name,
                               Type = bb.Type,
                               OTC = bb.OTC,
                               Location = loc,
                               Status = stt,
                               FromDate = booking.FromDate,
                               ToDate = booking.ToDate,
                               ChannelName = Constant.ConstantMediaChannel.DigitalBillboard
                           };

                var listReturn = from lst in list.ToList()
                                 join pic in pictures
                                     on lst.Id equals pic.DigitalBillboardId into picJ
                                 from pic in picJ.DefaultIfEmpty()
                                 select new MobileBillboardBookingRead
                                 {
                                     BookingId = lst.BookingId,
                                     Id = lst.Id,
                                     Name = lst.Name,
                                     Type = lst.Type,
                                     OTC = lst.OTC,
                                     Location = lst.Location,
                                     Status = lst.Status,
                                     FromDate = lst.FromDate,
                                     ToDate = lst.ToDate,
                                     ChannelName = Constant.ConstantMediaChannel.DigitalBillboard,
                                     IsTakePicture = pic != null ? ((dt - pic.LastUpdated).GetValueOrDefault().Days <= 3 ? true : false) : false
                                 };

                return listReturn;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public MobileBillboardDigitalBooking GetBillboardBookingPictureByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                var billboards = GetBillboardBookingByUser(campaignId, locationId, userId);
                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Billboard)
                                                .Select(x => x.PictureTypes).AsNoTracking().ToList();

                return new MobileBillboardDigitalBooking
                {
                    PictureTypes = types,
                    List = billboards
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        public MobileBillboardDigitalBooking GetDigitalBookingPictureByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                var digitals = GetDigitalBookingByUser(campaignId, locationId, userId);
                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Billboard)
                                                .Select(x => x.PictureTypes).AsNoTracking().ToList();

                return new MobileBillboardDigitalBooking
                {
                    PictureTypes = types,
                    List = digitals
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        /// <summary>
        /// NOT USE
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MobileBillboardDigitalBooking GetBillboardAndDigitalBookingByUser(int campaignId, int locationId, int userId)
        {
            try
            {
                var billboards = GetBillboardBookingByUser(campaignId, locationId, userId);
                var digitals = GetDigitalBookingByUser(campaignId, locationId, userId);
                var types = _context.MediaPictures.Include(x => x.PictureTypes)
                                                .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Billboard)
                                                .Select(x => x.PictureTypes).AsNoTracking().ToList();

                var list = digitals.Union(billboards).ToList() ?? null;
                return new MobileBillboardDigitalBooking
                {
                    PictureTypes = types,
                    List = list
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region [DASHBOARD]
        public object DashboardForBus(int companyId)
        {
            try
            {
                var camppaign = from camp in _context.Campaigns.Where(x => x.Statuses.Name != Constant.ConstantStatus.Canceled)
                                select camp;

                var models = (from camp in camppaign.ToList()
                              join inv in _context.Invoices on camp.Id equals inv.CampaignId
                              join bb in _context.BusBookings.Where(x => x.Buses.CompanyId == companyId) on inv.Id equals bb.InvoiceId
                              join stt in _context.Statuses on camp.StatusId equals stt.Id
                              group new { camp, bb } by stt into g
                              select new
                              {
                                  Status = g.Key.Name,
                                  Total = g.Count(),
                                  Fee = g.Sum(x => x.bb.TotalFee),
                              }).ToList();

                var status = _context.Statuses.Where(x => x.Type == Constant.ConstantStatus.TypeCampaign
                                                       && x.Name != Constant.ConstantStatus.Canceled).AsEnumerable();
                var result = from stt in status
                             join mo in models on stt.Name equals mo.Status into moG
                             from mo in moG.DefaultIfEmpty()
                             select new
                             {
                                 Status = stt.Name,
                                 Total = mo?.Total ?? 0,
                                 Fee = mo?.Fee ?? 0
                             };
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public object DashboardForTaxi(int companyId)
        {
            try
            {
                var camppaign = from camp in _context.Campaigns.Where(x => x.Statuses.Name != Constant.ConstantStatus.Canceled)
                                select camp;

                var models = (from camp in camppaign.ToList()
                              join inv in _context.Invoices on camp.Id equals inv.CampaignId
                              join bb in _context.TaxiBookings.Where(x => x.Taxis.CompanyId == companyId) on inv.Id equals bb.InvoiceId
                              join stt in _context.Statuses on camp.StatusId equals stt.Id
                              group new { camp, bb } by stt into g
                              select new
                              {
                                  Status = g.Key.Name,
                                  Total = g.Select(x => x.camp).Distinct().Count(),
                                  Fee = g.Sum(x => x.bb.TotalFee),
                              }).ToList();

                var status = _context.Statuses.Where(x => x.Type == Constant.ConstantStatus.TypeCampaign
                                                       && x.Name != Constant.ConstantStatus.Canceled).AsEnumerable();
                var result = from stt in status
                             join mo in models on stt.Name equals mo.Status into moG
                             from mo in moG.DefaultIfEmpty()
                             select new
                             {
                                 Status = stt.Name,
                                 Total = mo?.Total ?? 0,
                                 Fee = mo?.Fee ?? 0
                             };
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public object DashboardForShelter(int companyId)
        {
            try
            {
                var camppaign = from camp in _context.Campaigns.Where(x => x.Statuses.Name != Constant.ConstantStatus.Canceled)
                                select camp;

                var models = (from camp in camppaign.ToList()
                              join inv in _context.Invoices on camp.Id equals inv.CampaignId
                              join bb in _context.BusShelterBookings.Where(x => x.BusShelters.CompanyId == companyId) on inv.Id equals bb.InvoiceId
                              join stt in _context.Statuses on camp.StatusId equals stt.Id
                              group new { camp, bb } by stt into g
                              select new
                              {
                                  Status = g.Key.Name,
                                  Total = g.Select(x => x.camp).Distinct().Count(),
                                  Fee = g.Sum(x => x.bb.TotalFee),
                              }).ToList();

                var status = _context.Statuses.Where(x => x.Type == Constant.ConstantStatus.TypeCampaign
                                                       && x.Name != Constant.ConstantStatus.Canceled).AsEnumerable();
                var result = from stt in status
                             join mo in models on stt.Name equals mo.Status into moG
                             from mo in moG.DefaultIfEmpty()
                             select new
                             {
                                 Status = stt.Name,
                                 Total = mo?.Total ?? 0,
                                 Fee = mo?.Fee ?? 0
                             };
                return result;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public object DashboardForBillboard(int companyId)
        {
            try
            {
                var camppaign = from camp in _context.Campaigns.Where(x => x.Statuses.Name != Constant.ConstantStatus.Canceled)
                                select camp;

                var billboard = (from camp in camppaign.ToList()
                                 join inv in _context.Invoices on camp.Id equals inv.CampaignId
                                 join bb in _context.BillboardBookings.Where(x => x.Billboards.CompanyId == companyId) on inv.Id equals bb.InvoiceId
                                 join stt in _context.Statuses on camp.StatusId equals stt.Id
                                 group new { camp, bb } by stt into g
                                 select new
                                 {
                                     Status = g.Key.Name,
                                     Total = g.Select(x => x.camp).Distinct().Count(),
                                     Fee = g.Sum(x => x.bb.TotalFee),
                                 }).ToList();

                var digital = (from camp in camppaign.ToList()
                               join inv in _context.Invoices on camp.Id equals inv.CampaignId
                               join bb in _context.DigitalBillboardBookings.Where(x => x.DigitalBillboards.CompanyId == companyId) on inv.Id equals bb.InvoiceId
                               join stt in _context.Statuses on camp.StatusId equals stt.Id
                               group new { camp, bb } by stt into g
                               select new
                               {
                                   Status = g.Key.Name,
                                   Total = g.Select(x => x.camp).Distinct().Count(),
                                   Fee = g.Sum(x => x.bb.TotalFee),
                               }).ToList();

                var status = _context.Statuses.Where(x => x.Type == Constant.ConstantStatus.TypeCampaign
                                                       && x.Name != Constant.ConstantStatus.Canceled).AsEnumerable();
                var resultBB = from stt in status
                               join mo in billboard on stt.Name equals mo.Status into moG
                               from mo in moG.DefaultIfEmpty()
                               select new
                               {
                                   Status = stt.Name,
                                   Total = mo?.Total ?? 0,
                                   Fee = mo?.Fee ?? 0
                               };

                var resultDB = from stt in status
                               join mo in digital on stt.Name equals mo.Status into moG
                               from mo in moG.DefaultIfEmpty()
                               select new
                               {
                                   Status = stt.Name,
                                   Total = mo?.Total ?? 0,
                                   Fee = mo?.Fee ?? 0
                               };

                return new
                {
                    billboard = resultBB,
                    digital = resultDB
                };
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}