﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class UserCreateDto
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int RoleId { get; set; }
        [Required, Id]
        public int StatusId { get; set; }

    }
    
}
