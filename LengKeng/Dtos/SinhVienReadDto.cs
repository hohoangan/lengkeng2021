﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class SinhVienReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("StudentID")]
        public int StudentID { get; set; }
        public Student Students { get; set; }
    }
}
