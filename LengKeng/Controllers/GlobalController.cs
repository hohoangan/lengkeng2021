﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LengKeng.Data;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GlobalController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IStringLocalizer<Message> _localizer;

        public GlobalController(LengKengDbContext context, IStringLocalizer<Message> localizer)
        {
            _context = context;
            _localizer = localizer;
        }

        //GET api/global/location
        /// <summary>
        /// danh sách tỉnh thành
        /// </summary>
        /// <returns></returns>
        [HttpGet("locations")]
        public ActionResult GetLocation()
        {
            try
            {
                var models = _context.Locations.Where(x => x.Level == 1)
                                    .OrderBy(x => x.OrderBy).ThenBy(x => x.Name);
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/global/locations/district
        /// <summary>
        /// Danh sách quận huyện
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [HttpGet("locations/district")]
        public ActionResult GetDistrictForBooking(int locationId)
        {
            try
            {
                var models = _context.Locations
                                    .Where(x => x.ParentId == locationId)
                                    .ToList().OrderBy(x => x.Name, new StrCmpLogicalComparer()); ;

                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/global/media
        /// <summary>
        /// Danh sách media type
        /// </summary>
        /// <returns></returns>
        [HttpGet("media")]
        public ActionResult GetMediaChannels()
        {
            try
            {
                var model = _context.MediaChannels.Include(m => m.MediaPictures).ThenInclude(m => m.PictureTypes);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/global/media/{id}
        /// <summary>
        /// Loại hình ảnh của channel
        /// </summary>
        /// <param name="mediaChannelId"></param>
        /// <returns></returns>
        [HttpGet("media/{mediaChannelId}")]
        public ActionResult GetPictureTypes(int mediaChannelId)
        {
            try
            {
                var model = _context.MediaChannels.Where(m => m.Id == mediaChannelId).Include(m => m.MediaPictures).ThenInclude(m => m.PictureTypes);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/global/picturejob/
        /// <summary>
        /// Nhắc chụp hình
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [HttpPost("picturejob/{time}")]
        public ActionResult CreateTakePhotoJob(int time)
        {
            try
            {
                CronJob.CreateTakePictureJob(time);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/global/setPriceJob/
        /// <summary>
        /// Nhắc nhở đặt giá
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [HttpPost("setPriceJob/{time}")]
        public ActionResult CreateRemindSetPriceJob(int time)
        {
            try
            {
                CronJob.CreateRemindSetPriceJob(time);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //Post api/global/sendmail/
        /// <summary>
        /// Gửi mail KH
        /// </summary>
        /// <param name="content">trong content.to: nhập vào email nhận của KH cần gửi</param>
        /// <returns></returns>
        [HttpPost("sendmail")]
        [AllowAnonymous]
        public ActionResult SendMail(Mail content)
        {
            try
            {
                var emailList = new List<string> {"contact@lengkeng.com.vn"};
                if (!string.IsNullOrEmpty(content.to))
                    emailList = new List<string> { content.to };
                //sendmail
                bool kq = Gmail.SendMailTemplate(emailList, content.subject, content.body);
                if (kq == false)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }
                return Ok(_localizer[Constant.ConstantMessage.Successfully].Value);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //Post api/global/sendmail2cus/
        /// <summary>
        /// Gửi mail cho customer
        /// </summary>
        /// <param name="content"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpPost("sendmail2cus")]
        public ActionResult SendMail2Cus(Mail content, int campaignId)
        {
            try
            {
                var emailList = (from camp in _context.Campaigns.Where(x => x.Id == campaignId)
                           join us in _context.Users.Where(x=> x.Statuses.Name == Constant.ConstantStatus.Active)
                                    on camp.CreatedUserId equals us.Id
                           select us.Email).ToList();

                //sendmail
                bool kq = Gmail.SendMailTemplate(emailList, content.subject, content.body);
                if (kq == false)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }
                return Ok(_localizer[Constant.ConstantMessage.Successfully].Value);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //Post api/global/sendmail2super/
        /// <summary>
        /// Gửi mail cho supper user
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        [HttpPost("sendmail2super")]
        public ActionResult SendMail2Sale(Mail content)
        {
            try
            {
                var emailList = _context.UserRoles.Where(x => x.Roles.RoleName == Constant.ConstantRole.SuperUser
                                                        && x.Users.Statuses.Name == Constant.ConstantStatus.Active)
                                                  .Select(x => x.Users.Email).ToList();

                //sendmail
                bool kq = Gmail.SendMailTemplate(emailList, content.subject, content.body);
                if (kq == false)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }
                return Ok(_localizer[Constant.ConstantMessage.Successfully].Value);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //Post api/global/sendmail2cusandsupper/
        /// <summary>
        /// Gử mail cho Partner
        /// </summary>
        /// <param name="content"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpPost("sendmail2partner")]
        public ActionResult SendMail2Partner(Mail content, int campaignId)
        {
            try
            {
                /*
                var emailList = MyUltil.GetAllEmailPartnerOfCampaign(_context, campaignId);
                //sendmail
                bool kq = Gmail.SendMailTemplate(emailList, content.subject, content.body);
                if (kq == false)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.FailureSendMail].Value);
                }
                */
                return Ok(_localizer[Constant.ConstantMessage.Successfully].Value);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
    }
}
