﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BusShelterDiscounts_Month_LocationId_CompanyId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BillboardDiscounts_Month_LocationId_CompanyId",
                table: "BillboardDiscounts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "BillboardDiscounts");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "BillboardDiscounts");

            migrationBuilder.AddColumn<int>(
                name: "BusShelterDiscountId",
                table: "BusShelterDiscounts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BusShelterPriceId",
                table: "BusShelterDiscounts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BillboardPriceId",
                table: "BillboardDiscounts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDiscounts_BusShelterDiscountId",
                table: "BusShelterDiscounts",
                column: "BusShelterDiscountId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDiscounts_BusShelterPriceId",
                table: "BusShelterDiscounts",
                column: "BusShelterPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDiscounts_Month_BusShelterPriceId",
                table: "BusShelterDiscounts",
                columns: new[] { "Month", "BusShelterPriceId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDiscounts_BillboardPriceId",
                table: "BillboardDiscounts",
                column: "BillboardPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDiscounts_Month_BillboardPriceId",
                table: "BillboardDiscounts",
                columns: new[] { "Month", "BillboardPriceId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BillboardDiscounts_BillboardPrices_BillboardPriceId",
                table: "BillboardDiscounts",
                column: "BillboardPriceId",
                principalTable: "BillboardPrices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BusShelterDiscounts_BusShelterDiscounts_BusShelterDiscountId",
                table: "BusShelterDiscounts",
                column: "BusShelterDiscountId",
                principalTable: "BusShelterDiscounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusShelterDiscounts_BusShelterPrices_BusShelterPriceId",
                table: "BusShelterDiscounts",
                column: "BusShelterPriceId",
                principalTable: "BusShelterPrices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BillboardDiscounts_BillboardPrices_BillboardPriceId",
                table: "BillboardDiscounts");

            migrationBuilder.DropForeignKey(
                name: "FK_BusShelterDiscounts_BusShelterDiscounts_BusShelterDiscountId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropForeignKey(
                name: "FK_BusShelterDiscounts_BusShelterPrices_BusShelterPriceId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusShelterDiscounts_BusShelterDiscountId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusShelterDiscounts_BusShelterPriceId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusShelterDiscounts_Month_BusShelterPriceId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BillboardDiscounts_BillboardPriceId",
                table: "BillboardDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BillboardDiscounts_Month_BillboardPriceId",
                table: "BillboardDiscounts");

            migrationBuilder.DropColumn(
                name: "BusShelterDiscountId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropColumn(
                name: "BusShelterPriceId",
                table: "BusShelterDiscounts");

            migrationBuilder.DropColumn(
                name: "BillboardPriceId",
                table: "BillboardDiscounts");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "BusShelterDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "BusShelterDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "BillboardDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "BillboardDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDiscounts_Month_LocationId_CompanyId",
                table: "BusShelterDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDiscounts_Month_LocationId_CompanyId",
                table: "BillboardDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);
        }
    }
}
