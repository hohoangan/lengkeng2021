﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class DigitalBillboardPrice
    {
        [Key]
        public int Id { get; set; }
        [Required, price]
        public double Price { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
        [Required, Id]
        public int DigitalBillboardId { get; set; }
        [JsonIgnore]
        public DigitalBillboard DigitalBillboards { get; set; }
        [Required, Id]
        public int UnitId { get; set; }
        [JsonIgnore]
        public Unit Units { get; set; }

        [Required, Id]
        public int CreatedUserId { get; set; }
        [Required, Id]
        public int UserId { get; set; }
        [JsonIgnore]
        public User Users { get; set; }
    }
}
