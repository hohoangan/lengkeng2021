﻿using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace LengKeng.Controllers
{
    [Route("api/commands")]
    [ApiController]
    public class CommandsController : ControllerBase
    {
        private readonly ICommanderRepo _repository;
        private readonly IMapper _mapper;
        private readonly Data.LengKengDbContext _context;

        public CommandsController(ICommanderRepo reponsitory, IMapper mapper, Data.LengKengDbContext context)
        {
            _repository = reponsitory;
            _mapper = mapper;
            _context = context;
        }

        [HttpGet("students")]
        public ActionResult<IEnumerable<Student>> GetAllStudents()
        {
            var students = _context.Students;     
            return Ok(students);
        }

        [HttpGet("test")]
        public ActionResult test()
        {
            return Ok("123 test ok");
        }

        [HttpPost("sinhvienlst")]
        public ActionResult<List<SinhVien>> CreateSinhVienLst(List<SinhVienCreateDto> sinhvienCreateDto)
        {
            _context.Database.BeginTransaction();
            try
            {
                //map 1 list
                var svModelList = _mapper.Map<List<SinhVienCreateDto>, List<SinhVien>>(sinhvienCreateDto);
                _context.SinhViens.AddRange(svModelList);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok(svModelList);
            }
            catch (Exception)
            {
                _context.Database.RollbackTransaction();
                return NotFound();
            }
        }

        [HttpPost("sinhvien")]
        public ActionResult<SinhVien> CreateSinhVien(SinhVienCreateDto sinhvienCreateDto)
        {
            _context.Database.BeginTransaction();
            try
            {
                var sinhvienModel = _mapper.Map<SinhVien>(sinhvienCreateDto);
                _context.SinhViens.Add(sinhvienModel);
                _context.SaveChanges();

                //throw new Exception();
                var sinhvienReadDto = _mapper.Map<SinhVienReadDto>(sinhvienModel);
                sinhvienReadDto.Students = _context.Students.Find(sinhvienModel.StudentID);

                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetSinhViens), new { Id = sinhvienReadDto.Id }, sinhvienReadDto);

            }
            catch (Exception)
            {
                _context.Database.RollbackTransaction();
                return BadRequest();
            }

        }
         
        [HttpGet("sinhvien")]
        public ActionResult<IEnumerable<SinhVien>> GetAllSinhViens(int PageNumber, int PageSize)
        {
            var sinhViens = _context.SinhViens.Include(r => r.Students);
            var response =  PagedList<SinhVien>.ToPagedList(sinhViens, PageNumber, PageSize);

            Response.Headers.Add(response.HeaderInfo);
            return Ok(response);
        }

        [HttpGet("sinhvien/{id}", Name = "GetSinhViens")]
        public ActionResult<SinhVien> GetSinhViens(int id)
        {
            //var sinhViens = _context.SinhViens.Where(b=>b.Id == Id);  
            var sinhViens = _context.SinhViens.Include(r=>r.Students).Where(s=>s.Id==id).First();

            if (sinhViens != null)
            {
                return Ok(sinhViens);
            }
            return NotFound();
        }

        [HttpPut("sinhvien/{Id}")]
        public ActionResult UpdateSinhViens(int Id, SinhVien sinhVien)
        {
            var sinhViens = _context.SinhViens.Where(b=>b.Id == Id);
            if (sinhVien == null)
                return NotFound();

            _context.Entry(sinhVien).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return NoContent();
        }

        //private readonly MockCommanderRepo _repository = new MockCommanderRepo();
        //GET api/commands
        [HttpGet]
        public ActionResult<IEnumerable<CommandReadDto>> GetAllCommands()
        {
            var commandItems = _repository.GetAllCommands();
            return Ok(_mapper.Map<IEnumerable<CommandReadDto>>(commandItems));
        }

        //GET api/commands/5
        [HttpGet("{id}", Name = "GetCommandById")]
        public ActionResult <CommandReadDto> GetCommandById(int id)
        {
            var commandItem = _repository.GetCommandById(id);
            if(commandItem != null)
                return Ok(_mapper.Map<CommandReadDto>(commandItem));
            return NotFound();
        }

        //POST api/commands/
        [HttpPost]
        public ActionResult<CommandReadDto> CreateCommand(CommandCreateDto commandCreateDto)
        {
            var commandModel = _mapper.Map<Command>(commandCreateDto);
            _repository.CreateCommand(commandModel);
            _repository.SaveChanges();
            var commandReadDto = _mapper.Map<CommandReadDto>(commandModel);

            //tao thanh cong, tra ve thong tin trong Header: 201 Created
            return CreatedAtRoute(nameof(GetCommandById), new { Id = commandReadDto.Id }, commandReadDto);
            //return Ok(commandReadDto);
        }

        //PUT api/commands/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCommand(int id, CommandUpdateDto commandUpdateDto)
        {
            var commandModelFormRepo = _repository.GetCommandById(id);
            if (commandModelFormRepo == null)
                return NotFound();

            //vi dieu
            _mapper.Map(commandUpdateDto, commandModelFormRepo);
            _repository.UpdateCommand(commandModelFormRepo);
            _repository.SaveChanges();

            return NoContent();
        }

        //PATCH api/commands/{id}
        [HttpPatch("{id}")]
        public ActionResult ParticalCommandUpdate(int id, JsonPatchDocument<CommandUpdateDto> patchDoc)
        {
            var commandModelFormRepo = _repository.GetCommandById(id);
            if (commandModelFormRepo == null)
                return NotFound();
            var commandToPatch = _mapper.Map<CommandUpdateDto>(commandModelFormRepo);
            patchDoc.ApplyTo(commandToPatch, ModelState);
            if (!TryValidateModel(commandToPatch))
            {
                return ValidationProblem(ModelState);
            }
            //vi dieu
            _mapper.Map(commandToPatch, commandModelFormRepo);
            _repository.UpdateCommand(commandModelFormRepo);
            _repository.SaveChanges();

            return NoContent();
        }

        //DELETE api/commands/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCommand(int id)
        {
            var commandModelFormRepo = _repository.GetCommandById(id);
            if (commandModelFormRepo == null)
                return NotFound();
            _repository.DeleteCommand(commandModelFormRepo);
            _repository.SaveChanges();

            return NoContent();
        }
    }
}
