﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace LengKeng.Models
{
    public class Status
    {
        /**
         * Status.Name: “Reserved”, “Sold”, “Completed”, “Cancelled” when Type = “Campaign”
         * Status.Name: “Active”, “Inactive” when Type=“Booking-Downtime”
         */
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }

        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }
        [JsonIgnore]
        public virtual ICollection<Billboard> Billboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboard> DigitalBillboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelter> BusShelters { get; set; }
        [JsonIgnore]
        public virtual ICollection<Taxi> Taxis { get; set; }
        [JsonIgnore]
        public virtual ICollection<Bus> Buses { get; set; }

        [JsonIgnore]
        public virtual ICollection<BillboardDownTime> BillboardDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardDownTime> DigitalBillboardDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusDownTime> BusDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiDownTime> TaxiDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterDownTime> BusShelterDownTimes { get; set; }

        [JsonIgnore]
        public virtual ICollection<BillboardBooking> BillboardBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardBooking> DigitalBillboardBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterBooking> BusShelterBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusBooking> BusBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiBooking> TaxiBookings { get; set; }
    }
}
