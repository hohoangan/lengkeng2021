﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Taxi
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PlateNumber { get; set; }
        [Required, Id]
        public int TaxiTypeId { get; set; }
        [JsonIgnore]
        public TaxiType TaxiTypes { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        //[Required, Id]
        //public int OTC { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [Required, Id]
        public int TaxiAreaId { get; set; }
        [JsonIgnore]
        public TaxiArea TaxiAreas { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        public virtual ICollection<UserTaxi> UserTaxis { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiPicture> TaxiPictures { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiDownTime> TaxiDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiBooking> TaxiBookings { get; set; }
    }
}
