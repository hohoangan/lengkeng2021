﻿using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
	public class PagedList<T> : List<T>
	{
		public int CurrentPage { get; set; }
		public int TotalPages { get; set; }
		public int Available { get; set; }
		public int PageSize { get; set; }
		public int TotalCount { get; set; }
		public bool HasPrevious => CurrentPage > 1;
		public bool HasNext => CurrentPage < TotalPages;

		public KeyValuePair<string, StringValues> HeaderInfo { set; get; }

        public PagedList(List<T> items, int count, int pageNumber, int pageSize, int avail = 0)
		{
			TotalCount = count;
			Available = avail;
			PageSize = pageSize;
			CurrentPage = pageNumber;
			TotalPages = (int)Math.Ceiling(count / (double)pageSize);
			HeaderInfo = new KeyValuePair<string, StringValues>("X-Pagination", XPagination(this));
			AddRange(items);
		}

		public string XPagination(PagedList<T> obj)
        {
			var owners = obj;
			var metadata = new
			{
				owners.TotalCount,
				owners.Available,
				owners.PageSize,
				owners.CurrentPage,
				owners.TotalPages,
				owners.HasNext,
				owners.HasPrevious
			};
			return JsonConvert.SerializeObject(metadata);
		}

		public static PagedList<T> ToPagedList(IQueryable<T> source, int pageNumber, int pageSize, int avail = 0)
		{
			var count = source.Count();
			var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

			return new PagedList<T>(items, count, pageNumber, pageSize, avail);
		}

		public static PagedList<T> ToPagedListIEnumerable(IEnumerable<T> source, int pageNumber, int pageSize, int avail = 0)
		{
			var count = source.Count();
			var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

			return new PagedList<T>(items, count, pageNumber, pageSize, avail);
		}
	}

	public class Parameters
	{
		const int maxPageSize = 50;
		public int PageNumber { get; set; } = 1;

		private int _pageSize = 10;
		public int PageSize
		{
			get
			{
				return _pageSize;
			}
			set
			{
				_pageSize = (value > maxPageSize) ? maxPageSize : value;
			}
		}
	}
}
