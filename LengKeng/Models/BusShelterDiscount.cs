﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusShelterDiscount
    {
        [Required]
        public int Id { get; set; }
        [Required, Id]
        public int BusShelterPriceId { get; set; }
        [JsonIgnore]
        public BusShelterPrice BusShelterPrice { get; set; }
        //[Required, Id]
        //public int LocationId { get; set; }
        //[Required, Id]
        //public int CompanyId { get; set; }
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
        public virtual ICollection<BusShelterDiscount> BusShelterDiscountS { get; set; }

    }
}
