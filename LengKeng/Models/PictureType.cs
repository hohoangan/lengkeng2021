﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class PictureType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Remark { get; set; }
        //public virtual ICollection<MediaPicture> MediaPictures { get; set; }
    }
}
