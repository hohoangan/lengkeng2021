﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiType
    {
        //Sedan, hatchback, SUV, 4-seat, 7-seat
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public virtual ICollection<Taxi> Taxis { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiAreaPrice> TaxiAreaPrices { get; set; }

    }
}
