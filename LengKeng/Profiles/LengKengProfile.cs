﻿using AutoMapper;
using LengKeng.Dtos;
using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Profiles
{
    public class LengKengProfile : Profile
    {
        public LengKengProfile()
        {
            //<Source -> Target>
            CreateMap<Command, CommandReadDto>();
            CreateMap<CommandCreateDto, Command>();
            CreateMap<CommandUpdateDto, Command>();
            CreateMap<Command, CommandUpdateDto>();

            CreateMap<SinhVien, SinhVienReadDto>();
            CreateMap<SinhVienCreateDto, SinhVien>();

            CreateMap<User, UserReadDto>();
            CreateMap<UserCreateDto, User>();
            CreateMap<UserUpdateDto, User>();

            CreateMap<BusRouteUpdateDto, BusRoute>();
            CreateMap<BusUpdateDto, Bus>();
            CreateMap<BusRoutePriceUpdateDto, BusRoutePrice>();
            CreateMap<BusDownTimeUpdateDto, BusDownTime>();
            CreateMap<BusDiscountUpdateDto, BusDiscount>();

            CreateMap<TaxiUpdateDto, Taxi>();
            CreateMap<TaxiAreaUpdateDto, TaxiArea>();
            CreateMap<TaxiAreaPriceUpdateDto, TaxiAreaPrice>();
            CreateMap<TaxiDownTimeUpdateDto, TaxiDownTime>();
            CreateMap<TaxiDiscountUpdateDto, TaxiDiscount>();

            CreateMap<BusShelterUpdateDto, BusShelter>();
            CreateMap<BusShelterPriceUpdateDto, BusShelterPrice>();
            CreateMap<BusShelterDownTimeUpdateDto, BusShelterDownTime>();
            CreateMap<BusShelterDiscountUpdateDto, BusShelterDiscount>();

            CreateMap<BillboardUpdateDto, Billboard>();
            CreateMap<BillboardPriceUpdateDto, BillboardPrice>();
            CreateMap<BillboardDownTimeUpdateDto, BillboardDownTime>();

            CreateMap<DigitalBillboardUpdateDto, DigitalBillboard>();
            CreateMap<DigitalBillboardPriceUpdateDto, DigitalBillboardPrice>();
            CreateMap<DigitalBillboardDownTimeUpdateDto, DigitalBillboardDownTime>();

            CreateMap<BillboardDisplayUpdateDto, BillboardDisplay>();
            CreateMap<BusShelterDisplayUpdateDto, BusShelterDisplay>();
            CreateMap<DigitalBillboardDisplayUpdateDto, DigitalBillboardDisplay>();
            CreateMap<BusDisplayUpdateDto, BusDisplay>();
            CreateMap<TaxiDisplayUpdateDto, TaxiDisplay>();
        }
    }
}
