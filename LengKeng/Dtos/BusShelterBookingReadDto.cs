﻿using LengKeng.Models;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusShelterBookingReadDto
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public double Price { get; set; }
        public int MinMonthBooking { get; set; }
        public int Amount { get; set; }
        public double InstallationCost { get; set; }
        public int InstallationTimes { get; set; }
        public double DiscountPercent { get; set; }
        public double TotalFee { get; set; }
        public int BusShelterId { get; set; }
        [JsonIgnore]
        public BusShelter BusShelters { get; set; }
        public int UnitId { get; set; }
        public double OTC { get; set; }
        public int StatusId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        public bool IsAvailable { get; set; }
    }

    public class BusShelterAvailable{
        public BusShelter BusShelter { get; set; }
        public int LocId { get; set; }
        public bool IsAvailable { get; set; }
    }
}
