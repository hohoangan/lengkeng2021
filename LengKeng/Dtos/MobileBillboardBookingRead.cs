﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class MobileBillboardBookingRead
    {
        public int BookingId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public double OTC { get; set; }
        public Location Location { get; set; }
        public Status Status { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; }
        public string ChannelName { get; set; }
        public bool IsTakePicture { get; set; }
    }

    public class MobileBillboardDigitalBooking
    {
        public IEnumerable<PictureType> PictureTypes { get; set; }
        public IEnumerable<MobileBillboardBookingRead> List { get; set; }
    }
}
