﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusShelterDiscountUpdateDto
    {
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
    }
}
