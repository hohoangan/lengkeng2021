﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "TaxiBookings",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "TaxiAreaPrices",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "BusRoutePrices",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "BusBookings",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "BillboardPrices",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PromotePercent",
                table: "BillboardBookings",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "TaxiBookings");

            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "TaxiAreaPrices");

            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "BusRoutePrices");

            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "BusBookings");

            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "BillboardPrices");

            migrationBuilder.DropColumn(
                name: "PromotePercent",
                table: "BillboardBookings");
        }
    }
}
