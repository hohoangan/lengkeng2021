﻿using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using Microsoft.EntityFrameworkCore;
using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LengKeng.Ultils
{
    public static class MyUltil
    {
        public static string ConnectionString { get; set; }
        public static IServiceProvider Instance { get; set; }
        public static string encodeM5(string original)
        {
            try
            {
                string source = original;
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = GetMd5Hash(md5Hash, source);
                    return sha256(hash);
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }
        public static string sha256(string randomString)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        private static IEnumerable<Location> FindAllParents(Dictionary<int, Location> all_data, Location child)
        {
            if (!all_data.ContainsKey(child.ParentId.GetValueOrDefault()))//GetValueOrDefault => convert int? to int
                return Enumerable.Empty<Location>();

            var parent = all_data[child.ParentId.GetValueOrDefault()];

            return new[] { parent }.Concat(FindAllParents(all_data, parent));
        }

        public static IEnumerable<Location> GetLocation(LengKengDbContext context, int locId)
        {
            var elements = context.Locations;
            var dictionary = elements.ToDictionary(x => x.LocationId); //You need to do this only once to convert the list into a Dictionary

            var child = elements.First(x => x.LocationId == locId);
            var parents = new[] { child }.Concat(FindAllParents(dictionary, child).ToList());
            return parents;
        }

        public static int GetCurrentUserId(ClaimsIdentity claimsIdentity)
        {
            try
            {
                List<Claim> claims = claimsIdentity.Claims.ToList();
                var userName = claims[0].Value;
                var id = int.Parse(claims[1].Value);
                var companyId = claims[2].Value;
                var roleName = claims[3].Value;

                return id;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static UserIdentity GetCurrentUserInfo(ClaimsIdentity claimsIdentity)
        {
            try
            {
                UserIdentity us = new UserIdentity();
                List<Claim> claims = claimsIdentity.Claims.ToList();

                us.UserName = claims[0].Value;
                us.Id = int.Parse(claims[1].Value);
                us.CompanyId = claims[2].Value;
                us.RoleName = claims[3].Value;

                return us;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static UserRole GetUser(LengKengDbContext context, int userId)
        {
            try
            {
                var usr = context.UserRoles.Include(x => x.Users)
                                           .Include(x => x.Roles)
                                           .Where(x => x.UserId == userId).FirstOrDefault();
                return usr;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string RandomNum(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static IEnumerable<T> EnsureNotEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return Enumerable.Empty<T>();
            }
            else
            {
                return enumerable;
            }
        }

        public static IEnumerable<T> EmptyList<T>(this IEnumerable<T> enumerable)
        {
            return Enumerable.Empty<T>();
        }

        public static List<string> GetAllEmailPartnerOfCampaign(LengKengDbContext _context, int campaignId)
        {
            try
            {
                int invoiceId = _context.Invoices.Where(x => x.CampaignId == campaignId).Select(x => x.Id).FirstOrDefault();
                //Bus
                var companiesBus = _context.BusBookings.Where(x => x.InvoiceId == invoiceId)
                                                                    .Include(x => x.Buses.Companies).AsEnumerable()
                                                                    .GroupBy(x => x.Buses.Companies)
                                                                    .Select(x => x.First().Buses.Companies).ToList();
                //Taxi
                var companiesTaxi = _context.TaxiBookings.Where(x => x.InvoiceId == invoiceId)
                                                                    .Include(x => x.Taxis.Companies).AsEnumerable()
                                                                    .GroupBy(x => x.Taxis.Companies)
                                                                    .Select(x => x.First().Taxis.Companies).ToList();
                //Shelter
                var companiesShelter = _context.BusShelterBookings.Where(x => x.InvoiceId == invoiceId)
                                                                    .Include(x => x.BusShelters.Companies).AsEnumerable()
                                                                    .GroupBy(x => x.BusShelters.Companies)
                                                                    .Select(x => x.First().BusShelters.Companies).ToList();
                //Billboard
                var companiesBillboard = _context.BillboardBookings.Where(x => x.InvoiceId == invoiceId)
                                                                    .Include(x => x.Billboards.Companies).AsEnumerable()
                                                                    .GroupBy(x => x.Billboards.Companies)
                                                                    .Select(x => x.First().Billboards.Companies).ToList();
                //Digital
                var companiesDigital = _context.DigitalBillboardBookings.Where(x => x.InvoiceId == invoiceId)
                                                                    .Include(x => x.DigitalBillboards.Companies).AsEnumerable()
                                                                    .GroupBy(x => x.DigitalBillboards.Companies)
                                                                    .Select(x => x.First().DigitalBillboards.Companies).ToList();

                var companies = companiesBus.Union(companiesTaxi).Union(companiesShelter).Union(companiesBillboard).Union(companiesDigital);
                var userbooking = (from com in companies
                                   join user in _context.Users.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active) on com.CompanyId equals user.CompanyId
                                   join role in _context.UserRoles.Where(x => x.Roles.RoleName == Constant.ConstantRole.BusCompany
                                                                            || x.Roles.RoleName == Constant.ConstantRole.TaxiCompany
                                                                            || x.Roles.RoleName == Constant.ConstantRole.ShelterCompany
                                                                            || x.Roles.RoleName == Constant.ConstantRole.BillboardCompany)

                                             on user.Id equals role.UserId
                                   select user.Email).ToList();
                return userbooking;
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public static void AddTracking(LengKengDbContext _context, Tracking tracking)
        {
            try
            {
                _context.Trackings.Add(tracking);
                _context.SaveChanges();
            }
            catch { }
        }
        public static string ConvertToUnsign(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
                    string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }
    }
    //sort chuoi gom ca so vaf chu vd: 19, 20
    //string[] things = new string[] { "paul", "bob", "lauren", "007", "90", "101" };
    public class StrCmpLogicalComparer : Comparer<string>
    {
        [DllImport("Shlwapi.dll", CharSet = CharSet.Unicode)]
        private static extern int StrCmpLogicalW(string x, string y);

        public override int Compare(string x, string y)
        {
            return StrCmpLogicalW(x, y);
        }
    }
    //sort chuoi gom ca so vaf chu vd: 19, 20
    public class SemiNumericComparer : IComparer<string>
    {
        /// <summary>
        /// Method to determine if a string is a number
        /// </summary>
        /// <param name="value">String to test</param>
        /// <returns>True if numeric</returns>
        public static bool IsNumeric(string value)
        {
            return int.TryParse(value, out _);
        }

        /// <inheritdoc />
        public int Compare(string s1, string s2)
        {
            const int S1GreaterThanS2 = 1;
            const int S2GreaterThanS1 = -1;

            var IsNumeric1 = IsNumeric(s1);
            var IsNumeric2 = IsNumeric(s2);

            if (IsNumeric1 && IsNumeric2)
            {
                var i1 = Convert.ToInt32(s1);
                var i2 = Convert.ToInt32(s2);

                if (i1 > i2)
                {
                    return S1GreaterThanS2;
                }

                if (i1 < i2)
                {
                    return S2GreaterThanS1;
                }

                return 0;
            }

            if (IsNumeric1)
            {
                return S2GreaterThanS1;
            }

            if (IsNumeric2)
            {
                return S1GreaterThanS2;
            }

            return string.Compare(s1, s2, true, CultureInfo.InvariantCulture);
        }
    }
}
