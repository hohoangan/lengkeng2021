﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BusRoutePrice_Unique",
                table: "BusRoutePrices");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrice_Unique",
                table: "BusRoutePrices",
                columns: new[] { "BusRouteId", "BusTypeId", "BusTypeOfArtworkId", "UnitId", "FromDate", "ToDate", "CompanyId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BusRoutePrice_Unique",
                table: "BusRoutePrices");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrice_Unique",
                table: "BusRoutePrices",
                columns: new[] { "BusRouteId", "BusTypeId", "BusTypeOfArtworkId", "UnitId", "FromDate", "ToDate" },
                unique: true);
        }
    }
}
