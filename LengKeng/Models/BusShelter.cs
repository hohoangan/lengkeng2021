﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusShelter
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        public string HighlightFeature { get; set; }
        public double GPSLocX { get; set; }
        public double GPSLocY{ get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserShelter> UserShelters { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterPicture> BusShelterPictures { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterPrice> BusShelterPrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterDownTime> BusShelterDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterDisplay> BusShelterDisplays { get; set; }
    }
}
