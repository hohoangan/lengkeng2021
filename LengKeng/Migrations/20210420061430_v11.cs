﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lighting",
                table: "Billboards");

            migrationBuilder.DropColumn(
                name: "TrafficDensity",
                table: "Billboards");

            migrationBuilder.DropColumn(
                name: "Visibility",
                table: "Billboards");

            migrationBuilder.AddColumn<DateTime>(
                name: "OTCDate",
                table: "Billboards",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "BillboardCameras",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    BillboardId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardCameras", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardCameras_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BillboardCameras_BillboardId",
                table: "BillboardCameras",
                column: "BillboardId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BillboardCameras");

            migrationBuilder.DropColumn(
                name: "OTCDate",
                table: "Billboards");

            migrationBuilder.AddColumn<string>(
                name: "Lighting",
                table: "Billboards",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrafficDensity",
                table: "Billboards",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Visibility",
                table: "Billboards",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
