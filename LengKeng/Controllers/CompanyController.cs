﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration.Conventions;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RestSharp;
using RestSharp.Serialization.Json;
namespace LengKeng.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        public CompanyController(LengKengDbContext context, IStringLocalizer<Message> localizer, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
        }

        //GET api/Company/roles
        /// <summary>
        /// Danh sách loại công ty
        /// </summary>
        /// <returns></returns>
        [HttpGet("roles")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminSupperUser)]
        public ActionResult GetCompanyRoles()
        {
            try
            {
                var comps = _context.Roles.Where(x =>
                                                 x.RoleName == Constant.ConstantRole.BusCompany
                                                 || x.RoleName == Constant.ConstantRole.TaxiCompany
                                                 || x.RoleName == Constant.ConstantRole.ShelterCompany
                                                 || x.RoleName == Constant.ConstantRole.BillboardCompany
                                                 || x.RoleName == Constant.ConstantRole.InternalCompany
                                                 || x.RoleName == Constant.ConstantRole.Customer
                ).OrderBy(x => x.RoleName).ToList();
                return Ok(comps);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        //GET api/Company
        /// <summary>
        /// Lấy danh sách công ty phân theo loại
        /// </summary>
        /// <param name="roleId">loại công ty</param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="name">empty hoặc nhập tên công ty cần tìm</param>
        /// <returns></returns>
        [HttpGet()]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminSupperUser)]
        public ActionResult GetCompany(int roleId, int PageNumber=0, int PageSize=0, string name = "")
        {
            try
            {
                var comps = _context.Companies.Where(x => x.RoleId == roleId).OrderBy(x => x.Name).ToList();
                if (!string.IsNullOrEmpty(name))
                    comps = comps.Where(x => MyUltil.ConvertToUnsign(x.Name.ToLower()).Contains(MyUltil.ConvertToUnsign(name).ToLower())).OrderBy(x => x.Name).ToList();
                if (PageNumber != 0 && PageSize != 0)
                {
                    var response = PagedList<Company>.ToPagedListIEnumerable(comps, PageNumber, PageSize);
                    Response.Headers.Add(response.HeaderInfo);
                    return Ok(response);
                }
                else
                    return Ok(comps);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Company
        /// <summary>
        /// Tạo công ty
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        [HttpPost()]
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        public ActionResult CreateCompany(Company comp)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var v = _context.Companies.Where(x => x.Name == comp.Name).FirstOrDefault();
                if (v != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, comp.Name].Value);
                }
                _context.Companies.Add(comp);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = System.Text.Json.JsonSerializer.Serialize(comp),
                    UserId = userId
                });
            }
        }

        //GET api/Company
        /// <summary>
        /// Sửa thông tin công ty
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        [HttpPut()]
        [Authorize(Roles = Constant.ConstantRole.SystemAdmin)]
        public ActionResult UpdateCompany(Company comp)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var v = _context.Companies.Where(x => x.Name == comp.Name && x.CompanyId != comp.CompanyId).FirstOrDefault();
                if (v != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, comp.Name].Value);
                }
                _context.Companies.Update(comp);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = System.Text.Json.JsonSerializer.Serialize(comp),
                    UserId = userId
                });
            }
        }

        //cho a Lai them nhanh
        /// <summary>
        /// Tạo nhanh customer
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        [HttpGet("customer")]
        [AllowAnonymous]
        public ActionResult InsertCustomerCompany(string companyName)
        {
            try
            {
                _context.Database.BeginTransaction();
                var com = _context.Companies.Where(x => x.Name == companyName).FirstOrDefault();
                if (com != null)
                {
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.DuplicateX, companyName]);
                }
                _context.Companies.Add(new Company
                {
                    CompanyId = 0,
                    Name = companyName,
                    LocationId = 1,
                    RoleId = 4
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok("Success");
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex.Message);
            }
        }
    }
}
