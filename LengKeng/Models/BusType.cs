﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusType
    {
        //60-seat, 80-seat, 30-seat
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public virtual ICollection<Bus> Buses { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusRoutePrice> BusRoutePrice { get; set; }

    }
}
