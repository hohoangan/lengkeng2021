﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration.Conventions;
using Azure.Storage.Files.Shares.Models;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CampaignController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly Data.LengKengDbContext _context;
        private readonly ICampaignRepo _reponsitory;
        private readonly IBlobService _blobService;

        public CampaignController(ICampaignRepo reponsitory, IMapper mapper, Data.LengKengDbContext context, IBlobService blobService)
        {
            _mapper = mapper;
            _context = context;
            _reponsitory = reponsitory;
            _blobService = blobService;
        }

        //POST api/Campaign
        /// <summary>
        /// Tạo chiến dịch
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult Create(Booking booking)
        {
            try
            {
                Booking bk = _reponsitory.CreateCampaign(booking);
                if (bk.Campaigns.Id == 0)
                {
                    //khi gia bi thay doi
                    return BadRequest(bk);
                }
                return Ok(bk);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        #region CHECK PRICE/AMOUNT IN CART BEFORE BOOKING

        /// <summary>
        /// CheckCartBeforeBooking
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        [HttpPost("checkcart")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult CheckCartBeforeBooking(Booking booking)
        {
            string mess = string.Empty;
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                List<CheckBookingCart> lst = _reponsitory.CheckCartBeforeBooking(booking);
                return Ok(lst);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(booking),
                    UserId = booking.Campaigns.UserId
                });
            }
        }
        #endregion  

        #region [UPDATE] : CHANGE STATUS - MATCH COMPAIGN
        //PUT api/Campaign/{id}
        /// <summary>
        /// Update Campaign
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="campaign"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult Update(int Id, CampaignUpdateDto campaign)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Campaigns.Where(b => b.Id == Id);
                if (b == null)
                    return NotFound();

                _mapper.Map(campaign, b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/Campaign/changeStatus
        /// <summary>
        /// Thay đổi trạng thái campaign
        /// </summary>
        /// <param name="Id">Campaigns Id</param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult UpdateStatus(int Id, int statusId)
        {
            try
            {
                var b = _context.Campaigns.Where(b => b.Id == Id);
                if (b == null)
                    return NotFound();
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                _reponsitory.UpdateStatus(Id, statusId, userId);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/Campaign/cancalCampaign
        /// <summary>
        /// Hủy Campaign
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("cancelCampaign")]
        [Authorize(Roles = Constant.ConstantRole.Customer)]
        public ActionResult CancelCampaign(int campaignId, int statusId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                var status = _context.Statuses.Where(x => x.Id == statusId).Select(x => x.Name).FirstOrDefault();
                if (Constant.ConstantStatus.Canceled.Equals(status))
                {
                    _reponsitory.UpdateStatus(campaignId, statusId, userId);
                    return Ok();
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/Campaign/matchTaxi
        /// <summary>
        /// Match Taxi (Mobile)
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="plateNumber"></param>
        /// <returns></returns>
        [HttpPut("matchTaxi")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult<TaxiBooking> MatchTaxi(int bookingId, string plateNumber)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                return Ok(_reponsitory.MatchTaxi(bookingId, plateNumber, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/Campaign/matchBus
        /// <summary>
        /// Match Bus (Mobile)
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="plateNumber"></param>
        /// <returns></returns>
        [HttpPut("matchBus")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult<BusBooking> MatchBus(int bookingId, string plateNumber)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                return Ok(_reponsitory.MatchBus(bookingId, plateNumber, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/GetLink
        /// <summary>
        /// Get File Report of Campaign
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="fileType">Invoice| FinalReport</param>
        /// <returns></returns>
        [HttpGet("GetLink")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult GetLink(int campaignId, string fileType)
        {
            try
            {
                var file = _context.CampaignFiles.Where(x => x.CampaignId == campaignId && x.FileType == fileType).FirstOrDefault();
                return Ok(file);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/Campaign/PostLink
        /// <summary>
        /// Save file report of campaign to DB
        /// </summary>
        /// <param name="campaignFile"></param>
        /// <returns></returns>
        [HttpPost("PostLink")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult PostLink(CampaignFile campaignFile)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.CampaignFiles.Add(campaignFile);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Xoa 1 file cua campaign
        /// </summary>
        /// <param name="campaignFileName">Truyen url file can xoa</param>
        /// <returns></returns>
        [HttpDelete("DelLink")]
        public ActionResult DelLink(string campaignFileName)
        {
            try
            {
                _context.Database.BeginTransaction();
                var campfile = _context.CampaignFiles.Where(x => x.Url == campaignFileName).FirstOrDefault();
                _context.CampaignFiles.Remove(campfile);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(campaignFileName));
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region [NOT USE] : ADD/MATCH BUS-TAXI
        /*
        //POST api/Campaign/taxibooking/
        [HttpPost("taxibooking")]
        public ActionResult AddTaxiBooking(int campaignId, string plateNumber)
        {
            try
            {
                _reponsitory.AddTaxiBooking(campaignId, plateNumber);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/Campaign/busbooking/
        [HttpPost("busbooking")]
        public ActionResult AddBusBooking(int campaignId, string plateNumber)
        {
            try
            {
                _reponsitory.AddBusBooking(campaignId, plateNumber);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        */
        #endregion

        #region [BOOKING] : SEARCH INFO TO BOOKING
        //POST api/Campaign/shelterbooking/
        //[HttpPost("shelterbooking")]
        //[Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        //public ActionResult GetShelter(BookingSearch bookingSearch, int PageNumber, int PageSize, string sort = Constant.Sort.ASC)
        //{
        //    try
        //    {
        //        var listShelter = _reponsitory.GetShelter(bookingSearch, sort);
        //        int avail = listShelter.Where(x => x.IsAvailable == true).Count();
        //        var response = PagedList<BusShelterBookingReadDto>.ToPagedListIEnumerable(listShelter, PageNumber, PageSize, avail);
        //        Response.Headers.Add(response.HeaderInfo);

        //        return Ok(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //GET api/Campaign/shelterbooking/
        /// <summary>
        /// Booking : tìm danh sách shelter trong booking - chi tiết 1 shelter
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="Amount"></param>
        /// <param name="CompanyId"></param>
        /// <param name="BusShelterId"></param>
        /// <param name="IsDetail">true: cần truyền BusShelterId | false : serach All</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("shelterbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetShelterDetail(int PageNumber, int PageSize,
                                             DateTime ToDate, DateTime FromDate, int LocationId, int Amount,
                                             int CompanyId, int BusShelterId, bool IsDetail = false, string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                CompanyId = CompanyId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId
            };
            try
            {
                var models = _reponsitory.GetShelter(bookingSearch, sort);
                if (IsDetail == true)
                {
                    models = models.Where(x => x.BusShelterId == BusShelterId);
                    PageNumber = 1;
                }
                int avail = models.Where(x => x.IsAvailable == true).Count();
                var response = PagedList<BusShelterBookingReadDto>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
                Response.Headers.Add(response.HeaderInfo);

                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }

        //POST api/Campaign/billboardbooking/
        //[HttpPost("billboardbooking")]
        //[Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        //public ActionResult GetBillboard(BookingSearch bookingSearch, int PageNumber, int PageSize, string sort = Constant.Sort.ASC)
        //{
        //    try
        //    {
        //        var models = _reponsitory.GetBillboard(bookingSearch, sort);
        //        int avail = models.Where(x => x.IsAvailable == true).Count();
        //        var response = PagedList<BillboardBookingReadDto>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
        //        Response.Headers.Add(response.HeaderInfo);
        //        return Ok(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //GET api/Campaign/billboardbooking/
        /// <summary>
        /// Booking : tìm danh sách BB trong booking - chi tiết 1 BB
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="districtId">neu quan/huyen != 0, LocationId = districtId </param>
        /// <param name="Amount">Số tháng đặt</param>
        /// <param name="CompanyId"></param>
        /// <param name="BillboardId"></param>
        /// <param name="billboardType">vd: Billboard 1 mặt</param>
        /// <param name="SearchKeywordText">vd: Trường học,Chợ (Nối liền ngăn cách bằng dấu phảy (,))</param>
        /// <param name="IsDetail">true: cần truyền BillboardId | false : search All</param>
        /// <param name="fromSize">không nhập giá trị thì để null</param>
        /// <param name="toSize">không nhập giá trị thì để null</param>
        /// <param name="fromPrice">không nhập giá trị thì để null</param>
        /// <param name="toPrice">không nhập giá trị thì để null</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("billboardbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetBillboardDetail(int PageNumber, int PageSize,
                                               DateTime ToDate, DateTime FromDate, int LocationId, int Amount, 
                                               int CompanyId, int BillboardId, string billboardType, string SearchKeywordText = "",
                                               bool IsDetail = false, int fromSize = 0, int toSize = 9999,
                                               double fromPrice = 0, double toPrice = 999999999999,
                                               int districtId = 0,
                                               string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            UserIdentity users = MyUltil.GetCurrentUserInfo(identity);
            if (districtId != 0)
                LocationId = districtId;
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                CompanyId = CompanyId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId
            };
            try
            {
                var models = _reponsitory.GetBillboard(bookingSearch, users, sort);
                if (IsDetail == true)
                {
                    models = models.Where(x => x.BillboardId == BillboardId);
                    PageNumber = 1;
                }
                else
                {
                    #region //Advanced Search
                    if (billboardType != null)
                        models = models.Where(x => x.Billboards.Type.Equals(billboardType));
                    //size
                    models = models.Where(x => fromSize <= x.Billboards.DimensionW * x.Billboards.DimensionH && x.Billboards.DimensionW * x.Billboards.DimensionH <= toSize);
                    //price
                    models = models.Where(x => fromPrice <= ((x.Price * Amount) * (1 - x.DiscountPercent / 100)) * (1 - x.PromotePercent / 100)
                                            && ((x.Price * Amount) * (1 - x.DiscountPercent / 100)) * (1 - x.PromotePercent / 100) <= toPrice);
                    //keyworks
                    if (!string.IsNullOrEmpty(SearchKeywordText))
                        models = models.Where(x => (
                                                    (x.Billboards.SearchKeywordText == null ? ""
                                                    : x.Billboards.SearchKeywordText).Split(",").Intersect(SearchKeywordText.Split(",")).ToList().Count() > 0)
                                                    || (SearchKeywordText.Contains(Constant.PromotePercent) && x.PromotePercent > 0)
                                                   );
                    #endregion
                }
                int avail = models.Where(x => x.IsAvailable == true).Count();
                models = models.OrderByDescending(x => x.PromotePercent).ThenBy(x => x.Billboards.Name);
                var response = PagedList<BillboardBookingReadDto>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = users.Id//userId
                });
            }
        }

        //POST api/Campaign/digitalbooking/
        //[HttpPost("digitalbooking")]
        //[Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        //public ActionResult GetDigital(BookingSearch bookingSearch, int PageNumber, int PageSize, string sort = Constant.Sort.ASC)
        //{
        //    try
        //    {
        //        var models = _reponsitory.GetDigital(bookingSearch, sort);
        //        int avail = models.Where(x => x.IsAvailable == true).Count();
        //        var response = PagedList<DigitalBillboardBookingReadDto>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
        //        Response.Headers.Add(response.HeaderInfo);
        //        return Ok(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //GET api/Campaign/digitalbooking/
        /// <summary>
        /// Booking : tìm danh sách Digital trong booking - chi tiết 1 Digital
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="Amount"></param>
        /// <param name="CompanyId"></param>
        /// <param name="DigitalId"></param>
        /// <param name="IsDetail">true: truyền DigitalId | false : search all</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("digitalbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetDigitalDetail(int PageNumber, int PageSize,
                                             DateTime ToDate, DateTime FromDate, int LocationId, int Amount,
                                             int CompanyId, int DigitalId, bool IsDetail = false, string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                CompanyId = CompanyId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId
            };
            try
            {
                var models = _reponsitory.GetDigital(bookingSearch, sort);
                if (IsDetail == true)
                {
                    models = models.Where(x => x.DigitalBillboardId == DigitalId);
                    PageNumber = 1;
                }
                int avail = models.Where(x => x.IsAvailable == true).Count();
                var response = PagedList<DigitalBillboardBookingReadDto>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }

        //GET api/Campaign/taxibooking/
        /// <summary>
        /// Booking : Chi tiết taxi - TaxiDetail
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="Amount"></param>
        /// <param name="CompanyId"></param>
        /// <param name="CarTypeId"></param>
        /// <param name="ArtTypeId"></param>
        /// <param name="IsDetail">true</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("taxibooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetTaxiDetail(int PageNumber, int PageSize,
                                        DateTime ToDate, DateTime FromDate, int LocationId, int Amount,
                                        int CompanyId, int CarTypeId, int ArtTypeId, bool IsDetail = false, string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                CompanyId = CompanyId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId,
                ArtTypeId = ArtTypeId,
                CarTypeId = CarTypeId
            };
            try
            {
                var models = _reponsitory.GetTaxi(bookingSearch, sort);
                if (CarTypeId > 0)
                {
                    models = models.Where(x => x.TaxiTypeId == CarTypeId);
                }
                if (IsDetail == true)
                {
                    models = models.Where(x => x.TaxiTypeId == CarTypeId && x.TaxiBookings.TaxiTypeOfArtworkId == ArtTypeId);
                    PageNumber = 1;
                }
                int avail = models.Where(x => x.IsAvailable == true).Count();
                var response = PagedList<TaxiBookingInfo>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderByDescending(x => x.TaxiBookings.PromotePercent).ThenBy(x => x.TaxiTypeName));
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }
        //GET api/Campaign/taxibooking/all
        //[HttpGet("taxibooking/all")]
        //[Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        //public ActionResult GetTaxiAvailAllArea(int PageNumber, int PageSize,
        //                                DateTime ToDate, DateTime FromDate, int LocationId, int Amount,
        //                                 string sort = Constant.Sort.ASC)
        //{
        //    string mess = string.Empty;
        //    var identity = HttpContext.User.Identity as ClaimsIdentity;
        //    int userId = MyUltil.GetCurrentUserId(identity);
        //    BookingSearch bookingSearch = new BookingSearch
        //    {
        //        Amount = Amount,
        //        FromDate = FromDate,
        //        ToDate = ToDate,
        //        LocationId = LocationId
        //    };
        //    try
        //    {
        //        var models = _reponsitory.GetTaxiAvailAllRoute(bookingSearch, sort);
        //        var response = PagedList<TaxiBookingInfo>.ToPagedListIEnumerable(models, PageNumber, PageSize, 0);
        //        Response.Headers.Add(response.HeaderInfo);
        //        return Ok(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        mess = ex?.InnerException?.Message ?? ex.Message;
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //    finally
        //    {
        //        MyUltil.AddTracking(_context, new Tracking
        //        {
        //            Id = 0,
        //            CampaignId = 0,
        //            FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
        //            ErrorName = mess,
        //            Content = JsonSerializer.Serialize(bookingSearch),
        //            UserId = userId
        //        });
        //    }
        //}
        //GET api/Campaign/taxibooking/all
        /// <summary>
        /// Booking: searchAll Tất cả taxi - kèm giá
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="Amount">SL tháng đặt</param>
        /// <param name="CompanyId">truyen companyid khi search chi tiet</param>
        /// <param name="CarTypeId">Loại taxi</param>
        /// <param name="ArtTypeId">Kiểu dán</param>
        /// <param name="CarAmountFrom">SL xe từ</param>
        /// <param name="CarAmountTo">SL xe đến</param>
        /// <param name="PriceFrom">Giá từ</param>
        /// <param name="PriceTo">Giá đến</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("taxibooking/all")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetAllTaxiAreaWithPrice(int PageNumber, int PageSize,
                                        DateTime ToDate, DateTime FromDate, int LocationId, int Amount,
                                        int CompanyId = 0,
                                        int CarTypeId = 0, int ArtTypeId = 0,
                                        int CarAmountFrom = 0, int CarAmountTo = 999,
                                        double PriceFrom = 0, double PriceTo = 999999999999,
                                        string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                CompanyId = CompanyId,
                Amount = Amount,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId,
                CarTypeId = CarTypeId,
                ArtTypeId = ArtTypeId,
                CarAmountFrom = CarAmountFrom,
                CarAmountTo = CarAmountTo,
                PriceFrom = PriceFrom,
                PriceTo = PriceTo
            };
            try
            {
                var models = _reponsitory.GetAllTaxiAreaWithPrice(bookingSearch, sort);
                models = models.OrderByDescending(x => x.PromotePercent).ThenBy(x => x.TaxiAreaName).ToList();
                var response = PagedList<TaxiBookingInfoSearchAll>.ToPagedListIEnumerable(models, PageNumber, PageSize, 0);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }

        //GET api/Campaign/busbooking
        /// <summary>
        /// Booking : Chi tiết xe - bus detail 
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="BusRouteId"></param>
        /// <param name="Amount"></param>
        /// <param name="CompanyId"></param>
        /// <param name="CarTypeId"></param>
        /// <param name="ArtTypeId"></param>
        /// <param name="IsDetail">true: cần truyền BusRouteId</param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("busbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetBusDetail(int PageNumber, int PageSize,
                                        DateTime ToDate, DateTime FromDate, int LocationId, int BusRouteId, int Amount,
                                        int CompanyId, int CarTypeId, int ArtTypeId, bool IsDetail = false, string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                BusRouteId = BusRouteId,
                CompanyId = CompanyId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId,
                ArtTypeId = ArtTypeId,
                CarTypeId = CarTypeId
            };
            try
            {
                var models = _reponsitory.GetBus(bookingSearch, sort);
                if (IsDetail == true)
                {
                    models = models.Where(x => x.CompanyId == bookingSearch.CompanyId && x.BusTypeId == CarTypeId && x.BusBookings.BusTypeOfArtworkId == ArtTypeId);
                    PageNumber = 1;
                }
                int avail = models.Where(x => x.IsAvailable == true).Count();
                var response = PagedList<BusBookingInfo>.ToPagedListIEnumerable(models, PageNumber, PageSize, avail);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderByDescending(x => x.BusBookings.PromotePercent).ThenBy(x => x.CompanyName));
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }
        /*
        /// <summary>
        /// Booking : Không còn sử dụng
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="BusRouteId"></param>
        /// <param name="Amount"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        //GET api/Campaign/busbooking/all
        [HttpGet("busbooking/all")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetBusAvailAllRoute(int PageNumber, int PageSize, DateTime ToDate, DateTime FromDate, int LocationId,
                                                int BusRouteId, int Amount, string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                BusRouteId = BusRouteId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId
            };
            try
            {
                var models = _reponsitory.GetBusAvailAllRoute(bookingSearch, sort);
                var response = PagedList<BusBookingInfo>.ToPagedListIEnumerable(models, PageNumber, PageSize, 0);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }
        */
        /// <summary>
        /// Booking: SearchAll Lấy tất cả các tuyến bus theo khu vực bao gồm giá
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <param name="LocationId"></param>
        /// <param name="BusRouteId"></param>
        /// <param name="Amount">Số tháng đặt</param>
        /// <param name="CarTypeId">Loại xe</param>
        /// <param name="ArtTypeId">Kiểu dán</param>
        /// <param name="SearchKeywordText"></param>
        /// <param name="CarAmountFrom">số lương xe từ</param>
        /// <param name="CarAmountTo">số lượng xe đến</param>
        /// <param name="PriceFrom"></param>
        /// <param name="PriceTo"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        //GET api/Campaign/busbooking/allwithprice
        [HttpGet("busbooking/allwithprice")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetAllBusRouteWithPrice(int PageNumber, int PageSize, DateTime ToDate, DateTime FromDate, int LocationId,
                                                int BusRouteId, int Amount, int CarTypeId = 0, int ArtTypeId = 0,
                                                string SearchKeywordText = "",
                                                int CarAmountFrom = 0, int CarAmountTo = 999,
                                                double PriceFrom = 0, double PriceTo = 999999999999,
                                                string sort = Constant.Sort.ASC)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            BookingSearch bookingSearch = new BookingSearch
            {
                Amount = Amount,
                BusRouteId = BusRouteId,
                FromDate = FromDate,
                ToDate = ToDate,
                LocationId = LocationId,
                CarAmountFrom = CarAmountFrom,
                CarAmountTo = CarAmountTo,
                CarTypeId = CarTypeId,
                ArtTypeId = ArtTypeId,
                PriceFrom = PriceFrom,
                PriceTo = PriceTo
            };
            try
            {
                var models = _reponsitory.GetAllBusRouteWithPrice(bookingSearch, sort);

                #region //Advanced Search
                //keyworks
                if (!string.IsNullOrEmpty(SearchKeywordText))
                    models = models.Where(x => (
                                                (x.SearchKeywordText == null ? ""
                                                : x.SearchKeywordText).Split(",").Intersect(SearchKeywordText.Split(",")).ToList().Count() > 0)
                                                || (SearchKeywordText.Contains(Constant.PromotePercent) && x.PromotePercent > 0)
                                               ).ToList();
                #endregion
                models = models.OrderByDescending(x => x.PromotePercent).ThenBy(x => x.BusRoutes.Code, new StrCmpLogicalComparer()).ToList();//.OrderBy(x => x.Code, new StrCmpLogicalComparer())
                var response = PagedList<BusBookingInfoSearchAll>.ToPagedListIEnumerable(models, PageNumber, PageSize, 0);//BusAllPrice
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bookingSearch),
                    UserId = userId
                });
            }
        }
        #endregion

        #region [BOOKING] : SEARCH TIME LINE
        //GET api/Campaign/timeline/taxi/1
        /// <summary>
        /// Thông tin booking và downtime của 1 taxi
        /// </summary>
        /// <param name="taxiId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("timeline/taxi/{taxiId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetTaxiTimeLine(int taxiId, int year)
        {
            try
            {
                return Ok(_reponsitory.GetTaxiTimeLine(taxiId, year));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/timeline/taxi/1
        /// <summary>
        /// Thông tin booking và downtime của 1 bus
        /// </summary>
        /// <param name="busId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("timeline/bus/{busId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetBusTimeLine(int busId, int year)
        {
            try
            {
                return Ok(_reponsitory.GetBusTimeLine(busId, year));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/timeline/shelter/1
        /// <summary>
        /// Thông tin booking và downtime của 1 shelter
        /// </summary>
        /// <param name="shelterId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("timeline/shelter/{shelterId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetShelterTimeLine(int shelterId, int year)
        {
            try
            {
                return Ok(_reponsitory.GetShelterTimeLine(shelterId, year));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/timeline/billboard/1
        /// <summary>
        /// Thông tin booking và downtime của 1 BB
        /// </summary>
        /// <param name="billboardId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("timeline/billboard/{billboardId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetBillboardTimeLine(int billboardId, int year)
        {
            try
            {
                return Ok(_reponsitory.GetBillboardTimeLine(billboardId, year));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/timeline/digital/1
        /// <summary>
        /// Thông tin booking và downtime của 1 digital
        /// </summary>
        /// <param name="digitalId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="hourPerDay"></param>
        /// <param name="bookFromDate"></param>
        /// <param name="bookToDate"></param>
        /// <returns></returns>
        [HttpGet("timeline/digital/{digitalId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult GetDigitalTimeLine(int digitalId, DateTime fromDate, DateTime toDate, double hourPerDay, DateTime bookFromDate, DateTime bookToDate)
        {
            try
            {
                return Ok(_reponsitory.GetDigitalTimeLine(digitalId, fromDate, toDate, hourPerDay, bookFromDate, bookToDate));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region[REPORT] : CAMPAIGN FOR CUSTOMER
        //GET api/Campaign
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Tất cả thông tin chiến dịch cho customer
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="companyId">null: all | companyId của customer</param>
        /// <returns></returns>
        [HttpGet()]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult<IEnumerable<Campaign>> GetCampaignForCustomer(int PageNumber, int PageSize, int? companyId)
        {
            try
            {
                var models = _context.Campaigns
                                    .Include(b => b.Invoices)
                                    .Include(b => b.Companies)
                                    .Include(b => b.Statuses)
                                    .OrderBy(b => b.StatusId).ThenBy(x => x.FromDate)
                                    .AsNoTracking();
                if (companyId != null)
                    models = models.Where(b => b.CompanyId == companyId);
                var response = PagedList<Campaign>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{id}
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Chi tiết campaign
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetDetailCampaign")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult<Campaign> GetDetailCampaign(int Id)
        {
            try
            {
                var model = _context.Campaigns.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(b => b.Invoices)
                    .Include(b => b.Invoices.BillboardBookings).ThenInclude(x => x.Billboards)
                    .Include(b => b.Invoices.DigitalBillboardBookings).ThenInclude(x => x.DigitalBillboards)
                    .Include(b => b.Invoices.BusShelterBookings).ThenInclude(x => x.BusShelters)
                    .Include(b => b.Invoices.BusBookings).ThenInclude(x => x.Buses)
                    .Include(b => b.Invoices.TaxiBookings).ThenInclude(x => x.Taxis)
                    .AsNoTracking()
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //GET api/Campaign/{id}/Summary
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Summary của campaign
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("{Id}/Summary")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult<Campaign> GetSummaryCampaign(int Id, int? companyId)
        {
            try
            {
                var model = _context.Campaigns.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(b => b.Invoices)
                    .Include(b => b.Invoices.BillboardBookings).ThenInclude(x => x.Billboards)
                    .Include(b => b.Invoices.DigitalBillboardBookings).ThenInclude(x => x.DigitalBillboards)
                    .Include(b => b.Invoices.BusShelterBookings).ThenInclude(x => x.BusShelters)
                    .Include(b => b.Invoices.BusBookings).ThenInclude(x => x.Buses)
                    .Include(b => b.Invoices.TaxiBookings).ThenInclude(x => x.Taxis)
                    .AsNoTracking()
                    .Single();

                // Bus
                var sumBus = model.Invoices.BusBookings.Where(x => 1 == 1);
                if (companyId != null)
                    sumBus = sumBus.Where(x => x.Buses.CompanyId == companyId);
                var bb = sumBus.GroupBy(x => x.InvoiceId)
                        .Select(x => new { Count = x.Count(), TotalFee = x.Sum(s => s.TotalFee), TotalOTC = x.Sum(s => s.OTC) }).FirstOrDefault();
                // Taxi
                var sumTaxi = model.Invoices.TaxiBookings.Where(x => 1 == 1);
                if (companyId != null)
                    sumTaxi = sumTaxi.Where(x => x.Taxis.CompanyId == companyId);
                var tb = sumTaxi.GroupBy(x => x.InvoiceId)
                        .Select(x => new { Count = x.Count(), TotalFee = x.Sum(s => s.TotalFee), TotalOTC = x.Sum(s => s.OTC) }).FirstOrDefault();
                // Shelter
                var sumShelter = model.Invoices.BusShelterBookings.Where(x => 1 == 1);
                if (companyId != null)
                    sumShelter = sumShelter.Where(x => x.BusShelters.CompanyId == companyId);
                var sb = sumShelter.GroupBy(x => x.InvoiceId)
                        .Select(x => new { Count = x.Count(), TotalFee = x.Sum(s => s.TotalFee), TotalOTC = x.Sum(s => s.OTC) }).FirstOrDefault();
                // Billboard
                var sumBillboard = model.Invoices.BillboardBookings.Where(x => 1 == 1);
                if (companyId != null)
                    sumBillboard = sumBillboard.Where(x => x.Billboards.CompanyId == companyId);
                var bbb = sumBillboard.GroupBy(x => x.InvoiceId)
                        .Select(x => new { Count = x.Count(), TotalFee = x.Sum(s => s.TotalFee), TotalOTC = x.Sum(s => s.OTC) }).FirstOrDefault();
                // Digital 
                var sumDigitalBillboard = model.Invoices.DigitalBillboardBookings.Where(x => 1 == 1);
                if (companyId != null)
                    sumDigitalBillboard = sumDigitalBillboard.Where(x => x.DigitalBillboards.CompanyId == companyId);
                var dbb = sumDigitalBillboard.GroupBy(x => x.InvoiceId)
                        .Select(x => new { Count = x.Count(), TotalFee = x.Sum(s => s.TotalFee), TotalOTC = x.Sum(s => s.OTC) }).FirstOrDefault();

                var camp = _context.Campaigns.Where(b => b.Id == Id)
                                                    .Include(b => b.Companies)
                                                    .Include(b => b.Invoices).AsNoTracking().FirstOrDefault();
                if (companyId != null)
                {
                    var invoice = _context.Invoices.Where(b => b.CampaignId == camp.Id).ToList()
                        .Select(x => { x.TotalFee = 0; x.TotalFeeAfterVAT = 0; return x; }).FirstOrDefault();
                    camp.Invoices = invoice;
                }

                var summary = new
                {
                    Campaign = camp,
                    Bus = bb,
                    Taxi = tb,
                    BusShelter = sb,
                    //Billboard = (bbb == null && dbb == null) ? null : new
                    //{
                    //    Count = bbb?.Count ?? 0 + dbb?.Count ?? 0,
                    //    TotalFee = bbb?.TotalFee ?? 0 + dbb?.TotalFee ?? 0,
                    //    TotalOTC = bbb?.TotalOTC ?? 0 + dbb?.TotalOTC ?? 0
                    //}
                    Billboard = bbb,
                    Digital = dbb
                };

                return Ok(summary);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //GET api/Campaign/{campaignId}/customer/finalReport
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Get final report
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/finalReport")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerTaxiCompany)]
        public ActionResult GetFinalReport(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                PageNumber = 1;
                PageSize = 9999;
                var campaign = _context.Campaigns.Where(x => x.Id == campaignId).FirstOrDefault();
                var taxis = GetTaxibookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId, false);
                var buses = GetBusbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId, false);
                var shelters = GetShelterbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId, false);
                var billboards = GetBillboardbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId, false);
                var digitals = GetDigitalbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(new
                {
                    company = _context.Companies.Where(x => x.CompanyId == campaign.CompanyId).FirstOrDefault(),
                    taxis = taxis,
                    buses = buses,
                    shelters = shelters,
                    billboards = billboards,
                    digitals = digitals
                });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //GET api/Campaign/{campaignId}/customer/taxibookingByLocation
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Search taxi booking info
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/taxibookingByLocation")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerTaxiCompany)]
        public ActionResult GetTaxibookingByLocation(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                var model = GetTaxibookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        object GetTaxibookingByLocationFunc(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId, bool addHearder = true)
        {
            try
            {
                var model = _context.TaxiBookings.Include(x => x.Taxis.Companies).AsNoTracking().Where(x => x.InvoiceId == invoiceId);
                if (cityLocationId != null)
                    model = model.Where(x => x.Taxis.TaxiAreas.LocationId == cityLocationId);
                if (partnerId != null)
                    model = model.Where(x => x.Taxis.CompanyId == partnerId);

                //lay ra những busId theo phan trang
                var response = PagedList<TaxiBooking>.ToPagedList(model, PageNumber, PageSize);
                if (addHearder)
                    Response.Headers.Add(response.HeaderInfo);

                //lay list pic, groupby va lay hinh moi nhat moi loai
                var pictures = _context.TaxiPictures.Include(x => x.PictureTypes)
                                                    .Where(x => x.CampaignId == campaignId)
                                                    .Where(x => model.Select(x => x.TaxiId).Contains(x.TaxiId))
                                                    .AsEnumerable()
                                                    .GroupBy(x => new { x.TaxiId, x.PictureTypeId }, y => y)
                                                    .Select(x => x.OrderByDescending(x => x.LastUpdated).FirstOrDefault());

                var lstPicture = from pic in pictures
                                 group pic by pic.TaxiId into g
                                 select new { TaxiId = g.Key, Pictures = g };

                var list = from bb in response.ToList()
                           join pic in lstPicture on bb.TaxiId equals pic.TaxiId into picT
                           from pic in picT.DefaultIfEmpty()
                           select new
                           {
                               Id = bb.TaxiId,
                               bb.InstallationTimes,
                               bb.Taxis.PlateNumber,
                               bb.Taxis.CompanyId,
                               CompanyName = bb.Taxis.Companies.Name,
                               Pictures = MyUltil.EnsureNotEmpty(pic?.Pictures)
                           };

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message);
            }
        }

        //GET api/Campaign/{campaignId}/customer/busbookingByLocation
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Search bus booking info
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/busbookingByLocation")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerBusCompany)]
        public ActionResult GetBusbookingByLocation(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                var model = GetBusbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        object GetBusbookingByLocationFunc(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId, bool addHearder = true)
        {
            try
            {
                var model = _context.BusBookings.Include(x => x.Buses.Companies).AsNoTracking().Where(x => x.InvoiceId == invoiceId);
                if (cityLocationId != null)
                    model = model.Where(x => x.Buses.BusRoutes.LocationId == cityLocationId);
                if (partnerId != null)
                    model = model.Where(x => x.Buses.CompanyId == partnerId);

                //lay ra những busId theo phan trang
                var response = PagedList<BusBooking>.ToPagedList(model, PageNumber, PageSize);
                if (addHearder)
                    Response.Headers.Add(response.HeaderInfo);

                //lay list pic, groupby va lay hinh moi nhat moi loai
                var pictures = _context.BusPictures.Include(x => x.PictureTypes)
                                                    .Where(x => x.CampaignId == campaignId)
                                                    .Where(x => model.Select(x => x.BusId).Contains(x.BusId))
                                                    .AsEnumerable()
                                                    .GroupBy(x => new { x.BusId, x.PictureTypeId }, y => y)
                                                    .Select(x => x.OrderByDescending(x => x.LastUpdated).FirstOrDefault());

                var lstPicture = from pic in pictures
                                 group pic by pic.BusId into g
                                 select new { BusId = g.Key, Pictures = g };

                var list = from bb in response.ToList()
                           join pic in lstPicture on bb.BusId equals pic.BusId into picT
                           from pic in picT.DefaultIfEmpty()
                           select new
                           {
                               Id = bb.BusId,
                               bb.InstallationTimes,
                               bb.Buses.PlateNumber,
                               bb.Buses.CompanyId,
                               CompanyName = bb.Buses.Companies.Name,
                               Pictures = MyUltil.EnsureNotEmpty(pic?.Pictures)
                           };


                return list.ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message);
            }
        }

        //GET api/Campaign/{campaignId}/customer/shelterbookingByLocation
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Search shelter booking info
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/shelterbookingByLocation")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerShelterCompany)]
        public ActionResult GetShelterbookingByLocation(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                var model = GetShelterbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        object GetShelterbookingByLocationFunc(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId, bool addHearder = true)
        {
            try
            {
                var model = _context.BusShelterBookings.Include(x => x.BusShelters.Companies).AsNoTracking().Where(x => x.InvoiceId == invoiceId);
                if (cityLocationId != null)
                    model = model.Where(x => (x.BusShelters.Locations.ParentId ?? x.BusShelters.Locations.LocationId) == cityLocationId);
                if (partnerId != null)
                    model = model.Where(x => x.BusShelters.CompanyId == partnerId);

                //lay ra những busId theo phan trang
                var response = PagedList<BusShelterBooking>.ToPagedList(model, PageNumber, PageSize);
                if (addHearder)
                    Response.Headers.Add(response.HeaderInfo);

                //lay list pic, groupby va lay hinh moi nhat moi loai
                var pictures = _context.BusShelterPictures.Include(x => x.PictureTypes)
                                                    .Where(x => x.CampaignId == campaignId)
                                                    .Where(x => model.Select(x => x.BusShelterId).Contains(x.BusShelterId))
                                                    .AsEnumerable()
                                                    .GroupBy(x => new { x.BusShelterId, x.PictureTypeId }, y => y)
                                                    .Select(x => x.OrderByDescending(x => x.LastUpdated).FirstOrDefault());

                var lstPicture = from pic in pictures
                                 group pic by pic.BusShelterId into g
                                 select new { BusShelterId = g.Key, Pictures = g };

                var list = from bb in response.ToList()
                           join pic in lstPicture on bb.BusShelterId equals pic.BusShelterId into picT
                           from pic in picT.DefaultIfEmpty()
                           select new
                           {
                               Id = bb.BusShelterId,
                               bb.InstallationTimes,
                               bb.BusShelters.Name,
                               bb.BusShelters.CompanyId,
                               CompanyName = bb.BusShelters.Companies.Name,
                               Pictures = MyUltil.EnsureNotEmpty(pic?.Pictures)
                           };

                return list.ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message);
            }
        }

        //GET api/Campaign/{campaignId}/customer/BillboardbookingByLocation
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Search billboard booking info
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/BillboardbookingByLocation")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerBillboardCompany)]
        public ActionResult GetBillboardbookingByLocation(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                var model = GetBillboardbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        object GetBillboardbookingByLocationFunc(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId, bool addHearder = true)
        {
            try
            {
                var model = _context.BillboardBookings.Include(x => x.Billboards.Companies).AsNoTracking().Where(x => x.InvoiceId == invoiceId);
                if (cityLocationId != null)
                    model = model.Where(x => (x.Billboards.Locations.ParentId ?? x.Billboards.Locations.LocationId) == cityLocationId);
                if (partnerId != null)
                    model = model.Where(x => x.Billboards.CompanyId == partnerId);

                //lay ra những busId theo phan trang
                var response = PagedList<BillboardBooking>.ToPagedList(model, PageNumber, PageSize);
                if (addHearder)
                    Response.Headers.Add(response.HeaderInfo);

                //lay list pic, groupby va lay hinh moi nhat moi loai
                var pictures = _context.BillboardPictures.Include(x => x.PictureTypes)
                                                    .Where(x => x.CampaignId == campaignId)
                                                    .Where(x => model.Select(x => x.BillboardId).Contains(x.BillboardId))
                                                    .AsEnumerable()
                                                    .GroupBy(x => new { x.BillboardId, x.PictureTypeId }, y => y)
                                                    .Select(x => x.OrderByDescending(x => x.LastUpdated).FirstOrDefault());

                var lstPicture = from pic in pictures
                                 group pic by pic.BillboardId into g
                                 select new { BillboardId = g.Key, Pictures = g };

                //CAMERA
                var cameras = from bb in response.ToList()
                              join cam in _context.BillboardCameras.AsNoTracking() on bb.BillboardId equals cam.BillboardId
                              select cam;

                var listCam = from cam in cameras
                              group cam by cam.BillboardId into g
                              select new { BillboardId = g.Key, BillboardCameras = g };

                var list = from bb in response.ToList()
                           join pic in lstPicture on bb.BillboardId equals pic.BillboardId into picT
                           from pic in picT.DefaultIfEmpty()
                           join cam in listCam on bb.BillboardId equals cam.BillboardId into camT
                           from cam in camT.DefaultIfEmpty()
                           select new
                           {
                               Id = bb.BillboardId,
                               bb.InstallationTimes,
                               bb.Billboards.Name,
                               bb.Billboards.CompanyId,
                               CompanyName = bb.Billboards.Companies.Name,
                               Pictures = MyUltil.EnsureNotEmpty(pic?.Pictures),
                               Cameras = MyUltil.EnsureNotEmpty(cam?.BillboardCameras),
                           };
                return list.ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message);
            }
        }

        //GET api/Campaign/{campaignId}/customer/DigitalbookingByLocation
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR CUSTOMER | Search digital booking info
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="campaignId"></param>
        /// <param name="invoiceId"></param>
        /// <param name="cityLocationId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/customer/DigitalbookingByLocation")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireCustomerAdminRole)]
        public ActionResult<Campaign> GetDigitalbookingByLocation(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId)
        {
            try
            {
                var model = GetDigitalbookingByLocationFunc(PageNumber, PageSize, campaignId, invoiceId, cityLocationId, partnerId);
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        object GetDigitalbookingByLocationFunc(int PageNumber, int PageSize, int campaignId, int invoiceId, int? cityLocationId, int? partnerId, bool addHearder = true)
        {
            try
            {
                var model = _context.DigitalBillboardBookings.Include(x => x.DigitalBillboards.Companies).AsNoTracking().Where(x => x.InvoiceId == invoiceId);
                if (cityLocationId != null)
                    model = model.Where(x => (x.DigitalBillboards.Locations.ParentId ?? x.DigitalBillboards.Locations.LocationId) == cityLocationId);
                if (partnerId != null)
                    model = model.Where(x => x.DigitalBillboards.CompanyId == partnerId);

                //lay ra những busId theo phan trang
                var response = PagedList<DigitalBillboardBooking>.ToPagedList(model, PageNumber, PageSize);
                if (addHearder)
                    Response.Headers.Add(response.HeaderInfo);

                //lay list pic, groupby va lay hinh moi nhat moi loai
                var pictures = _context.DigitalBillboardPictures.Include(x => x.PictureTypes)
                                                    .Where(x => x.CampaignId == campaignId)
                                                    .Where(x => model.Select(x => x.DigitalBillboardId).Contains(x.DigitalBillboardId))
                                                    .AsEnumerable()
                                                    .GroupBy(x => new { x.DigitalBillboardId, x.PictureTypeId }, y => y)
                                                    .Select(x => x.OrderByDescending(x => x.LastUpdated).FirstOrDefault());

                var lstPicture = from pic in pictures
                                 group pic by pic.DigitalBillboardId into g
                                 select new { DigitalBillboardId = g.Key, Pictures = g };

                var list = from bb in response.ToList()
                           join pic in lstPicture on bb.DigitalBillboardId equals pic.DigitalBillboardId into picT
                           from pic in picT.DefaultIfEmpty()
                           select new
                           {
                               Id = bb.DigitalBillboardId,
                               bb.DigitalBillboards.Name,
                               bb.DigitalBillboards.CompanyId,
                               CompanyName = bb.DigitalBillboards.Companies.Name,
                               Pictures = MyUltil.EnsureNotEmpty(pic?.Pictures)
                           };


                return list.ToList();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message);
            }
        }
        #endregion

        #region[REPORT] : CAMPAIGN FOR PARTNER
        //GET api/Campaign/partnerGroup/
        /// <summary>
        /// [REPORT] : CAMPAIGN FOR PARTNER | Tất cả chiến dịch của Partner(NCC)
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet("partnerGroup")]
        [Authorize(Policy = Constant.ConstantPolicy.RequirePartnerGroup)]
        public ActionResult GetCaimpaignForPartnerGroup(int PageNumber, int PageSize, string sort = Constant.Sort.ASC)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                var models = _reponsitory.GetCaimpaignForPartnerGroup(userId);
                var response = PagedList<Campaign>.ToPagedListIEnumerable(models, PageNumber, PageSize);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        /*
        //GET api/Campaign/partnerGroup/taxibooking/
        [Authorize]
        [HttpGet("partnerGroup/taxibooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult GetTaxiBookingOfCompany(int invoiceId, int companyId)
        {
            try
            {
                var list = _context.TaxiBookings.Include(x => x.Taxis).Where(x => x.InvoiceId == invoiceId && x.Taxis.CompanyId == companyId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/partnerGroup/busbooking/
        [Authorize]
        [HttpGet("partnerGroup/busbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult GetBusBookingOfCompany(int invoiceId, int companyId)
        {
            try
            {
                var list = _context.BusBookings.Include(x => x.Buses).Where(x => x.InvoiceId == invoiceId && x.Buses.CompanyId == companyId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/partnerGroup/busbooking/
        [Authorize]
        [HttpGet("partnerGroup/shelterbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult GetShelterBookingOfCompany(int invoiceId, int companyId)
        {
            try
            {
                var list = _context.BusShelterBookings.Include(x => x.BusShelters).Where(x => x.InvoiceId == invoiceId && x.BusShelters.CompanyId == companyId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/partnerGroup/billboardbooking/
        [Authorize]
        [HttpGet("partnerGroup/billboardbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetBillboardBookingOfCompany(int invoiceId, int companyId)
        {
            try
            {
                var list = _context.BillboardBookings.Include(x => x.Billboards).Where(x => x.InvoiceId == invoiceId && x.Billboards.CompanyId == companyId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/partnerGroup/billboardbooking/
        [Authorize]
        [HttpGet("partnerGroup/digitalbooking")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetDigitalBookingOfCompany(int invoiceId, int companyId)
        {
            try
            {
                var list = _context.DigitalBillboardBookings.Include(x => x.DigitalBillboards)
                                                            .Where(x => x.InvoiceId == invoiceId && x.DigitalBillboards.CompanyId == companyId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        */

        #endregion

        #region [MOBILE - WEB] : GET LOCATION/COMPANY/BUSROUTE IN BOOKING OF CAMPAIGN
        //GET api/Campaign/{Id}/taxibooking/location
        /// <summary>
        /// Lấy location chạy campaign có taxi
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/taxibooking/location")]
        public ActionResult GetLocationOfTaxiBooking(int campaignId)
        {
            try
            {
                var list = _context.TaxiBookings.Include(x => x.Taxis.TaxiAreas.Locations)
                    .Where(x => x.Invoices.CampaignId == campaignId).Select(x => x.Taxis.TaxiAreas.Locations).Distinct();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/busbooking/location
        /// <summary>
        /// Lấy location chạy campaign có bus
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/busbooking/location")]
        public ActionResult GetLocationOfBusBooking(int campaignId)
        {
            try
            {
                var list = _context.BusBookings.Include(x => x.Buses.BusRoutes.Locations)
                    .Where(x => x.Invoices.CampaignId == campaignId).Select(x => x.Buses.BusRoutes.Locations).Distinct();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/busbooking/routes
        /// <summary>
        /// Lấy danh sách tuyến bus trong campaign theo location
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/busbooking/routes")]
        public ActionResult GetRouteOfBusBookingByLocation(int campaignId, int locationId)
        {
            try
            {
                var list = _context.BusBookings.Include(x => x.Buses.BusRoutes)
                    .Where(x => x.Buses.BusRoutes.LocationId == locationId)
                    .Where(x => x.Invoices.CampaignId == campaignId)
                    .Select(x => x.Buses.BusRoutes).Distinct();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/taxibooking/company
        /// <summary>
        /// Danh sách NCC taxi của campaign
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/taxibooking/company")]
        public ActionResult GetCompanyOfTaxiBookingByLocation(int campaignId, int locationId)
        {
            try
            {
                var list = _context.TaxiBookings.Include(x => x.Taxis.Companies)
                    .Where(x => x.Taxis.TaxiAreas.LocationId == locationId)
                    .Where(x => x.Invoices.CampaignId == campaignId)
                    .Select(x => x.Taxis.Companies).Distinct();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/shelterbooking/location
        /// <summary>
        /// Lấy location chạy campaign có shelter
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/shelterbooking/location")]
        public ActionResult GetLocationOfShelterBooking(int campaignId)
        {
            try
            {
                var list = _context.BusShelterBookings.Include(x => x.BusShelters.Locations)
                    .Where(x => x.Invoices.CampaignId == campaignId).Select(x => x.BusShelters.Locations).Distinct();

                var listLoc = from parent in list
                              join loc in _context.Locations
                              on parent.ParentId ?? parent.LocationId equals loc.LocationId
                              select loc;
                return Ok(listLoc);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/billboardbooking/location
        /// <summary>
        /// Lấy location chạy campaign có BB
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/billboardbooking/location")]
        public ActionResult GetLocationOfBBBooking(int campaignId)
        {
            try
            {
                var billboard = _context.BillboardBookings.Include(x => x.Billboards.Locations)
                                                                .Where(x => x.Invoices.CampaignId == campaignId)
                                                                .Select(x => x.Billboards.Locations).Distinct();
                var listLoc = from parent in billboard
                              join loc in _context.Locations
                              on parent.ParentId ?? parent.LocationId equals loc.LocationId
                              select loc;
                return Ok(listLoc);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/digitalbooking/location
        /// <summary>
        /// Lấy location chạy campaign có digital
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/digitalbooking/location")]
        public ActionResult GetLocationOfDBBooking(int campaignId)
        {
            try
            {
                var digital = _context.DigitalBillboardBookings.Include(x => x.DigitalBillboards.Locations)
                                                                .Where(x => x.Invoices.CampaignId == campaignId)
                                                                .Select(x => x.DigitalBillboards.Locations).Distinct();
                var listLoc = from parent in digital
                              join loc in _context.Locations
                              on parent.ParentId ?? parent.LocationId equals loc.LocationId
                              select loc;
                return Ok(listLoc);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/billboardanddigitalbooking/location
        /// <summary>
        /// Lấy location chạy campaign có billboard và digital BB
        /// </summary>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{campaignId}/billboardanddigitalbooking/location")]
        public ActionResult GetLocationOfBBDBBooking(int campaignId)
        {
            try
            {
                var billboard = _context.BillboardBookings.Include(x => x.Billboards.Locations)
                                                                .Where(x => x.Invoices.CampaignId == campaignId)
                                                                .Select(x => x.Billboards.Locations).Distinct();
                var digital = _context.DigitalBillboardBookings.Include(x => x.DigitalBillboards.Locations)
                                                                .Where(x => x.Invoices.CampaignId == campaignId)
                                                                .Select(x => x.DigitalBillboards.Locations).Distinct();
                var list = digital.Union(billboard);
                var listLoc = from parent in list
                              join loc in _context.Locations
                              on parent.ParentId ?? parent.LocationId equals loc.LocationId
                              select loc;
                return Ok(listLoc);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        #endregion

        #region [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO
        //GET api/campaign/
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | lấy danh sách campaign của KH
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("owner")]
        public ActionResult<IEnumerable<Campaign>> GetCampaignOwner()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);
                return Ok(_reponsitory.GetCaimpaignOwner(userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/busbooking
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | thông tin busbooking theo từng users
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <param name="busRouteId"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/busbooking")]
        public ActionResult<MobileBusBooking> GetBusBookingByUser(int campaignId, int locationId, int? busRouteId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetBusBookingByUser(campaignId, locationId, userId, busRouteId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/taxibooking
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | thông tin taxibooking theo từng users
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <param name="taxiCompanyId"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/taxibooking")]
        public ActionResult<MobileTaxiBooking> GetTaxiBookingByUser(int campaignId, int locationId, int? taxiCompanyId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetTaxiBookingByUser(campaignId, locationId, userId, taxiCompanyId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/shelterbooking
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | thông tin shelterbooking theo từng users
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/shelterbooking")]
        public ActionResult<MobileShelterBooking> GetShelterBookingByUser(int campaignId, int locationId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetShelterBookingByUser(campaignId, locationId, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/billboardbooking
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | thông tin billboardbooking theo từng users
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/billboardbooking")]
        public ActionResult<MobileBillboardDigitalBooking> Billboardbooking(int campaignId, int locationId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetBillboardBookingPictureByUser(campaignId, locationId, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Campaign/{Id}/digitalbooking
        /// <summary>
        /// [MOBILE] : GET CAMPAIGN INFO TO TAKE PHOTO | thông tin digitalbooking theo từng users
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/digitalbooking")]
        public ActionResult<MobileBillboardDigitalBooking> Digitalbooking(int campaignId, int locationId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetDigitalBookingPictureByUser(campaignId, locationId, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// NOT USE
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        //GET api/Campaign/{Id}/billboardanddigitalbooking
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        [HttpGet("{campaignId}/billboardanddigitalbooking")]
        public ActionResult<MobileBillboardDigitalBooking> GetBillboardBookingByUser(int campaignId, int locationId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                return Ok(_reponsitory.GetBillboardAndDigitalBookingByUser(campaignId, locationId, userId));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region [DASHBOARD] 
        //GET api/Campaign
        /// <summary>
        /// Dashboard
        /// </summary>
        /// <returns></returns>
        [HttpGet("dashboard")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireAdminCustomerAllPartnerCompany)]
        public ActionResult DashboardForCompany()
        {
            try
            {
                /*-------------------------- GET role -----------------------------*/
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                var userRole = _context.UserRoles.Where(r => r.UserId == userId).Select(s => new
                {
                    RoleId = s.Roles.Id,
                    RoleName = s.Roles.RoleName,
                    CompanyId = s.Users.CompanyId
                }).FirstOrDefault();
                var roleName = userRole?.RoleName ?? string.Empty;

                if (roleName.Equals(Constant.ConstantRole.SuperUser) || roleName.Equals(Constant.ConstantRole.Customer))
                {
                    var camppaign = from camp in _context.Campaigns.Where(x => x.Statuses.Name != Constant.ConstantStatus.Canceled)
                                    select camp;
                    if (roleName.Equals(Constant.ConstantRole.Customer))
                        camppaign = camppaign.Where(x => x.CompanyId == userRole.CompanyId);

                    var models = (from camp in camppaign.ToList()
                                  join inv in _context.Invoices on camp.Id equals inv.CampaignId
                                  join stt in _context.Statuses on camp.StatusId equals stt.Id
                                  group new { camp, inv } by stt into g
                                  select new
                                  {
                                      Status = g.Key.Name,
                                      Total = g.Count(),
                                      Fee = g.Sum(x => x.inv.TotalFee),
                                      FeeVAT = g.Sum(x => x.inv.TotalFeeAfterVAT),
                                  }).ToList();

                    var status = _context.Statuses.Where(x => x.Type == Constant.ConstantStatus.TypeCampaign
                                                           && x.Name != Constant.ConstantStatus.Canceled).AsEnumerable();
                    var result = from stt in status
                                 join mo in models on stt.Name equals mo.Status into moG
                                 from mo in moG.DefaultIfEmpty()
                                 select new
                                 {
                                     Status = stt.Name,
                                     Total = mo?.Total ?? 0,
                                     Fee = mo?.Fee ?? 0,
                                     FeeVAT = mo?.FeeVAT ?? 0
                                 };

                    return Ok(result);
                }
                if (roleName.Equals(Constant.ConstantRole.TaxiCompany))
                {
                    return Ok(_reponsitory.DashboardForTaxi(userRole.CompanyId));
                }
                else if (roleName.Equals(Constant.ConstantRole.BusCompany))
                {
                    return Ok(_reponsitory.DashboardForBus(userRole.CompanyId));
                }
                else if (roleName.Equals(Constant.ConstantRole.BillboardCompany))
                {
                    return Ok(_reponsitory.DashboardForBillboard(userRole.CompanyId));
                }
                else if (roleName.Equals(Constant.ConstantRole.ShelterCompany))
                {
                    return Ok(_reponsitory.DashboardForShelter(userRole.CompanyId));
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}
