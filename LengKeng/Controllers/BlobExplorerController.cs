﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LengKeng.Data;
using LengKeng.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LengKeng.Controllers
{
    [Route("api/blobs")]
    [ApiController]
    public class BlobExplorerController : ControllerBase
    {
        private readonly IBlobService _blobService;

        public BlobExplorerController(IBlobService blobService)
        {
            _blobService = blobService;
        }
        /// <summary>
        /// Lấy file trên store
        /// </summary>
        /// <param name="blobName"></param>
        /// <returns></returns>
        [HttpGet("{blobName}")]
        public async Task<ActionResult> GetBlob(string blobName)
        {
            try
            {
                var data = await _blobService.GetBlobAsync(blobName);
                return File(data.Content, data.ContentType);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [HttpGet("list")]
        public async Task<ActionResult> ListBlobs()
        {
            try
            {
                return Ok(await _blobService.ListBlobAsync());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [HttpPost("uploadfile")]
        public async Task<ActionResult> UploadFile(UploadFileRequest request)
        {
            try
            {
                //if (!System.IO.File.Exists(request.FilePath))
                //{
                //    Response.Headers.Add("Message","File not exits.");
                //    return NotFound("File not exist.");
                //}
                await _blobService.UploadFileBlobAsync(request.FilePath, request.FileName);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
            
        }

        [HttpPost("uploadcontent")]
        public async Task<ActionResult> UploadContent(UploadContentRequest request)
        {
            try
            {
                await _blobService.UploadContentBlobAsync(request.Content, request.FileName);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
            
        }
        /// <summary>
        /// Delete file
        /// </summary>
        /// <param name="blobName"></param>
        /// <returns></returns>
        [HttpDelete("{blobName}")]
        public async Task<ActionResult> DeleteFile(string blobName)
        {
            try
            {
                await _blobService.DeleteBlobAsync(blobName);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Upload file lên store với định dạng bất kỳ chứa trong HttpContext.Request.ReadFormAsync()
        /// </summary>
        /// <returns></returns>
        [HttpPost("uploadimg")]
        //[RequestFormLimits(MultipartBodyLengthLimit = 1073741824)]
        //[RequestFormLimits(ValueLengthLimit = int.MaxValue, 
        //    MemoryBufferThreshold = int.MaxValue,
        //    MultipartBodyLengthLimit = long.MaxValue, ValueCountLimit = 10)]
        public async Task<ActionResult> UploadIMG()
        {
            try
            {
                var request = await HttpContext.Request.ReadFormAsync();
                var files = request.Files;
                List<KeyValuePair<string,string>> listName = new List<KeyValuePair<string, string>>();
                for (int i = 0; i < files.Count; i++)
                {
                    string extension = Path.GetExtension(files[i].FileName);
                    string name = string.Format(@"{0}{1}", Guid.NewGuid(), extension);
                    using (var stream = files[i].OpenReadStream())
                    {
                        var url = await _blobService.UploadImageBlobAsync(stream, name);
                        listName.Add(new KeyValuePair<string, string>(name,url));
                    }
                }
                return Ok(listName);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
