﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int CampaignId { get; set; }
        [JsonIgnore]
        public Campaign Campaigns { get; set; }
        /// <summary>
        /// Tổng số bus đặt
        /// </summary>
        public int BusAmount { get; set; }
        public int BusShelterAmount { get; set; }
        /// <summary>
        /// Tổng số taxi đặt
        /// </summary>
        public int TaxiAmount { get; set; }
        /// <summary>
        /// Tổng số billboard đặt
        /// </summary>
        public int BillboardAmount { get; set; }
        public int DigitalBillboardAmount { get; set; }
        public double VAT { get; set; }
        public double TotalFee { get; set; }
        public double TotalFeeAfterVAT { get; set; }
        public string Remark { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdate { get; set; } = DateTime.Now;
        [JsonIgnore]
        public virtual ICollection<TaxiBooking> TaxiBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusBooking> BusBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelterBooking> BusShelterBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardBooking> BillboardBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboardBooking> DigitalBillboardBookings { get; set; }

    }
}
