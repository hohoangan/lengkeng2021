﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BillboardTimeLine
    {
        public IEnumerable<BillboardBooking> bookings { get; set; }
        public IEnumerable<BillboardDownTime> downTimes { get; set; }
    }
}
