﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiTypeOfArtwork
    {
        // 2-door, 4-door
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiAreaPrice> TaxiAreaPrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiBooking> TaxiBookings { get; set; }
    }
}
