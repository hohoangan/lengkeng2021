﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class UserBus
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int UserId { get; set; }
        [JsonIgnore]
        public User Users { get; set; }
        [Required, Id]
        public int BusId { get; set; }
        [JsonIgnore]
        public Bus Buses { get; set; }
    }
}
