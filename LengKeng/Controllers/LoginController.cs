﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LengKeng.Data;
using LengKeng.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using LengKeng.Ultils;
using System.Reflection.Metadata;
using LengKeng.Secure;

namespace LengKeng.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly Data.LengKengDbContext _context;

        public LoginController(IConfiguration configuration, Data.LengKengDbContext context)
        {
            _config = configuration;
            _context = context;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ismobile"></param>
        /// <returns></returns>
        [HttpGet]
        //[ValidateReferrer]//chan login tu postman,...
        //[RequestLimit("Limit-Login", NoOfRequest = 5, Seconds = 10)]//gioi han truy cap lien tuc
        public ActionResult Login(string username, string password, bool ismobile = false)
        {
            try
            {
                ActionResult response = Unauthorized();
                var user = _context.Users.Where(u => u.Username == username || u.Email == username)
                                         .Where(u => u.Password == MyUltil.encodeM5(password))
                                         .Where(x=>x.Statuses.Name == Ultils.Constant.ConstantStatus.Active)
                           .Include(u=>u.UserRoles.Roles).Include(s => s.Companies).FirstOrDefault();

                if (user != null)
                {
                    if (ismobile == true)
                    {
                        if (user.UserRoles.Roles.RoleName != Ultils.Constant.ConstantRole.TaxiDriver
                            && user.UserRoles.Roles.RoleName != Ultils.Constant.ConstantRole.BusDriver
                            && user.UserRoles.Roles.RoleName != Ultils.Constant.ConstantRole.BillboardOperator
                            && user.UserRoles.Roles.RoleName != Ultils.Constant.ConstantRole.Coordinator
                            )
                        {
                            return response;
                        }
                    }
                    else
                    {
                        if (user.UserRoles.Roles.RoleName == Ultils.Constant.ConstantRole.TaxiDriver
                            || user.UserRoles.Roles.RoleName == Ultils.Constant.ConstantRole.BusDriver
                            || user.UserRoles.Roles.RoleName == Ultils.Constant.ConstantRole.BillboardOperator
                            || user.UserRoles.Roles.RoleName == Ultils.Constant.ConstantRole.Coordinator
                            )
                        {
                            return response;
                        }
                    }

                    user.Password = string.Empty;
                    var tokenStr = GenerateJSONWebToken(user);
                    response = Ok(new
                    {
                        token = tokenStr,
                        userInfo = user
                    });
                }

                return response;
            }
            catch (Exception ex)
            {
                return Unauthorized(ex?.InnerException?.Message ?? ex.Message);
            }
            
        }

        private object GenerateJSONWebToken(User userinfo)
        {
            try
            {
                var secureKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(secureKey, SecurityAlgorithms.HmacSha256);
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.FamilyName, userinfo.Username),
                    new Claim(JwtRegisteredClaimNames.NameId, userinfo.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, userinfo.CompanyId.ToString()),
                    new Claim(ClaimTypes.Role, userinfo?.UserRoles?.Roles?.RoleName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var token = new JwtSecurityToken(
                    issuer: _config["Jwt:Issuer"],
                    audience: _config["Jwt:Issuer"],
                    claims,
                    expires: DateTime.Now.AddMinutes(120),
                    signingCredentials: credentials
                    );
                var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
                return encodetoken;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Thông tin users
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("UserLogin")]
        public ActionResult GetUserLogin()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                List<Claim> claims = identity.Claims.ToList();
                var userName = claims[0].Value;
                var id = claims[1].Value;
                var companyId = claims[2].Value;
                var roleName = claims[3].Value;
                return Ok(new
                {
                    userName = userName,
                    id = id,
                    companyId = companyId,
                    roleName = roleName
                });
            }
            catch (Exception)
            {
                return Unauthorized();
            }
            
        }
    }
}
