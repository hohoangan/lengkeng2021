﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Bus
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PlateNumber { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        [JsonIgnore]
        public BusType BusTypes { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        //[Required, Id]
        //public int OTC { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [Required, Id]
        public int BusRouteId { get; set; }
        [JsonIgnore]
        public BusRoute BusRoutes { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserBus> UserBuses { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusPicture> BusPictures { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusDownTime> BusDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusBooking> BusBookings { get; set; }
    }
}
