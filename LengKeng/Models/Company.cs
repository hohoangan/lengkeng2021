﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Company
    {
        [Key]
        public int CompanyId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [Required]
        public int RoleId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        [JsonIgnore]
        public Role Roles { get; set; }
        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }
        [JsonIgnore]
        public virtual ICollection<Bus> Buses { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusShelter> BusShelters { get; set; }
        [JsonIgnore]
        public virtual ICollection<Taxi> Taxis { get; set; }
        [JsonIgnore]
        public virtual ICollection<Billboard> Billboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<DigitalBillboard> DigitalBillboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<Campaign> Campaigns { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiArea> TaxiAreas { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusRouteCompany> BusRouteCompanies { get; set; }
    }
}
