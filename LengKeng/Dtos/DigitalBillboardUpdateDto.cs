﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LengKeng.Ultils;

namespace LengKeng.Dtos
{
    public class DigitalBillboardUpdateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Orientation { get; set; }
        [Required]
        public string TVC { get; set; }//Type of TVC (sec)	30s, 15s
        [Required]
        public string Spot { get; set; }
        [Required]
        public string DurationCircle { get; set; }
        [Required]
        public string HighlightFeature { get; set; }
        [Required]
        public string Visibility { get; set; }
        [Required]
        public string TrafficDensity { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        [Required, Id]
        public double OTC { get; set; }
        public double GPSLocX { get; set; }
        public double GPSLocY { get; set; }
    }
}
