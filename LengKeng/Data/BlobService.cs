﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using LengKeng.Extensions;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LengKeng.Data
{
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly BlobContainerClient containerClient;

        public BlobService(BlobServiceClient blobServiceClient)
        {
            _blobServiceClient = blobServiceClient;
            if(Constant.Environment.Permission.Equals(Constant.Environment.Develop))
                containerClient = _blobServiceClient.GetBlobContainerClient("lkmediadata-dev");
            else if (Constant.Environment.Permission.Equals(Constant.Environment.Production))
                containerClient = _blobServiceClient.GetBlobContainerClient("lkmediadata-prod");
        }

        public async Task<BlobInfox> GetBlobAsync(string fileName)
        {
            //var containerClient = _blobServiceClient.GetBlobContainerClient("picads");
            var blobClient = containerClient.GetBlobClient(fileName);
            var blodInfo = await blobClient.DownloadAsync();
            return new BlobInfox { Content= blodInfo.Value.Content, ContentType=blodInfo.Value.ContentType };
        }

        public async Task<IEnumerable<string>> ListBlobAsync()
        {
            //var containerClient = _blobServiceClient.GetBlobContainerClient("picads");
            var items = new List<string>();
            await foreach (var item in containerClient.GetBlobsAsync())
            {
                items.Add(item.Name);
            }
            return items;
        }

        public async Task UploadFileBlobAsync(string filePath, string fileName)
        {
            var blobClient = containerClient.GetBlobClient(fileName);
            await blobClient.UploadAsync(filePath, new BlobHttpHeaders { ContentType = filePath.GetContentType() });
        }
        public async Task UploadContentBlobAsync(string content, string fileName)
        {
            var blobClient = containerClient.GetBlobClient(fileName);
            var bytes = Encoding.UTF8.GetBytes(content);
            using var memoryStream = new MemoryStream(bytes);
            await blobClient.UploadAsync(memoryStream, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
        }

        public async Task<string> UploadImageBlobAsync(Stream stream, string fileName)
        {
            var blobClient = containerClient.GetBlobClient(fileName);
            //var bytes = Encoding.UTF8.GetBytes(content);
            //using var memoryStream = new MemoryStream(bytes);
            await blobClient.UploadAsync(stream, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
            return blobClient.Uri.AbsoluteUri;
        }

        public async Task DeleteBlobAsync(string blobName)
        {
            var blobClient = containerClient.GetBlobClient(blobName);
            await blobClient.DeleteIfExistsAsync();
        }
    }
}
