﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiCopyPriceDto
    {
        public int? LocationId { get; set; }
        public int CompanyId { get; set; }
        public int FromYear { get; set; }
        public int ToYear { get; set; }
    }
}
