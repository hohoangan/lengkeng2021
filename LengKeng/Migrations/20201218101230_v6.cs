﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TaxiDiscounts_Month_LocationId_CompanyId",
                table: "TaxiDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusDiscounts_Month_LocationId_CompanyId",
                table: "BusDiscounts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "TaxiDiscounts");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "TaxiDiscounts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "BusDiscounts");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "BusDiscounts");

            migrationBuilder.AddColumn<int>(
                name: "TaxiAreaPriceId",
                table: "TaxiDiscounts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "TaxiBookings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "TotalFeeAfterVAT",
                table: "Invoices",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VAT",
                table: "Invoices",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "DigitalBillboardBookings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "BusShelterBookings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BusRoutePriceId",
                table: "BusDiscounts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "BusBookings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "BillboardBookings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDiscounts_TaxiAreaPriceId",
                table: "TaxiDiscounts",
                column: "TaxiAreaPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDiscounts_Month_TaxiAreaPriceId",
                table: "TaxiDiscounts",
                columns: new[] { "Month", "TaxiAreaPriceId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusDiscounts_BusRoutePriceId",
                table: "BusDiscounts",
                column: "BusRoutePriceId");

            migrationBuilder.CreateIndex(
                name: "IX_BusDiscounts_Month_BusRoutePriceId",
                table: "BusDiscounts",
                columns: new[] { "Month", "BusRoutePriceId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BusDiscounts_BusRoutePrices_BusRoutePriceId",
                table: "BusDiscounts",
                column: "BusRoutePriceId",
                principalTable: "BusRoutePrices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaxiDiscounts_TaxiAreaPrices_TaxiAreaPriceId",
                table: "TaxiDiscounts",
                column: "TaxiAreaPriceId",
                principalTable: "TaxiAreaPrices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusDiscounts_BusRoutePrices_BusRoutePriceId",
                table: "BusDiscounts");

            migrationBuilder.DropForeignKey(
                name: "FK_TaxiDiscounts_TaxiAreaPrices_TaxiAreaPriceId",
                table: "TaxiDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_TaxiDiscounts_TaxiAreaPriceId",
                table: "TaxiDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_TaxiDiscounts_Month_TaxiAreaPriceId",
                table: "TaxiDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusDiscounts_BusRoutePriceId",
                table: "BusDiscounts");

            migrationBuilder.DropIndex(
                name: "IX_BusDiscounts_Month_BusRoutePriceId",
                table: "BusDiscounts");

            migrationBuilder.DropColumn(
                name: "TaxiAreaPriceId",
                table: "TaxiDiscounts");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "TaxiBookings");

            migrationBuilder.DropColumn(
                name: "TotalFeeAfterVAT",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "VAT",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "DigitalBillboardBookings");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "BusShelterBookings");

            migrationBuilder.DropColumn(
                name: "BusRoutePriceId",
                table: "BusDiscounts");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "BusBookings");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "BillboardBookings");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "TaxiDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "TaxiDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "BusDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "BusDiscounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDiscounts_Month_LocationId_CompanyId",
                table: "TaxiDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusDiscounts_Month_LocationId_CompanyId",
                table: "BusDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);
        }
    }
}
