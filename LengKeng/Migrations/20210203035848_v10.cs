﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ErrorName",
                table: "Trackings",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdated",
                table: "Trackings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ErrorName",
                table: "Trackings");

            migrationBuilder.DropColumn(
                name: "LastUpdated",
                table: "Trackings");
        }
    }
}
