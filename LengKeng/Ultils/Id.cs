﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Ultils
{
    public class Id : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            return Convert.ToInt32(value) > 0 ?
                ValidationResult.Success :
                new ValidationResult($"The {validationContext.DisplayName} field is required.");
        }
    }

    public class price : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            return Convert.ToDouble(value) > 0 ?
                ValidationResult.Success :
                new ValidationResult($"The {validationContext.DisplayName} field is required.");
        }
    }
}
