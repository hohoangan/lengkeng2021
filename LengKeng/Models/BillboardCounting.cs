﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BillboardCounting
    {
        [Key]
        public int Id { get; set; }
        public int BillboardId { get; set; }
        public int BillboardCameraId { get; set; }
        public double PersonAmount { get; set; }
        public double MotoAmount { get; set; }
        public double CarAmount { get; set; }
        public double BusAmount { get; set; }
        public double Other { get; set; }
        public string Type { get; set; } //Day | Night
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime LastUpdated { get; set; } = DateTime.Now;
        public string TeamViewId { get; set; } 
        public string TeamViewPasswords { get; set; } 
        public string Desciption { get; set; } 

    }
}
