﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Booking
    {
        public Campaign Campaigns { get; set; }
        public Invoice Invoices { get; set; }
        public CampaignFile CampaignFile { get; set; }
        public List<TaxiBookingInfo> TaxiBookingInfoList { get; set; }
        public List<BusBookingInfo> BusBookingInfoList { get; set; }
        public List<BillboardBooking> BillboardBookings { get; set; }
        public List<BusShelterBooking> BusShelterBookings { get; set; }
        public List<DigitalBillboardBooking> DigitalBillboardBookings { get; set; }
    }

    public class TaxiBookingInfo
    {
        public double OTC { get; set; }
        public DateTime OTCDate { get; set; } = DateTime.Now;
        public int MinCarBooking { get; set; }
        public bool IsAvailable { get; set; }
        [Required, Id]
        public int LocationId { get; set; } // thuoc tinh/tp
        public int TaxiAreaId { get; set; }
        public string TaxiAreaName { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int CarAmount { get; set; }
        public int NumberOfVehicles { get; set; }
        [Required, Id]
        public int TaxiTypeId { get; set; }
        public string TaxiTypeName { get; set; }
        public string TaxiTypeOfArtworkName { get; set; }
        [Required]
        public TaxiBooking TaxiBookings { get; set; }
        public IEnumerable<TaxiDisplay> TaxiDisplays { get; set; }
        public string Type { get; set; }
        public string SearchText { get; set; }
    }

    public class TaxiBookingInfoSearchAll
    {
        public double OTC { get; set; }
        public int MinCarBooking { get; set; }
        public bool IsAvailable { get; set; }
        [Required, Id]
        public int LocationId { get; set; } // thuoc tinh/tp
        public int TaxiAreaId { get; set; }
        public string TaxiAreaName { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int CarAmount { get; set; }
        public IEnumerable<TaxiDisplay> TaxiDisplays { get; set; }
        [Required, Id]
        public int TaxiTypeId { get; set; }
        public string TaxiTypeName { get; set; }
        [Required]
        //public List<TaxiBooking> TaxiBookings { get; set; }
        public TaxiBooking TaxiBookings { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public double PromotePercent { get; set; }
        public string Type { get; set; }//la bus hay taxi
        public string SearchText { get; set; }
    }

    public class BusBookingInfo
    {
        public double OTC { get; set; }
        public DateTime OTCDate { get; set; } = DateTime.Now;
        public int MinCarBooking { get; set; }
        public bool IsAvailable { get; set; }
        [Required, Id]
        public int LocationId { get; set; } // thuoc tinh/tp
        [Required, Id]
        public int BusRouteId { get; set; }
        public BusRoute BusRoutes { get; set; }
        public string BusRouteName { get; set; }
        public string Description { get; set; }
        public string SearchKeywordText { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        [Required, Id]
        public int CarAmount { get; set; }
        public int NumberOfVehicles { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        public string BusTypeName { get; set; }
        public string BusTypeOfArtworkName { get; set; }

        [Required]
        public BusBooking BusBookings { get; set; }
        public IEnumerable<BusDisplay> BusDisplays { get; set; }
        public string Type { get; set; }
        public string SearchText { get; set; }
    }
    public class BusBookingInfoSearchAll
    {
        public double OTC { get; set; }
        public int MinCarBooking { get; set; }
        public bool IsAvailable { get; set; }
        [Required, Id]
        public int LocationId { get; set; } // thuoc tinh/tp
        [Required, Id]
        public int BusRouteId { get; set; }
        public BusRoute BusRoutes { get; set; }
        public string BusRouteName { get; set; }
        public string Description { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        [Required, Id]
        public int CarAmount { get; set; }
        public IEnumerable<BusDisplay> BusDisplays { get; set; }
        [Required, Id]
        public int BusTypeId { get; set; }
        public string BusTypeName { get; set; }
        [Required]
        //public List<BusBooking> BusBookings { get; set; }
        public BusBooking BusBookings { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public double PromotePercent { get; set; }
        public string SearchKeywordText { get; set; }
        public string Type { get; set; }
        public string SearchText { get; set; }
    }
    public class BusAllPrice
    {
        [Required, Id]
        public int LocationId { get; set; } // thuoc tinh/tp
        [Required, Id]
        public int BusRouteId { get; set; }
        public string BusRouteCode { get; set; }
        public string BusRouteName { get; set; }
        public string Description { get; set; }
        public string SearchKeywordText { get; set; }
        public int TotalCar { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public double PromotePercent { get; set; }
        public List<BusBookingInfoSearchAll> BusBookingInfo { get; set; }
    }
}
