﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Tracking
    {
        [Key]
        public int Id { get; set; }
        public string FunctionName { get; set; }
        public string ErrorName { get; set; }
        public int CampaignId { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
    }
}
