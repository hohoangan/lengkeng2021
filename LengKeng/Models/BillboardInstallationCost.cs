﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BillboardInstallationCost
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int UnitId { get; set; }
        public Unit Units { get; set; }
        [Required, price]
        public double Price { get; set; }
    }
}
