﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BillboardCountings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BillboardId = table.Column<int>(nullable: false),
                    PersonAmount = table.Column<double>(nullable: false),
                    MotoAmount = table.Column<double>(nullable: false),
                    CarAmount = table.Column<double>(nullable: false),
                    BusAmount = table.Column<double>(nullable: false),
                    Other = table.Column<double>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    TeamViewId = table.Column<string>(nullable: true),
                    TeamViewPasswords = table.Column<string>(nullable: true),
                    Desciption = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardCountings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BillboardCountings");
        }
    }
}
