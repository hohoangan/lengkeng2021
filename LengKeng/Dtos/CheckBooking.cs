﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class CheckBooking
    {
        /// <summary>
        /// StatusCode: 200/Status400BadRequest/Status404NotFound
        /// Item: file json convert tu loai => taxibookinginfo, busbookinginfo, billboardbooking....
        /// KindOf400: la 1 trong 2 loai =>  { PriceChanged, NotEnoughCar}
        /// Message404: la thong báo lỗi từ hệ thống khi statuscode 404 
        /// -------------------------------------
        /// code: 200 => khong lam gi ca
        /// code: 400 => check lai giá hoặc số lương xe và refresh đơn hàng
        /// code: 404 => show lỗi, đề nghị xóa đơn hàng
        /// </summary>
        public int StatusCode { get; set; }
        public string KindOf400 { get; set; }//Bad request
        public string Message404 { get; set; }//not found -> loi he thong
        public object Item { get; set; }
    }

    /// <summary>
    /// Type:  loai channel (bus/taxi/billboard...)
    /// </summary>
    public class CheckBookingCart
    {
        public string Type { get; set; }
        public List<CheckBooking> CheckBookings { get; set; }
    }
}
