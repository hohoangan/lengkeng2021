﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiInstallationCost
    {
        [Required]
        public int Id { get; set; }
        [Required, price]
        public double Price { get; set; }
        [Required, Id]
        public int TaxiTypeId { get; set; }
        [JsonIgnore]
        public TaxiType TaxiTypes { get; set; }
        [Required, Id]
        public int TaxiTypeOfArtworkId { get; set; }
        [JsonIgnore]
        public TaxiTypeOfArtwork TaxiTypeOfArtworks { get; set; }
    }
}
