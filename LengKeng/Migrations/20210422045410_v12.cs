﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OTC",
                table: "Taxis");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "Buses");

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "TaxiBookings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<double>(
                name: "OTC",
                table: "TaxiAreas",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "OTCDate",
                table: "TaxiAreas",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "DigitalBillboards",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<DateTime>(
                name: "OTCDate",
                table: "DigitalBillboards",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "DigitalBillboardBookings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "BusShelters",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<DateTime>(
                name: "OTCDate",
                table: "BusShelters",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "BusShelterBookings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<double>(
                name: "OTC",
                table: "BusRoutes",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "OTCDate",
                table: "BusRoutes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "BusBookings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "Billboards",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<double>(
                name: "OTC",
                table: "BillboardBookings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OTC",
                table: "TaxiAreas");

            migrationBuilder.DropColumn(
                name: "OTCDate",
                table: "TaxiAreas");

            migrationBuilder.DropColumn(
                name: "OTCDate",
                table: "DigitalBillboards");

            migrationBuilder.DropColumn(
                name: "OTCDate",
                table: "BusShelters");

            migrationBuilder.DropColumn(
                name: "OTC",
                table: "BusRoutes");

            migrationBuilder.DropColumn(
                name: "OTCDate",
                table: "BusRoutes");

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "Taxis",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "TaxiBookings",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "DigitalBillboards",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "DigitalBillboardBookings",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "BusShelters",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "BusShelterBookings",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<int>(
                name: "OTC",
                table: "Buses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "BusBookings",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "Billboards",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "OTC",
                table: "BillboardBookings",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
