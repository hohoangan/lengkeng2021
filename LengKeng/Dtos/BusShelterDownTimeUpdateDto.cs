﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusShelterDownTimeUpdateDto
    {
        public string Remark { get; set; }
        [Required, Id]
        public int BusShelterId { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
    }
}
