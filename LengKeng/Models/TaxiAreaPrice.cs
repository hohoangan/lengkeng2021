﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiAreaPrice
    {
        [Key]
        public int Id { get; set; }
        [Required, price]
        public double Price { get; set; }
        public double PromotePercent { get; set; } = 0;
        [Required]
        public int MinCarBooking { get; set; }//10 taxi/month
        [Required, Id]
        public int TaxiTypeOfArtworkId { get; set; }
        [JsonIgnore]
        public TaxiTypeOfArtwork TaxiTypeOfArtworks { get; set; }
        [Required, Id]
        public int TaxiTypeId { get; set; }
        [JsonIgnore]
        public TaxiType TaxiTypes { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
        [Required, Id]
        public int UnitId { get; set; }
        [JsonIgnore]
        public Unit Units { get; set; }
        [Required, Id]
        public int TaxiAreaId { get; set; }
        [JsonIgnore]
        public TaxiArea TaxiAreas { get; set; }

        [Required, Id]
        public int CreatedUserId { get; set; }
        [Required, Id]
        public int UserId { get; set; }
        [JsonIgnore]
        public User Users { get; set; }
        [JsonIgnore]
        public virtual ICollection<TaxiDiscount> TaxiDiscounts { get; set; }

    }
}
