﻿using LengKeng.Dtos;
using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Data
{
    public interface ICampaignRepo
    {
        bool SaveChanges();
        Booking CreateCampaign(Booking booking);
        List<CheckBookingCart> CheckCartBeforeBooking(Booking booking);

        #region CHANGE CAMPAIGN
        //void AddTaxiBooking(int campaignId, string plateNumber);
        //void AddBusBooking(int campaignId, string plateNumber);
        void UpdateStatus(int campaignId, int statusId, int userId);
        TaxiBooking MatchTaxi(int bookingId, string plateNumber, int userId);
        BusBooking MatchBus(int bookingId, string plateNumber, int userId);
        #endregion

        #region SEARCH INFO TO BOOKING
        IEnumerable<BusShelterBookingReadDto> GetShelter(BookingSearch bookingSearch, string sort);
        IEnumerable<BillboardBookingReadDto> GetBillboard(BookingSearch bookingSearch, UserIdentity user, string sort);
        IEnumerable<DigitalBillboardBookingReadDto> GetDigital(BookingSearch bookingSearch, string sort);
        IEnumerable<TaxiBookingInfo> GetTaxi(BookingSearch bookingSearch, string sort);
        IEnumerable<TaxiBookingInfo> GetTaxiAvailAllRoute(BookingSearch bookingSearch, string sort);
        List<TaxiBookingInfoSearchAll> GetAllTaxiAreaWithPrice(BookingSearch bookingSearch, string sort);
        IEnumerable<BusBookingInfo> GetBus(BookingSearch bookingSearch, string sort);
        IEnumerable<BusBookingInfo> GetBusAvailAllRoute(BookingSearch bookingSearch, string sort);
        List<BusBookingInfoSearchAll> GetAllBusRouteWithPrice(BookingSearch bookingSearch, string sort);//List<BusAllPrice>
        #endregion

        #region TIME LINE
        TaxiTimeLine GetTaxiTimeLine(int id, int year);
        BusTimeLine GetBusTimeLine(int id, int year);
        BusShelterTimeLine GetShelterTimeLine(int id, int year);
        BillboardTimeLine GetBillboardTimeLine(int id, int year);
        DigitalTimeLine GetDigitalTimeLine(int id, DateTime fromDate, DateTime toDate, double hourPerDay, DateTime bookFromDate, DateTime bookToDate);
        #endregion

        IEnumerable<Campaign> GetCaimpaignForPartnerGroup(int userId);

        #region MOBILE: GET CAMPAIGN INFO TO TAKE PHOTO
        IEnumerable<Campaign> GetCaimpaignOwner(int userId);
        MobileBusBooking GetBusBookingByUser(int campaignId, int locationId, int userId, int? busRouteId);
        MobileTaxiBooking GetTaxiBookingByUser(int campaignId, int locationId, int userId, int? taxiCompanyId);
        MobileShelterBooking GetShelterBookingByUser(int campaignId, int locationId, int userId);
        MobileBillboardDigitalBooking GetBillboardBookingPictureByUser(int campaignId, int locationId, int userId);
        MobileBillboardDigitalBooking GetDigitalBookingPictureByUser(int campaignId, int locationId, int userId);
        /// <summary>
        /// NOT USE
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="locationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        MobileBillboardDigitalBooking GetBillboardAndDigitalBookingByUser(int campaignId, int locationId, int userId);
        #endregion

        #region [DASHBOARD]
        object DashboardForBus(int companyId);
        object DashboardForTaxi(int companyId);
        object DashboardForShelter(int companyId);
        object DashboardForBillboard(int companyId);
        #endregion
    }
}
