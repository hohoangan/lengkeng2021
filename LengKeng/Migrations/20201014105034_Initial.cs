﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BillboardDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Percent = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Percent = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusDisplays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(nullable: true),
                    CarTypeId = table.Column<int>(nullable: false),
                    ArtTypeId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    TypeName = table.Column<string>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusDisplays", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Percent = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusTypeOfArtworks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusTypeOfArtworks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Commands",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HowTo = table.Column<string>(maxLength: 250, nullable: false),
                    Line = table.Column<string>(nullable: false),
                    Platform = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    OrderBy = table.Column<int>(nullable: false, defaultValue: 1)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.LocationId);
                });

            migrationBuilder.CreateTable(
                name: "MediaChannels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MediaChannels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PictureTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaxiDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Percent = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaxiDisplays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(nullable: true),
                    CarTypeId = table.Column<int>(nullable: false),
                    ArtTypeId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    TypeName = table.Column<string>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiDisplays", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaxiTypeOfArtworks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiTypeOfArtworks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaxiTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BusInstallationCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    BusTypeId = table.Column<int>(nullable: false),
                    BusTypeOfArtworkId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusInstallationCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusInstallationCosts_BusTypes_BusTypeId",
                        column: x => x.BusTypeId,
                        principalTable: "BusTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusInstallationCosts_BusTypeOfArtworks_BusTypeOfArtworkId",
                        column: x => x.BusTypeOfArtworkId,
                        principalTable: "BusTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusRoutes_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MediaPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MediaChannelId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MediaPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MediaPictures_MediaChannels_MediaChannelId",
                        column: x => x.MediaChannelId,
                        principalTable: "MediaChannels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MediaPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    CompanyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.CompanyId);
                    table.ForeignKey(
                        name: "FK_Companies_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Companies_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SinhViens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 250, nullable: false),
                    StudentID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SinhViens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SinhViens_Students_StudentID",
                        column: x => x.StudentID,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiInstallationCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    TaxiTypeId = table.Column<int>(nullable: false),
                    TaxiTypeOfArtworkId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiInstallationCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiInstallationCosts_TaxiTypes_TaxiTypeId",
                        column: x => x.TaxiTypeId,
                        principalTable: "TaxiTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiInstallationCosts_TaxiTypeOfArtworks_TaxiTypeOfArtworkId",
                        column: x => x.TaxiTypeOfArtworkId,
                        principalTable: "TaxiTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillboardInstallationCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnitId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardInstallationCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardInstallationCosts_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterInstallationCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnitId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterInstallationCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterInstallationCosts_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Billboards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Orientation = table.Column<string>(nullable: false),
                    Lighting = table.Column<string>(nullable: true),
                    HighlightFeature = table.Column<string>(nullable: true),
                    Visibility = table.Column<string>(nullable: true),
                    TrafficDensity = table.Column<string>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    DimensionH = table.Column<double>(nullable: false),
                    DimensionW = table.Column<double>(nullable: false),
                    OTC = table.Column<int>(nullable: false),
                    GPSLocX = table.Column<double>(nullable: false),
                    GPSLocY = table.Column<double>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Billboards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Billboards_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Billboards_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Billboards_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Buses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PlateNumber = table.Column<string>(nullable: false),
                    BusTypeId = table.Column<int>(nullable: false),
                    DimensionH = table.Column<double>(nullable: false),
                    DimensionW = table.Column<double>(nullable: false),
                    OTC = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    BusRouteId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buses_BusRoutes_BusRouteId",
                        column: x => x.BusRouteId,
                        principalTable: "BusRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Buses_BusTypes_BusTypeId",
                        column: x => x.BusTypeId,
                        principalTable: "BusTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Buses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Buses_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    OTC = table.Column<int>(nullable: false),
                    HighlightFeature = table.Column<string>(nullable: true),
                    GPSLocX = table.Column<double>(nullable: false),
                    GPSLocY = table.Column<double>(nullable: false),
                    DimensionH = table.Column<double>(nullable: false),
                    DimensionW = table.Column<double>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelters_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusShelters_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelters_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Orientation = table.Column<string>(nullable: false),
                    TVC = table.Column<string>(nullable: true),
                    Spot = table.Column<string>(nullable: true),
                    BroadcastingTime = table.Column<int>(nullable: false),
                    DurationCircle = table.Column<string>(nullable: true),
                    HighlightFeature = table.Column<string>(nullable: true),
                    Visibility = table.Column<string>(nullable: true),
                    TrafficDensity = table.Column<string>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    DimensionH = table.Column<double>(nullable: false),
                    DimensionW = table.Column<double>(nullable: false),
                    OTC = table.Column<int>(nullable: false),
                    GPSLocX = table.Column<double>(nullable: false),
                    GPSLocY = table.Column<double>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboards_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalBillboards_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboards_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiAreas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiAreas_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxiAreas_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillboardDisplays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BillboardId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    TypeName = table.Column<string>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardDisplays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardDisplays_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillboardDownTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remark = table.Column<string>(nullable: true),
                    BillboardId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardDownTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardDownTimes_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillboardDownTimes_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillboardPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    BillboardId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardPictures_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillboardPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusDownTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remark = table.Column<string>(nullable: true),
                    BusId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusDownTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusDownTimes_Buses_BusId",
                        column: x => x.BusId,
                        principalTable: "Buses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusDownTimes_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BusPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    BusId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusPictures_Buses_BusId",
                        column: x => x.BusId,
                        principalTable: "Buses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterDisplays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BusShelterId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    TypeName = table.Column<string>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterDisplays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterDisplays_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterDownTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remark = table.Column<string>(nullable: true),
                    BusShelterId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterDownTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterDownTimes_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelterDownTimes_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    BusShelterId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterPictures_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelterPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboardDisplays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DigitalBillboardId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    TypeName = table.Column<string>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboardDisplays", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardDisplays_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboardDownTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remark = table.Column<string>(nullable: true),
                    DigitalBillboardId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboardDownTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardDownTimes_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardDownTimes_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboardPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    DigitalBillboardId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboardPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardPictures_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Taxis",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PlateNumber = table.Column<string>(nullable: false),
                    TaxiTypeId = table.Column<int>(nullable: false),
                    DimensionH = table.Column<double>(nullable: false),
                    DimensionW = table.Column<double>(nullable: false),
                    OTC = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    TaxiAreaId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxis", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Taxis_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Taxis_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Taxis_TaxiAreas_TaxiAreaId",
                        column: x => x.TaxiAreaId,
                        principalTable: "TaxiAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Taxis_TaxiTypes_TaxiTypeId",
                        column: x => x.TaxiTypeId,
                        principalTable: "TaxiTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillboardPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    MinMonthBooking = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    BillboardId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardPrices_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillboardPrices_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillboardPrices_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusRoutePrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    MinCarBooking = table.Column<int>(nullable: false),
                    BusTypeOfArtworkId = table.Column<int>(nullable: false),
                    BusTypeId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    BusRouteId = table.Column<int>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusRoutePrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusRoutePrices_BusRoutes_BusRouteId",
                        column: x => x.BusRouteId,
                        principalTable: "BusRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusRoutePrices_BusTypes_BusTypeId",
                        column: x => x.BusTypeId,
                        principalTable: "BusTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusRoutePrices_BusTypeOfArtworks_BusTypeOfArtworkId",
                        column: x => x.BusTypeOfArtworkId,
                        principalTable: "BusTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusRoutePrices_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusRoutePrices_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    MinMonthBooking = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    BusShelterId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterPrices_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelterPrices_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelterPrices_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Campaigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Descriptione = table.Column<string>(nullable: false),
                    UrlPDFReport = table.Column<string>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Campaigns_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Campaigns_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Campaigns_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboardPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    DigitalBillboardId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboardPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardPrices_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardPrices_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardPrices_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reminders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    campaignId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reminders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reminders_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reminders_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiAreaPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    MinCarBooking = table.Column<int>(nullable: false),
                    TaxiTypeOfArtworkId = table.Column<int>(nullable: false),
                    TaxiTypeId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    TaxiAreaId = table.Column<int>(nullable: false),
                    CreatedUserId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiAreaPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiAreaPrices_TaxiAreas_TaxiAreaId",
                        column: x => x.TaxiAreaId,
                        principalTable: "TaxiAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiAreaPrices_TaxiTypes_TaxiTypeId",
                        column: x => x.TaxiTypeId,
                        principalTable: "TaxiTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiAreaPrices_TaxiTypeOfArtworks_TaxiTypeOfArtworkId",
                        column: x => x.TaxiTypeOfArtworkId,
                        principalTable: "TaxiTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiAreaPrices_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiAreaPrices_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserBillboards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    BillboardId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBillboards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBillboards_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBillboards_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserBuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    BusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBuses_Buses_BusId",
                        column: x => x.BusId,
                        principalTable: "Buses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBuses_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserDigitalBillboards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    DigitalBillboardId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDigitalBillboards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDigitalBillboards_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserDigitalBillboards_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserLocations_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserLocations_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserShelters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    BusShelterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserShelters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserShelters_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserShelters_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiDownTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remark = table.Column<string>(nullable: true),
                    TaxiId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiDownTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiDownTimes_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxiDownTimes_Taxis_TaxiId",
                        column: x => x.TaxiId,
                        principalTable: "Taxis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiPictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: true),
                    TaxiId = table.Column<int>(nullable: false),
                    PictureTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiPictures_PictureTypes_PictureTypeId",
                        column: x => x.PictureTypeId,
                        principalTable: "PictureTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiPictures_Taxis_TaxiId",
                        column: x => x.TaxiId,
                        principalTable: "Taxis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTaxis",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    TaxiId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTaxis", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTaxis_Taxis_TaxiId",
                        column: x => x.TaxiId,
                        principalTable: "Taxis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTaxis_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CampaignId = table.Column<int>(nullable: false),
                    BusAmount = table.Column<int>(nullable: false),
                    BusShelterAmount = table.Column<int>(nullable: false),
                    TaxiAmount = table.Column<int>(nullable: false),
                    BillboardAmount = table.Column<int>(nullable: false),
                    DigitalBillboardAmount = table.Column<int>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserCampaigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCampaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserCampaigns_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserCampaigns_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillboardBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    InstallationCost = table.Column<double>(nullable: false),
                    InstallationTimes = table.Column<int>(nullable: false),
                    DiscountPercent = table.Column<double>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    BillboardId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillboardBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillboardBookings_Billboards_BillboardId",
                        column: x => x.BillboardId,
                        principalTable: "Billboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillboardBookings_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillboardBookings_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillboardBookings_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    InstallationCost = table.Column<double>(nullable: false),
                    InstallationTimes = table.Column<int>(nullable: false),
                    DiscountPercent = table.Column<double>(nullable: false),
                    BusTypeOfArtworkId = table.Column<int>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    BusId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusBookings_Buses_BusId",
                        column: x => x.BusId,
                        principalTable: "Buses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusBookings_BusTypeOfArtworks_BusTypeOfArtworkId",
                        column: x => x.BusTypeOfArtworkId,
                        principalTable: "BusTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusBookings_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusBookings_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusBookings_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusShelterBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    InstallationCost = table.Column<double>(nullable: false),
                    InstallationTimes = table.Column<int>(nullable: false),
                    DiscountPercent = table.Column<double>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    BusShelterId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusShelterBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusShelterBookings_BusShelters_BusShelterId",
                        column: x => x.BusShelterId,
                        principalTable: "BusShelters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusShelterBookings_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusShelterBookings_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusShelterBookings_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DigitalBillboardBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    HourPerDay = table.Column<double>(nullable: false),
                    DiscountPercent = table.Column<double>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    DigitalBillboardId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DigitalBillboardBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardBookings_DigitalBillboards_DigitalBillboardId",
                        column: x => x.DigitalBillboardId,
                        principalTable: "DigitalBillboards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardBookings_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardBookings_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DigitalBillboardBookings_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxiBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceId = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    InstallationCost = table.Column<double>(nullable: false),
                    InstallationTimes = table.Column<int>(nullable: false),
                    DiscountPercent = table.Column<double>(nullable: false),
                    TaxiTypeOfArtworkId = table.Column<int>(nullable: false),
                    TotalFee = table.Column<double>(nullable: false),
                    TaxiId = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxiBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxiBookings_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxiBookings_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxiBookings_Taxis_TaxiId",
                        column: x => x.TaxiId,
                        principalTable: "Taxis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiBookings_TaxiTypeOfArtworks_TaxiTypeOfArtworkId",
                        column: x => x.TaxiTypeOfArtworkId,
                        principalTable: "TaxiTypeOfArtworks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaxiBookings_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BillboardBookings_BillboardId",
                table: "BillboardBookings",
                column: "BillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardBookings_InvoiceId",
                table: "BillboardBookings",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardBookings_StatusId",
                table: "BillboardBookings",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardBookings_UnitId",
                table: "BillboardBookings",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDiscounts_Month_LocationId_CompanyId",
                table: "BillboardDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDisplays_BillboardId",
                table: "BillboardDisplays",
                column: "BillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDownTimes_BillboardId",
                table: "BillboardDownTimes",
                column: "BillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardDownTimes_StatusId",
                table: "BillboardDownTimes",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardInstallationCosts_UnitId",
                table: "BillboardInstallationCosts",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardPictures_BillboardId",
                table: "BillboardPictures",
                column: "BillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardPictures_PictureTypeId",
                table: "BillboardPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardPrices_UnitId",
                table: "BillboardPrices",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardPrices_UserId",
                table: "BillboardPrices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BillboardPrice_Unique",
                table: "BillboardPrices",
                columns: new[] { "BillboardId", "UnitId", "FromDate", "ToDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Billboards_CompanyId",
                table: "Billboards",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Billboards_LocationId",
                table: "Billboards",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Billboard_Name",
                table: "Billboards",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Billboards_StatusId",
                table: "Billboards",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusBookings_BusId",
                table: "BusBookings",
                column: "BusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusBookings_BusTypeOfArtworkId",
                table: "BusBookings",
                column: "BusTypeOfArtworkId");

            migrationBuilder.CreateIndex(
                name: "IX_BusBookings_InvoiceId",
                table: "BusBookings",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_BusBookings_StatusId",
                table: "BusBookings",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusBookings_UnitId",
                table: "BusBookings",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BusDiscounts_Month_LocationId_CompanyId",
                table: "BusDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusDownTimes_BusId",
                table: "BusDownTimes",
                column: "BusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusDownTimes_StatusId",
                table: "BusDownTimes",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Buses_BusRouteId",
                table: "Buses",
                column: "BusRouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Buses_BusTypeId",
                table: "Buses",
                column: "BusTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Buses_CompanyId",
                table: "Buses",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Bus_PlateNumber",
                table: "Buses",
                column: "PlateNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Buses_StatusId",
                table: "Buses",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusInstallationCosts_BusTypeId",
                table: "BusInstallationCosts",
                column: "BusTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusInstallationCosts_BusTypeOfArtworkId_BusTypeId",
                table: "BusInstallationCosts",
                columns: new[] { "BusTypeOfArtworkId", "BusTypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusPictures_BusId",
                table: "BusPictures",
                column: "BusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusPictures_PictureTypeId",
                table: "BusPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrices_BusTypeId",
                table: "BusRoutePrices",
                column: "BusTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrices_BusTypeOfArtworkId",
                table: "BusRoutePrices",
                column: "BusTypeOfArtworkId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrices_UnitId",
                table: "BusRoutePrices",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrices_UserId",
                table: "BusRoutePrices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutePrice_Unique",
                table: "BusRoutePrices",
                columns: new[] { "BusRouteId", "BusTypeId", "BusTypeOfArtworkId", "UnitId", "FromDate", "ToDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusRoutes_LocationId",
                table: "BusRoutes",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BusRoute_LocationName",
                table: "BusRoutes",
                columns: new[] { "Name", "LocationId", "Code" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterBookings_BusShelterId",
                table: "BusShelterBookings",
                column: "BusShelterId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterBookings_InvoiceId",
                table: "BusShelterBookings",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterBookings_StatusId",
                table: "BusShelterBookings",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterBookings_UnitId",
                table: "BusShelterBookings",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDiscounts_Month_LocationId_CompanyId",
                table: "BusShelterDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDisplays_BusShelterId",
                table: "BusShelterDisplays",
                column: "BusShelterId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDownTimes_BusShelterId",
                table: "BusShelterDownTimes",
                column: "BusShelterId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterDownTimes_StatusId",
                table: "BusShelterDownTimes",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterInstallationCosts_UnitId",
                table: "BusShelterInstallationCosts",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterPictures_BusShelterId",
                table: "BusShelterPictures",
                column: "BusShelterId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterPictures_PictureTypeId",
                table: "BusShelterPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterPrices_UnitId",
                table: "BusShelterPrices",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterPrices_UserId",
                table: "BusShelterPrices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelterPrice_Unique",
                table: "BusShelterPrices",
                columns: new[] { "BusShelterId", "UnitId", "FromDate", "ToDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusShelters_CompanyId",
                table: "BusShelters",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelters_LocationId",
                table: "BusShelters",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BusShelters_StatusId",
                table: "BusShelters",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusTypeOfArtwork_Name",
                table: "BusTypeOfArtworks",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusType_Name",
                table: "BusTypes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_CompanyId",
                table: "Campaigns",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_StatusId",
                table: "Campaigns",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_UserId",
                table: "Campaigns",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_LocationId",
                table: "Companies",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_RoleId",
                table: "Companies",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardBookings_DigitalBillboardId",
                table: "DigitalBillboardBookings",
                column: "DigitalBillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardBookings_InvoiceId",
                table: "DigitalBillboardBookings",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardBookings_StatusId",
                table: "DigitalBillboardBookings",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardBookings_UnitId",
                table: "DigitalBillboardBookings",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardDisplays_DigitalBillboardId",
                table: "DigitalBillboardDisplays",
                column: "DigitalBillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardDownTimes_DigitalBillboardId",
                table: "DigitalBillboardDownTimes",
                column: "DigitalBillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardDownTimes_StatusId",
                table: "DigitalBillboardDownTimes",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardPictures_DigitalBillboardId",
                table: "DigitalBillboardPictures",
                column: "DigitalBillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardPictures_PictureTypeId",
                table: "DigitalBillboardPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardPrices_UnitId",
                table: "DigitalBillboardPrices",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardPrices_UserId",
                table: "DigitalBillboardPrices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboardPrice_Unique",
                table: "DigitalBillboardPrices",
                columns: new[] { "DigitalBillboardId", "UnitId", "FromDate", "ToDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboards_CompanyId",
                table: "DigitalBillboards",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboards_LocationId",
                table: "DigitalBillboards",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboard_Name",
                table: "DigitalBillboards",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DigitalBillboards_StatusId",
                table: "DigitalBillboards",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_CampaignId",
                table: "Invoices",
                column: "CampaignId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MediaPictures_PictureTypeId",
                table: "MediaPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MediaPicture_MediaPictypeId",
                table: "MediaPictures",
                columns: new[] { "MediaChannelId", "PictureTypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_StatusId",
                table: "Reminders",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_UserId",
                table: "Reminders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SinhViens_StudentID",
                table: "SinhViens",
                column: "StudentID");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreaPrices_TaxiTypeId",
                table: "TaxiAreaPrices",
                column: "TaxiTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreaPrices_TaxiTypeOfArtworkId",
                table: "TaxiAreaPrices",
                column: "TaxiTypeOfArtworkId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreaPrices_UnitId",
                table: "TaxiAreaPrices",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreaPrices_UserId",
                table: "TaxiAreaPrices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreaPrice_Unique",
                table: "TaxiAreaPrices",
                columns: new[] { "TaxiAreaId", "TaxiTypeId", "TaxiTypeOfArtworkId", "UnitId", "FromDate", "ToDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreas_CompanyId",
                table: "TaxiAreas",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiAreas_LocationId_CompanyId",
                table: "TaxiAreas",
                columns: new[] { "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiArea_LocationName",
                table: "TaxiAreas",
                columns: new[] { "Name", "LocationId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiBookings_InvoiceId",
                table: "TaxiBookings",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiBookings_StatusId",
                table: "TaxiBookings",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiBookings_TaxiId",
                table: "TaxiBookings",
                column: "TaxiId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiBookings_TaxiTypeOfArtworkId",
                table: "TaxiBookings",
                column: "TaxiTypeOfArtworkId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiBookings_UnitId",
                table: "TaxiBookings",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDiscounts_Month_LocationId_CompanyId",
                table: "TaxiDiscounts",
                columns: new[] { "Month", "LocationId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDownTimes_StatusId",
                table: "TaxiDownTimes",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiDownTimes_TaxiId",
                table: "TaxiDownTimes",
                column: "TaxiId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiInstallationCosts_TaxiTypeId",
                table: "TaxiInstallationCosts",
                column: "TaxiTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiInstallationCosts_TaxiTypeOfArtworkId_TaxiTypeId",
                table: "TaxiInstallationCosts",
                columns: new[] { "TaxiTypeOfArtworkId", "TaxiTypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiPictures_PictureTypeId",
                table: "TaxiPictures",
                column: "PictureTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiPictures_TaxiId",
                table: "TaxiPictures",
                column: "TaxiId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxis_CompanyId",
                table: "Taxis",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxi_PlateNumber",
                table: "Taxis",
                column: "PlateNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Taxis_StatusId",
                table: "Taxis",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxis_TaxiAreaId",
                table: "Taxis",
                column: "TaxiAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxis_TaxiTypeId",
                table: "Taxis",
                column: "TaxiTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxiTypeOfArtwork_Name",
                table: "TaxiTypeOfArtworks",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaxiType_Name",
                table: "TaxiTypes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Unit_Name",
                table: "Units",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserBillboards_BillboardId",
                table: "UserBillboards",
                column: "BillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBillboards_UserId_BillboardId",
                table: "UserBillboards",
                columns: new[] { "UserId", "BillboardId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserBuses_BusId",
                table: "UserBuses",
                column: "BusId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBuses_UserId",
                table: "UserBuses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCampaigns_CampaignId",
                table: "UserCampaigns",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCampaigns_UserId_CampaignId",
                table: "UserCampaigns",
                columns: new[] { "UserId", "CampaignId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserDigitalBillboards_DigitalBillboardId",
                table: "UserDigitalBillboards",
                column: "DigitalBillboardId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDigitalBillboards_UserId_DigitalBillboardId",
                table: "UserDigitalBillboards",
                columns: new[] { "UserId", "DigitalBillboardId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserLocations_LocationId",
                table: "UserLocations",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLocations_UserId_LocationId",
                table: "UserLocations",
                columns: new[] { "UserId", "LocationId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_UserId",
                table: "UserRoles",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_Unique",
                table: "UserRoles",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompanyId",
                table: "Users",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UsernamePhone",
                table: "Users",
                columns: new[] { "Username", "Phone" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserShelters_BusShelterId",
                table: "UserShelters",
                column: "BusShelterId");

            migrationBuilder.CreateIndex(
                name: "IX_UserShelter_Unique",
                table: "UserShelters",
                columns: new[] { "UserId", "BusShelterId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserTaxis_TaxiId",
                table: "UserTaxis",
                column: "TaxiId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTaxi_Unique",
                table: "UserTaxis",
                columns: new[] { "UserId", "TaxiId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BillboardBookings");

            migrationBuilder.DropTable(
                name: "BillboardDiscounts");

            migrationBuilder.DropTable(
                name: "BillboardDisplays");

            migrationBuilder.DropTable(
                name: "BillboardDownTimes");

            migrationBuilder.DropTable(
                name: "BillboardInstallationCosts");

            migrationBuilder.DropTable(
                name: "BillboardPictures");

            migrationBuilder.DropTable(
                name: "BillboardPrices");

            migrationBuilder.DropTable(
                name: "BusBookings");

            migrationBuilder.DropTable(
                name: "BusDiscounts");

            migrationBuilder.DropTable(
                name: "BusDisplays");

            migrationBuilder.DropTable(
                name: "BusDownTimes");

            migrationBuilder.DropTable(
                name: "BusInstallationCosts");

            migrationBuilder.DropTable(
                name: "BusPictures");

            migrationBuilder.DropTable(
                name: "BusRoutePrices");

            migrationBuilder.DropTable(
                name: "BusShelterBookings");

            migrationBuilder.DropTable(
                name: "BusShelterDiscounts");

            migrationBuilder.DropTable(
                name: "BusShelterDisplays");

            migrationBuilder.DropTable(
                name: "BusShelterDownTimes");

            migrationBuilder.DropTable(
                name: "BusShelterInstallationCosts");

            migrationBuilder.DropTable(
                name: "BusShelterPictures");

            migrationBuilder.DropTable(
                name: "BusShelterPrices");

            migrationBuilder.DropTable(
                name: "Commands");

            migrationBuilder.DropTable(
                name: "DigitalBillboardBookings");

            migrationBuilder.DropTable(
                name: "DigitalBillboardDisplays");

            migrationBuilder.DropTable(
                name: "DigitalBillboardDownTimes");

            migrationBuilder.DropTable(
                name: "DigitalBillboardPictures");

            migrationBuilder.DropTable(
                name: "DigitalBillboardPrices");

            migrationBuilder.DropTable(
                name: "MediaPictures");

            migrationBuilder.DropTable(
                name: "Reminders");

            migrationBuilder.DropTable(
                name: "SinhViens");

            migrationBuilder.DropTable(
                name: "TaxiAreaPrices");

            migrationBuilder.DropTable(
                name: "TaxiBookings");

            migrationBuilder.DropTable(
                name: "TaxiDiscounts");

            migrationBuilder.DropTable(
                name: "TaxiDisplays");

            migrationBuilder.DropTable(
                name: "TaxiDownTimes");

            migrationBuilder.DropTable(
                name: "TaxiInstallationCosts");

            migrationBuilder.DropTable(
                name: "TaxiPictures");

            migrationBuilder.DropTable(
                name: "UserBillboards");

            migrationBuilder.DropTable(
                name: "UserBuses");

            migrationBuilder.DropTable(
                name: "UserCampaigns");

            migrationBuilder.DropTable(
                name: "UserDigitalBillboards");

            migrationBuilder.DropTable(
                name: "UserLocations");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserShelters");

            migrationBuilder.DropTable(
                name: "UserTaxis");

            migrationBuilder.DropTable(
                name: "BusTypeOfArtworks");

            migrationBuilder.DropTable(
                name: "MediaChannels");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "TaxiTypeOfArtworks");

            migrationBuilder.DropTable(
                name: "PictureTypes");

            migrationBuilder.DropTable(
                name: "Billboards");

            migrationBuilder.DropTable(
                name: "Buses");

            migrationBuilder.DropTable(
                name: "DigitalBillboards");

            migrationBuilder.DropTable(
                name: "BusShelters");

            migrationBuilder.DropTable(
                name: "Taxis");

            migrationBuilder.DropTable(
                name: "Campaigns");

            migrationBuilder.DropTable(
                name: "BusRoutes");

            migrationBuilder.DropTable(
                name: "BusTypes");

            migrationBuilder.DropTable(
                name: "TaxiAreas");

            migrationBuilder.DropTable(
                name: "TaxiTypes");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
