﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SearchKeyworkText",
                table: "Billboards");

            migrationBuilder.AddColumn<string>(
                name: "SearchKeywordText",
                table: "Billboards",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SearchKeywordText",
                table: "Billboards");

            migrationBuilder.AddColumn<string>(
                name: "SearchKeyworkText",
                table: "Billboards",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
