﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class UploadContentRequest
    {
        [Required]
        public string Content { get; set; }
        [Required]
        public string FileName { get; set; }
    }
}
