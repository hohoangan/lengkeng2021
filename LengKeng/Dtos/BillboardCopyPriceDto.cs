﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BillboardCopyPriceDto
    {
        public int? LocationId { get; set; }
        public int? CompanyId { get; set; }
        public int? BillboardId { get; set; }
        public int FromYear { get; set; }
        public int ToYear { get; set; }
    }
}
