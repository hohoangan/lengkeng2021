﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LengKeng.Migrations
{
    public partial class v17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "TaxiDisplays",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "DigitalBillboardDisplays",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "BusShelterDisplays",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "BusDisplays",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "BillboardDisplays",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "TaxiDisplays");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "DigitalBillboardDisplays");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "BusShelterDisplays");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "BusDisplays");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "BillboardDisplays");
        }
    }
}
