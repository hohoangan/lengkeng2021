﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }
        //1 user belong to 1 company
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [JsonIgnore]
        public virtual UserRole UserRoles { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserBillboard> UserBillboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserBus> UserBuses { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserDigitalBillboard> UserDigitalBillboards { get; set; }
        //1 user belong to >= 2 locations
        [JsonIgnore]
        public virtual ICollection<UserLocation> UserLocations { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserShelter> UserShelters { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserTaxi> UserTaxis { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserCampaign> UserCampaigns { get; set; }
    }
}
