﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BusController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;

        public BusController(LengKengDbContext context, IMapper mapper, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _blobService = blobService;
        }

        #region CREATE - GET - UPDATE
        //GET api/Bus
        /// <summary>
        /// Lấy danh sách bus
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="busRouteId"></param>
        /// <param name="companyId"></param>
        /// <param name="plateNumber">tìm theo số xe</param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<IEnumerable<Bus>> GetAll(int PageNumber, int PageSize,
                                                     int busRouteId, int companyId, string plateNumber)
        {
            try
            {
                var models = _context.Buses.Include(x => x.BusTypes).AsNoTracking().Where(b => b.CompanyId == companyId).Where(x => x.BusRouteId == busRouteId);
                if (!string.IsNullOrEmpty(plateNumber))
                    models = models.Where(x => x.PlateNumber.Contains(plateNumber));
                
                var response = PagedList<Bus>.ToPagedList(models.OrderBy(x => x.BusTypeId).ThenBy(x => x.PlateNumber), PageNumber, PageSize);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        //GET api/Bus/{id}
        /// <summary>
        /// Thông tin chi tiết của 1 bus
        /// </summary>
        /// <param name="Id">BusId</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetDetailBus")]
        public ActionResult<Bus> GetDetailBus(int Id)
        {
            try
            {
                var model = _context.Buses.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(b => b.UserBuses)
                    .Include(b => b.BusPictures)
                    .Include(b => b.BusDownTimes)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/Bus
        /// <summary>
        /// Tạo mới 1 bus
        /// </summary>
        /// <param name="bus"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult Create(Bus bus)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.Buses.Add(bus);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetDetailBus), new { id = bus.Id }, bus);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/Bus/AddBusFromList
        /// <summary>
        /// Thêm danh sách bus
        /// </summary>
        /// <param name="bus"></param>
        /// <returns></returns>
        [HttpPost("AddBusFromList")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddBusFromList(BusCreadDto bus)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Bus> lst = new List<Bus>();
                List<string> plateNumbers = bus.PlateNumbers;
                List<string> wrongFormat = new List<string>();
                var status = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).FirstOrDefault();

                foreach (var item in plateNumbers)
                {
                    var plate = item.Trim().Replace(" ", "").Replace(".", "");
                    if (plate.Split("-").Count() < 2 || string.Empty.Equals(plate.Split("-")[1]) || string.Empty.Equals(plate.Split("-")[0]))
                    {
                        wrongFormat.Add(item);
                        continue;
                    }

                    Bus busNew = new Bus
                    {
                        Id = 0,
                        PlateNumber = plate,
                        StatusId = status.Id,
                        CompanyId = bus.Bus.CompanyId,
                        DimensionH = bus.Bus.DimensionH,
                        DimensionW = bus.Bus.DimensionW,
                        //OTC = bus.Bus.OTC,
                        BusTypeId = bus.Bus.BusTypeId,
                        BusRouteId = bus.Bus.BusRouteId,
                    };
                    lst.Add(busNew);
                }

                if (wrongFormat.Count() == 0)
                {
                    _context.Buses.AddRange(lst);
                    _context.SaveChanges();
                    _context.Database.CommitTransaction();
                    return Ok();
                }
                else
                {
                    return NotFound(wrongFormat);
                }
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bus),
                    UserId = userId
                });
            }
        }

        //PUT api/Bus/{id}
        /// <summary>
        /// Thay đổi thông tin bus
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="busUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult Update(int Id, BusUpdateDto busUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Buses.Where(b => b.Id == Id).FirstOrDefault();
                if (b == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "Bus"].Value);
                else
                {
                    var v = _context.Buses.Where(x => x.PlateNumber == busUpdateDto.PlateNumber && x.Id != Id).FirstOrDefault();
                    if (v != null)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, busUpdateDto.PlateNumber].Value);
                    }
                }

                _mapper.Map(busUpdateDto, b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busUpdateDto),
                    UserId = userId
                });
            }
        }

        //PUT api/bus/{id}
        [HttpPut("{Id}/changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult ChangeStatus(int Id, int statusId)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Buses.Find(Id);
                if (b == null)
                    return NotFound();

                var status = _context.Statuses.Find(statusId);

                if (status.Name == Constant.ConstantStatus.Inactive)
                {
                    var booking = _context.BusBookings.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .Where(x => DateTime.Now <= x.ToDate).FirstOrDefault();
                    if (booking != null)
                        return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()]);
                }
                b.StatusId = statusId;

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("BusID: {0}, statusId: {1}", Id, statusId),
                    UserId = userId
                });
            }
        }

        //GET api/Bus
        /// <summary>
        /// Lấy danh sách loại xe
        /// </summary>
        /// <returns></returns>
        [HttpGet("cartype")]
        public ActionResult<IEnumerable<BusType>> GetAllType()
        {
            try
            {
                var types = _context.BusTypes.ToList();
                return Ok(types);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus
        /// <summary>
        /// Lấy danh sách loại/kiểu dán của xe
        /// </summary>
        /// <returns></returns>
        [HttpGet("arttype")]
        public ActionResult<IEnumerable<BusTypeOfArtwork>> GetAllArtType()
        {
            try
            {
                var types = _context.BusTypeOfArtworks.ToList();
                return Ok(types);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// SearchKeywordText
        /// </summary>
        /// <returns></returns>
        [HttpGet("SearchKeywordText")]
        public ActionResult GetSearchKeywordText()
        {
            try
            {
                var model = _context.BusKeywords.OrderBy(x => x.Name).ToList();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region ROUTE
        //GET api/Bus/routes/locations/1
        /// <summary>
        /// Danh sách tuyến bus theo tỉnh/tp và công ty
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="companyId">khác null thì lấy theo NCC</param>
        /// <returns></returns>
        [HttpGet("routes/locations/{locationId}")]
        public ActionResult GetAllRouteByLocation(int locationId, int? companyId)
        {
            try
            {
                var models = _context.BusRoutes.Where(x => x.LocationId == locationId);
                if (companyId != null)
                {
                    models = models.Where(x => x.BusRouteCompanies.Any(x => x.CompanyId == companyId));
                }

                var routes = models.ToList().OrderBy(x => x.Code, new StrCmpLogicalComparer());

                return Ok(routes);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus/routes/company/1
        /// <summary>
        /// Danh sách công ty quản lý tuyến bus
        /// </summary>
        /// <param name="busRouteId"></param>
        /// <param name="companyId">dùng cho partner</param>
        /// <returns></returns>
        [HttpGet("routes/company/{busRouteId}")]
        public ActionResult GetAllCompanyByRoute(int busRouteId, int? companyId)
        {
            try
            {
                var models = _context.BusRouteCompanies
                                    .Where(x => x.BusRouteId == busRouteId)
                                    .Include(x => x.Companies)
                                    .Select(x => x.Companies).AsNoTracking();
                if (companyId != null)
                {
                    models = models.Where(x => x.CompanyId == companyId);
                }

                return Ok(models.OrderBy(x=>x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus/routes/locations
        /// <summary>
        /// Lấy danh sách tỉnh thành có tuyến bus
        /// </summary>
        /// <param name="companyId">khác null thì lấy theo công ty NCC</param>
        /// <returns></returns>
        [HttpGet("routes/locations")]
        public ActionResult GetAllRouteLocation(int? companyId)
        {
            try
            {
                if (companyId != null)
                {
                    return Ok(_context.BusRoutes.Where(x => x.BusRouteCompanies.Any(x => x.CompanyId == companyId))
                                                .Include(x => x.Locations)
                                                .Select(x => x.Locations).Distinct()
                                                .OrderBy(x => x.OrderBy).ThenBy(x => x.Name));
                }
                else
                {
                    return Ok(_context.BusRoutes.Include(x => x.Locations)
                                        .Select(x => x.Locations).Distinct()
                                        .OrderBy(x => x.OrderBy).ThenBy(x => x.Name));
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus/routes/{id}
        /// <summary>
        /// Thông tin chi tiết 1 tuyến bus
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("routes/{Id}", Name = "GetDetailRoute")]
        public ActionResult<BusRoute> GetDetailRoute(int Id)
        {
            try
            {
                var model = _context.BusRoutes.Where(b => b.Id == Id)
                    .Include(b => b.Locations)
                    .Include(b => b.Buses)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/Bus/routes
        /// <summary>
        /// Thêm mới tuyến bus
        /// </summary>
        /// <param name="busRoute"></param>
        /// <returns></returns>
        [HttpPost("routes")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddRoute(BusRoute busRoute)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                var checkDuplicate = _context.BusRoutes.Where(x => x.LocationId == busRoute.LocationId && x.Code == busRoute.Code).FirstOrDefault();
                if (checkDuplicate != default)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, busRoute.Code].Value);
                }
                _context.Database.BeginTransaction();
                _context.BusRoutes.Add(busRoute);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetDetailRoute), new { id = busRoute.Id }, busRoute);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busRoute),
                    UserId = userId
                });
            }
        }

        //PUT api/Bus/routes/id
        /// <summary>
        /// Sửa thông tin tuyến bus
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="busRouteUpdateDto">Nếu sửa OTC thì mới cập nhật lại OTC Date</param>
        /// <returns></returns>
        [HttpPut("routes/{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult UpdateRoute(int Id, BusRouteUpdateDto busRouteUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusRoutes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                var checkDuplicate = _context.BusRoutes.Where(x => x.LocationId == busRouteUpdateDto.LocationId 
                                                        && x.Code == busRouteUpdateDto.Code
                                                        && x.Id != Id).FirstOrDefault();
                if (checkDuplicate != default)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, busRouteUpdateDto.Code].Value);
                }

                //vi dieu
                _mapper.Map(busRouteUpdateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busRouteUpdateDto),
                    UserId = userId
                });
            }
        }
        #endregion

        #region COMPANY
        //GET api/routes/{busRouteId}/company
        /// <summary>
        /// Danh sách công ty quản lý tuyến bus | dùng cho quản lý BusRouteCompanies
        /// </summary>
        /// <param name="busRouteId"></param>
        /// <returns></returns>
        [HttpGet("routes/{busRouteId}/company")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult GetCompanyInBusroute(int busRouteId)
        {
            try
            {
                var models = _context.BusRouteCompanies.Where(x => x.BusRouteId == busRouteId)
                                                        .Include(x => x.Companies)
                                                        .ToList()
                                                        .Select(x => { x.Companies.BusRouteCompanies = null; return x.Companies; })
                                                        .OrderBy(x=>x.Name);
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/routes/{busRouteId}/company
        /// <summary>
        /// Danh sách công ty không quản lý tuyến bus | dùng cho quản lý BusRouteCompanies
        /// </summary>
        /// <param name="busRouteId"></param>
        /// <returns></returns>
        [HttpGet("routes/{busRouteId}/company/NotIn")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult GetCompanyNotInBusroute(int busRouteId)
        {
            try
            {
                var models = _context.Companies.Where(x => x.Roles.RoleName == Constant.ConstantRole.BusCompany)
                                           .Where(x =>
                                                !(_context.BusRouteCompanies.Where(x => x.BusRouteId == busRouteId)
                                                .Select(x => x.CompanyId)).Contains(x.CompanyId)
                                           ).OrderBy(x => x.Name);
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/Bus/routes/company
        /// <summary>
        /// Thêm cty NCC cho tuyến bus | dùng cho quản lý BusRouteCompanies
        /// </summary>
        /// <param name="busRouteCompany"></param>
        /// <returns></returns>
        [HttpPost("routes/company")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddRouteCompany(BusRouteCompany busRouteCompany)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                var checkDuplicate = _context.BusRouteCompanies
                                            .Where(x => x.CompanyId == busRouteCompany.CompanyId 
                                            && x.BusRouteId == busRouteCompany.BusRouteId).FirstOrDefault();
                if (checkDuplicate != default)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, ""].Value);
                }
                _context.Database.BeginTransaction();
                _context.BusRouteCompanies.Add(busRouteCompany);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busRouteCompany),
                    UserId = userId
                });
            }
        }

        //DEL api/Bus/routes/company
        /// <summary>
        /// Xóa NCC quản lý tuyến bus | dùng cho quản lý BusRouteCompanies
        /// </summary>
        /// <param name="busRouteId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpDelete("routes/company")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult DelRouteCompany(int busRouteId, int companyId)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                var check = _context.BusRouteCompanies.Where(x => x.CompanyId == companyId && x.BusRouteId == busRouteId).FirstOrDefault();
                if (check == null)
                {
                    return NotFound();
                }

                var bus = _context.Buses.Where(x => x.BusRouteId == busRouteId && x.CompanyId == companyId).FirstOrDefault();
                var price = _context.BusRoutePrices.Where(x => x.BusRouteId == busRouteId && x.CompanyId == companyId).FirstOrDefault();
                if (bus != null || price != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.BusRouteNotSuitable].Value);
                }

                _context.Database.BeginTransaction();
                _context.BusRouteCompanies.Remove(check);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("BusRouteId:{0} | CompanyId:{1}", busRouteId, companyId),
                    UserId = userId
                });
            }
        }
        #endregion

        #region PRICE
        //GET api/Bus/price
        /// <summary>
        /// Giá tuyến bus trong năm
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="busRouteId"></param>
        /// <param name="year"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("price")]
        public ActionResult<IEnumerable<BusRoutePrice>> GetAllPrice(int PageNumber, int PageSize, int busRouteId, int year, int companyId)
        {
            try
            {
                var models = _context.BusRoutePrices.Where(x => x.BusRouteId == busRouteId && x.FromDate.Year == year && x.CompanyId == companyId)
                                                    .Include(x => x.Units)
                                                    .Include(x => x.BusTypes)
                                                    .Include(x => x.BusTypeOfArtworks)
                                                    .AsNoTracking();
                var response = PagedList<BusRoutePrice>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderBy(x=>x.BusTypes.Name).ThenBy(x=>x.BusTypeOfArtworks.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus/price/{Id}
        /// <summary>
        /// Giá tuyến bus từ ngày - đến ngày
        /// </summary>
        /// <param name="Id">BusRouteId</param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("price/{Id}")]
        public ActionResult<IEnumerable<BusRoutePrice>> GetPrice(int Id, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = _context.BusRoutePrices.Where(b => b.BusRouteId == Id)
                    .Where(b => fromDate.Date <= b.ToDate.Date)
                    .Where(b => b.FromDate.Date <= toDate.Date)
                    .Include(b => b.BusTypes).OrderBy(b => b.FromDate)
                    .ToList();
                
                    return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/Bus/price
        /// <summary>
        /// Thêm giá bus
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        [HttpPost("price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult AddPrice(BusRoutePrice price)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //check timeoverlap
                var priceModel = _context.BusRoutePrices.Where(x => x.BusRouteId == price.BusRouteId
                                                            && x.UnitId == price.UnitId
                                                            && x.BusTypeId == price.BusTypeId
                                                            && x.BusTypeOfArtworkId == price.BusTypeOfArtworkId
                                                            && x.FromDate <= price.ToDate && price.FromDate <= x.ToDate
                                                            && x.CompanyId == price.CompanyId
                                                            );
                if (priceModel.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                _context.BusRoutePrices.Add(price);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok(price);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(price),
                    UserId = userId
                });
            }
        }

        //POST api/Bus/priceList
        /// <summary>
        /// Thêm danh sách giá
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("priceList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult AddPriceList(List<BusRoutePrice> lst)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusRoutePrices.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/Bus/price/id
        /// <summary>
        /// Thay đổi giá
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="busRoutePriceUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult UpdatePrice(int Id, BusRoutePriceUpdateDto busRoutePriceUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusRoutePrices.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //check timeoverlap
                var priceModel = _context.BusRoutePrices.Where(x => x.Id != Id
                                                            && x.BusRouteId == busRoutePriceUpdateDto.BusRouteId
                                                            && x.UnitId == busRoutePriceUpdateDto.UnitId
                                                            && x.BusTypeId == busRoutePriceUpdateDto.BusTypeId
                                                            && x.BusTypeOfArtworkId == busRoutePriceUpdateDto.BusTypeOfArtworkId
                                                            && x.FromDate <= busRoutePriceUpdateDto.ToDate && busRoutePriceUpdateDto.FromDate <= x.ToDate
                                                            && x.CompanyId == busRoutePriceUpdateDto.CompanyId
                                                            );
                if (priceModel.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);

                busRoutePriceUpdateDto.FromDate = busRoutePriceUpdateDto.FromDate.Date;
                busRoutePriceUpdateDto.ToDate = busRoutePriceUpdateDto.ToDate.Date;
                //vi dieu
                _mapper.Map(busRoutePriceUpdateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busRoutePriceUpdateDto),
                    UserId = userId
                });
            }
        }

        //DEL api/bus/price/{Id}
        /// <summary>
        /// Xóa giá
        /// </summary>
        /// <param name="Id">BusRoutePriceId</param>
        /// <returns></returns>
        [HttpDelete("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DeletePrice(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var busRoutePrice = _context.BusRoutePrices.Find(Id);
                if (busRoutePrice == null)
                    return NotFound();

                var booking = _context.BusBookings
                                                  .Where(x => x.Statuses.Name == Constant.ConstantStatus.Active || x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                  .Where(x => x.FromDate <= busRoutePrice.ToDate && busRoutePrice.FromDate <= x.ToDate)
                                                  .Where(x => x.BusTypeOfArtworkId == busRoutePrice.BusTypeOfArtworkId
                                                            && x.Buses.BusTypeId == busRoutePrice.BusTypeId
                                                            && x.Buses.BusRouteId == busRoutePrice.BusRouteId
                                                            )
                                                  .FirstOrDefault();
                if (booking != null)
                    return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()].ToString());
                else
                    _context.BusRoutePrices.Remove(busRoutePrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("busRoutePrice Id: {0}", Id),
                    UserId = userId
                });
            }
        }
        #endregion

        #region PICTURES
        //PUT api/bus/Pictures
        /// <summary>
        /// Thêm hình ảnh chụp bus campaign (mobile)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost("Pictures")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult AddPictures(List<BusPicture> list)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BusPictures.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(list),
                    UserId = userId
                });
            }
        }

        //GET api/bus/Pictures/{Id}
        /// <summary>
        /// danh sách hình bus trong campaign (mobile-web)
        /// </summary>
        /// <param name="busId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("Pictures/{busId}")]
        public ActionResult GetPicture(int busId, int campaignId)
        {
            try
            {
                var query = _context.BusPictures.Where(b => b.BusId == busId && b.CampaignId == campaignId).ToList();

                var list = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Bus)
                                                 .AsNoTracking()
                                                 .AsEnumerable()
                                                 .Select(item => new
                                                 {
                                                     item.PictureTypes.Id,
                                                     item.PictureTypes.Name,
                                                     list = query.Where(x => x.PictureTypeId == item.PictureTypes.Id).OrderByDescending(b => b.LastUpdated)
                                                 });

                return Ok(list);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //DEL api/bus/Pictures/{busPictureId}
        /// <summary>
        /// xóa hình trong campaign
        /// </summary>
        /// <param name="busPictureId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpDelete("Pictures/{busPictureId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelPicture(int busPictureId, int campaignId)
        {
            try
            {
                var pic = _context.BusPictures.Where(b => b.Id == busPictureId && b.CampaignId == campaignId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BusPictures.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren clond
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //get display
        /// <summary>
        /// Lấy danh sách hình đại diện của bus
        /// </summary>
        /// <param name="cartypeId"></param>
        /// <param name="arttypeId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("display")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult GetPictureDisplay(int cartypeId, int arttypeId, int? companyId)
        {
            try
            {
                var pic = _context.BusDisplays.Where(b => b.CarTypeId == cartypeId && b.ArtTypeId == arttypeId);
                if(companyId != null)
                    pic = pic.Where(x => x.CompanyId == companyId);
                else
                    pic = pic.Where(x => x.CompanyId == null);

                return Ok(pic.OrderBy(x=>x.Order).ToList());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Thêm hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        [HttpPost("display")]
        public ActionResult AddDislay(List<BusDisplay> list)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                if (list.Count > 0)
                {
                    var busDis = list.First();
                    var checkOrder = _context.BusDisplays.Where(x => x.ArtTypeId == busDis.ArtTypeId && x.CarTypeId == busDis.CarTypeId
                                                                && list.Select(c => c.Order).Contains(x.Order)).ToList();
                    if (busDis.CompanyId != null)
                        checkOrder.Where(x => x.CompanyId == busDis.CompanyId);

                    if (checkOrder.Count > 0)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, "Order"].Value);
                    }
                    _context.BusDisplays.AddRange(list);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(list),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Update hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        [HttpPut("display")]
        public ActionResult UpdateDislay(List<BusDisplayUpdateDto> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                list.ForEach(item =>
                {
                    var b = _context.BusDisplays.Find(item.Id);
                    _mapper.Map(item, b);
                    _context.SaveChanges();
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// xóa hình đại hiện, chỉ cần truyền id vào, hình xóa cả trong DB và cloud
        /// </summary>
        /// <param name="busDisplayId"></param>
        /// <returns></returns>
        [HttpDelete("display/{busDisplayId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelPictureDisplay(int busDisplayId)
        {
            try
            {
                var pic = _context.BusDisplays.Where(b => b.Id == busDisplayId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BusDisplays.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region DISCOUNT
        //GET api/bus/discount
        /// <summary>
        /// Láy discount của 1 dòng giá
        /// </summary>
        /// <param name="busRoutePriceId"></param>
        /// <returns></returns>
        [HttpGet("discount")]
        public ActionResult<IEnumerable<BusDiscount>> GetDiscount(int busRoutePriceId)
        {
            try
            {
                var query = _context.BusDiscounts.Where(x=>x.BusRoutePriceId == busRoutePriceId).OrderBy(x => x.Month).ToList();
                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/bus/discount/
        /// <summary>
        /// thêm discount cho bảng giá
        /// </summary>
        /// <param name="discount"></param>
        /// <returns></returns>
        [HttpPost("discount")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult AddDiscount(BusDiscount discount)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusDiscounts.Add(discount);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/bus/discountList/
        /// <summary>
        /// thêm danh sach discount cho giá
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("discountList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult AddDiscountList(IEnumerable<BusDiscount> lst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BusDiscounts.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(lst),
                    UserId = userId
                });
            }
        }

        //PUT api/bus/discount/id
        /// <summary>
        /// update discount
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("discount/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult UpdateDiscount(int Id, BusDiscountUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusDiscounts.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/bus/discount/id
        /// <summary>
        /// update danh sách discount
        /// </summary>
        /// <param name="updatLst"></param>
        /// <returns></returns>
        [HttpPut("discount/updatelist")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult UpdateDiscountList(IEnumerable<BusDiscount> updatLst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BusDiscounts.UpdateRange(updatLst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updatLst),
                    UserId = userId
                });
            }
        }

        //PUT api/bus/discount/{Id}
        /// <summary>
        /// xóa discount
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("discount/{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult DelDiscount(int Id)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusDiscounts.Remove(_context.BusDiscounts.Find(Id));
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region DOWNTIME
        //GET api/bus/downtime/{Id}
        /// <summary>
        /// Lấy thông tin downtime của 1 bus trong năm
        /// </summary>
        /// <param name="Id">BusId</param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("downtime/{Id}")]
        public ActionResult<IEnumerable<BusDownTime>> GetDowntime(int Id, int year)
        {
            try
            {
                var query = _context.BusDownTimes.Where(b => b.BusId == Id)
                    .Where(b => b.FromDate.Year == year)
                    .Include(b => b.Buses).OrderByDescending(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/bus/downtime/
        /// <summary>
        /// Thêm downtime
        /// </summary>
        /// <param name="downTime"></param>
        /// <returns></returns>
        [HttpPost("downtime")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult AddDowntime(List<BusDownTime> downTime)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Bus> lst = new List<Bus>();
                for (int i = 0; i < downTime.Count; i++)
                {
                    //check timeoverlap
                    var check = _context.BusDownTimes.Where(x => x.BusId == downTime[i].BusId
                                                                && x.FromDate <= downTime[i].ToDate && downTime[i].FromDate <= x.ToDate
                                                            ).Include(x=>x.Buses).FirstOrDefault();
                    if (check != null)
                    {
                        lst.Add(check.Buses);
                        //throw new NotImplementedException(string.Format("{0} {1}-{2}:{3}",
                        //            check.Buses.PlateNumber, check.FromDate, check.ToDate, _localizer[Constant.ConstantMessage.TimeOverlap]));
                    }

                }
                if (lst.Count > 0)
                {
                    return NotFound(lst);
                }
                _context.BusDownTimes.AddRange(downTime);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(downTime),
                    UserId = userId
                });
            }
        }

        //PUT api/bus/downtime/id
        /// <summary>
        /// update downtime
        /// </summary>
        /// <param name="Id">BusDownTimes Id</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult UpdateDowntime(int Id, BusDownTimeUpdateDto updateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updateDto),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// xóa downtime
        /// </summary>
        /// <param name="Id">BusDownTimes Id</param>
        /// <returns></returns>
        [HttpDelete("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelDowntime(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                _context.Remove(b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion

        #region COPY PRICE
        //POST api/Bus/copyPrice
        /// <summary>
        /// CopyPrice giá 1 tuyến bus cho tuyến khác của 1 NCC
        /// </summary>
        /// <param name="busRoutePriceId">Id của bảng giá để copy</param>
        /// <param name="busRoutes">danh sách các tuyến sẽ được áp dụng giá copy</param>
        /// <returns></returns>
        [HttpPost("copyPrice/{busRoutePriceId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult CopyPrice(int busRoutePriceId, List<int> busRoutes)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();

                var busPrice = (from bs in _context.BusRoutePrices.Where(x => x.Id == busRoutePriceId)
                               select bs).FirstOrDefault();
                var busDiscount = from dis in _context.BusDiscounts.Where(x => x.BusRoutePriceId == busRoutePriceId)
                                  select dis;
                List<BusRoute> errRoutes = new List<BusRoute>();
                //add price to routes
                busRoutes.ForEach(item => {
                    BusRoutePrice price = new BusRoutePrice
                    {
                        Id = 0,
                        Price = busPrice.Price,
                        PromotePercent = busPrice.PromotePercent,
                        MinCarBooking = busPrice.MinCarBooking,
                        BusTypeOfArtworkId = busPrice.BusTypeOfArtworkId,
                        BusTypeId = busPrice.BusTypeId,
                        FromDate = busPrice.FromDate,
                        ToDate = busPrice.ToDate,
                        UnitId = busPrice.UnitId,
                        BusRouteId = item, //RouteId
                        CreatedUserId = userId,
                        UserId = userId,
                        CompanyId = busPrice.CompanyId
                    };
                    //check timeoverlap
                    var priceModel = _context.BusRoutePrices.Include(x=>x.BusRoutes).Where(x => x.BusRouteId == price.BusRouteId
                                                                && x.UnitId == price.UnitId
                                                                && x.BusTypeId == price.BusTypeId
                                                                && x.BusTypeOfArtworkId == price.BusTypeOfArtworkId
                                                                && x.FromDate <= price.ToDate && price.FromDate <= x.ToDate
                                                                && x.CompanyId == price.CompanyId
                                                                ).FirstOrDefault();
                    if (priceModel != null)
                    {
                        errRoutes.Add(priceModel.BusRoutes);
                        //throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                    }
                    else
                    {
                        //add price to list
                        _context.BusRoutePrices.Add(price);
                        _context.SaveChanges();
                        //add discount
                        if (busDiscount != null)
                        {
                            var discountLst = busDiscount.ToList().Select(x => { x.BusRoutePriceId = price.Id; x.Id = 0; return x; });
                            _context.BusDiscounts.AddRange(discountLst);
                            _context.SaveChanges();
                        }
                    }
                });

                //neu co loi thi xuat ra
                if(errRoutes.Count > 0)
                {
                    _context.Database.RollbackTransaction();
                    return NotFound(errRoutes);
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("busRoutePriceId:{0}, busRoutes:{1}",
                                                busRoutePriceId, busRoutes),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// sao chép giá tất cả tuyến bus của 1 NCC, theo khu vực từ năm xxxx sang năm yyyy
        /// </summary>
        /// <param name="busCopyPriceDto">LocationId: null nếu chọn tất cả, BusRouteId: null nếu chọn tất cả</param>
        /// <returns></returns>
        //POST api/Bus/copyPrice
        [HttpPost("copyPrice")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult copyPrice(BusCopyPriceDto busCopyPriceDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //price of fromyear
                var bPriceFromYear = _context.BusRoutePrices
                                .Where(x => x.FromDate.Year == busCopyPriceDto.FromYear)
                                .Where(x => x.CompanyId == busCopyPriceDto.CompanyId);
                if (busCopyPriceDto.LocationId != null)//location của tỉnh/tp
                    bPriceFromYear = bPriceFromYear.Where(x => x.BusRoutes.LocationId == busCopyPriceDto.LocationId);
                if (busCopyPriceDto.BusRouteId != null)//null = all routes
                    bPriceFromYear = bPriceFromYear.Where(x => x.BusRouteId == busCopyPriceDto.BusRouteId);

                if (bPriceFromYear.Count() == 0)
                    return NotFound();

                //price of toyear
                var busPriceToYear = _context.BusRoutePrices
                                .Where(x => x.FromDate.Year == busCopyPriceDto.ToYear)
                                .Where(x => x.CompanyId == busCopyPriceDto.CompanyId);
                if (busCopyPriceDto.LocationId != null)//location của tỉnh/tp
                    busPriceToYear = busPriceToYear.Where(x => x.BusRoutes.LocationId == busCopyPriceDto.LocationId);
                if (busCopyPriceDto.BusRouteId != null)//null = all routes
                    busPriceToYear = busPriceToYear.Where(x => x.BusRouteId == busCopyPriceDto.BusRouteId);

                //chi lay nhung busroute chua co gia trong ToYear
                var busPrice = bPriceFromYear.Where(x => !busPriceToYear.Select(x => x.BusRouteId).Contains(x.BusRouteId));

                //if (busPrice.Count() == 0)
                //    return NotFound(_localizer[Constant.ConstantMessage.TimeOverlap].Value);

                foreach (var price in busPrice.ToList())
                {
                    //tao price moi
                    var bPrice = new BusRoutePrice
                    {
                        Id = 0,
                        Price = price.Price,
                        PromotePercent = price.PromotePercent,
                        MinCarBooking = price.MinCarBooking,
                        BusTypeOfArtworkId = price.BusTypeOfArtworkId,
                        BusTypeId = price.BusTypeId,
                        FromDate = price.FromDate.AddYears(busCopyPriceDto.ToYear - busCopyPriceDto.FromYear),
                        ToDate = price.ToDate.AddYears(busCopyPriceDto.ToYear - busCopyPriceDto.FromYear),
                        UnitId = price.UnitId,
                        BusRouteId = price.BusRouteId,
                        CreatedUserId = userId,
                        CompanyId = price.CompanyId,
                        UserId = userId
                    };

                    _context.BusRoutePrices.Add(bPrice);
                    _context.SaveChanges();
                    //copy Discount tu bang gia cu
                    var discount = from dis in _context.BusDiscounts.Where(x => x.BusRoutePriceId == price.Id)
                                   select new BusDiscount
                                   {
                                       Id = 0,
                                       Month = dis.Month,
                                       Percent = dis.Percent,
                                       BusRoutePriceId = bPrice.Id
                                   };
                    _context.BusDiscounts.AddRange(discount.ToList());
                    _context.SaveChanges();
                };
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(busCopyPriceDto),
                    UserId = userId
                });
            }
        }
        #endregion
    }
}
