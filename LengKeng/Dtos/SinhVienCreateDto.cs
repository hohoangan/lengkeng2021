﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class SinhVienCreateDto
    {
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
        [ForeignKey("StudentID")]
        public int StudentID { get; set; }
    }
}
