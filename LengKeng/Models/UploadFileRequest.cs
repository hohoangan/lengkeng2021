﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class UploadFileRequest
    {
        [Required(ErrorMessage ="Check file path")]
        public string FilePath { get; set; }
        [Required]
        public string FileName { get; set; }
    }
}
