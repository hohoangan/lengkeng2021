﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusTypeOfArtwork
    {
        // Body, die-cut on glass, Full glass
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusRoutePrice> BusRoutePrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusBooking> BusBookings { get; set; }
    }
}
