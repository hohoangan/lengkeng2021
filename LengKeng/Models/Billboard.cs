﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class Billboard
    {
        [Key]
        public int Id { get; set; }   
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Orientation { get; set; }
        //public string Lighting { get; set; }
        public string HighlightFeature { get; set; }
        //public string Visibility { get; set; }
        //public string TrafficDensity { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Companies { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        public double GPSLocX { get; set; }
        public double GPSLocY { get; set; }
        public string SearchKeywordText { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserBillboard> UserBillboards { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardPicture> BillBoardPictures { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardPrice> BillboardPrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardDownTime> BillboardDownTimes { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardBooking> BillboardBookings { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardDisplay> BillboardDisplays { get; set; }
        [JsonIgnore]
        public virtual ICollection<BillboardCamera> BillboardCameras { get; set; }
    }
}
