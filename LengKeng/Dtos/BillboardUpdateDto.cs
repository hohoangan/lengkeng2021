﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BillboardUpdateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Orientation { get; set; }
        [Required]
        public string HighlightFeature { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [Required, Id]
        public double DimensionH { get; set; }
        [Required, Id]
        public double DimensionW { get; set; }
        [Required, Id]
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        public double GPSLocX { get; set; }
        public double GPSLocY { get; set; }
        public string SearchKeywordText { get; set; }
    }
}
