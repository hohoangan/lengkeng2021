﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TaxiController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;

        public TaxiController(LengKengDbContext context, IMapper mapper, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _blobService = blobService;
        }

        #region CREATE - GET - UPDATE
        //GET api/taxi
        /// <summary>
        /// Lất danh sách tất cả taxi
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="taxiAreaId"></param>
        /// <param name="companyId"></param>
        /// <param name="plateNumber"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<IEnumerable<Taxi>> GetAll(int PageNumber, int PageSize,
                                                     int taxiAreaId, int companyId, string plateNumber)
        {
            try
            {
                var models = _context.Taxis.Include(x => x.TaxiTypes).AsNoTracking().Where(b => b.CompanyId == companyId).Where(x => x.TaxiAreaId == taxiAreaId);
                if (!string.IsNullOrEmpty(plateNumber))
                    models = models.Where(x => x.PlateNumber.Contains(plateNumber));

                var response = PagedList<Taxi>.ToPagedList(models.OrderBy(x=>x.TaxiTypeId).ThenBy(x=>x.PlateNumber), PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/taxi/{id}
        /// <summary>
        /// Thông tin 1 taxi
        /// </summary>
        /// <param name="Id">TaxiId</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetDetailTaxi")]
        public ActionResult<Taxi> GetDetailTaxi(int Id)
        {
            try
            {
                var model = _context.Taxis.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(b => b.UserTaxis)
                    .Include(b => b.TaxiPictures)
                    .Include(b => b.TaxiDownTimes)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/taxi
        /// <summary>
        /// Tạo mới
        /// </summary>
        /// <param name="taxi"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult Create(Taxi taxi)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.Taxis.Add(taxi);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetDetailTaxi), new { id = taxi.Id }, taxi);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/taxi/AddTaxiFromList
        /// <summary>
        /// Tạo taxi từ danh sách
        /// </summary>
        /// <param name="taxi"></param>
        /// <returns></returns>
        [HttpPost("AddTaxiFromList")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddTaxiFromList(TaxiCreateDto taxi)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Taxi> lst = new List<Taxi>();
                List<string> plateNumbers = taxi.PlateNumbers;
                List<string> wrongFormat = new List<string>();
                var status = _context.Statuses.Where(x => x.Name == Constant.ConstantStatus.Active).FirstOrDefault();

                foreach (var item in plateNumbers)
                {
                    var plate = item.Trim().Replace(" ", "").Replace(".", "");
                    if (plate.Split("-").Count() < 2 || string.Empty.Equals(plate.Split("-")[1]) || string.Empty.Equals(plate.Split("-")[0]))
                    {
                        wrongFormat.Add(item);
                        continue;
                    }

                    Taxi taxiNew = new Taxi
                    {
                        Id = 0,
                        PlateNumber = plate,
                        StatusId = status.Id,
                        CompanyId = taxi.Taxi.CompanyId,
                        DimensionH = taxi.Taxi.DimensionH,
                        DimensionW = taxi.Taxi.DimensionW,
                        //OTC = taxi.Taxi.OTC,
                        TaxiTypeId = taxi.Taxi.TaxiTypeId,
                        TaxiAreaId = taxi.Taxi.TaxiAreaId,
                    };
                    lst.Add(taxiNew);
                }

                if (wrongFormat.Count() == 0)
                {
                    _context.Taxis.AddRange(lst);
                    _context.SaveChanges();
                    _context.Database.CommitTransaction();
                    return Ok();
                }
                else
                {
                    return NotFound(wrongFormat);
                }
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxi),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/{id}
        /// <summary>
        /// Sửa thông tin
        /// </summary>
        /// <param name="Id">TaxiId</param>
        /// <param name="taxiUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult Update(int Id, TaxiUpdateDto taxiUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Taxis.FirstOrDefault(b=>b.Id == Id);
                if (b == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "Taxi"].Value);
                else
                {
                    var v = _context.Taxis.Where(x => x.PlateNumber == taxiUpdateDto.PlateNumber && x.Id != Id).FirstOrDefault();
                    if (v != null)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, taxiUpdateDto.PlateNumber].Value);
                    }
                }

                _mapper.Map(taxiUpdateDto, b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiUpdateDto),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/{id}
        /// <summary>
        /// Thay đổi trạng thái
        /// </summary>
        /// <param name="Id">TaxiId</param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("{Id}/changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult ChangeStatus(int Id, int statusId)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Taxis.Find(Id);
                if (b == null)
                    return NotFound();

                var status = _context.Statuses.Find(statusId);

                if (status.Name == Constant.ConstantStatus.Inactive)
                {
                    var booking = _context.TaxiBookings.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .Where(x => DateTime.Now <= x.ToDate).FirstOrDefault();
                    if (booking != null)
                        return NotFound(_localizer[Constant.ConstantMessage.ItisReserved,booking.FromDate.ToShortDateString(), booking.ToDate.ToShortDateString()]);
                }
                b.StatusId = statusId;

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("TaxiID: {0}, statusId: {1}", Id, statusId),
                    UserId = userId
                });
            }
        }

        //GET api/Bus
        /// <summary>
        /// Lấy danh sách loại xe
        /// </summary>
        /// <returns></returns>
        [HttpGet("cartype")]
        public ActionResult<IEnumerable<TaxiType>> GetAllType()
        {
            try
            {
                var types = _context.TaxiTypes.ToList();
                return Ok(types);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Bus
        /// <summary>
        /// Lấy danh sách kiểu dán
        /// </summary>
        /// <returns></returns>
        [HttpGet("arttype")]
        public ActionResult<IEnumerable<TaxiTypeOfArtwork>> GetAllArtType()
        {
            try
            {
                var types = _context.TaxiTypeOfArtworks.ToList();
                return Ok(types);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region AREA
        //GET api/taxi/area
        /// <summary>
        /// Tất cả khu vực
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpGet("area")]
        public ActionResult<IEnumerable<TaxiArea>> GetAllArea(int PageNumber, int PageSize)
        {
            try
            {
                var models = _context.TaxiAreas.Include(b => b.Locations);
                var response = PagedList<TaxiArea>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/taxi/area/location/1
        /// <summary>
        /// Danh sách khu vực theo vị trí và NCC
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="companyId">nếu khác null -> lấy theo công ty</param>
        /// <returns></returns>
        [HttpGet("area/location/{locationId}")]
        public ActionResult GetAllAreaByLocation(int locationId, int? companyId)
        {
            try
            {
                var models = _context.TaxiAreas.Where(x=>x.LocationId == locationId);
                if(companyId != null)
                {
                    models = models.Where(x => x.CompanyId == companyId);
                }

                return Ok(models.OrderBy(x=>x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/taxi/area/location
        /// <summary>
        /// Danh sách tỉnh thành có taxi
        /// </summary>
        /// <param name="companyId">nếu khác null -> lấy theo công ty</param>
        /// <returns></returns>
        [HttpGet("area/location")]
        public ActionResult GetAllLocations(int? companyId)
        {
            try
            {
                if (companyId != null)
                    return Ok(_context.TaxiAreas.Where(x=>x.CompanyId== companyId)
                        .Include(b => b.Locations)
                        .Select(x => x.Locations)
                        .OrderBy(x => x.OrderBy).ThenBy(x => x.Name)
                        .ToList());
                var models = _context.TaxiAreas.Include(b => b.Locations)
                    .Select(x=>x.Locations)
                    .Distinct()
                    .OrderBy(x => x.OrderBy).ThenBy(x => x.Name);
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/taxi/area/{id}
        /// <summary>
        /// Chi tiết của 1 khu vực taxi
        /// </summary>
        /// <param name="Id">TaxiAreaId</param>
        /// <returns></returns>
        [HttpGet("area/{Id}", Name = "GetDetailArea")]
        public ActionResult<BusRoute> GetDetailArea(int Id)
        {
            try
            {
                var model = _context.TaxiAreas.Where(b => b.Id == Id)
                    .Include(b => b.Locations)
                    .Include(b => b.Taxis)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/taxi/area
        /// <summary>
        /// Thêm mới 1 taxiArea
        /// </summary>
        /// <param name="taxiArea"></param>
        /// <returns></returns>
        [HttpPost("area")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddArea(TaxiArea taxiArea)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                var checkDuplicate = _context.TaxiAreas.Where(x => x.LocationId == taxiArea.LocationId && x.CompanyId == taxiArea.CompanyId).FirstOrDefault();
                if (checkDuplicate != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, checkDuplicate.Name].Value);
                }
                _context.Database.BeginTransaction();
                _context.TaxiAreas.Add(taxiArea);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetDetailArea), new { id = taxiArea.Id }, taxiArea);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiArea),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/area/id
        /// <summary>
        /// Sửa thông tin Area
        /// </summary>
        /// <param name="Id">TaxiAreaId</param>
        /// <param name="taxiAreaUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("area/{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult UpdateTaxiArea(int Id, TaxiAreaUpdateDto taxiAreaUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.TaxiAreas.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                var checkDuplicate = _context.TaxiAreas.Where(x => x.LocationId == taxiAreaUpdateDto.LocationId 
                                                                && x.CompanyId == taxiAreaUpdateDto.CompanyId
                                                                && x.Id != Id).FirstOrDefault();
                if (checkDuplicate != default)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, checkDuplicate.Name].Value);
                }

                //vi dieu
                _mapper.Map(taxiAreaUpdateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiAreaUpdateDto),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Del taxi area
        /// </summary>
        /// <param name="taxiAreaId"></param>
        /// <returns></returns>
        [HttpDelete("area/{taxiAreaId}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult DelArea(int taxiAreaId)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                var check = _context.TaxiAreas.Where(x => x.Id == taxiAreaId).FirstOrDefault();
                if (check == null)
                {
                    return NotFound();
                }

                var tx = _context.Taxis.Where(x => x.TaxiAreaId == taxiAreaId).FirstOrDefault();
                var price = _context.TaxiAreaPrices.Where(x => x.TaxiAreaId == taxiAreaId).FirstOrDefault();
                if (tx != null || price != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.TaxiAreaNotSuitable].Value);
                }
                _context.Database.BeginTransaction();
                _context.TaxiAreas.Remove(check);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiAreaId),
                    UserId = userId
                });
            }
        }
        #endregion

        #region PRICE
        //GET api/taxi/price
        /// <summary>
        /// Danh sách bảng giá taxi
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="cityLocation"></param>
        /// <param name="companyId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("price")]
        public ActionResult<IEnumerable<TaxiAreaPrice>> GetAllPrice(int PageNumber, int PageSize, int cityLocation, int companyId, int year)
        {
            try
            {
                var models = _context.TaxiAreaPrices.Where(x => x.TaxiAreas.LocationId == cityLocation && x.TaxiAreas.CompanyId == companyId)
                                                               .Where(x => x.FromDate.Year == year)
                                                               .Include(x=>x.Units)
                                                               .Include(x => x.TaxiAreas)
                                                               .Include(x=>x.TaxiTypes)
                                                               .Include(x => x.TaxiTypeOfArtworks).AsNoTracking();
                var response = PagedList<TaxiAreaPrice>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderBy(x=>x.TaxiTypes.Name).ThenBy(x=>x.TaxiTypeOfArtworks.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/taxi/price/{Id}
        /// <summary>
        /// Tìm bảng khác của 1 TaxiArea từ ngày - đến ngày
        /// </summary>
        /// <param name="TaxiAreaId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("price/{Id}")]
        public ActionResult<IEnumerable<TaxiAreaPrice>> GetPrice(int TaxiAreaId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = _context.TaxiAreaPrices.Where(b => b.TaxiAreaId == TaxiAreaId)
                    .Where(b => fromDate.Date <= b.FromDate.Date)
                    .Where(b => b.FromDate.Date <= toDate.Date)
                    .Include(b => b.TaxiTypes).OrderBy(b => b.FromDate)
                    .ToList();
                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/taxi/price
        /// <summary>
        /// Thêm bảng giá
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        [HttpPost("price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult AddPrice(TaxiAreaPrice price)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //check timeoverlap
                var priceModel = _context.TaxiAreaPrices.Where(x => x.TaxiAreaId == price.TaxiAreaId
                                                            && x.UnitId == price.UnitId
                                                            && x.TaxiTypeId == price.TaxiTypeId
                                                            && x.TaxiTypeOfArtworkId == price.TaxiTypeOfArtworkId
                                                            && x.FromDate <= price.ToDate && price.FromDate <= x.ToDate
                                                            );
                if (priceModel.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                _context.TaxiAreaPrices.Add(price);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok(price);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(price),
                    UserId = userId
                });
            }
        }

        //POST api/taxi/priceList
        /// <summary>
        /// Thêm bảng giá từ danh sách
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("priceList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult AddPriceList(List<TaxiAreaPrice> lst)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiAreaPrices.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        /// <summary>
        /// Copy gia taxi của 1 NCC, theo khu vực từ năm xxxx sang năm yyyy
        /// </summary>
        /// <param name="taxiCopyPriceDto">CompanyId: lấy cột companyId trong listbox(công ty) tránh lấy nhầm TaxiAreaId |
        /// LocationId:null nếu lấy tất cả location</param>
        /// <returns></returns>
        //POST api/taxi/copyPrice
        [HttpPost("copyPrice")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult copyPrice(TaxiCopyPriceDto taxiCopyPriceDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //price of fromyear
                var taxiPriceFromYear = _context.TaxiAreaPrices
                                .Where(x => x.FromDate.Year == taxiCopyPriceDto.FromYear)
                                .Where(x => x.TaxiAreas.CompanyId == taxiCopyPriceDto.CompanyId);
                if (taxiCopyPriceDto.LocationId != null)//location của tỉnh/tp
                    taxiPriceFromYear = taxiPriceFromYear.Where(x => x.TaxiAreas.LocationId == taxiCopyPriceDto.LocationId);

                if (taxiPriceFromYear.Count() == 0)
                    return NotFound();

                //price of toyear
                var taxiPriceToYear = _context.TaxiAreaPrices
                               .Where(x => x.FromDate.Year == taxiCopyPriceDto.ToYear)
                               .Where(x => x.TaxiAreas.CompanyId == taxiCopyPriceDto.CompanyId);
                if (taxiCopyPriceDto.LocationId != null)//location của tỉnh/tp
                    taxiPriceToYear = taxiPriceToYear.Where(x => x.TaxiAreas.LocationId == taxiCopyPriceDto.LocationId);

                //chi lay nhung taxiArea chua co gia trong ToYear
                var taxiPrice = taxiPriceFromYear.Where(x => !taxiPriceToYear.Select(x => x.TaxiAreaId).Contains(x.TaxiAreaId));

                //if (taxiPrice.Count() == 0)
                //    return NotFound(_localizer[Constant.ConstantMessage.TimeOverlap].Value);

                foreach (var price in taxiPrice.ToList())
                {
                    //tao price moi
                    var taxiAreaPrice = new TaxiAreaPrice
                    {
                        Id = 0,
                        Price = price.Price,
                        PromotePercent = price.PromotePercent,
                        MinCarBooking = price.MinCarBooking,
                        TaxiTypeOfArtworkId = price.TaxiTypeOfArtworkId,
                        TaxiTypeId = price.TaxiTypeId,
                        FromDate = price.FromDate.AddYears(taxiCopyPriceDto.ToYear - taxiCopyPriceDto.FromYear),
                        ToDate = price.ToDate.AddYears(taxiCopyPriceDto.ToYear - taxiCopyPriceDto.FromYear),
                        UnitId = price.UnitId,
                        TaxiAreaId = price.TaxiAreaId,
                        CreatedUserId = userId,
                        UserId = userId
                    };

                    _context.TaxiAreaPrices.Add(taxiAreaPrice);
                    _context.SaveChanges();
                    //copy Discount tu bang gia cu
                    var discount = from dis in _context.TaxiDiscounts.Where(x => x.TaxiAreaPriceId == price.Id)
                                     select new TaxiDiscount
                                     {
                                         Id = 0,
                                         Month = dis.Month,
                                         Percent = dis.Percent,
                                         TaxiAreaPriceId = taxiAreaPrice.Id
                                     };
                    _context.TaxiDiscounts.AddRange(discount.ToList());
                    _context.SaveChanges();
                };
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiCopyPriceDto),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/price/id
        /// <summary>
        /// Thay đổi thông tin bảng giá
        /// </summary>
        /// <param name="Id">TaxiAreaPriceId</param>
        /// <param name="taxiAreaPriceUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult Updateprice(int Id, TaxiAreaPriceUpdateDto taxiAreaPriceUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.TaxiAreaPrices.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //check timeoverlap
                var priceModel = _context.TaxiAreaPrices.Where(x => x.Id != Id
                                                            && x.TaxiAreaId == taxiAreaPriceUpdateDto.TaxiAreaId
                                                            && x.UnitId == taxiAreaPriceUpdateDto.UnitId
                                                            && x.TaxiTypeId == taxiAreaPriceUpdateDto.TaxiTypeId
                                                            && x.TaxiTypeOfArtworkId == taxiAreaPriceUpdateDto.TaxiTypeOfArtworkId
                                                            && x.FromDate <= taxiAreaPriceUpdateDto.ToDate && taxiAreaPriceUpdateDto.FromDate <= x.ToDate
                                                            );
                if (priceModel.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);

                taxiAreaPriceUpdateDto.FromDate = taxiAreaPriceUpdateDto.FromDate.Date;
                taxiAreaPriceUpdateDto.ToDate = taxiAreaPriceUpdateDto.ToDate.Date;
                //vi dieu
                _mapper.Map(taxiAreaPriceUpdateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(taxiAreaPriceUpdateDto),
                    UserId = userId
                });
            }
        }

        //DEL api/taxi/price/{Id}
        /// <summary>
        /// Xóa bảng giá
        /// </summary>
        /// <param name="Id">TaxiAreaPriceId</param>
        /// <returns></returns>
        [HttpDelete("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult DeletePrice(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var taxiAreaPrice = _context.TaxiAreaPrices.Find(Id);
                if (taxiAreaPrice == null)
                    return NotFound();

                var booking = _context.TaxiBookings
                                                  .Where(x => x.Statuses.Name == Constant.ConstantStatus.Active || x.Statuses.Name == Constant.ConstantStatus.Unconfirmed)
                                                  .Where(x => x.FromDate <= taxiAreaPrice.ToDate && taxiAreaPrice.FromDate <= x.ToDate)
                                                  .Where(x => x.TaxiTypeOfArtworkId == taxiAreaPrice.TaxiTypeOfArtworkId
                                                            && x.Taxis.TaxiTypeId == taxiAreaPrice.TaxiTypeId
                                                            && x.Taxis.TaxiAreaId == taxiAreaPrice.TaxiAreaId
                                                            )
                                                  .FirstOrDefault();
                if (booking != null)
                    return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()].ToString());
                else
                    _context.TaxiAreaPrices.Remove(taxiAreaPrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion

        #region PICTURES
        //PUT api/taxi/Pictures
        /// <summary>
        /// Thêm hình taxi trong campaign (mobile)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost("Pictures")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult AddPictures(List<TaxiPicture> list)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiPictures.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(list),
                    UserId = userId
                });
            }
        }

        //GET api/taxi/Pictures/{Id}
        /// <summary>
        /// Danh sách hình taxi trong campaign (mobile-web)
        /// </summary>
        /// <param name="Id">TaxiId</param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("Pictures/{Id}")]
        public ActionResult<IEnumerable<TaxiPicture>> GetPicture(int Id, int campaignId)
        {
            try
            {
                var query = _context.TaxiPictures.Where(b => b.TaxiId == Id && b.CampaignId == campaignId).ToList();

                var list = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Taxi)
                                                 .AsNoTracking()
                                                 .AsEnumerable()
                                                 .Select(item => new
                                                 {
                                                     item.PictureTypes.Id,
                                                     item.PictureTypes.Name,
                                                     list = query.Where(x => x.PictureTypeId == item.PictureTypes.Id).OrderByDescending(b => b.LastUpdated)
                                                 });
                return Ok(list);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //DEL api/taxi/Pictures/{taxiPictureId}
        /// <summary>
        /// Xóa hình trong campaign (DB-store)
        /// </summary>
        /// <param name="taxiPictureId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpDelete("Pictures/{taxiPictureId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult DelPicture(int taxiPictureId, int campaignId)
        {
            try
            {
                var pic = _context.TaxiPictures.Where(b => b.Id == taxiPictureId && b.CampaignId == campaignId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.TaxiPictures.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren clond
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //get display
        /// <summary>
        /// Hình đại diện của taxi
        /// </summary>
        /// <param name="cartypeId"></param>
        /// <param name="arttypeId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("display")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult GetPictureDisplay(int cartypeId, int arttypeId, int? companyId)
        {
            try
            {
                var pic = _context.TaxiDisplays.Where(b => b.CarTypeId == cartypeId && b.ArtTypeId == arttypeId);
                if (companyId != null)
                    pic = pic.Where(x => x.CompanyId == companyId);
                else
                    pic = pic.Where(x => x.CompanyId == null);

                return Ok(pic.OrderBy(x => x.Order).ToList());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Thêm hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        [HttpPost("display")]
        public ActionResult AddDislay(List<TaxiDisplay> list)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                if (list.Count > 0)
                {
                    var taxiDis = list.First();
                    var checkOrder = _context.BusDisplays.Where(x => x.ArtTypeId == taxiDis.ArtTypeId && x.CarTypeId == taxiDis.CarTypeId
                                                                && list.Select(c => c.Order).Contains(x.Order)).ToList();
                    if (taxiDis.CompanyId != null)
                        checkOrder.Where(x => x.CompanyId == taxiDis.CompanyId);

                    if (checkOrder.Count > 0)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, "Order"].Value);
                    }
                    _context.TaxiDisplays.AddRange(list);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(list),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Sửa hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        [HttpPut("display")]
        public ActionResult UpdateDislay(List<TaxiDisplayUpdateDto> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                list.ForEach(item =>
                {
                    var b = _context.TaxiDisplays.Find(item.Id);
                    _mapper.Map(item, b);
                    _context.SaveChanges();
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Xóa hình đại diện
        /// </summary>
        /// <param name="taxiDisplayId"></param>
        /// <returns></returns>
        [HttpDelete("display/{taxiDisplayId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelPictureDisplay(int taxiDisplayId)
        {
            try
            {
                var pic = _context.TaxiDisplays.Where(b => b.Id == taxiDisplayId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.TaxiDisplays.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region DISCOUNT
        //https://stackoverflow.com/questions/13513932/algorithm-to-detect-overlapping-periods/13513973
        //bool overlap = tStartA <= tEndB && tStartB <= tEndA;
        //GET api/taxi/discount
        /// <summary>
        /// Discount 1 bảng giá taxi
        /// </summary>
        /// <param name="taxiAreaPriceId"></param>
        /// <returns></returns>
        [HttpGet("discount")]
        public ActionResult<IEnumerable<TaxiDiscount>> GetDiscount(int taxiAreaPriceId)
        {
            try
            {
                var query = _context.TaxiDiscounts.Where(x=>x.TaxiAreaPriceId == taxiAreaPriceId).OrderBy(x=>x.Month).ToList();
                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/taxi/discount/
        /// <summary>
        /// Add discount
        /// </summary>
        /// <param name="discount"></param>
        /// <returns></returns>
        [HttpPost("discount")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddDiscount(TaxiDiscount discount)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiDiscounts.Add(discount);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/taxi/discountList/
        /// <summary>
        /// Add discount form list 
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("discountList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult AddDiscountList(IEnumerable<TaxiDiscount> lst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiDiscounts.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(lst),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/discount/id
        /// <summary>
        /// Update discount
        /// </summary>
        /// <param name="Id">TaxiDiscountId</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("discount/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult UpdateDiscount(int Id, TaxiDiscountUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.TaxiDiscounts.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/taxi/discount/updatelist
        /// <summary>
        /// Update discount form list
        /// </summary>
        /// <param name="updateLst"></param>
        /// <returns></returns>
        [HttpPut("discount/updatelist")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult UpdateDiscountList(IEnumerable<TaxiDiscount> updateLst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiDiscounts.UpdateRange(updateLst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updateLst),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/discount/{Id}
        /// <summary>
        /// Del discount
        /// </summary>
        /// <param name="Id">TaxiDiscountId</param>
        /// <returns></returns>
        [HttpDelete("discount/{Id}")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult DelDiscount(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.TaxiDiscounts.Remove(_context.TaxiDiscounts.Find(Id));
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion

        #region DOWNTIME
        /// <summary>
        /// Danh sách downtime của taxi trong năm
        /// </summary>
        /// <param name="Id">TaxiId</param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("downtime/{Id}")]
        public ActionResult<IEnumerable<TaxiDownTime>> GetDowntime(int Id, int year)
        {
            try
            {
                var query = _context.TaxiDownTimes.Where(b => b.TaxiId == Id)
                    .Where(b => b.FromDate.Year == year)
                    .Include(b => b.Taxis).OrderByDescending(b => b.FromDate)
                    .ToList();
                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/taxi/downtime/
        /// <summary>
        /// Add downtime from list
        /// </summary>
        /// <param name="downTime"></param>
        /// <returns></returns>
        [HttpPost("downtime")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult AddDowntime(List<TaxiDownTime> downTime)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Taxi> lst = new List<Taxi>();
                for (int i = 0; i < downTime.Count; i++)
                {
                    //check timeoverlap
                    var check = _context.TaxiDownTimes.Where(x => x.TaxiId == downTime[i].TaxiId
                                                                && x.FromDate <= downTime[i].ToDate && downTime[i].FromDate <= x.ToDate
                                                            ).Include(x=>x.Taxis).FirstOrDefault();
                    if (check !=null)
                    {
                        lst.Add(check.Taxis);
                        //throw new NotImplementedException(string.Format("{0} {1}-{2}:{3}", 
                        //            check.Taxis.PlateNumber, check.FromDate, check.ToDate, _localizer[Constant.ConstantMessage.TimeOverlap]));
                    }
                }
                if (lst.Count > 0)
                {
                    return NotFound(lst);
                }
                _context.TaxiDownTimes.AddRange(downTime);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(downTime),
                    UserId = userId
                });
            }
        }

        //PUT api/taxi/downtime/id
        /// <summary>
        /// Update downtime
        /// </summary>
        /// <param name="Id">TaxiDownTimeId</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult UpdateDowntime(int Id, TaxiDownTimeUpdateDto updateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.TaxiDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updateDto),
                    UserId = userId
                });
            }
        }

        /// <summary>
        /// Del downtime
        /// </summary>
        /// <param name="Id">TaxiDownTimeId</param>
        /// <returns></returns>
        [HttpDelete("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireTaxiAdminRole)]
        public ActionResult DelDowntime(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.TaxiDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                _context.Remove(b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion
    }
}
