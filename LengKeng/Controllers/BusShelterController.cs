﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BusShelterController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;

        public BusShelterController(LengKengDbContext context, IMapper mapper, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _blobService = blobService;
        }

        #region GET - CREATE - UPDATE
        //GET api/BusShelter
        /// <summary>
        /// Get All BusShelter
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="locationId"></param>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<IEnumerable<BusShelter>> GetAllBusShelter(int PageNumber, int PageSize,
                                                                      int locationId, int companyId, string name)
        {
            try
            {
                var models = _context.BusShelters.Where(b => b.CompanyId == companyId).Where(x => x.LocationId == locationId);
                if (!string.IsNullOrEmpty(name))
                    models = models.Where(x => x.Name.Contains(name));
                            
                var response = PagedList<BusShelter>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/BusShelter/{id}
        /// <summary>
        /// Bus Shelter Info 
        /// </summary>
        /// <param name="Id">Shelter Id</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetBusShelter")]
        public ActionResult<BusShelter> GetBusShelter(int Id)
        {
            try
            {
                var model = _context.BusShelters.Where(b => b.Id == Id)
                    .Include(b => b.Locations)
                    .Include(b => b.UserShelters)
                    .Include(b => b.BusShelterPictures)
                    //.Include(b=>b.BusShelterPrices)
                    .Include(b => b.BusShelterDownTimes)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/BusShelter
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="busShelter"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult Create(BusShelter busShelter)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelters.Add(busShelter);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetBusShelter), new { id = busShelter.Id }, busShelter);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/BusShelter/{id}
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="Id">Shelter Id</param>
        /// <param name="busShelterUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult Update(int Id, BusShelterUpdateDto busShelterUpdateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var shelter = _context.BusShelters.Find(Id);
                if (shelter == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "Shelter"].Value);
                else
                {
                    var v = _context.BusShelters.Where(x => x.Name == busShelterUpdateDto.Name && x.Id != Id).FirstOrDefault();
                    if (v != null)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, busShelterUpdateDto.Name].Value);
                    }
                }

                _mapper.Map(busShelterUpdateDto, shelter);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/BusShelter/{id}
        /// <summary>
        /// Change Status
        /// </summary>
        /// <param name="Id">Shelter Id</param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("{Id}/changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult ChangeStatus(int Id, int statusId)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusShelters.Find(Id);
                if (b == null)
                    return NotFound();

                var status = _context.Statuses.Find(statusId);

                if (status.Name == Constant.ConstantStatus.Inactive)
                {
                    var booking = _context.BusShelterBookings.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .Where(x => DateTime.Now <= x.ToDate).FirstOrDefault();
                    if (booking != null)
                        return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()]);
                }
                b.StatusId = statusId;

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        #endregion

        #region LOCATION
        //GET api/BusShelter/locations
        /// <summary>
        /// LOCATION | Danh sách tỉnh thành có Shelter
        /// </summary>
        /// <returns></returns>
        [HttpGet("locations")]
        public ActionResult GetCityForBooking()
        {
            try
            {
                var shelter = _context.BusShelters.Include(x => x.Locations).Select(x => x.Locations).Distinct();

                var models = from loc in _context.Locations.Where(x => x.Level == 1)
                             join bb in shelter on loc.LocationId equals (bb.ParentId ?? bb.LocationId)
                             select loc;

                return Ok(models.Distinct());
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/BusShelter/locations/district
        /// <summary>
        /// LOCATION | Danh sách quận huyện
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [HttpGet("locations/district")]
        public ActionResult GetDistrictForBooking(int locationId)
        {
            try
            {
                var billboard = _context.BusShelters.Include(x => x.Locations).Where(x => x.Locations.ParentId == locationId).Select(x => x.Locations).Distinct();

                var models = from loc in _context.Locations.Where(x => x.Level == 2)
                             join bb in billboard on loc.LocationId equals bb.LocationId
                             select loc;

                return Ok(models.Distinct().OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region PICTURES
        //PUT api/BusShelter/Pictures
        /// <summary>
        /// PICTURES - MOBILE | Thêm ảnh 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost("Pictures")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult AddPictures(List<BusShelterPicture> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterPictures.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/BusShelter/Pictures/{Id}
        /// <summary>
        /// PICTURES - MOBILE | Lấy danh sách hình 1 shelter trong campaign
        /// </summary>
        /// <param name="Id">Shelter id</param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("Pictures/{Id}")]
        public ActionResult GetPicture(int Id, int campaignId)
        {
            try
            {
                var query = _context.BusShelterPictures.Where(b => b.BusShelterId == Id && b.CampaignId == campaignId).ToList();

                var list = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.BusShelter)
                                                 .AsNoTracking()
                                                 .AsEnumerable()
                                                 .Select(item => new
                                                 {
                                                     item.PictureTypes.Id,
                                                     item.PictureTypes.Name,
                                                     list = query.Where(x => x.PictureTypeId == item.PictureTypes.Id).OrderByDescending(b => b.LastUpdated)
                                                 });
                return Ok(list);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// PICTURES - MOBILE | Xóa ảnh trong campaign
        /// </summary>
        /// <param name="shelterPictureId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpDelete("Pictures/{shelterPictureId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult DelPicture(int shelterPictureId, int campaignId)
        {
            try
            {
                var pic = _context.BusShelterPictures.Where(b => b.Id == shelterPictureId && b.CampaignId == campaignId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BusShelterPictures.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren clond
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// PICTURES - WEB | Thêm hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        [HttpPost("display")]
        public ActionResult AddDislay(List<BusShelterDisplay> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterDisplays.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// PICTURES - WEB | Sửa hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        [HttpPut("display")]
        public ActionResult UpdateDislay(List<BusShelterDisplayUpdateDto> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                list.ForEach(item =>
                {
                    var b = _context.BusShelterDisplays.Find(item.Id);
                    _mapper.Map(item, b);
                    _context.SaveChanges();
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// PICTURES - WEB | Xóa hình đại diện
        /// </summary>
        /// <param name="shelterDisplayId"></param>
        /// <returns></returns>
        [HttpDelete("display/{shelterDisplayId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelPictureDisplay(int shelterDisplayId)
        {
            try
            {
                var pic = _context.BusShelterDisplays.Where(b => b.Id == shelterDisplayId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BusShelterDisplays.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region PRICES
        //GET api/BusShelter/price
        /// <summary>
        /// PRICES | Get All
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [HttpGet("price")]
        public ActionResult<IEnumerable<BusShelterPrice>> GetAllPrice(int PageNumber, int PageSize)
        {
            try
            {
                var models = _context.BusShelterPrices.AsTracking();
                var response = PagedList<BusShelterPrice>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/BusShelter/price/{Id}
        /// <summary>
        /// PRICES | Giá 1 Shelter từ ngày đến ngày
        /// </summary>
        /// <param name="Id">BusShelterId</param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("price/{Id}")]
        public ActionResult<IEnumerable<BusShelterPrice>> GetPrice(int Id, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = _context.BusShelterPrices.Where(b => b.BusShelterId == Id)
                    .Where(b => fromDate.Date <= b.ToDate.Date)
                    .Where(b => b.FromDate.Date <= toDate.Date)
                    .Include(b => b.BusShelters).OrderBy(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/BusShelter/Price
        /// <summary>
        /// PRICES | Thêm giá
        /// </summary>
        /// <param name="busShelterPrice"></param>
        /// <returns></returns>
        [HttpPost("Price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult AddShelterPrice(BusShelterPrice busShelterPrice)
        {
            try
            {
                _context.Database.BeginTransaction();
                //check timeoverlap
                var price = _context.BusShelterPrices.Where(x => x.BusShelterId == busShelterPrice.BusShelterId
                                                            && x.FromDate <= busShelterPrice.ToDate && busShelterPrice.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                _context.BusShelterPrices.Add(busShelterPrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/BusShelter/Price/AddList
        /// <summary>
        /// PRICES | Thêm danh sách giá
        /// </summary>
        /// <param name="busShelterPrices"></param>
        /// <returns></returns>
        [HttpPost("Price/AddList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult AddListShelterPrice(IEnumerable<BusShelterPrice> busShelterPrices)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterPrices.AddRange(busShelterPrices);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/BusShelter/Price/{id}
        /// <summary>
        /// PRICES | Sửa giá
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="busShelterPriceUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("Price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult UpdatePrices(int Id, BusShelterPriceUpdateDto busShelterPriceUpdateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var shelter = _context.BusShelterPrices.FirstOrDefault(p => p.Id == Id);
                if (shelter == null)
                    return NotFound();

                //check timeoverlap
                var price = _context.BusShelterPrices.Where(x => x.Id != Id
                                                            && x.BusShelterId == busShelterPriceUpdateDto.BusShelterId
                                                            && x.FromDate <= busShelterPriceUpdateDto.ToDate && busShelterPriceUpdateDto.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                //vi dieu
                _mapper.Map(busShelterPriceUpdateDto, shelter);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/BusShelter/copyPrice
        /// <summary>
        /// PRICES | Copy giá của 1 company (chưa hoàn chỉnh)
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="oldYear"></param>
        /// <param name="newYear"></param>
        /// <returns></returns>
        [HttpPost("copyPrice")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult copyPrice(int? companyId, int oldYear, int newYear)
        {
            try
            {
                _context.Database.BeginTransaction();
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int userId = MyUltil.GetCurrentUserId(identity);

                var shelter = from tx in _context.BusShelterPrices.Where(x => x.FromDate.Year == oldYear) select tx;
                if (companyId != null)
                    shelter = shelter.Where(x => x.BusShelters.CompanyId == companyId);

                var lst = from price in shelter
                          select new BusShelterPrice
                          {
                              Id = 0,
                              Price = price.Price,
                              MinMonthBooking = price.MinMonthBooking,
                              FromDate = price.FromDate.AddYears(newYear - oldYear),
                              ToDate = price.ToDate.AddYears(newYear - oldYear),
                              UnitId = price.UnitId,
                              BusShelterId = price.BusShelterId,
                              CreatedUserId = userId,
                              UserId = userId
                          };
                if (lst == null)
                    return NotFound();
                _context.BusShelterPrices.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/BusShelter/price/{Id}
        /// <summary>
        /// PRICES | Xóa bảng giá
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequirePartnerGroup)]
        public ActionResult DeletePrice(int Id)
        {
            try
            {
                _context.Database.BeginTransaction();
                var busShelterPrice = _context.BusShelterPrices.Find(Id);
                if (busShelterPrice == null)
                    return NotFound();

                var booking = _context.BusShelterBookings
                                                  .Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                  .Where(x => x.FromDate <= busShelterPrice.ToDate && busShelterPrice.FromDate <= x.ToDate)
                                                  .Where(x => x.BusShelterId == busShelterPrice.BusShelterId)
                                                  .FirstOrDefault();
                if (booking != null)
                    return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()].ToString());
                else
                    _context.BusShelterPrices.Remove(busShelterPrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region DISCOUNT
        //GET api/BusShelter/discount
        /// <summary>
        /// DISCOUNT | bảng discount cho shelter
        /// </summary>
        /// <param name="BusShelterPriceId"></param>
        /// <returns></returns>
        [HttpGet("discount")]
        public ActionResult<IEnumerable<BusShelterDiscount>> GetDiscount(int BusShelterPriceId)
        {
            try
            {
                //var query = _context.BusShelterDiscounts.Where(x => x.LocationId == locationId && x.CompanyId == x.CompanyId).OrderBy(x => x.Month).ToList();
                var query = _context.BusShelterDiscounts.Where(x => x.BusShelterPriceId == BusShelterPriceId).OrderBy(x => x.Month).ToList();

                var list = query.ToList();
                if (list != null)
                    return Ok(list);
                return NotFound();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/BusShelter/discount/
        /// <summary>
        /// DISCOUNT | Thêm discount
        /// </summary>
        /// <param name="discount"></param>
        /// <returns></returns>
        [HttpPost("discount")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddDiscount(BusShelterDiscount discount)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterDiscounts.Add(discount);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //POST api/BusShelter/discountList/
        /// <summary>
        /// DISCOUNT | thêm danh sách discount
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("discountList")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult AddDiscountList(IEnumerable<BusShelterDiscount> lst)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterDiscounts.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //PUT api/bus/discount/id
        /// <summary>
        /// DISCOUNT | sửa discount
        /// </summary>
        /// <param name="updatLst"></param>
        /// <returns></returns>
        [HttpPut("discount/updatelist")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult UpdateDiscountList(IEnumerable<BusShelterDiscount> updatLst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BusShelterDiscounts.UpdateRange(updatLst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updatLst),
                    UserId = userId
                });
            }
        }
        #endregion

        #region DOWNTIME
        //GET api/busshelter/downtime/{Id}
        /// <summary>
        /// DOWNTIME | Danh sách downtime trong năm
        /// </summary>
        /// <param name="Id">Shelter Id</param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("downtime/{Id}")]
        public ActionResult<IEnumerable<BusShelterDownTime>> GetDowntime(int Id, int year)
        {
            try
            {
                var query = _context.BusShelterDownTimes.Where(b => b.BusShelterId == Id)
                    .Where(b => b.FromDate.Year == year)
                    .Include(b => b.BusShelters).OrderByDescending(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/busshelter/downtime/
        /// <summary>
        /// DOWNTIME | thêm downtime 
        /// </summary>
        /// <param name="downTime"></param>
        /// <returns></returns>
        [HttpPost("downtime")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult AddDowntime(List<BusShelterDownTime> downTime)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<BusShelter> lst = new List<BusShelter>();
                for (int i = 0; i < downTime.Count; i++)
                {
                    //check timeoverlap
                    var check = _context.BusShelterDownTimes.Where(x => x.BusShelterId == downTime[i].BusShelterId
                                                                && x.FromDate <= downTime[i].ToDate && downTime[i].FromDate <= x.ToDate
                                                            ).Include(x => x.BusShelters).FirstOrDefault();
                    if (check != null)
                    {
                        lst.Add(check.BusShelters);
                        //throw new NotImplementedException(string.Format("{0} {1}-{2}:{3}",
                        //            check.BusShelters.Name, check.FromDate, check.ToDate, _localizer[Constant.ConstantMessage.TimeOverlap]));
                    }

                }
                if(lst.Count > 0)
                {
                    return NotFound(lst);
                }
                _context.BusShelterDownTimes.AddRange(downTime);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(downTime),
                    UserId = userId
                });
            }
        }

        //PUT api/BusShelter/downtime/id
        /// <summary>
        /// DOWNTIME | sửa downtime
        /// </summary>
        /// <param name="Id">BusShelterDownTimes Id</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        public ActionResult UpdateDowntime(int Id, BusShelterDownTimeUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusShelterDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// DOWNTIME | Xóa downtime
        /// </summary>
        /// <param name="Id">BusShelterDownTimes Id</param>
        /// <returns></returns>
        [HttpDelete("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBusAdminRole)]
        public ActionResult DelDowntime(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BusShelterDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                _context.Remove(b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name +"."+ System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion

        //GET api/BusShelter/Owner/{Id}
        /// <summary>
        /// Danh sách users quản lý shelter
        /// </summary>
        /// <param name="Id">BusShelter Id</param>
        /// <returns></returns>
        [HttpGet("Owner/{Id}", Name = "GetUserBelongToBusShelter")]
        public ActionResult<IEnumerable<User>> GetUserBelongToBusShelter(int Id)
        {
            try
            {
                var model = _context.Users.Include(b => b.UserShelters).ThenInclude(b => b.BusShelter)
                            .Where(t => t.UserShelters.Any(x => x.BusShelter.Id == Id));
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

    }
}
