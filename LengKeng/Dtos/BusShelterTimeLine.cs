﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusShelterTimeLine
    {
        public IEnumerable<BusShelterBooking> bookings { get; set; }
        public IEnumerable<BusShelterDownTime> downTimes { get; set; }
    }
}
