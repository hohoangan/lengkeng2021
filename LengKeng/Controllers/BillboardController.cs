﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using AutoMapper;
using LengKeng.Data;
using LengKeng.Dtos;
using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LengKeng.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BillboardController : ControllerBase
    {
        private readonly LengKengDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<Message> _localizer;
        private readonly IBlobService _blobService;

        public BillboardController(LengKengDbContext context, IMapper mapper, IStringLocalizer<Message> localizer, IBlobService blobService)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _blobService = blobService;
        }

        #region GET - CREATE - UPDATE

        //GET api/Billboard
        /// <summary>
        /// Lấy danh sách BB
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="locationId">có thể là tỉnh/tp hoặc quận/huyện</param>
        /// <param name="companyId"></param>
        /// <param name="name">tìm kiếm nhanh theo tên</param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<IEnumerable<Billboard>> GetAllBillboard(int PageNumber, int PageSize,
                                                                     int locationId, int companyId, string name)
        {
            try
            {
                //lay BB theo location: tinh/tp hoac quan/huyen
                var models = _context.Billboards.Where(b => b.CompanyId == companyId)
                                .Where(x => x.LocationId == locationId || x.Locations.ParentId == locationId);
                if (!string.IsNullOrEmpty(name))
                    models = models.Where(x => x.Name.Contains(name));

                var response = PagedList<Billboard>.ToPagedList(models, PageNumber, PageSize);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/{id}
        /// <summary>
        /// Thông tin chi tiết của 1 BB
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "GetBillboard")]
        public ActionResult<Billboard> GetBillboard(int Id)
        {
            try
            {
                var model = _context.Billboards.Where(b => b.Id == Id)
                    .Include(b => b.Companies)
                    .Include(b => b.Locations)
                    .Include(b => b.UserBillboards)
                    .Include(b => b.BillBoardPictures)
                    .Include(b => b.BillboardDownTimes)
                    .Single();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/billboard
        /// <summary>
        /// Tạo mới 1 BB
        /// </summary>
        /// <param name="billboard"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult Create(Billboard billboard)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var bb = _context.Billboards.Where(x => x.Name.Equals(billboard.Name)).FirstOrDefault();
                if (bb != null)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, billboard.Name].Value);
                }
                _context.Billboards.Add(billboard);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return CreatedAtRoute(nameof(GetBillboard), new { id = billboard.Id }, billboard);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(billboard),
                    UserId = userId
                });
            }
        }

        //PUT api/billboard/{id}
        /// <summary>
        /// Thay đổi thông tin của BB
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="billboardUpdateDto"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult Update(int Id, BillboardUpdateDto billboardUpdateDto)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Billboards.Find(Id);
                if (b == null)
                    return NotFound(_localizer[Constant.ConstantMessage.NotFoundX, "Billboard"].Value);
                else
                {
                    var v = _context.Billboards.Where(x => x.Name == billboardUpdateDto.Name && x.Id != Id).FirstOrDefault();
                    if (v != null)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, billboardUpdateDto.Name].Value);
                    }
                }

                _mapper.Map(billboardUpdateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(billboardUpdateDto),
                    UserId = userId
                });
            }
        }

        //PUT api/billboard/{id}
        /// <summary>
        /// Thay đổi trạng thái của BB
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        [HttpPut("{Id}/changeStatus")]
        [Authorize(Roles = Constant.ConstantRole.SuperUser)]
        public ActionResult ChangeStatus(int Id, int statusId)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.Billboards.Find(Id);
                if (b == null)
                    return NotFound();

                var status = _context.Statuses.Find(statusId);

                if (status.Name == Constant.ConstantStatus.Inactive)
                {
                    var booking = _context.BillboardBookings.Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                            .Where(x => DateTime.Now <= x.ToDate).FirstOrDefault();
                    if (booking != null)
                        return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()]);
                }
                b.StatusId = statusId;

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/type
        /// <summary>
        /// Lấy danh sách các loại BB
        /// </summary>
        /// <returns></returns>
        [HttpGet("type")]
        public ActionResult GetBillboardType()
        {
            try
            {
                var model = _context.BillboardTypes.OrderBy(x => x.Name).ToList();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Danh sách keywords để của BB
        /// </summary>
        /// <returns></returns>
        [HttpGet("SearchKeywordText")]
        public ActionResult GetSearchKeywordText()
        {
            try
            {
                var model = _context.BillboardKeywords.OrderBy(x => x.Name).ToList();
                return Ok(model);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region LOCATION
        //GET api/Billboard/locations
        /// <summary>
        /// Lấy danh sách tỉnh/tp của BB
        /// </summary>
        /// <param name="companyId">nếu khác null thì chỉ lấy tỉnh/tp có BB của NCC đó thôi</param>
        /// <returns></returns>
        [HttpGet("locations")]
        public ActionResult GetCityForBooking(int? companyId)
        {
            try
            {
                var billboard = _context.Billboards.Include(x => x.Locations).Where(x => 1 == 1);
                if (companyId != null)
                {
                    billboard = billboard.Where(x => x.CompanyId == companyId);
                }
                var locations = billboard.Select(x => x.Locations).Distinct();

                if (companyId != null)
                {
                    var models = from loc in _context.Locations.Where(x => x.Level == 1)
                                 join bb in locations on loc.LocationId equals (bb.ParentId ?? bb.LocationId)
                                 select loc;
                    return Ok(models.Distinct().OrderBy(x => x.OrderBy).ThenBy(x => x.Name));
                }
                else
                {
                    var models = from loc in _context.Locations.Where(x => x.Level == 1)
                                 select loc;
                    return Ok(models.Distinct().OrderBy(x => x.OrderBy).ThenBy(x => x.Name));
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/Billboard/locations/district
        /// <summary>
        /// Lấy danh sách quận/huyện BB 
        /// </summary>
        /// <param name="locationId">thuộc tỉnh/tp nào</param>
        /// <param name="companyId">nếu khác null thì chỉ lấy quận/huyện có BB của NCC đó thôi</param>
        /// <returns></returns>
        [HttpGet("locations/district")]
        public ActionResult GetDistrictForBooking(int locationId, int? companyId)
        {
            try
            {
                var billboard = _context.Billboards.Include(x => x.Locations)
                                .Where(x => x.Locations.ParentId == locationId);
                if (companyId != null)
                {
                    billboard = billboard.Where(x => x.CompanyId == companyId);
                }
                var locations = billboard.Select(x => x.Locations).Distinct();

                if (companyId != null)
                {
                    var models = from loc in _context.Locations.Where(x => x.Level == 2 && x.ParentId == locationId)
                                 join bb in locations on loc.LocationId equals bb.LocationId
                                 select loc;
                    return Ok(models.Distinct().ToList().OrderBy(x => x.Name, new StrCmpLogicalComparer()));
                }
                else
                {
                    var models = from loc in _context.Locations.Where(x => x.Level == 2 && x.ParentId == locationId)
                                 select loc;
                    return Ok(models.Distinct().ToList().OrderBy(x => x.Name, new StrCmpLogicalComparer()));
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region PICTURES
        //PUT api/billboard/pictures
        /// <summary>
        /// Thêm hình ảnh cho BB (mobile)
        /// </summary>
        /// <param name="list">danh sách hình</param>
        /// <returns></returns>
        [HttpPost("pictures")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireMobileRole)]
        public ActionResult AddPictures(List<BillboardPicture> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BillboardPictures.AddRange(list);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// xóa hình ảnh BB chụp trong campaign (mobile)
        /// </summary>
        /// <param name="billboardPictureId"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpDelete("Pictures/{billboardPictureId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult DelPicture(int billboardPictureId, int campaignId)
        {
            try
            {
                var pic = _context.BillboardPictures.Where(b => b.Id == billboardPictureId && b.CampaignId == campaignId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BillboardPictures.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren clond
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //get display
        /// <summary>
        /// Lấy danh sách hình đại diện của BB
        /// </summary>
        /// <param name="billboardId"></param>
        /// <returns></returns>
        [HttpGet("display/{billboardId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetPictureDisplay(int billboardId)
        {
            try
            {
                var pic = _context.BillboardDisplays.Where(b => b.BillboardId == billboardId).OrderBy(x => x.Order).ToList();
                return Ok(pic);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Thêm hình ảnh đại diện của BB
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        [HttpPost("display")]
        public ActionResult AddDislay(List<BillboardDisplay> list)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                if (list.Count > 0)
                {
                    int BBId = list.First().BillboardId;
                    var checkOrder = _context.BillboardDisplays.Where(x => x.BillboardId == BBId
                                                                && list.Select(c => c.Order).Contains(x.Order)).ToList();

                    if (checkOrder.Count > 0)
                    {
                        return NotFound(_localizer[Constant.ConstantMessage.DuplicateX, "Order"].Value);
                    }
                    _context.BillboardDisplays.AddRange(list);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Ok(list);
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(list),
                    UserId = userId
                });
            }
        }
        
        /// <summary>
        /// Thay đổi hình đại diện
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPut("display")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult UpdateDislay(List<BillboardDisplayUpdateDto> list)
        {
            try
            {
                _context.Database.BeginTransaction();
                list.ForEach(item =>
                {
                    var b = _context.BillboardDisplays.Find(item.Id);
                    _mapper.Map(item, b);
                    _context.SaveChanges();
                });
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Xóa hình đại diện
        /// </summary>
        /// <param name="billboardDisplayId"></param>
        /// <returns></returns>
        [HttpDelete("display/{billboardDisplayId}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult DelPictureDisplay(int billboardDisplayId)
        {
            try
            {
                var pic = _context.BillboardDisplays.Where(b => b.Id == billboardDisplayId).FirstOrDefault();
                _context.Database.BeginTransaction();
                _context.BillboardDisplays.Remove(pic);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                //xoa tren cloud
                _blobService.DeleteBlobAsync(Path.GetFileName(pic.Url));
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //GET api/billboard/pictures/{Id}
        /// <summary>
        /// Lấy danh sách hình BB của 1 campaign
        /// </summary>
        /// <param name="Id">BB id</param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        [HttpGet("pictures/{Id}")]
        public ActionResult GetPicture(int Id, int campaignId)
        {
            try
            {
                var query = _context.BillboardPictures.Where(b => b.BillboardId == Id && b.CampaignId == campaignId).ToList();

                var list = _context.MediaPictures.Include(x => x.PictureTypes)
                                                 .Where(x => x.MediaChannels.Code == Constant.ConstantMediaChannel.Billboard)
                                                 .AsNoTracking()
                                                 .AsEnumerable()
                                                 .Select(item => new
                                                 {
                                                     item.PictureTypes.Id,
                                                     item.PictureTypes.Name,
                                                     list = query.Where(x => x.PictureTypeId == item.PictureTypes.Id).OrderByDescending(b => b.LastUpdated)
                                                 });
                return Ok(list);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion

        #region PRICES
        [AllowAnonymous]
        [HttpGet("xmlfile")]
        public ActionResult GetFileXML()
        {
            return Ok(Constant.XmlCommentsFilePath);
        }
        //GET api/billboard/company/{locationId}
        /// <summary>
        /// Lấy danh sách công ty theo locationID cho inventory/setprice
        /// </summary>
        /// <param name="locationId">có thể là tỉnh/tp hoặc quận/huyện</param>
        /// <param name="partnerCompanyId">không bắt buộc, chỉ dùng cho user là partner</param>
        /// <param name="create">inventory:create=true và không cần truyền locationId</param>
        /// <returns></returns>
        [HttpGet("company/{locationId}")]
        public ActionResult GetAllCompanyByLocation(int locationId, int? partnerCompanyId, bool create = false)
        {
            try
            {
                var models = _context.Billboards
                    .Where(x => x.Locations.ParentId == locationId || x.LocationId == locationId)
                    .Include(x => x.Companies)
                    .AsNoTracking()
                    .Select(x => x.Companies)
                    .Distinct().OrderBy(x => x.Name)
                    .ToList();

                if (create == true)
                {
                    models = _context.Companies
                        .Where(x => x.Roles.RoleName == Constant.ConstantRole.BillboardCompany)
                        .AsNoTracking()
                        .OrderBy(x => x.Name)
                        .ToList();
                }
                if (partnerCompanyId != null)
                {
                    models = models.Where(x => x.CompanyId == partnerCompanyId).ToList();
                }
                return Ok(models);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/price
        /// <summary>
        /// Lấy thông tin bảng giá theo năm/ công ty
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="locationId"></param>
        /// <param name="year"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet("price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult<IEnumerable<BillboardPrice>> GetAllPrice(int PageNumber, int PageSize, int locationId, int year, int companyId)
        {
            try
            {
                var models = _context.BillboardPrices.Include(x => x.Units)
                                                    .Include(x => x.Billboards)
                                                    .Where(x => x.Billboards.CompanyId == companyId)
                                                    .Where(x => x.FromDate.Year == year)
                                                    .Where(x => x.Billboards.Locations.ParentId == locationId
                                                                                || x.Billboards.LocationId == locationId)
                                                    .AsNoTracking();

                var response = PagedList<BillboardPrice>.ToPagedList(models, PageNumber, PageSize);

                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderBy(x => x.Billboards.Name).ThenBy(x=>x.FromDate));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/price/{Id}
        /// <summary>
        /// Lấy bảng giá của 1BB từ ngày - đến ngày
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("price/{Id}")]
        public ActionResult<IEnumerable<BillboardPrice>> GetPrice(int Id, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = _context.BillboardPrices.Where(b => b.BillboardId == Id)
                    .Where(b => fromDate.Date <= b.ToDate.Date)
                    .Where(b => b.FromDate.Date <= toDate.Date)
                    .Include(b => b.Billboards).OrderBy(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/billboard/Price
        /// <summary>
        /// Thêm giá BB
        /// </summary>
        /// <param name="billboardPrice"></param>
        /// <returns></returns>
        [HttpPost("Price")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult AddPrice(BillboardPrice billboardPrice)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //check timeoverlap
                var price = _context.BillboardPrices.Where(x => x.BillboardId == billboardPrice.BillboardId
                                                            && x.FromDate <= billboardPrice.ToDate && billboardPrice.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);
                _context.BillboardPrices.Add(billboardPrice);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok(billboardPrice);
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(billboardPrice),
                    UserId = userId
                });
            }
        }

        //POST api/billboard/PriceList
        //[HttpPost("PriceList")]
        //[Authorize(Policy = Constant.ConstantPolicy.RequireShelterAdminRole)]
        //public ActionResult AddPriceList(IEnumerable<BillboardPrice> lst)
        //{
        //    try
        //    {
        //        _context.Database.BeginTransaction();
        //        _context.BillboardPrices.AddRange(lst);
        //        _context.SaveChanges();
        //        _context.Database.CommitTransaction();
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        _context.Database.RollbackTransaction();
        //        return NotFound(ex?.InnerException?.Message ?? ex.Message);
        //    }
        //}

        //PUT api/billboard/Price/{id}
        /// <summary>
        /// Thay đổi giá của BB
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("Price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult UpdatePrices(int Id, BillboardPriceUpdateDto model)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BillboardPrices.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //check timeoverlap
                var price = _context.BillboardPrices.Where(x => x.Id != Id
                                                            && x.BillboardId == model.BillboardId
                                                            && x.FromDate <= model.ToDate && model.FromDate <= x.ToDate
                                                            );
                if (price.Count() > 0)
                    throw new NotImplementedException(_localizer[Constant.ConstantMessage.TimeOverlap]);

                _mapper.Map(model, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(model),
                    UserId = userId
                });
            }
        }

        //POST api/billboard/copyprice
        /// <summary>
        /// Copy giá BB từ năm - đến năm
        /// </summary>
        /// <param name="bbcopyprice"></param>
        /// <returns></returns>
        [HttpPost("copyprice")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult CopyPrice(BillboardCopyPriceDto bbcopyprice)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                //price of fromyear
                var bb = _context.BillboardPrices.Where(x => x.FromDate.Year == bbcopyprice.FromYear);
                if (bbcopyprice.LocationId != null)//location của tỉnh/tp
                    bb = bb.Where(x => x.Billboards.Locations.ParentId == bbcopyprice.LocationId);
                if (bbcopyprice.CompanyId != null)
                    bb = bb.Where(x => x.Billboards.CompanyId == bbcopyprice.CompanyId);
                if (bbcopyprice.BillboardId != null)
                    bb = bb.Where(x => x.BillboardId == bbcopyprice.BillboardId);

                if (bb.Count() == 0)
                    return NotFound();

                //price of toyear
                var bbPriceToYear = _context.BillboardPrices.Where(x => x.FromDate.Year == bbcopyprice.ToYear);
                if (bbcopyprice.LocationId != null)
                    bb = bb.Where(x => x.Billboards.Locations.ParentId == bbcopyprice.LocationId);
                if (bbcopyprice.CompanyId != null)
                    bbPriceToYear = bbPriceToYear.Where(x => x.Billboards.CompanyId == bbcopyprice.CompanyId);
                if (bbcopyprice.BillboardId != null)
                    bbPriceToYear = bbPriceToYear.Where(x => x.BillboardId == bbcopyprice.BillboardId);

                //chi lay nhung billboardprice chua co gia trong ToYear
                var bbPrice = bb.Where(x => !bbPriceToYear.Select(x => x.BillboardId).Contains(x.BillboardId));

                foreach (var price in bbPrice.ToList())
                {
                    //tao price moi
                    var BBPrice = new BillboardPrice
                    {
                        Id = 0,
                        Price = price.Price,
                        PromotePercent = price.PromotePercent,
                        MinMonthBooking = price.MinMonthBooking,
                        FromDate = price.FromDate.AddYears(bbcopyprice.ToYear - bbcopyprice.FromYear),
                        ToDate = price.ToDate.AddYears(bbcopyprice.ToYear - bbcopyprice.FromYear),
                        UnitId = price.UnitId,
                        BillboardId = price.BillboardId,
                        CreatedUserId = userId,
                        UserId = userId
                    }; 

                    _context.BillboardPrices.Add(BBPrice);
                    _context.SaveChanges();
                    //copy Discount tu bang gia cu
                    var BBdiscount = from dis in _context.BillboardDiscounts.Where(x=>x.BillboardPriceId == price.Id)
                                         select new BillboardDiscount
                                         {
                                             Id = 0,
                                             Month = dis.Month,
                                             Percent = dis.Percent,
                                             BillboardPriceId = BBPrice.Id
                                         };
                    _context.BillboardDiscounts.AddRange(BBdiscount.ToList());
                    _context.SaveChanges();
                };
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(bbcopyprice),
                    UserId = userId
                });
            }
        }

        //DEL api/billboard/price/{Id}
        /// <summary>
        /// Xóa 1 dòng giá của BB trong BillboardPrice
        /// </summary>
        /// <param name="Id">BillboardPriceId</param>
        /// <returns></returns>
        [HttpDelete("price/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult DeletePrice(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var billboardPrice = _context.BillboardPrices.Find(Id);
                if (billboardPrice == null)
                    return NotFound();

                var booking = _context.BillboardBookings
                                                  .Where(x => x.Statuses.Name == Constant.ConstantStatus.Active)
                                                  .Where(x => x.FromDate <= billboardPrice.ToDate && billboardPrice.FromDate <= x.ToDate)
                                                  .Where(x => x.BillboardId == billboardPrice.BillboardId)
                                                  .FirstOrDefault();
                if (booking != null)
                    return NotFound(_localizer[Constant.ConstantMessage.ItisReserved, booking.FromDate.ToLongDateString(), booking.ToDate.ToLongDateString()].ToString());
                else
                {
                    var discount = _context.BillboardDiscounts.Where(x => x.BillboardPriceId == billboardPrice.Id);
                    _context.BillboardDiscounts.RemoveRange(discount);
                    _context.BillboardPrices.Remove(billboardPrice);
                }
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = string.Format("BillboardPrice Id: {0}", Id),
                    UserId = userId
                });
            }
        }
        #endregion

        #region DISCOUNT
        //GET api/bus/discount
        /// <summary>
        /// Lấy discount của 1 dòng BBprice
        /// </summary>
        /// <param name="BillboardPriceId"></param>
        /// <returns></returns>
        [HttpGet("discount")]
        public ActionResult<IEnumerable<BillboardDiscount>> GetDiscount(int BillboardPriceId)
        {
            try
            {
                //var query = _context.BillboardDiscounts.Where(x => x.LocationId == locationId && x.CompanyId == x.CompanyId).OrderBy(x => x.Month).ToList();
                var query = _context.BillboardDiscounts.Where(x => x.BillboardPriceId == BillboardPriceId).OrderBy(x => x.Month).ToList();
                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/bus/discountList/
        /// <summary>
        /// thêm bảng discount cho 1 dòng giá BB
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost("discountList")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult AddDiscountList(IEnumerable<BillboardDiscount> lst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BillboardDiscounts.AddRange(lst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(lst),
                    UserId = userId
                });
            }
        }

        //PUT api/bus/discount/id
        /// <summary>
        /// thay đổi discount cho 1 dòng giá BB
        /// </summary>
        /// <param name="updatLst"></param>
        /// <returns></returns>
        [HttpPut("discount/updatelist")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult UpdateDiscountList(IEnumerable<BillboardDiscount> updatLst)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                _context.BillboardDiscounts.UpdateRange(updatLst);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(updatLst),
                    UserId = userId
                });
            }
        }
        #endregion

        #region DOWNTIME
        //GET api/billboard/downtime/{Id}
        /// <summary>
        /// Lấy thông tin downtime BB trong 1 năm
        /// </summary>
        /// <param name="Id">BBId</param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("downtime/{Id}")]
        public ActionResult<IEnumerable<BillboardDownTime>> GetDowntime(int Id, int year)
        {
            try
            {
                var query = _context.BillboardDownTimes.Where(b => b.BillboardId == Id)
                    .Where(b => b.FromDate.Year == year)
                    .Include(b => b.Billboards).OrderByDescending(b => b.FromDate)
                    .ToList();

                return Ok(query);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        //POST api/billboard/downtime/
        /// <summary>
        /// Them downtime cho BB
        /// </summary>
        /// <param name="downTime"></param>
        /// <returns></returns>
        [HttpPost("downtime")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult AddDowntime(List<BillboardDownTime> downTime)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                List<Billboard> lst = new List<Billboard>();
                for (int i = 0; i < downTime.Count; i++)
                {
                    //check timeoverlap
                    var check = _context.BillboardDownTimes.Where(x => x.BillboardId == downTime[i].BillboardId
                                                                && x.FromDate <= downTime[i].ToDate && downTime[i].FromDate <= x.ToDate
                                                            ).Include(x => x.Billboards).FirstOrDefault();
                    if (check != null)
                    {
                        lst.Add(check.Billboards);
                        //throw new NotImplementedException(string.Format("{0} {1}-{2}:{3}",
                        //            check.Billboards.Name, check.FromDate, check.ToDate, _localizer[Constant.ConstantMessage.TimeOverlap]));
                    }

                }
                if (lst.Count > 0)
                {
                    return NotFound(lst);
                }
                _context.BillboardDownTimes.AddRange(downTime);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                mess = ex?.InnerException?.Message ?? ex.Message;
                _context.Database.RollbackTransaction();
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = JsonSerializer.Serialize(downTime),
                    UserId = userId
                });
            }
        }

        //PUT api/billboard/downtime/id
        /// <summary>
        /// Thay đổi downtime BB
        /// </summary>
        /// <param name="Id">BillboardDowntimeId</param>
        /// <param name="updateDto"></param>
        /// <returns></returns>
        [HttpPut("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult UpdateDowntime(int Id, BillboardDownTimeUpdateDto updateDto)
        {
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BillboardDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();

                //vi dieu
                _mapper.Map(updateDto, b);

                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Xóa downtime
        /// </summary>
        /// <param name="Id">BillboardDowntimeId</param>
        /// <returns></returns>
        [HttpDelete("downtime/{Id}")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult DelDowntime(int Id)
        {
            string mess = string.Empty;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int userId = MyUltil.GetCurrentUserId(identity);
            try
            {
                _context.Database.BeginTransaction();
                var b = _context.BillboardDownTimes.FirstOrDefault(p => p.Id == Id);
                if (b == null)
                    return NotFound();
                _context.Remove(b);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                mess = ex?.InnerException?.Message ?? ex.Message;
                return NotFound(mess);
            }
            finally
            {
                MyUltil.AddTracking(_context, new Tracking
                {
                    Id = 0,
                    CampaignId = 0,
                    FunctionName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ErrorName = mess,
                    Content = Id.ToString(),
                    UserId = userId
                });
            }
        }
        #endregion

        #region COUNTING
        //POST api/billboard/counting
        //lay du lieu tu app
        /// <summary>
        /// Thêm data counting
        /// </summary>
        /// <param name="billboard"></param>
        /// <returns></returns>
        [HttpPost("counting")]
        [AllowAnonymous]
        public ActionResult CreateCounting(BillboardCounting billboard)
        {
            try
            {
                _context.Database.BeginTransaction();
                _context.BillboardCountings.Add(billboard);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/counting/listBB
        /// <summary>
        /// Lấy danh sách BB có CAM thuộc cty
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="locationId"></param>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("counting/listBB")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetAllBBWithCamera(int PageNumber, int PageSize, int locationId, int companyId, string name)
        {
            try
            {
                //lay BB theo location: tinh/tp hoac quan/huyen
                var models = _context.Billboards.Include(x => x.BillboardCameras)
                                .Where(b => b.CompanyId == companyId)
                                .Where(x => x.LocationId == locationId || x.Locations.ParentId == locationId)
                                .Where(x => x.BillboardCameras.Count > 0);
                if (!string.IsNullOrEmpty(name))
                    models = models.Where(x => x.Name.Contains(name));

                var response = PagedList<Billboard>.ToPagedList(models, PageNumber, PageSize);
                Response.Headers.Add(response.HeaderInfo);
                return Ok(response.OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/counting/camera
        /// <summary>
        /// Lấy danh sách CAM của BB
        /// </summary>
        /// <param name="billboardId"></param>
        /// <returns></returns>
        [HttpGet("counting/camera")]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetCameraListCounting(int billboardId)
        {
            try
            {
                var data = _context.BillboardCameras.Where(x => x.BillboardId == billboardId)
                            .Where(x => x.Type.Equals(Constant.CameraType.View) || (x.Type.Equals(Constant.CameraType.Count) && x.CountOn == true))
                            .OrderBy(x => x.Type)
                            .ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        //GET api/billboard/counting/DateList
        /// <summary>
        /// Lấy data counting của 1 BB
        /// </summary>
        /// <param name="billboardId"></param>
        /// <param name="billboardCameraid"></param>
        /// <param name="campaignId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet("counting/DateList")]
        //[AllowAnonymous]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetDateListCounting(int billboardId, int? billboardCameraid, int? campaignId, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var listDate = GetDateList(_context, billboardId, billboardCameraid, campaignId, fromDate, toDate);
                return Ok(listDate);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        /// <summary>
        /// Lấy data counting của 1 BB
        /// </summary>
        /// <param name="_context"></param>
        /// <param name="billboardId"></param>
        /// <param name="billboardCameraid"></param>
        /// <param name="campaignId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public static List<string> GetDateList(LengKengDbContext _context, int billboardId, int? billboardCameraid, int? campaignId, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var data = _context.BillboardCountings.Where(x => x.BillboardId == billboardId);
                if (campaignId != null)
                {
                    var campaign = _context.Campaigns.Where(x => x.Id == campaignId).FirstOrDefault();
                    data = data.Where(x => campaign.FromDate <= x.LastUpdated.Date && x.LastUpdated.Date <= campaign.ToDate);
                }
                if (!string.IsNullOrEmpty(billboardCameraid.ToString()))
                {
                    data = data.Where(x => x.BillboardCameraId == billboardCameraid);
                }
                if (fromDate != null && toDate != null)
                {
                    data = data.Where(x => x.LastUpdated.Date >= fromDate && x.LastUpdated.Date <= toDate);
                }
                var result = data.GroupBy(x => x.LastUpdated.Date).Select(x => x.Key.Date.ToString("yyyy-MM-dd")).ToList();
                return result;
            }
            catch
            {
                return new List<string>();
            }
        }


        /// <summary>
        /// Tính phần % phương tiện theo tổng, group theo ngày đêm | giờ| tổng số ngày có data
        /// </summary>
        /// <param name="billboardId"></param>
        /// <param name="billboardCameraid">có thể ko cần</param>
        /// <param name="campaignId">có thể ko cần</param>
        /// <param name="day">có thể ko cần</param>
        /// <param name="fromDate">nên có</param>
        /// <param name="toDate">nên có</param>
        /// <param name="ratioMoto"></param>
        /// <param name="ratioCar"></param>
        /// <param name="ratioBus"></param>
        /// <returns></returns>
        //POST api/billboard/counting
        [HttpGet("counting")]
        //[AllowAnonymous]
        [Authorize(Policy = Constant.ConstantPolicy.RequireBillboardAdminRole)]
        public ActionResult GetReportCounting(int billboardId, int? billboardCameraid, int? campaignId, DateTime? day, DateTime? fromDate, DateTime? toDate
                                              , double ratioMoto = 1.3, double ratioCar = 2, double ratioBus = 4.3)
        {
            try
            {
                var totalDay = GetDateList(_context, billboardId, billboardCameraid, campaignId, fromDate, toDate).Count();
                var data = _context.BillboardCountings.Where(x => x.BillboardId == billboardId);
                if (campaignId != null)
                {
                    var campaign = _context.Campaigns.Where(x => x.Id == campaignId).FirstOrDefault();
                    data = data.Where(x => campaign.FromDate <= x.LastUpdated.Date && x.LastUpdated.Date <= campaign.ToDate);
                }
                if (!string.IsNullOrEmpty(billboardCameraid.ToString()))
                {
                    data = data.Where(x => x.BillboardCameraId == billboardCameraid);
                }
                //du lieu tong chua tinh ngay
                //var dataAll = data;
                //var resultAll = dataAll.ToList().GroupBy(x => new { x.BillboardId, x.LastUpdated.Hour })
                //            .Select(x => new
                //            {
                //                Hour = x.Key.Hour,
                //                Type = x.Select(x => x.Type).First(),
                //                CarAmount = x.Sum(c => c.CarAmount),
                //                MotoAmount = x.Sum(c => c.MotoAmount),
                //                BusAmount = x.Sum(c => c.BusAmount),
                //                Total = x.Sum(c => c.CarAmount + c.MotoAmount + c.BusAmount)
                //            }).OrderBy(x => x.Hour);

                //du lieu tinh theo 1 ngay
                if (day != null)
                {
                    data = data.Where(x => x.LastUpdated.Date == day);
                }
                if (fromDate != null && toDate != null)
                {
                    data = data.Where(x => x.LastUpdated.Date >= fromDate && x.LastUpdated.Date <= toDate);
                }

                if (data.Count() == 0)
                {
                    return NotFound(_localizer[Constant.ConstantMessage.NoData].Value);
                }

                //theo gio
                var resultHours = data.ToList().GroupBy(x => new { x.BillboardId, x.LastUpdated.Hour })
                            .Select(x => new
                            {
                                Hour = x.Key.Hour,
                                Type = x.Select(x => x.Type).First(),
                                CarAmount = x.Sum(c => c.CarAmount) / totalDay * ratioCar * 30,
                                MotoAmount = x.Sum(c => c.MotoAmount) / totalDay * ratioMoto * 30,
                                BusAmount = x.Sum(c => c.BusAmount) / totalDay * ratioBus * 30,
                                Total = x.Sum(c => c.CarAmount * ratioCar + c.MotoAmount * ratioMoto + c.BusAmount * ratioBus) / totalDay * 30
                            }).OrderBy(x => x.Hour);
                //theo thu
                DayOfWeek[] daysArr = new DayOfWeek[]{
                    DayOfWeek.Sunday,
                    DayOfWeek.Monday,
                    DayOfWeek.Tuesday,
                    DayOfWeek.Wednesday,
                    DayOfWeek.Thursday,
                    DayOfWeek.Friday,
                    DayOfWeek.Saturday
                };
                var resultDayOfWeek = data.ToList().GroupBy(x => new { x.BillboardId, x.LastUpdated.DayOfWeek })//, x.Type
                            .Select(x => new
                            {
                                DayOfWeek = x.Key.DayOfWeek.ToString(),
                                Order = x.Key.DayOfWeek,
                                Day = x.Select(x => x).Where(x => x.Type == "Day").Sum(c => c.CarAmount * ratioCar + c.MotoAmount * ratioMoto + c.BusAmount * ratioBus) / totalDay * 30,
                                Night = x.Select(x => x).Where(x => x.Type == "Night").Sum(c => c.CarAmount * ratioCar + c.MotoAmount * ratioMoto + c.BusAmount * ratioBus) / totalDay * 30,
                            }).OrderBy(x => x.Order).ToList();
                var dayLess = daysArr.Where(x => !resultDayOfWeek.Select(x => x.DayOfWeek).Contains(x.ToString())).Select(x => new
                {
                    DayOfWeek = x.ToString(),
                    Order = x,
                    Day = 0.0,
                    Night = 0.0
                }).ToList();

                resultDayOfWeek.AddRange(dayLess);

                //xuat thong tin report
                var report = new
                {
                    //report theo phuong tien
                    Vehicle = new
                    {
                        CarAmount = data.Sum(x => x.CarAmount) / totalDay * ratioCar * 30,
                        MotoAmount = data.Sum(x => x.MotoAmount) / totalDay * ratioMoto * 30,
                        BusAmount = data.Sum(x => x.BusAmount) / totalDay * ratioBus * 30,
                        Total = data.Sum(c => c.CarAmount * ratioCar + c.MotoAmount * ratioMoto + c.BusAmount * ratioBus) / totalDay * 30
                    },
                    //theo Ngay - Dem
                    DayNight = new
                    {
                        CarAmount = new
                        {
                            Day = data.Where(x => x.Type.Equals("Day")).Sum(x => x.CarAmount) / totalDay * ratioCar * 30,
                            Night = data.Where(x => x.Type.Equals("Night")).Sum(x => x.CarAmount) / totalDay * ratioCar * 30
                        },
                        MotoAmount = new
                        {
                            Day = data.Where(x => x.Type.Equals("Day")).Sum(x => x.MotoAmount) / totalDay * ratioMoto * 30,
                            Night = data.Where(x => x.Type.Equals("Night")).Sum(x => x.MotoAmount) / totalDay * ratioMoto * 30
                        },
                        BusAmount = new
                        {
                            Day = data.Where(x => x.Type.Equals("Day")).Sum(x => x.BusAmount) / totalDay * ratioBus * 30,
                            Night = data.Where(x => x.Type.Equals("Night")).Sum(x => x.BusAmount) / totalDay * ratioBus * 30
                        },
                    },
                    //theo gio
                    Hours = resultHours,
                    DayOfWeeks = resultDayOfWeek.OrderBy(x => x.Order),
                    TotalDays = totalDay
                };
                return Ok(report);
            }
            catch (Exception ex)
            {
                return NotFound(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}
