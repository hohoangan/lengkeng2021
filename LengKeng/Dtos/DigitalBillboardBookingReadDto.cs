﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class DigitalBillboardBookingReadDto
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public double Price { get; set; }
        public int Amount { get; set; }
        public int HourPerDay { get; set; }
        public double DiscountPercent { get; set; }
        public double TotalFee { get; set; }
        public int DigitalBillboardId { get; set; }
        [JsonIgnore]
        public DigitalBillboard DigitalBillboards { get; set; }
        public int UnitId { get; set; }
        public int StatusId { get; set; }
        public double OTC { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        public bool IsAvailable { get; set; }
    }
}
