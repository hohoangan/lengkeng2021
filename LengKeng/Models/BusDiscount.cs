﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace LengKeng.Models
{
    public class BusDiscount
    {
        [Required]
        public int Id { get; set; }
        [Required, Id]
        public int BusRoutePriceId { get; set; }
        [JsonIgnore]
        public BusRoutePrice BusRoutePrice { get; set; }
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
    }
}
