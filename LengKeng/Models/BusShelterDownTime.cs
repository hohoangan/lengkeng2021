﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusShelterDownTime
    {
        [Key]
        public int Id { get; set; }
        public string Remark { get; set; }
        [Required, Id]
        public int BusShelterId { get; set; }
        [JsonIgnore]
        public BusShelter BusShelters { get; set; }
        [Required, Id]
        public int StatusId { get; set; }
        [JsonIgnore]
        public Status Statuses { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
    }
}
