﻿using Microsoft.Extensions.PlatformAbstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LengKeng.Ultils
{
    public static class Constant
    {
        public const string PromotePercent = "Khuyến mại";
        public static class ConstantMediaChannel
        {
            public const string Bus = "Bus";
            public const string BusShelter = "BusShelter";
            public const string Billboard = "Billboard";
            public const string DigitalBillboard = "DigitalBillboard";
            public const string Taxi = "Taxi";
        }
        public static class ConstantStatus
        {
            public const string TypeCampaign = "Campaign";
            public const string Reserved = "Reserved";
            public const string Sold = "Sold";
            public const string Completed = "Completed";
            public const string Canceled = "Canceled";

            public const string TypeBookingDowntime = "Booking-Downtime";
            public const string Unconfirmed = "Unconfirmed";
            public const string Active = "Active";
            public const string Inactive = "Inactive";

            public const string TypeRemind = "Remind";
            public const string New = "New";
            public const string Confirmed = "Confirmed";
        }
        public static class ConstantMessage
        {
            public const string ItWasReservedByOtherPeople = "ItWasReservedByOtherPeople";
            public const string NotEnoughCar = "NotEnoughCar";
            public const string NotEnoughBus = "NotEnoughBus";
            public const string NotEnoughTaxi = "NotEnoughTaxi";
            public const string NotEnoughTime = "NotEnoughTime";
            public const string NotFoundTaxi = "NotFoundTaxi";
            public const string NotFoundBus = "NotFoundBus";
            public const string TaxiNotSuitable = "TaxiNotSuitable";
            public const string BusNotSuitable = "BusNotSuitable";
            public const string TimeOverlap = "TimeOverlap";
            public const string IncorrectPassword = "IncorrectPassword";
            public const string ResetPassword = "ResetPassword";
            public const string FailureSendMail = "FailureSendMail";
            public const string ItisReserved = "ItisReserved";
            public const string DuplicateName = "DuplicateName";
            public const string DuplicateX = "DuplicateX";
            public const string NotFoundX = "NotFoundX";
            public const string Successfully = "Successfully";
            public const string PriceChanged = "PriceChanged";
            public const string NotFoundPrice = "NotFoundPrice";
            public const string RemindNewCampaign = "RemindNewCampaign";
            public const string CarBookingNotEnough = "CarBookingNotEnough";
            public const string NoData = "NoData";
            public const string TaxiAreaNotSuitable = "TaxiAreaNotSuitable";
            public const string BusRouteNotSuitable = "BusRouteNotSuitable";
        }
        public static class ConstantPolicy
        {
            public const string RequireMobileRole = "RequireMobileRole";
            public const string RequirePartnerGroup = "RequirePartnerGroup";
            public const string RequireBusAdminRole = "RequireBusAdminRole";
            public const string RequireTaxiAdminRole = "RequireTaxiAdminRole";
            public const string RequireShelterAdminRole = "RequireShelterAdminRole";
            public const string RequireDigitalAdminRole = "RequireDigitalAdminRole";
            public const string RequireCustomerAdminRole = "RequireCustomerAdminRole";
            public const string RequireBillboardAdminRole = "RequireBillboardAdminRole";

            public const string RequireAdminCustomerBusCompany = "RequireAdminCustomerBusCompany";
            public const string RequireAdminCustomerTaxiCompany = "RequireAdminCustomerTaxiCompany";
            public const string RequireAdminCustomerShelterCompany = "RequireAdminCustomerShelterCompany";
            public const string RequireAdminCustomerBillboardCompany = "RequireAdminCustomerBillboardCompany";

            public const string RequireAdminCustomerAllPartnerCompany = "RequireAdminCustomerAllPartnerCompany";
            public const string RequireAdminSupperUser = "RequireAdminSupperUser";

        }
        public static class ConstantRole
        {
            /*----------------------------- STAFF ---------------------------------*/
            public const string TaxiDriver = "Taxi Driver";                //chup hinh: taxi
            public const string BusDriver = "Bus Driver";                  //chup hinh: bus
            public const string BillboardOperator = "Billboard Operator";  //chup hinh: billboard + digital

            /*----------------------------- VINSTAR GROUP --------------------------*/
            public const string SystemAdmin = "System Admin";              //create user/company | push nofi 
            public const string SuperUser = "Super User";                  //quan ly: compaign | change status | assign coordinator | view | set price | downtime
            public const string Coordinator = "Coordinator";               //chup hinh: compaign

            /*----------------------------- MANAGE CAMPAIGN -------------------------*/
            public const string Customer = "Customer";    

            /*----------------------------- MANAGE REPORT ---------------------------*/
            public const string TaxiCompany = "Taxi Company";              
            public const string BusCompany = "Bus Company";
            public const string BillboardCompany = "Billboard Company";
            public const string ShelterCompany = "Shelter Company";
            public const string InternalCompany = "Internal Company";

        }
        public static class ConstantUnit
        {
            public const string Month = "Month";
            public const string Day = "Day";
            public const string Hour = "Hour";
            public const string m2 = "m2";
        }
        public static class Sort
        {
            public const string ASC = "ASC";
            public const string DESC = "DESC";
        }
        public static class FileType
        {
            public const string Invoice = "Invoice";
            public const string Share = "Share";
            public const string FinalReport = "FinalReport";
        }
        public static class Environment
        {
            public static string Permission = "Develope";
            public const string Production = "Production";
            public const string Develop = "Develop";
        }
        public static class CameraType
        {
            public const string View = "View";
            public const string Count = "Count";
        }
        public static class PictureType
        {
            public const string Main = "Main";
            public const string Detail = "Detail";
            public const string Video = "Video";
        }
        public static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
