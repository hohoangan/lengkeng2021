﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusRoute
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        public double OTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime OTCDate { get; set; } = DateTime.Now; // ngay cap nhat OTC
        public string Description { get; set; }
        public string SearchKeywordText { get; set; }
        [Required, Id]
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Locations { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusRoutePrice> BusRoutePrices { get; set; }
        [JsonIgnore]
        public virtual ICollection<Bus> Buses { get; set; }
        [JsonIgnore]
        public virtual ICollection<BusRouteCompany> BusRouteCompanies { get; set; }
    }
}
