﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class MediaPicture
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int MediaChannelId { get; set; }
        [JsonIgnore]
        public MediaChannel MediaChannels { get; set; }
        [Required, Id]
        public int PictureTypeId { get; set; }
        [JsonIgnore]
        public PictureType PictureTypes { get; set; }
    }
}
