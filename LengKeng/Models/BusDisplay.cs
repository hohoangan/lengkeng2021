﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class BusDisplay
    {
        [Key]
        public int Id { get; set; }
        public int? CompanyId { get; set; }
        [Required, Id]
        public int CarTypeId { get; set; }
        [Required, Id]
        public int ArtTypeId { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string TypeName { get; set; } // main, detail, video
        public int Order { get; set; } // main = 0
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
    }
}
