﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class DigitalBillboardDisplayUpdateDto
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int DigitalBillboardId { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string TypeName { get; set; } // main, detail, video
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? LastUpdated { get; set; } = DateTime.Now;
    }
}
