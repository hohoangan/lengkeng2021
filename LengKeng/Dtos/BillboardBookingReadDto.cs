﻿using LengKeng.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BillboardBookingReadDto
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public double Price { get; set; }
        public int MinMonthBooking { get; set; }
        public int Amount { get; set; }
        public double InstallationCost { get; set; }
        public int InstallationTimes { get; set; }
        public double DiscountPercent { get; set; }
        public double PromotePercent { get; set; } = 0;
        public double TotalFee { get; set; }
        public int BillboardId { get; set; }
        [JsonIgnore]
        public Billboard Billboards { get; set; }
        public int UnitId { get; set; }
        public double OTC { get; set; }
        public int StatusId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        public bool IsAvailable { get; set; }
        //public List<BillboardCamera> BillboardCameras { get; set; }
    }
}
