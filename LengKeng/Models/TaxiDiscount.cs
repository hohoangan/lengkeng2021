﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class TaxiDiscount
    {
        [Required]
        public int Id { get; set; }
        [Required, Id]
        public int TaxiAreaPriceId { get; set; }
        [JsonIgnore]
        public TaxiAreaPrice TaxiAreaPrice { get; set; }
        [Required, Id]
        public int Month { get; set; }
        [Required]
        public double Percent { get; set; }
    }
}
