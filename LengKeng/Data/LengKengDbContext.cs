﻿using LengKeng.Models;
using LengKeng.Ultils;
using Microsoft.EntityFrameworkCore;
using System;

namespace LengKeng.Data
{
    public class LengKengDbContext : DbContext
    {
        public LengKengDbContext(DbContextOptions<LengKengDbContext> opt) : base(opt)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region many-to-one
            modelBuilder.Entity<Company>()
                .HasMany(p => p.Users)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.Billboards)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.Buses)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.Taxis)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.DigitalBillboards)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.BusShelters)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.TaxiAreas)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .HasMany(p => p.BusRouteCompanies)
                .WithOne(t => t.Companies)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invoice>()
                .HasMany(p => p.BusBookings)
                .WithOne(t => t.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invoice>()
                .HasMany(p => p.TaxiBookings)
                .WithOne(t => t.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invoice>()
                .HasMany(p => p.BillboardBookings)
                .WithOne(t => t.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invoice>()
                .HasMany(p => p.DigitalBillboardBookings)
                .WithOne(t => t.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Invoice>()
                .HasMany(p => p.BusShelterBookings)
                .WithOne(t => t.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasMany(p => p.UserCampaigns)
                .WithOne(t => t.Users)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion

            #region STATUS
            modelBuilder.Entity<Status>()
                .HasMany(p => p.BillboardDownTimes)
                .WithOne(t => t.Statuses)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.DigitalBillboardDownTimes)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.BusShelterDownTimes)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.BusDownTimes)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.TaxiDownTimes)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.BillboardBookings)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.DigitalBillboardBookings)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.BusShelterBookings)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.BusBookings)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
               .HasMany(p => p.TaxiBookings)
               .WithOne(t => t.Statuses)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Status>()
                .HasMany(p => p.Users)
                .WithOne(t => t.Statuses)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            //one-to-many
            //modelBuilder.Entity<TaxiBooking>()
            //    .HasOne(p => p.Statuses)
            //    .WithMany(t => t.TaxiBookings)
            //    .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<TaxiBooking>()
            //   .HasOne(p => p.Taxis)
            //   .WithMany(t => t.TaxiBookings)
            //   .OnDelete(DeleteBehavior.Restrict);

            #region one-to-one
            modelBuilder.Entity<Campaign>()
            .HasOne(a => a.Invoices)
            .WithOne(a => a.Campaigns)
            .HasForeignKey<Invoice>(a => a.CampaignId);
            #endregion

            #region Default values
            modelBuilder.Entity<Location>()
            .Property(b => b.OrderBy)
            .HasDefaultValue(1);

            modelBuilder.Entity<User>()
            .Property(b => b.StatusId)
            .HasDefaultValue(1);
            #endregion

            #region Create Unique Indexes
            modelBuilder.Entity<User>()
            .HasIndex(p => new { p.Username, p.Phone })
            .IsUnique(true)
            .HasName("IX_User_UsernamePhone");

            //modelBuilder.Entity<BusShelter>()
            //.HasIndex(p => new { p.GPSLocX, p.GPSLocY })
            //.IsUnique(true)
            //.HasName("IX_BusShelter_XY");

            modelBuilder.Entity<Bus>()
            .HasIndex(p => new { p.PlateNumber })
            .IsUnique(true)
            .HasName("IX_Bus_PlateNumber");

            modelBuilder.Entity<Taxi>()
            .HasIndex(p => new { p.PlateNumber })
            .IsUnique(true)
            .HasName("IX_Taxi_PlateNumber");

            modelBuilder.Entity<Billboard>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_Billboard_Name");

            modelBuilder.Entity<DigitalBillboard>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_DigitalBillboard_Name");

            modelBuilder.Entity<MediaPicture>()
            .HasIndex(p => new { p.MediaChannelId, p.PictureTypeId })
            .IsUnique(true)
            .HasName("IX_MediaPicture_MediaPictypeId");

            modelBuilder.Entity<BusType>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_BusType_Name");

            modelBuilder.Entity<TaxiType>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_TaxiType_Name");

            modelBuilder.Entity<BusRoute>()
            .HasIndex(p => new { p.Name, p.LocationId, p.Code })
            .IsUnique(true)
            .HasName("IX_BusRoute_LocationName");

            modelBuilder.Entity<TaxiArea>()
            .HasIndex(p => new { p.Name, p.LocationId })
            .IsUnique(true)
            .HasName("IX_TaxiArea_LocationName");

            modelBuilder.Entity<BusRoutePrice>()
            .HasIndex(p => new { p.BusRouteId, p.BusTypeId, p.BusTypeOfArtworkId, p.UnitId, p.FromDate, p.ToDate, p.CompanyId})
            .IsUnique(true)
            .HasName("IX_BusRoutePrice_Unique");

            modelBuilder.Entity<TaxiAreaPrice>()
            .HasIndex(p => new { p.TaxiAreaId, p.TaxiTypeId, p.TaxiTypeOfArtworkId, p.UnitId, p.FromDate, p.ToDate })
            .IsUnique(true)
            .HasName("IX_TaxiAreaPrice_Unique");

            modelBuilder.Entity<BusTypeOfArtwork>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_BusTypeOfArtwork_Name");

            modelBuilder.Entity<TaxiTypeOfArtwork>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_TaxiTypeOfArtwork_Name");

            modelBuilder.Entity<Unit>()
            .HasIndex(p => new { p.Name })
            .IsUnique(true)
            .HasName("IX_Unit_Name");

            modelBuilder.Entity<BusShelterPrice>()
            .HasIndex(p => new { p.BusShelterId, p.UnitId, p.FromDate, p.ToDate })
            .IsUnique(true)
            .HasName("IX_BusShelterPrice_Unique");

            modelBuilder.Entity<BillboardPrice>()
            .HasIndex(p => new { p.BillboardId, p.UnitId, p.FromDate, p.ToDate })
            .IsUnique(true)
            .HasName("IX_BillboardPrice_Unique");

            modelBuilder.Entity<DigitalBillboardPrice>()
            .HasIndex(p => new { p.DigitalBillboardId, p.UnitId, p.FromDate, p.ToDate })
            .IsUnique(true)
            .HasName("IX_DigitalBillboardPrice_Unique");

            modelBuilder.Entity<UserRole>()
            .HasIndex(p => new { p.UserId, p.RoleId})
            .IsUnique(true)
            .HasName("IX_UserRole_Unique");

            modelBuilder.Entity<UserShelter>()
            .HasIndex(p => new { p.UserId, p.BusShelterId })
            .IsUnique(true)
            .HasName("IX_UserShelter_Unique");

            modelBuilder.Entity<UserTaxi>()
            .HasIndex(p => new { p.UserId, p.TaxiId })
            .IsUnique(true)
            .HasName("IX_UserTaxi_Unique");

            modelBuilder.Entity<UserBillboard>()
            .HasIndex(p => new { p.UserId, p.BillboardId })
            .IsUnique(true);

            modelBuilder.Entity<UserDigitalBillboard>()
            .HasIndex(p => new { p.UserId, p.DigitalBillboardId })
            .IsUnique(true);

            modelBuilder.Entity<UserLocation>()
            .HasIndex(p => new { p.UserId, p.LocationId })
            .IsUnique(true);

            modelBuilder.Entity<TaxiDiscount>()
            .HasIndex(p => new { p.Month, p.TaxiAreaPriceId })
            .IsUnique(true);

            modelBuilder.Entity<BusDiscount>()
            .HasIndex(p => new { p.Month, p.BusRoutePriceId })
            .IsUnique(true);

            modelBuilder.Entity<BusShelterDiscount>()
            .HasIndex(p => new { p.Month, p.BusShelterPriceId })
            .IsUnique(true);

            modelBuilder.Entity<BillboardDiscount>()
            .HasIndex(p => new { p.Month, p.BillboardPriceId })
            .IsUnique(true);

            modelBuilder.Entity<TaxiArea>()
            .HasIndex(p => new { p.LocationId, p.CompanyId })
            .IsUnique(true);

            modelBuilder.Entity<UserCampaign>()
            .HasIndex(p => new { p.UserId, p.CampaignId })
            .IsUnique(true);

            modelBuilder.Entity<BusInstallationCost>()
            .HasIndex(p => new { p.BusTypeOfArtworkId, p.BusTypeId })
            .IsUnique(true);

            modelBuilder.Entity<TaxiInstallationCost>()
           .HasIndex(p => new { p.TaxiTypeOfArtworkId, p.TaxiTypeId })
           .IsUnique(true);
            #endregion

            #region FOREIGN KEY
            #endregion
        }

        public DbSet<Command> Commands { get; set; }
        public DbSet<SinhVien> SinhViens { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Tracking> Trackings { get; set; }

        #region DISPLAY PICTURE
        public DbSet<TaxiDisplay> TaxiDisplays { get; set; }
        public DbSet<BusDisplay> BusDisplays { get; set; }
        public DbSet<BusShelterDisplay> BusShelterDisplays { get; set; }
        public DbSet<BillboardDisplay> BillboardDisplays { get; set; }
        public DbSet<DigitalBillboardDisplay> DigitalBillboardDisplays { get; set; }
        #endregion

        #region DEFINE CONSTANT
        public DbSet<Location> Locations { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<PictureType> PictureTypes { get; set; }
        public DbSet<MediaChannel> MediaChannels { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<BillboardType> BillboardTypes { get; set; }
        #endregion

        #region USERS AND ROLE
        public DbSet<Company> Companies { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserLocation> UserLocations { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Billboard> Billboards { get; set; }
        public DbSet<BillboardCamera> BillboardCameras { get; set; }
        public DbSet<BillboardKeyword> BillboardKeywords { get; set; }
        public DbSet<UserBillboard> UserBillboards { get; set; }
        public DbSet<Bus> Buses { get; set; }
        public DbSet<BusKeyword> BusKeywords { get; set; }
        public DbSet<UserBus> UserBuses { get; set; }
        public DbSet<BusShelter> BusShelters { get; set; }
        public DbSet<UserShelter> UserShelters { get; set; }
        public DbSet<DigitalBillboard> DigitalBillboards { get; set; }
        public DbSet<UserDigitalBillboard> UserDigitalBillboards { get; set; }
        public DbSet<Taxi> Taxis { get; set; }
        public DbSet<UserTaxi> UserTaxis { get; set; }
        public DbSet<UserCampaign> UserCampaigns { get; set; }
        #endregion

        #region MEDIA CHANNEL
        public DbSet<MediaPicture> MediaPictures { get; set; }
        public DbSet<BusShelterPicture> BusShelterPictures { get; set; }
        public DbSet<BusPicture> BusPictures { get; set; }
        public DbSet<TaxiPicture> TaxiPictures { get; set; }
        public DbSet<BillboardPicture> BillboardPictures { get; set; }
        public DbSet<DigitalBillboardPicture> DigitalBillboardPictures { get; set; }
        #endregion

        #region PRICE
        public DbSet<Unit> Units { get; set; }
        public DbSet<BusShelterPrice> BusShelterPrices { get; set; }
        public DbSet<BillboardPrice> BillboardPrices { get; set; }
        public DbSet<DigitalBillboardPrice> DigitalBillboardPrices { get; set; }
        public DbSet<BusRoute> BusRoutes { get; set; }
        public DbSet<BusRouteCompany> BusRouteCompanies { get; set; }

        public DbSet<BusRoutePrice> BusRoutePrices { get; set; }
        public DbSet<TaxiArea> TaxiAreas { get; set; }
        public DbSet<TaxiAreaPrice> TaxiAreaPrices { get; set; }
        public DbSet<BusType> BusTypes { get; set; }
        public DbSet<BusTypeOfArtwork> BusTypeOfArtworks { get; set; }
        public DbSet<TaxiType> TaxiTypes { get; set; }
        public DbSet<TaxiTypeOfArtwork> TaxiTypeOfArtworks { get; set; }

        #endregion

        #region COMPAIGN - INVOICE
        public DbSet<Status> Statuses { get; set; }
        public DbSet<TaxiDownTime> TaxiDownTimes { get; set; }
        public DbSet<BillboardDownTime> BillboardDownTimes { get; set; }
        public DbSet<BusDownTime> BusDownTimes { get; set; }
        public DbSet<DigitalBillboardDownTime> DigitalBillboardDownTimes { get; set; }
        public DbSet<BusShelterDownTime> BusShelterDownTimes { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CampaignFile> CampaignFiles { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<TaxiBooking> TaxiBookings { get; set; }
        public DbSet<BusBooking> BusBookings { get; set; }
        public DbSet<BillboardBooking> BillboardBookings { get; set; }
        public DbSet<BusShelterBooking> BusShelterBookings { get; set; }
        public DbSet<BusDiscount> BusDiscounts { get; set; }
        public DbSet<TaxiDiscount> TaxiDiscounts { get; set; }
        public DbSet<BusShelterDiscount> BusShelterDiscounts { get; set; }
        public DbSet<BillboardDiscount> BillboardDiscounts { get; set; }
        public DbSet<BusInstallationCost> BusInstallationCosts { get; set; }
        public DbSet<TaxiInstallationCost> TaxiInstallationCosts { get; set; }
        public DbSet<BusShelterInstallationCost> BusShelterInstallationCosts { get; set; }
        public DbSet<BillboardInstallationCost> BillboardInstallationCosts { get; set; }
        public DbSet<DigitalBillboardBooking> DigitalBillboardBookings { get; set; }
        #endregion

        #region COUNTING
        public DbSet<BillboardCounting> BillboardCountings { get; set; }
        #endregion
    }
}
