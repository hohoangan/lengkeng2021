﻿using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Models
{
    public class CampaignFile
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int CampaignId { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string FileType { get; set; }
    }
}
