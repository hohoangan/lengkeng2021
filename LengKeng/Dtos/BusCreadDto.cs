﻿using LengKeng.Models;
using LengKeng.Ultils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LengKeng.Dtos
{
    public class BusCreadDto
    {
        [Required]
        public Bus Bus { get; set; }
        [Required]
        public List<string> PlateNumbers { get; set; }
    }
}
